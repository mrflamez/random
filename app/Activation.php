<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Activation extends Model
{
    use SoftDeletes;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at',
        'start_date',
        'end_date',
    ];

	/**
     * The User of the Activation.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * The User activator
     */
    public function activator()
    {
        return $this->belongsTo('App\User', 'activator_id');
    }

    /**
     * Get the Payment of Activation
     */
    public function payment()
    {
        return $this->belongsTo('App\Payment', 'payment_id');
    }
}
