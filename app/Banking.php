<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banking extends Model
{
    /**
     * The User of the Activation.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
