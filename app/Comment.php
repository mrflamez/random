<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
    use SoftDeletes;

    /**
     * The Mass assigned attributes.
     *
     * @var array
     */
    protected $fillable = [
        'content', 'is_donator', 'is_parent', 'user_id', 'project_id', 'subject'
    ];

    protected $casts = [
        'is_donator' => 'boolean',
        'is_parent' => 'boolean',
    ];

     /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

    /**
     * The project of the Comment.
     */
    public function project()
    {
        return $this->belongsTo('App\Project');
    }


    /**
     * The User of the Comment.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

	/**
     * The Replies of the Comment.
     */
    public function replies()
    {
        return $this->hasMany('App\Reply');
    }


}
