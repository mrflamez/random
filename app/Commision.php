<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Commision extends Model
{
    /**
     * Get the Payment of Activation
     */
    public function payment()
    {
        return $this->belongsTo('App\Payment', 'payment_id');
    }

    /**
     * The User of the Activation.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * The User refered
     */
    public function refer()
    {
        return $this->belongsTo('App\User', 'refer_id');
    }
}
