<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    /**
     * The images that belong to the User.
     */
    public function users()
    {
        return $this->belongsToMany('App\User');
    }
}
