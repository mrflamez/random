<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Donator extends Model
{
	/**
     * The Mass assigned attributes.
     *
     * @var array
     */
    protected $fillable = [
        'cust_id', 'customer_code', 'first_name', 'last_name', 'email', 'phone', 'metadata', 'risk_action', 'password'
    ];

    /**
     * Get the Payment transaction. for the Donator.
     */
    public function payments()
    {
        return $this->hasMany('App\Payment');
    }

    /**
     * The user of this Donator account.
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
