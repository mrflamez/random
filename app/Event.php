<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model
{
	use SoftDeletes;

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'tag_line', 'description', 'venue', 'fee', 'start_date', 'end_date', 'status'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'start_date',
        'end_date',
        'deleted_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'fee' => 'float',
        'status' => 'boolean',
    ];

    /**
     * Set the Fee to float on insert .
     *
     * @param  string  $value
     * @return void
     */
    public function setFeeAttribute($value)
    {
        $this->attributes['fee'] = round(floatval($value), 2 );
    }

    /**
     * Get the user that owns the event.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

     /**
     * The images that belong to the Event.
     */
    public function images()
    {
        return $this->belongsToMany('App\Image')->withTimestamps();
    }
}
