<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Faq extends Model
{
    //
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'description', 'status', 'type'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'status' => 'boolean',
        'type' => 'boolean',
    ];

     /**
     * Set the status to boolean on insert .
     *
     * @param  string  $value
     * @return void
     */
    public function setStatusAttribute($value)
    {

        $this->attributes['status'] = filter_var( $value, FILTER_VALIDATE_BOOLEAN );
    }

    /**
     * Set the status to boolean on insert .
     *
     * @param  string  $value
     * @return void
     */
    public function setTypeAttribute($value)
    {

        $this->attributes['type'] = filter_var( $value, FILTER_VALIDATE_BOOLEAN );
    }

    /**
     * Get the user that owns the faq post.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
