<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Founder extends Model
{
    use SoftDeletes;

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'facebook' , 'twitter', 'image_id'
    ];

     /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

    /**
     * Get the image of the testimonial user.
     */
    public function image()
    {
        return $this->belongsTo('App\Image');
    }

    /**
     * The User of the testimonial.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
