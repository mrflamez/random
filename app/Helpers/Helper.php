<?php

namespace App\Helpers;

class Helper {

	 public static function activeMenu($uri='')
	 {
	  $active = '';

	  if (Request::is(Request::segment(1) . '/' . $uri . '/*') || Request::is(Request::segment(1) . '/' . $uri) || Request::is($uri))
	  {
	   $active = 'is-active';
	  }

	  return $active;
	 }

	/**
	 * Limit the number of words in a string.
	 *
	 * @param  string  $value
	 * @param  int     $words
	 * @param  string  $end
	 * @return string
	 */
	public static function words($value, $words = 100, $end = '...')
	{
		return \Illuminate\Support\Str::words($value, $words, $end);
	}

}
