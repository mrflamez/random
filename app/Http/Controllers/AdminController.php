<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\User;
use App\Event;
use App\Project;
use App\Post;
use App\Payment;
use App\Role;
use App\Faq;
use Carbon\Carbon;
use App\Setting;
use App\Commision;
use App\Country;
use App\RuleRegulation;

class AdminController extends Controller
{
	/**
     * The  incoming request object.
     *
     * @var  \Illuminate\Http\Request
     */
	protected $request;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
		$this -> request = $request;
    }

    /**
     * Show the application Admin dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$users = User::all();
		$projects = Project::all();
		$events = Event::all();
		$posts = Post::all();

        return view('admin.index',[
						'users' => $users->count(),
						'projects' => $projects->count(),
						'events' => $events->count(),
						'posts' => $posts->count(),
					]);
    }

     /**
     * Show the application Admin dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function alluserstable()
    {
		$users = User::all();
		$deleted = User::onlyTrashed()->count();

        return view('admin.alluserstable', ['admin_users' => $users -> where('is_admin', true )->count() ,
											'active_users' => $users -> where('status', true )->count() ,
											'deleted_users' => $deleted,
											'inactive_users' => $users -> where('status', false )->count() ]);
    }

     /**
     * Show the application Admin dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function alluserschart()
    {
		$users = User::all();
		$deleted = User::onlyTrashed()->count();

        return view('admin.alluserschart', ['admin_users' => $users -> where('is_admin', true )->count() ,
											'active_users' => $users -> where('status', true )->count() ,
											'deleted_users' => $deleted,
											'inactive_users' => $users -> where('status', false )->count() ]);

    }

    /**
     * Show the Permisions dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function allpermisions()
    {

        return view('admin.allpermisions');
    }

    /**
     * Show the Projects Table dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function allprojects()
    {
		$projects = Project::all();
		$deleted = Project::onlyTrashed()->count();

        return view('admin.allprojects', ['all_projects' => $projects->count() ,
											'active_projects' => $projects -> where('status', true )->count() ,
											'deleted_projects' => $deleted,
											'deactivated_projects' => $projects -> where('status', false )->count() ]);

    }

    /**
     * Show the Projects Chart dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function allprojectschart()
    {
		$projects = Project::all();
		$deleted = Project::onlyTrashed()->count();

        return view('admin.allprojectschart', ['all_projects' => $projects->count() ,
											'active_projects' => $projects -> where('status', true )->count() ,
											'deleted_projects' => $deleted,
											'deactivated_projects' => $projects -> where('status', false )->count() ]);

    }

    /**
     * Show the Events table dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function alleventstable()
    {
		$events = Event::all();
		$deleted = Event::onlyTrashed()->count();

        return view('admin.alleventstable', ['all_events' => $events->count() ,
											'active_events' => $events -> where('status', true )->count() ,
											'deleted_events' => $deleted,
											'inactive_events' => $events -> where('status', false )->count() ]);


    }

    /**
     * Show the Events Chart dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function alleventschart()
    {
		$events = Event::all();
		$deleted = Event::onlyTrashed()->count();

        return view('admin.alleventschart', ['all_events' => $events->count() ,
											'active_events' => $events -> where('status', true )->count() ,
											'deleted_events' => $deleted,
											'inactive_events' => $events -> where('status', false )->count() ]);

    }

    /**
     * Show the new Event view.
     *
     * @return \Illuminate\Http\Response
     */
    public function newevent()
    {

        return view('dashboard.newevent');
    }

    /**
     * Show the Blog Posts table dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function allblogposts()
    {
		$posts = Post::all();
		$deleted = Post::onlyTrashed()->count();

        return view('admin.allblogposts', ['all_posts' => $posts->count() ,
											'active_posts' => $posts -> where('status', true )->count() ,
											'deleted_posts' => $deleted,
											'inactive_posts' => $posts -> where('status', false )->count() ]);

    }

    /**
     * Show the Blog Posts Chart dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function allblogpostschart()
    {
		$posts = Post::all();
		$deleted = Post::onlyTrashed()->count();

        return view('admin.allblogpostschart', ['all_posts' => $posts->count() ,
											'active_posts' => $posts -> where('status', true )->count() ,
											'deleted_posts' => $deleted,
											'inactive_posts' => $posts -> where('status', false )->count() ]);

    }

    /**
     * Show the new Blog Post view.
     *
     * @return \Illuminate\Http\Response
     */
    public function newblogpost()
    {

        return view('dashboard.newblogpost');
    }

    /**
     * Show the all Payments Received.
     *
     * @return \Illuminate\Http\Response
     */
    public function allPaymentsreceived()
    {
		$date =  Carbon::now();
		$payments = Payment::all();
		$payments_month = Payment::whereMonth('created_at', $date->month )
					->whereYear('created_at', $date->year)
					->get();

        return view('admin.allpayments', ['all_payments' => $payments->count() ,
											'month_transactions' =>  $payments_month->count(),
											'month_payments' => $payments_month->sum('amount'),
											'inactive_payments' => $payments -> where('status', false )->count() ]);

        return view('admin.allpayments');
    }

    /**
     * Show the single user page admin.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function singleUser($id)
    {
		$user = User::where('id', (int)$id)->first();

		$projects =  $user -> projects() ->with(['category', 'payments', 'images' =>  function($q) {
						 $q->where('featured', true);
					}])->get();

		$funds_count = 0 ;
		$payments_count = 0 ;

		$payments = $user -> donators() ->with(['payments'])->get();

	   foreach ($projects as $project) {
			$funds_count = $funds_count + $project->payments->sum('amount');
	   }

	   foreach ($payments as $payment) {
			$payments_count = $payments_count + $payment->payments->count();
	   }

	   return view('admin.singleuser', ['projects' => $projects, 'user' => $user, 'payments_count' => $payments_count, 'funds' => $funds_count] );
    }

     /**
     * Show the single user page admin.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function singleUsertrasactions($id)
    {
		$user = User::where('id', (int)$id)->first();

		$projects =  $user -> projects() ->with(['category', 'payments', 'images' =>  function($q) {
						 $q->where('featured', true);
					}])->get();

		$funds_count = 0 ;
		$payments_count = 0 ;

		$payments = $user -> donators() ->with(['payments'])->get();

	   foreach ($projects as $project) {
			$funds_count = $funds_count + $project->payments->sum('amount');
	   }

	   foreach ($payments as $payment) {
			$payments_count = $payments_count + $payment->payments->count();
	   }

	   //~ return $payments;
	   //dd($payments);

	   return view('admin.singleusertrans', [
												'projects' => $projects,
												'user' => $user,
												'payments' => $payments,
												'payments_count' => $payments_count,
												'funds' => $funds_count] );
    }

	 /**
     * Show the single user page admin.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function singleUserprofile($id)
    {
		$user = User::where('id', (int)$id)->first();

		$payments_count = 0 ;

		$payments = $user -> donators() ->with(['payments'])->get();

        foreach ($payments as $payment) {
			$payments_count = $payments_count + $payment->payments->count();
	    }

	    $countries = Country::all();
	   return view('admin.singleuserprofile', [
												'user' => $user,
												'countries' => $countries,
												'payments_count' => $payments_count, ] );
    }


    /**
     * Show the single user page admin.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function singleUsermembership($id)
    {
		$user = User::where('id', (int)$id)  ->with(['activations'])->first();

		$payments_count = 0 ;

		$payments = $user -> donators() ->with(['payments'])->get();

        foreach ($payments as $payment) {
			$payments_count = $payments_count + $payment->payments->count();
	    }
	    $now = Carbon::now();
	    //return $user;
	   return view('admin.singleusermembership', ['user' => $user, 'payments_count' => $payments_count, 'now' => $now ] );
    }

     /**
     * Show the single user page admin.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function singleUsercommisions($id)
    {
		$user = User::where('id', (int)$id)  ->with(['commisions'])->first();

		$payments_count = 0 ;

		$payments = $user -> donators() ->with(['payments'])->get();

        foreach ($payments as $payment) {
			$payments_count = $payments_count + $payment->payments->count();
	    }
	    $now = Carbon::now();
	    //return $user;
	   return view('admin.singleusercommisions', ['user' => $user, 'payments_count' => $payments_count, 'now' => $now ] );
    }

    /**
     * Show the single user page admin.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function singleUserbanking($id)
    {
		$user = User::where('id', (int)$id)  ->with(['commisions'])->first();

		$payments_count = 0 ;

		$payments = $user -> donators() ->with(['payments'])->get();

        foreach ($payments as $payment) {
			$payments_count = $payments_count + $payment->payments->count();
	    }
	    $now = Carbon::now();
	    //return $user;
	   return view('admin.singleuserbanking', ['user' => $user, 'payments_count' => $payments_count, 'now' => $now ] );
    }

    /**
     * Show the permission  page admin.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function singlePermision($id)
    {
		$role = Role::where('id', (int)$id)->first();
		$users = User::all();

	   return view('admin.singlepermision',['users' => $users, 'role' => $role] );
    }

    /**
     * Add the permission to user ajax request.
     *
     * @return \Illuminate\Http\Response
     */
    public function addUserRole()
    {

	   $data = [
		  'role_id'      => $this->request->role_id,
		  'user_id'      => $this->request->user_id
		];

        $rules = [
			'role_id'     =>  'required',
			'user_id'     =>  'required'
		];

		if($this->request->ajax()){

			$validator = Validator::make($data,$rules);
			if($validator->fails()) {
				return response()->json([
					'success' => false,
					'errors' => $validator->getMessageBag()->toArray()
				]);
			} else {
				$userId = $this->request->user_id;
				$roleId = $this->request->role_id;
				$user = User::find((int)$userId);
				$user->roles()->syncWithoutDetaching([(int)$roleId]);
				return response()->json([
					'success' => true
				]);
			}
		}
    }

	/**
     * Add the permission to user ajax request.
     *
     * @param  int  $userid, $roleid
     * @return \Illuminate\Http\Response
     */
    public function removeroleuser($userid, $roleid)
    {

		if($this->request->ajax()){

				$user = User::find((int)$userid);
				$user->roles()->toggle([(int)$roleid]);
				return response()->json([
					'success' => true
				]);

		}
    }

    /**
     * Show the single project  page admin.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function singleProject($id)
    {
		$project = Project::where('id', (int)$id)->first();
		// $users = User::all();

	   return view('admin.singleproject',['project' => $project] );
    }

     /**
     * Show the a single user Project donations.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewprojectdonations($id)
    {
		$project = Project::where('id', $id) ->first();
        //~ return $project->payments;
       return view('admin.projectdonations', [
					 'project' => $project
					 ]);
    }

    /**
     * Show the a single user Project messages.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewprojectmessages($id)
    {
		$project = Project::where('id', $id) ->with(['comments'  =>  function($q) {
						  $q->where('is_parent', true)->with(['replies'])->take(5)->latest();
					}])->first();

       return view('admin.projectmessages', [
					 'project' => $project
					 ]);
    }

    /**
     * Show the a single user Project settings.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewprojectsettings($id)
    {
		$project = Project::where('id', $id) ->first();

       return view('admin.projectsettings', [
					 'project' => $project
					 ]);
    }

    /**
     * Show the a single user Project stats.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewprojectstats($id)
    {
		$project = Project::where('id', $id) ->first();

       return view('admin.projectstats', [
					 'project' => $project
					 ]);
    }

    /**
     * Show the single event  page admin.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function singleEvent($id)
    {
		$event = Event::where('id', (int)$id)->first();

	   return view('admin.singleevent',['event' => $event] );
    }

    /**
     * Show the single blog post  page admin.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function singlePost($id)
    {
		$post = Post::where('id', (int)$id)->first();

	   return view('admin.singlepost',['post' => $post] );
    }

    /**
     * Show the single  Payment received Trans   page admin.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function singlePaymentreceivedTrans($id)
    {
		$payment = Payment::where('id', (int)$id)->first();

	   return view('admin.singlepaymenttrans',['payment' => $payment] );
    }

	/**
     * Show the all faq post  page admin.
     *
     * @return \Illuminate\Http\Response
     */
    public function allFaqs()
    {

	   return view('admin.allfaqs');
    }

    /**
     * Show the new faq post  page admin.
     *
     * @return \Illuminate\Http\Response
     */
    public function createFaq()
    {
	   return view('admin.newfaq' );
    }

	/**
     * Show the single faq post  page admin.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function singleFaq($id)
    {
		$faq = Faq::where('id', (int)$id)->first();

	   return view('admin.singlefaq',['faq' => $faq] );
    }

    /**
     * Show the single event  page admin.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function singleEventEdit($id)
    {
		$event = Event::where('id', (int)$id)->with('images')->first();
        //~ return  $event;
	   return view('admin.singleeventedit',['event' => $event] );
    }

     /**
     * Show the single event  page admin.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function singlePostEdit($id)
    {
		$post = Post::where('id', (int)$id)->with('images')->first();
        //~ return  $event;
	   return view('admin.singlepostedit',['post' => $post] );
    }

    /**
     * Show the single event  page admin.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function singleFaqEdit($id)
    {
		$faq = Faq::where('id', (int)$id)->first();
        //~ return  $event;
	   return view('admin.singlefaqedit',['faq' => $faq] );
    }

    /**
     * Show the single event  page admin.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function homepageSetings()
    {
        $setting = Setting::find(1);
	   return view('admin.homepagesettings',['setting' => $setting] );
    }

     /**
     * Show the single event  page admin.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function aboutSetings()
    {
        $setting = Setting::find(1);
	   return view('admin.aboutpagesettings',['setting' => $setting] );
    }

    /**
     * Show the single event  page admin.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function generalSettings()
    {
        $setting = Setting::find(1);
	   return view('admin.generalsettings',['setting' => $setting] );
    }

    /**
     * Show the all rules  page admin.
     *
     * @return \Illuminate\Http\Response
     */
    public function allRules()
    {
	  return view('admin.allrules');
    }

    /**
     * Show the new faq post  page admin.
     *
     * @return \Illuminate\Http\Response
     */
    public function createRules()
    {
	   return view('admin.newrule' );
    }

	/**
     * Show the single faq post  page admin.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function singleRules($id)
    {
		$rule = RuleRegulation::where('id', (int)$id)->first();

	   return view('admin.singlerule',['rule' => $rule] );
    }

    /**
     * Show the single event  page admin.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function singleRulesEdit($id)
    {
		$rule = RuleRegulation::where('id', (int)$id)->first();

	   return view('admin.singleruleedit',['rule' => $rule] );
    }

}
