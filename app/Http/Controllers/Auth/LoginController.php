<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //~ protected $redirectTo = '/dashboard';

    protected $redirectAfterLogout = '/';

    /**
     * Handle an authentication attempt.
     *
     * @return Response
     */
    protected function credentials(Request $request)
    {
		 return array_merge($request->only($this->username(), 'password'), ['is_pending' => 0]);

    }

    protected function redirectTo ()
    {
		$user = Auth::user();

		if ($user->is_paid && ! $user->is_admin ) {
			return '/dashboard';
		} elseif (!$user->is_paid && ! $user->is_admin ) {
			// return '/dashboard';
			Auth::logout();
		    return route('membershippayment', ['memberid' => $user -> member_id]);
		} elseif (!$user->status) {
            Auth::logout();
			return route('suspended');
		} elseif ($user->is_admin ) {
            return route('administrator.dashboard');
		} else {
			Auth::logout();
			abort(404);
		}

	}

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


}
