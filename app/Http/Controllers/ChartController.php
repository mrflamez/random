<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Project;
use App\Event;
use App\Post;
use App\Payment;
use DB;
use Carbon\Carbon;
class ChartController extends Controller
{
    //
    public function getMonthName($monthNumber)
	{
		return date("F", mktime(0, 0, 0, $monthNumber, 1));
	}

    /**
	 * Process Chart request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function usersVsMonth(Request $request)
	{
	$date =  Carbon::now();

	$users = User::select([
				DB::raw('MONTH(created_at) AS date'),
				DB::raw('COUNT(id) AS count'),
			 ])
			 ->whereYear('created_at', $date->year)
			 ->groupBy('date')
			 ->orderBy('date', 'ASC')
			 ->get();

    $usersDataByMonth = [];

	foreach($users as $u) {
		$usersDataByMonth[$u->date] = (int)$u->count;
	}

	$date =  Carbon::now();
	$months = [];

	for($i = 0; $i < 12; $i++) {
		$m = $date->month;
		$months[] = $m ;
		if(!isset($usersDataByMonth[ $m ])) {
			$usersDataByMonth[ $m ] = 0;
		}

		$date->subMonth();
	}
	$ret = [];
	ksort($usersDataByMonth);
	//~ return $usersDataByMonth ;
	foreach($usersDataByMonth as $u => $d) {
		$ret[$this->getMonthName((int)$u)] = (int)$d;
	}
	//~ return $ret ;
	return response()->json([
				'success' => true,
				'data' => $ret
			]);

	}

	/**
	 * Process Chart request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function projectsVsmonth(Request $request)
	{

	$projects = Project::select([
				DB::raw('MONTH(created_at) AS date'),
				DB::raw('COUNT(id) AS count'),
			 ])
			 ->whereYear('created_at', '2017')
			 //~ ->whereBetween('created_at', [Carbon::now()->subDays(7), Carbon::now()])
			 ->groupBy('date')
			 ->orderBy('date', 'ASC')
			 ->get();

    $projectsDataByMonth = [];

	foreach($projects as $u) {
		$projectsDataByMonth[$u->date] = (int)$u->count;
	}

	$date =  Carbon::now();
	$months = [];

	for($i = 0; $i < 12; $i++) {
		$m = $date->month;
		$months[] = $m ;
		if(!isset($projectsDataByMonth[ $m ])) {
			$projectsDataByMonth[ $m ] = 0;
		}

		$date->subMonth();
	}
	$ret = [];
	ksort($projectsDataByMonth);
	//~ return $projectsDataByMonth ;
	foreach($projectsDataByMonth as $u => $d) {
		$ret[$this->getMonthName((int)$u)] = (int)$d;
	}
	//~ return $ret ;
	return response()->json([
				'success' => true,
				'data' => $ret
			]);

	}

	/**
	 * Process Chart request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function eventsVsmonth(Request $request)
	{

	$events = Event::select([
				DB::raw('MONTH(created_at) AS date'),
				DB::raw('COUNT(id) AS count'),
			 ])
			 ->whereYear('created_at', '2017')
			 ->groupBy('date')
			 ->orderBy('date', 'ASC')
			 ->get();

    $eventsDataByMonth = [];

	foreach($events as $u) {
		$eventsDataByMonth[$u->date] = (int)$u->count;
	}

	$date =  Carbon::now();
	$months = [];

	for($i = 0; $i < 12; $i++) {
		$m = $date->month;
		$months[] = $m ;
		if(!isset($eventsDataByMonth[ $m ])) {
			$eventsDataByMonth[ $m ] = 0;
		}

		$date->subMonth();
	}
	$ret = [];
	ksort($eventsDataByMonth);
	foreach($eventsDataByMonth as $u => $d) {
		$ret[$this->getMonthName((int)$u)] = (int)$d;
	}
	return response()->json([
				'success' => true,
				'data' => $ret
			]);

	}

	/**
	 * Process Chart request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function postsVsmonth(Request $request)
	{

	$posts = Post::select([
				DB::raw('MONTH(created_at) AS date'),
				DB::raw('COUNT(id) AS count'),
			 ])
			 ->whereYear('created_at', '2017')
			 ->groupBy('date')
			 ->orderBy('date', 'ASC')
			 ->get();

    $postsDataByMonth = [];

	foreach($posts as $u) {
		$postsDataByMonth[$u->date] = (int)$u->count;
	}

	$date =  Carbon::now();
	$months = [];

	for($i = 0; $i < 12; $i++) {
		$m = $date->month;
		$months[] = $m ;
		if(!isset($postsDataByMonth[ $m ])) {
			$postsDataByMonth[ $m ] = 0;
		}

		$date->subMonth();
	}
	$ret = [];
	ksort($postsDataByMonth);
	foreach($postsDataByMonth as $u => $d) {
		$ret[$this->getMonthName((int)$u)] = (int)$d;
	}
	return response()->json([
				'success' => true,
				'data' => $ret
			]);

	}

	/**
	 * Process Chart request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function donationsVsmonth(Request $request, $id)
	{

	$payments = Payment::select([
				DB::raw('MONTH(created_at) AS date'),
				DB::raw('COUNT(id) AS count'),
			 ])
			 ->where('project_id', (int)$id)
			 ->whereYear('created_at', '2017')
			 ->groupBy('date')
			 ->orderBy('date', 'ASC')
			 ->get();

    $paymentsDataByMonth = [];

	foreach($payments as $u) {
		$paymentsDataByMonth[$u->date] = (int)$u->count;
	}

	$date =  Carbon::now();
	$months = [];

	for($i = 0; $i < 12; $i++) {
		$m = $date->month;
		$months[] = $m ;
		if(!isset($paymentsDataByMonth[ $m ])) {
			$paymentsDataByMonth[ $m ] = 0;
		}

		$date->subMonth();
	}
	$ret = [];
	ksort($paymentsDataByMonth);
	//~ return $paymentsDataByMonth ;
	foreach($paymentsDataByMonth as $u => $d) {
		$ret[$this->getMonthName((int)$u)] = (int)$d;
	}
	//~ return $ret ;
	return response()->json([
				'success' => true,
				'data' => $ret
			]);

	}

}
