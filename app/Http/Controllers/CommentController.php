<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Project;
use App\Comment;
use App\Reply;
use Mail;
use App\Mail\CommentReceived;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = [
		  'comment_text'      => $request->comment_text,
		  'comment_subject'      => $request->comment_subject
		];

        $rules = [
			'comment_text'     =>  'required|string',
			'comment_subject'     =>  'required|string',
		];

		if($request->ajax()){

			$validator = Validator::make($data,$rules);
			if($validator->fails())
				return response()->json([
					'success' => false,
					'errors' => $validator->getMessageBag()->toArray()
				]);
			else {
				$project = Project::find($request->project_id);
				$user = $request -> user() ;

				$comment = new Comment;
				$comment->content = $request->comment_text;
				$comment->subject = $request->comment_subject;
				$comment->project()->associate($project);
				$comment->user()->associate($user);
				$comment->save();

				Mail::to($project->user)->send(new CommentReceived($user, $comment, $project));

				// $comment->users()->attach($user -> id);

				return response()->json([
					'success' => true
				]);
			}
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$return = ['success' => false];

		if ($request->has('action') && $request->action === 'new_reply') {
			$data = [
			  'comment_text'      => $request->comment_text
			];

			$rules = [
				'comment_text'     =>  'required|string'
			];

			$validator = Validator::make($data,$rules);
			if($validator->fails()) {
				$return =[
					'success' => false,
					'errors' => $validator->getMessageBag()->toArray()
				];
			} else {
				$comment = Comment::find((int)$id);

				$reply = new Reply;
				$reply->content = $request->comment_text;
				$reply->comment()->associate($comment);
				$reply->user()->associate($request -> user());
				$reply->save();

				$return = [
					'success' => true
				];
			}
		}

		return response()->json($return);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
