<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Datatables;
use DB;
use Carbon\Carbon;
use App\User;
use App\Role;
use App\Project;
use App\Event;
use App\Post;
use App\Payment;
use App\Faq;
use App\Testimony;
use App\Setting;
use App\Founder;
use App\Image;
use App\RuleRegulation;

class DatatableController extends Controller
{
	/**
     * The  incoming request object.
     *
     * @var  \Illuminate\Http\Request
     */
	protected $request;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
		$this -> request = $request;
    }

    //
	/**
	 * Process datatables ajax request of newusers.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function newusers()
	{
		DB::statement(DB::raw('set @rownum=0'));
		$users = User::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            'id',
            'f_name',
            'email']);
        $datatables = Datatables::of($users)
						->addColumn('action', function ($user) {
							return '<a href="#edit-'.$user->id.'" class="button is-small is-primary">
									<span>View</span>
								  </a>';
						});

        if ($keyword = $this->request->get('search')['value']) {
            $datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }

        return $datatables->make(true);
	}

	//
	/**
	 * Process datatables ajax request of newusers.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function allusers()
	{
		DB::statement(DB::raw('set @rownum=0'));
		$users = User::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            'id',
            'member_id',
            'f_name',
            'l_name',
            'email',
            'created_at',
            'status',
            'is_pending',
            'is_paid',
            ]);
        $datatables = Datatables::of($users)
						->addColumn('action', function ($user) {
							return '<a href="'. route('administrator.users.profile', ['id' => $user->id]) .'" class="button is-small is-primary">
									<span>Manage</span>
								  </a>';
						})
						->addColumn('status_btn', 'partials.datatables.accountstatus')
						->editColumn('created_at', function ($faqs) {
							return $faqs->created_at->format('Y/m/d');
						})
						->filterColumn('status_btn', function ($query, $keyword) {
							if ($keyword === 'SUSPENDED') {
							  $query->where(['status' => false, 'is_pending' => false ]);
						    }
						    if ($keyword === 'ACTIVE') {
							  $query->where(['status' => true, 'is_pending' => false, 'is_paid' => true ]);
						    }
						    if ($keyword === 'PENDING') {
							  $query->where('is_pending' ,  true );
						    }
						    if ($keyword === 'NOT PAID') {
							  $query->where('is_paid' ,  false );
						    }
						})
						->filterColumn('created_at', function ($query, $keyword) {
							$query->whereRaw("DATE_FORMAT(created_at,'%Y/%m/%d') like ?", ["%$keyword%"]);
						});


        if ($keyword = $this->request->get('search')['value']) {
            $datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }

        return $datatables->make(true);
	}

	/**
	 * Process datatables ajax request of allpermisions.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function allpermisions()
	{

		$roles = Role::with('users')
				->selectRaw('distinct roles.*');

        $datatables = Datatables::of($roles)
						->addColumn('counts', function ($roles) {
							return $roles->users->count();
						})
						->addColumn('action', function ($roles) {
							return '<a href="'.route('administrator.permisions.single', ['id' => $roles->id]).'" class="button is-small is-primary">
									<span>Manage</span>
								  </a>';
						});

        return $datatables->make(true);
	}

	/**
	 * Process datatables ajax request of allprojects.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function allprojects()
	{
		DB::statement(DB::raw('set @rownum=0'));
		$projects = Project::with(['user', 'payments'])
					->select([
						DB::raw('@rownum  := @rownum  + 1 AS rownum'),
						'projects.*'
					]);
        $datatables = Datatables::of($projects)
						->addColumn('action', function ($projects) {
							return '<a href="'.route('administrator.projects.single', ['id' => $projects->id]).'" class="button is-small is-primary">
									<span>Manage</span>
								  </a>';
						})
						->addColumn('funds_raised', function ($projects) {
							return $projects->payments->sum('amount');
						})
						->addColumn('days_remaining', function ($projects) {
							return $projects-> end_date->diffInDays(null, true);
						})
						->editColumn('title', '{!! str_limit($title, 50) !!}')
						->editColumn('start_date', function ($projects) {
							return $projects->start_date->format('Y/m/d');
						})
						->editColumn('end_date', function ($projects) {
							return $projects->end_date->format('Y/m/d');
						})
						->filterColumn('start_date', function ($query, $keyword) {
							$query->whereRaw("DATE_FORMAT(start_date,'%Y/%m/%d') like ?", ["%$keyword%"]);
						})
						->filterColumn('end_date', function ($query, $keyword) {
							$query->whereRaw("DATE_FORMAT(end_date,'%Y/%m/%d') like ?", ["%$keyword%"]);
						});

        return $datatables->make(true);
	}

	/**
	 * Process datatables ajax request of allevents.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function allevents()
	{
		DB::statement(DB::raw('set @rownum=0'));
		$events = Event::with(['user'])
					->select([
						DB::raw('@rownum  := @rownum  + 1 AS rownum'),
						'events.*'
					]);
        $datatables = Datatables::of($events)
						->addColumn('action', function ($events) {
							return '<a href="'.route('administrator.events.single', ['id' => $events->id]).'" class="button is-small is-primary">
									<span>Manage</span>
								  </a>';
						})
						->editColumn('title', '{!! str_limit($title, 50) !!}')
						->editColumn('venue', '{!! str_limit($venue, 20) !!}')
						->editColumn('start_date', function ($events) {
							return $events->start_date->format('Y/m/d');
						})
						->editColumn('end_date', function ($events) {
							return $events->start_date->format('Y/m/d');
						})
						->filterColumn('start_date', function ($query, $keyword) {
							$query->whereRaw("DATE_FORMAT(start_date,'%Y/%m/%d') like ?", ["%$keyword%"]);
						})
						->filterColumn('end_date', function ($query, $keyword) {
							$query->whereRaw("DATE_FORMAT(end_date,'%Y/%m/%d') like ?", ["%$keyword%"]);
						});

        return $datatables->make(true);
	}

	/**
	 * Process datatables ajax request of allevents.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function allposts()
	{
		DB::statement(DB::raw('set @rownum=0'));
		$posts = Post::with(['user'])
					->select([
						DB::raw('@rownum  := @rownum  + 1 AS rownum'),
						'posts.*'
					]);
        $datatables = Datatables::of($posts)
						->addColumn('action', function ($posts) {
							return '<a href="'.route('administrator.blog.single', ['id' => $posts->id]).'" class="button is-small is-primary">
									<span>Manage</span>
								  </a>';
						})
						->editColumn('title', '{!! str_limit($title, 50) !!}')
						->editColumn('created_at', function ($posts) {
							return $posts->created_at->format('Y/m/d');
						})
						->filterColumn('created_at', function ($query, $keyword) {
							$query->whereRaw("DATE_FORMAT(created_at,'%Y/%m/%d') like ?", ["%$keyword%"]);
						});

        return $datatables->make(true);
	}

	/**
	 * Process datatables ajax request of allevents.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function allpaymentsreceived()
	{
		DB::statement(DB::raw('set @rownum=0'));
		$payments = Payment::with(['donator', 'project', 'activation'])
					->select([
						DB::raw('@rownum  := @rownum  + 1 AS rownum'),
						'payments.*'
					]);
        $datatables = Datatables::eloquent($payments)
						->addColumn('action', function ($payments) {
							return '<a href="'. route('administrator.payments.single', ['id' => $payments->id]) .'" class="button is-small is-primary">
									<span>Manage</span>
								  </a>';
						})
						->addColumn('status_btn', 'partials.datatables.status')
						->addColumn('transaction_type', 'partials.datatables.trans_type')
						->editColumn('transaction_date', function ($payments) {
							return $payments->transaction_date->format('Y/m/d');
						})
						->editColumn('amount', function ($payments) {
							return $payments->amount/100;
						})
						->editColumn('fees', function ($payments) {
							return $payments->fees/100;
						})
						->rawColumns(['status_btn', 'transaction_type', 'action'])
						->filterColumn('transaction_date', function ($query, $keyword) {
							$query->whereRaw("DATE_FORMAT(transaction_date,'%Y/%m/%d') like ?", ["%$keyword%"]);
						});

        return $datatables->make(true);
	}

	/**
	 * Process datatables ajax request of single permision users.
	 *
	 *  @param  int  $id
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function singlePermision($id)
	{
		$roles = Role::find($id) -> users;

        $datatables = Datatables::of($roles)
						->addColumn('action', function ($roles) {
							return '<button value="'.route('administrator.permisions.removeroleuser', ['userid' => $roles->id, 'roleid' => $roles->id]).'" class="button is-small is-primary remove-role_user-btn-control">
									<span>Remove Role</span>
								  </button>';
						});

        return $datatables->make(true);
	}

	/**
	 * Process datatables ajax request of allevents.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function allfaqs()
	{
		DB::statement(DB::raw('set @rownum=0'));
		$faqs = Faq::with(['user'])
					->select([
						DB::raw('@rownum  := @rownum  + 1 AS rownum'),
						'faqs.*'
					]);
        $datatables = Datatables::of($faqs)
						->addColumn('action', function ($faqs) {
							return '<a href="'.route('administrator.faqs.single', ['id' => $faqs->id]).'" class="button is-small is-primary">
									<span>Manage</span>
								  </a>';
						})
						->editColumn('type', 'partials.datatables.faqtype')
						->editColumn('title', '{!! str_limit($title, 50) !!}')
						->editColumn('created_at', function ($faqs) {
							return $faqs->created_at->format('Y/m/d');
						})
						->filterColumn('type', function ($query, $keyword) {
							if ($keyword === 'Members') {
							  $query->where("type", false);
						    }
						    if ($keyword === 'Donators') {
							  $query->where("type", true);
						    }
						})
						->filterColumn('created_at', function ($query, $keyword) {
							$query->whereRaw("DATE_FORMAT(created_at,'%Y/%m/%d') like ?", ["%$keyword%"]);
						});

        return $datatables->make(true);
	}

	/**
	 * Process datatables ajax request of newusers.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function testmonials()
	{
		DB::statement(DB::raw('set @rownum=0'));
		$testimony = Testimony::select([
					DB::raw('@rownum  := @rownum  + 1 AS rownum'),
					'id','name', 'alias', 'content', 'created_at'
					]);
        $datatables = Datatables::of($testimony)
						->addColumn('action', function ($testimony) {
							return '<button value="'. $testimony->id .'" class="button is-small is-primary testmonial-del">
									<span>Delete</span>
								  </button>';
						})
						->editColumn('created_at', function ($testimony) {
							return $testimony->created_at->format('Y/m/d');
						})
						->filterColumn('created_at', function ($query, $keyword) {
							$query->whereRaw("DATE_FORMAT(created_at,'%Y/%m/%d') like ?", ["%$keyword%"]);
						});

        if ($keyword = $this->request->get('search')['value']) {
            $datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }

        return $datatables->make(true);
	}

	/**
	 * Process datatables ajax request of newusers.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function settingSlider()
	{
		$sliders = Setting::find(1)->sliderimages()->get();
        //~ return $setting ;

        $datatables = Datatables::of($sliders)
					->addColumn('action', function ($sliders) {
						return '<button value="'. $sliders->id .'" class="button is-small is-primary slider-del">
								<span>Delete</span>
							  </button>';
					})
					->addColumn('image', 'partials.datatables.image')
					->editColumn('created_at', function ($sliders) {
						return $sliders->created_at->format('Y/m/d');
					})
					->rawColumns(['image', 'action'])
					->filterColumn('created_at', function ($query, $keyword) {
						$query->whereRaw("DATE_FORMAT(created_at,'%Y/%m/%d') like ?", ["%$keyword%"]);
					});

        return $datatables->make(true);
	}

	/**
	 * Process datatables ajax request of newusers.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function settingSponsors()
	{
		$sponsors = Setting::find(1)->sponsorsimages()->get();

        $datatables = Datatables::of($sponsors)
					->addColumn('action', function ($sponsors) {
						return '<button value="'. $sponsors->id .'" class="button is-small is-primary sponsors-del">
								<span>Delete</span>
							  </button>';
					})
					->addColumn('image', 'partials.datatables.image')
					->rawColumns(['image', 'action']);

        return $datatables->make(true);
	}

	/**
	 * Process datatables ajax request of newusers.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function founders()
	{
		$founder = Founder::with('image')
				  ->select('founders.*');
        $datatables = Datatables::of($founder)
						->addColumn('action', function ($founder) {
							return '<button value="'. $founder->id .'" class="button is-small is-primary founder-del">
									<span>Delete</span>
								  </button>';
						})
						->addColumn('founderimage', function (Founder $founder) {
							return view('partials.datatables.founderimage', ['img' => $founder -> image -> name ]);
						})
						->editColumn('created_at', function ($founder) {
							return $founder->created_at->format('Y/m/d');
						})
						->filterColumn('created_at', function ($query, $keyword) {
							$query->whereRaw("DATE_FORMAT(created_at,'%Y/%m/%d') like ?", ["%$keyword%"]);
						})
						->rawColumns(['founderimage', 'action']);


        return $datatables->make(true);
	}

	/**
	 * Process datatables ajax request of newusers.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function deleteSlider(Request $request)
	{
		//~ $now  = Carbon::now() ;
		DB::table('image_setting')
			->where([
                    ['image_id', '=', (int) $request->id ],
                    ['is_slider', '=', 1]
            ]) -> delete();
            //~ ->whereNull('deleted_at')
            //~ ->update(['deleted_at' => $now ]);
        return response()->json(['success' => true]);
	}

	/**
	 * Process datatables ajax request of newusers.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function deleteSponsor(Request $request)
	{
		//~ $now  = Carbon::now() ;
		DB::table('image_setting')
			->where([
                    ['image_id', '=', (int) $request->id ],
                    ['is_slider', '=', 0]
            ]) -> delete();
            //~ ->whereNull('deleted_at')
            //~ ->update(['deleted_at' => $now ]);
        return response()->json(['success' => true]);
	}

	/**
	 * Process datatables ajax request of newusers.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function deleteTestmonial(Request $request)
	{
		Testimony::destroy( (int) $request->id );
        return response()->json(['success' => true]);
	}

	/**
	 * Process datatables ajax request of newusers.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function deleteFounder(Request $request)
	{
		Founder::destroy( (int) $request->id );
        return response()->json(['success' => true]);
	}

	/**
	 * Process datatables ajax request of allevents.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function allrules()
	{
		DB::statement(DB::raw('set @rownum=0'));
		$rules = RuleRegulation::select([
						DB::raw('@rownum  := @rownum  + 1 AS rownum'),
						'rule_regulations.*'
					]);
        $datatables = Datatables::of($rules)
						->addColumn('action', function ($rules) {
							return '<a href="'.route('administrator.rules.single', ['id' => $rules->id]).'" class="button is-small is-primary">
									<span>Manage</span>
								  </a>';
						})
						->editColumn('title', '{!! str_limit($title, 50) !!}')
						->editColumn('created_at', function ($rules) {
							return $rules->created_at->format('Y/m/d');
						})
						->filterColumn('created_at', function ($query, $keyword) {
							$query->whereRaw("DATE_FORMAT(created_at,'%Y/%m/%d') like ?", ["%$keyword%"]);
						});

        return $datatables->make(true);
	}


}
