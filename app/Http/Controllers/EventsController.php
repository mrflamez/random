<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Event;
class EventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $events = Event::latest()->paginate(6);
		return view('events', ['events' => $events] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = [
		  'title'      => $request->title,
		  'description'      => $request->description
		];

        $rules = [
			'title'      =>  'required|string',
			'description'     =>  'required|string'
		];

		$validator = Validator::make($data,$rules);
		if($validator->fails())
			return response()->json([
				'success' => false,
				'errors' => $validator->getMessageBag()->toArray()
			]);
		else {
			$event = Event::firstOrNew([
				'title' => $request->title,
				'tag_line' => $request->tag_line,
				'description' => $request->description,
				'venue' =>  $request->venue,
				'fee'      => $request->fee,
				'start_date'      => $request->start,
				'end_date'      => $request->end
			]);
			$event->user()->associate($request->user());
			$event->save();

			return response()->json([
				'success' => true,
				'id' => $event->id
			]);
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $event = Event::find((int) $id);
		return view('eventsingle', ['event' => $event] );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = [
		  'title'      => $request->title,
		  'description'      => $request->description
		];

        $rules = [
			'title'      =>  'required|string',
			'description'     =>  'required|string'
		];

		$validator = Validator::make($data,$rules);
		if($validator->fails())
			return response()->json([
				'success' => false,
				'errors' => $validator->getMessageBag()->toArray()
			]);
		else {
			Event::where('id', $id)
					   ->update([
							'title' => $request->title,
							'tag_line' => $request->tag_line,
							'description' => $request->description,
							'venue' =>  $request->venue,
							'fee'      => $request->fee,
							'start_date'      => $request->start,
							'end_date'      => $request->end
						]);

			return response()->json([
				'success' => true,
				'id' => $id
			]);
		}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $res = Event::destroy($id);
        return response()->json([
			'success' => filter_var( $res, FILTER_VALIDATE_BOOLEAN )
		]);
    }
}
