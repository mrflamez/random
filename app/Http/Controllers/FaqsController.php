<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Faq;
class FaqsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $faq_members = Faq::where('type', false)->latest()->get();
        $faq_donators = Faq::where('type', true)->latest()->get();
        return view('faqs', [
							'faq_members' => $faq_members,
							'faq_donators' => $faq_donators,
							]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $data = [
		  'title'      => $request->title,
		  'description'      => $request->description
		];

        $rules = [
			'title'      =>  'required|string',
			'description'     =>  'required|string'
		];

		$validator = Validator::make($data,$rules);
		if($validator->fails()) {
			return response()->json([
				'success' => false,
				'errors' => $validator->getMessageBag()->toArray()
			]);
		} else {
			$faq = Faq::firstOrNew([
				'title' => $request->title,
				'description' => $request->description,
				'type' => $request->type
			]);
			$faq->user()->associate($request->user());
			$faq->save();

			return response()->json([
				'success' => true,
				'id' => $faq->id
			]);
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $faq = Faq::find( (int) $id);

	   return view('faqsingle',['faq' => $faq] );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = [
		  'title'      => $request->title,
		  'description'      => $request->description
		];

        $rules = [
			'title'      =>  'required|string',
			'description'     =>  'required|string'
		];

		$validator = Validator::make($data,$rules);
		if($validator->fails()) {
			return response()->json([
				'success' => false,
				'errors' => $validator->getMessageBag()->toArray()
			]);
		} else {
			Faq::where('id', $id)
			->update([
				'title' => $request->title,
				'description' => $request->description,
				'type' => $request->type
			]);

			return response()->json([
				'success' => true,
				'id' => $id
			]);
		}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $res = Faq::destroy($id);
        return response()->json([
			'success' => filter_var( $res, FILTER_VALIDATE_BOOLEAN )
		]);

    }
}
