<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Image;
use App\Founder;
use Validator;

class FounderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $data = [
		  'name'      => $request->name,
		  'twitter'      => $request->twitter,
		  'facebook'      => $request->facebook,
		  'image_id'      => $request->image_id
		];

        $rules = [
			'name'      =>  'required|string',
			'twitter'     =>  'required|string',
			'facebook'     =>  'required|string',
			'image_id'     =>  'required',
		];

		$validator = Validator::make($data,$rules);
		if($validator->fails()) {
			return response()->json([
				'success' => false,
				'errors' => $validator->getMessageBag()->toArray()
			]);
		} else {
			// $image = Image::find((int)$request->image_id)->first();

			$founder = Founder::firstOrNew([
				  'name'      => $request->name,
				  'twitter'   => $request->twitter,
				  'facebook'  => $request->facebook,
				  'image_id'  => (int)$request->image_id,
			]);
			$founder->user()->associate($request->user());
			// $founder->image()->associate($image);
			$founder->save();

			return response()->json([
				'success' => true,
				'id' => $founder
			]);
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
