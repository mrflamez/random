<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Project;
use App\Comment;
use App\Payment;
use App\User;
use App\Country;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		$user = $request->user();

		if ($user->is_admin) {
            return redirect()->action('AdminController@index');
        }

		$projects =  $user -> projects() -> has('images')->with(['category', 'payments', 'images' =>  function($q) {
						 $q->where('featured', true);
					}])->paginate(6);
		$funds_count = 0 ;
	   foreach ($projects as $project) {
			$funds_count = $funds_count + $project->payments->sum('amount');
	   }
	   //dd($funds_count);
        return view('dashboard.index', ['projects' => $projects, 'user' => $user, 'funds' => $funds_count] );
    }

    /**
     * Show the create Project view.
     *
     * @return \Illuminate\Http\Response
     */
    public function createproject()
    {
		$categories = Category::all();
		$now = Carbon::now();
        return view('dashboard.createproject', ['categories' => $categories, 'now' => $now]);
    }

    /**
     * Show the listing a user Projects.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewprojects(Request $request)
    {
		$projects = $request->user() -> projects() -> has('images')->with(['category', 'images' =>  function($q) {
						return $q->where('featured', true);
					}])->paginate(6);

        return view('dashboard.projects', ['projects' => $projects]);
    }

    /**
     * Show the a single user Project.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewproject(Request $request, $id)
    {
		$project = Project::where('id', (int)$id) ->first();
        //return $project;
       return view('dashboard.project', [
					 'project' => $project
					 ]);
    }

    /**
     * Show the a single user Project donations.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewprojectdonations(Request $request, $id)
    {
		$project = Project::where('id', (int)$id) ->first();
        //~ return $project->payments;
       return view('dashboard.projectdonations', [
					 'project' => $project
					 ]);
    }

    /**
     * Show the a single user Project messages.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewprojectmessages(Request $request, $id)
    {
		$project = Project::where('id', (int)$id) ->with(['comments'  =>  function($q) {
						  $q->where('is_parent', true)->with(['replies'])->latest();
					}])->first();
		// $project = Project::where('id', $id) ->first();
        //return $project;
       return view('dashboard.projectmessages', [
					 'project' => $project
					 ]);
    }

    /**
     * Show the a single user Project settings.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewprojectsettings(Request $request, $id)
    {
		$project = Project::where('id', (int)$id) ->first();
        //return $project;
       return view('dashboard.projectsettings', [
					 'project' => $project
					 ]);
    }

    /**
     * Show the a single user Project stats.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewprojectstats(Request $request, $id)
    {
		$project = Project::where('id', (int)$id) ->first();
        //return $project;
       return view('dashboard.projectstats', [
					 'project' => $project
					 ]);
    }

    /**
     * Show the a single user Donation Contribuions view.
     *
     * @return \Illuminate\Http\Response
     */
    public function mycontributions(Request $request)
    {
		$user = $request->user();

		$projects =  $user -> projects() ->with(['payments'])->get();
		$donators = $user -> donators() ->with(['payments'])->get();

		$funds_count = 0 ;
		$funds_made = 0 ;

		foreach ($projects as $project) {
			$funds_count = $funds_count + $project->payments->sum('amount');
		}
		foreach ($donators as $donator) {
			$funds_made = $funds_made + $donator->payments->sum('amount');
		 }

        //~ return $projects;
       return view('dashboard.donations',[
											'donators' => $donators,
											'total_received' => $funds_count,
											'funds_made' => $funds_made,
										]);
    }

    /**
     * Show the a single user Donation receveied view.
     *
     * @return \Illuminate\Http\Response
     */
    public function mydonations(Request $request)
    {
		$user = $request->user();

		$projects =  $user -> projects() ->with(['payments'])->get();
		$donators = $user -> donators() ->with(['payments'])->get();

		$funds_count = 0 ;
		$funds_made = 0 ;
		foreach ($projects as $project) {
				$funds_count = $funds_count + $project->payments->sum('amount');
		 }
		 foreach ($donators as $donator) {
				$funds_made = $funds_made + $donator->payments->sum('amount');
		 }
        //return $projects;
       return view('dashboard.donationsreceived',['projects' => $projects,
													'total_received' => $funds_count,
													'funds_made' => $funds_made,
													]);
    }


    /**
     * Show the a single user Messages view.
     *
     * @return \Illuminate\Http\Response
     */
    public function mymessages(Request $request)
    {
		$emails = User::whereNotIn('id', [$request->user()->id])
					->where(['is_admin' => false , 'status' => true, 'is_pending' => false ])
					->pluck('email');
		//return $emails;
       return view('dashboard.messages', ['emails' => $emails]);
    }

     /**
     * Show the a single user Project messages.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewprojectmessagescomments(Request $request, $id, $comment)
    {
		$project = Project::where('id', (int)$id) ->first();
		$comments = Comment::where('id', (int)$comment) -> with(['replies'])->first();

        //return $comments;
       return view('dashboard.projectmessagescomments', [
					 'project' => $project,
					 'comment' => $comments
					 ]);
    }

    /**
     * Show the a single user Project messages.
     *
     * @return \Illuminate\Http\Response
     */
    public function myProfile(Request $request)
    {
		$user = $request ->user();
		$countries = Country::all();
       return view('dashboard.profile', [
					 'user' => $user,
					 'countries' => $countries
					 ]);
    }

    /**
     * Show the a single user Messages view.
     *
     * @return \Illuminate\Http\Response
     */
    public function myTransactions(Request $request)
    {
	  $payments = $request ->user() -> donators() ->with(['payments'])->get();
       return view('dashboard.transactions', ['payments' => $payments]);
    }

	/**
     * Show the a single user Messages view.
     *
     * @return \Illuminate\Http\Response
     */
    public function myMembership(Request $request)
    {
		$user = $request ->user();
       return view('dashboard.membership', ['user' => $user]);
    }

	/**
     * Show the a single user Messages view.
     *
     * @return \Illuminate\Http\Response
     */
    public function myCommisions(Request $request)
    {
		$user = $request ->user();
       return view('dashboard.commisions', ['user' => $user]);
    }

    /**
     * Show the a single user Messages view.
     *
     * @return \Illuminate\Http\Response
     */
    public function myBanking(Request $request)
    {
		$user = $request ->user();

       return view('dashboard.banking', ['user' => $user]);
    }

}
