<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Image;

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if ($request->hasFile('file')) {
			$path = $request->file->store('images', 'public');
            $return = [ 'success' => false ];

			$image = new Image;
            $image->name = $path;
			$image->save();

			if ($request->has('userid')) {
				$image->users()->attach($request -> userid);
				$return = [
					'path' => asset('storage/'.$image->name),
					'id' => $request -> projectid
				];
			}

			if ($request->has('projectid')) {
				if ($request->has('isfeatured')) {
					$image->featured = 1;
					$image->save();
			    }

			    $image->projects()->attach($request -> projectid);
			    $return = [
					'path' => asset('storage/'.$image->name),
					'id' => $request -> projectid
				];
			}

			if ($request->has('eventid')) {
				if ($request->has('isfeatured')) {
					$image->featured = 1;
					$image->save();
			    }

			    $image->events()->attach($request -> eventid);
			    $return = [
					'path' => asset('storage/'.$image->name),
					'id' => $request -> eventid
				];
			}

			if ($request->has('blogid')) {
				if ($request->has('isfeatured')) {
					$image->featured = 1;
					$image->save();
			    }

			    $image->posts()->attach($request -> blogid);
			    $return = [
					'path' => asset('storage/'.$image->name),
					'id' => $request -> blogid
				];
			}

			if ($request->has('action') && ($request->action === 'testimonial' )) {

			    $return = [
					'id' => $image -> id
				];
			}

			if ($request->has('action') && ($request->action === 'slider' )) {
				$image->featured = 1;
				$image->save();

                $image->settings()->attach(1, [
							'is_slider' => 1,
							'title' => $request->title,
							'subtitle' => $request->subtitle,
							'content' => $request->content,
						]);
			    $return = [
					'id' => $image -> id,
					'success' => true,
				];
			}

			if ($request->has('action') && ($request->action === 'sponser' )) {
				$image->featured = 1;
				$image->save();

                $image->settings()->attach(1, ['is_sponsor' => 1]);
			    $return = [
					'success' => true,
					'id' => $image -> id
				];
			}

			if ($request->has('action') && ($request->action === 'founders' )) {
				//~ $image->featured = 1;
				//~ $image->save();

			    $return = [
					'success' => true,
					'id' => $image -> id
				];
			}

			return response()->json($return);
		}

		return Response::json('error', 400);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
