<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mailbox;
use App\User;
use Validator;

class MailboxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = [
		  'message'      => $request->message,
		  'subject'      => $request->subject,
		  'email'      => $request->email,
		];

        $rules = [
			'message'     =>  'required|string',
			'subject'     =>  'required|string',
			'email' => 'required|string|email',
		];

		if($request->ajax()){

			$validator = Validator::make($data,$rules);
			if($validator->fails())
				return response()->json([
					'success' => false,
					'errors' => $validator->getMessageBag()->toArray()
				]);
			else {
				$to = User::where('email', $request->email)->first();
				$user = $request -> user() ;

				$mailbox = new Mailbox;
				$mailbox->content = $request->message;
				$mailbox->subject = $request->subject;
				$mailbox->is_parent = 1;
				$mailbox->outbox()->associate($user);
				$mailbox->inbox()->associate($to);
				$mailbox->save();

				// $comment->users()->attach($user -> id);

				return response()->json([
					'success' => true
				]);
			}
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

     /**
     * Get Sent mail of particuler user resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function sentmail(Request  $request, $id)
    {
        //
        $mail = User::where('id', $id) -> with(['outboxes' => function($q) {
						return $q->with(['inbox']);
					}])->first();
        //return $mail;
		return response()->json([
						'success' => true,
						'mail' => $mail
					]);
    }

     /**
     * Get Inbox mail of particuler user resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function inboxmail(Request  $request, $id)
    {
        //
        $mail = User::where('id', $id) -> with(['inboxes' => function($q) {
						return $q->with(['outbox']);
					}])->first();
       // return $mail;
		return response()->json([
						'success' => true,
						'mail' => $mail
					]);
    }
}
