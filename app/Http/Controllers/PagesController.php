<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\Post;
use App\Setting;
use App\Testimony;
use App\Founder;
use App\Country;

class PagesController extends Controller
{
    /**
     * The  incoming request object.
     *
     * @var  \Illuminate\Http\Request
     */
	protected $request;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
		$this -> request = $request;
    }

    /**
     * Show the homepage.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$event = Event::latest()->first();
		$posts = Post::latest()->take(5)->get();
		$settings = Setting::find(1)->with('sponsorsimages', 'sliderimages')->first();
        $testimony = Testimony::with('image') -> latest()->take(5)->get();

       // return $settings ;
        return view('index', [
								'event' => $event,
								'posts' => $posts,
								'settings' => $settings,
								'testimony' => $testimony,
							 ] );
    }

    /**
     * Show the homepage.
     *
     * @return \Illuminate\Http\Response
     */
    public function about()
    {
		$settings = Setting::find(1);
        $founders = Founder::with('image') -> latest()->take(4)->get();

        //return $settings ;
        return view('about', [
								'settings' => $settings,
								'founders' => $founders,
							 ] );
    }

    /**
     * Show the homepage.
     *
     * @return \Illuminate\Http\Response
     */
    public function donations()
    {

        return view('donations');
    }

    /**
     * Show the join.
     *
     * @return \Illuminate\Http\Response
     */
    public function join()
    {
		$countries = Country::all();
        return view('join', ['countries' => $countries ]);
    }
}
