<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Paystack;
use Validator;
use Hash;
use Auth;
use Carbon\Carbon;
use App\Payment;
use App\Donator;
use App\Project;
use App\User;
use App\Activation;
use App\Commision;
use Mail;
use App\Mail\DonationReceived;
use App\Mail\AccountActive;

class PaymentController extends Controller
{
      /**
     * Redirect the User to Paystack Payment Page
     * @param  Illuminate\Http\Request $request
     * @return Illuminate\Http\Response
     */
    public function redirectToGateway(Request $request)
    {


		if($request->ajax()){

			$payreq = [
			  'first_name'      => $request->first_name,
			  'last_name'      => $request->last_name,
			  'email'      => $request->email,
			  'amount'      => $request->amount
			];

			$rules = [
				'first_name'      =>  'required|string',
				'last_name'     =>  'required|string',
				'email'     =>  'required|email',
				'amount'     =>  'required'
			];

			$validator = Validator::make($payreq,$rules);
			if($validator->fails()) {
				return response()->json([
					'success' => false,
					'errors' => $validator->getMessageBag()->toArray()
				]);
			} else {
				return response()->json([
					'success' => true
				]);
			}
		} else {
			return Paystack::getAuthorizationUrl()->redirectNow();
		}
    }

    /**
     * Obtain Paystack payment information
     * @return Illuminate\Http\Response
     */
    public function handleGatewayCallback()
    {
        $paymentDetails = Paystack::getPaymentData();
        if (array_has($paymentDetails, 'data.metadata.custom_fields.0.variable_name')) {

			$payDet =  array_dot($paymentDetails) ;
			//~ dd($paymentDetails);

			if ($payDet["data.metadata.custom_fields.0.variable_name"] === "member_id") {
				$user =  User::where('member_id',  $payDet["data.metadata.custom_fields.0.value"])->firstOrFail();

				$donator = Donator::firstOrNew(
					['cust_id' => $payDet["data.customer.id"]],
					[
						'customer_code' => $payDet["data.customer.customer_code"],
						'first_name' => $user->f_name,
						//~ 'first_name' => 'jkk',
						//~ 'last_name' => 'jkk',
						'last_name' => $user->l_name,
						'email' => $payDet["data.customer.email"],
						'phone' => $payDet["data.customer.phone"],
						'metadata' => json_encode(array_get($paymentDetails, 'data.customer.metadata')),
						'risk_action' => $payDet["data.customer.risk_action"],
						'password' => Hash::make(str_random(10))
					]
				);
				$donator->user()->associate($user);
				$donator->save();

				$payment = Payment::firstOrNew(
					['tranx_id' => $payDet["data.id"]],
					[
						'status' => $payDet["data.status"],
						'reference' => $payDet["data.reference"],
						'currency' => $payDet["data.currency"],
						'amount' => $payDet["data.amount"],
						'fees' => $payDet["data.fees"],
						'channel' => $payDet["data.channel"],
						'gateway_response' => $payDet["data.gateway_response"],
						'referrer' =>  $payDet["data.metadata.referrer"],
						'domain' => $payDet["data.domain"],
						'is_membership' => true,
						'metadata' => json_encode(array_get($paymentDetails, 'data.metadata.custom_fields')),
						'ip_address' => $payDet["data.ip_address"],
						'transaction_date' => Carbon::now()
					]
				);

				$payment->donator()->associate($donator);
				$payment->save();

				$activation = new Activation;
				$activation->start_date = Carbon::now();
				$activation->end_date = Carbon::now()->addYear();
				$activation->activator_id = $user->id;
				$activation->payment_id = $payment->id;
				$activation->user()->associate($user);
				$activation->status = true;
				$activation->save();

				if (Commision::where('refer_id',  $user->id)->exists()) {
                   $commision = Commision::where('refer_id',  $user->id)->latest()->first();
                   $commision->status = true;
                   $commision->payment_id = $payment->id;
                   $commision->save();
				}

				$user->is_paid = true ;
                $user->status = true ;
                $user->save();

                Auth::login($user);

				Mail::to($user)->send(new AccountActive($user));
				return redirect()->route('dashboard');

			} elseif ($payDet["data.metadata.custom_fields.0.variable_name"] === "action") {

				$donator = Donator::firstOrNew(
					['cust_id' => $payDet["data.customer.id"]],
					[
						'customer_code' => $payDet["data.customer.customer_code"],
						 //~ 'first_name' => $payDet["data.customer.first_name"],
						'first_name' => 'jkk',
						'last_name' => 'jkk',
						 //~ 'last_name' => $payDet["data.customer.last_name"],
						'email' => $payDet["data.customer.email"],
						//~ 'user_id' => ,
						'phone' => $payDet["data.customer.phone"],
						'metadata' => json_encode(array_get($paymentDetails, 'data.customer.metadata')),
						'risk_action' => $payDet["data.customer.risk_action"],
						'password' => Hash::make(str_random(10))
					]
				);
				// $donator->user()->associate($user);
				$donator->save();

				$payment = Payment::firstOrNew(
					['tranx_id' => $payDet["data.id"]],
					[
						'status' => $payDet["data.status"],
						'reference' => $payDet["data.reference"],
						'currency' => $payDet["data.currency"],
						'amount' => $payDet["data.amount"],
						'fees' => $payDet["data.fees"],
						'channel' => $payDet["data.channel"],
						'gateway_response' => $payDet["data.gateway_response"],
						'referrer' =>  $payDet["data.metadata.referrer"],
						'domain' => $payDet["data.domain"],
						'is_donations' => true,
						'metadata' => json_encode(array_get($paymentDetails, 'data.metadata.custom_fields')),
						'ip_address' => $payDet["data.ip_address"],
						'transaction_date' => Carbon::now()
					]
				);

				$payment->donator()->associate($donator);
				$payment->save();

				//~ Mail::to($user)->send(new AccountActive($user));

				return redirect()->route('paymentsuccess');

			} else {
				$project = Project::where('id', intval($payDet["data.metadata.custom_fields.1.value"]))->first() ;
				// dd($project);


				if (! Auth::check()) {
					$user = User::firstOrNew(
					['email' => $payDet["data.customer.email"]],
					[
						'f_name' => $payDet["data.customer.first_name"],
						'l_name' => $payDet["data.customer.last_name"],
						'password' => Hash::make(str_random(10)),
						'username'      => str_random(10),
						'address'      => 'no data',
						'phone'      => 'no data',
						'email_token' =>  Hash::make(str_random(10)),
					]);
					$user->save();

				} else {
				   $user = Auth::user();
				}

				$donator = Donator::firstOrNew(
					['cust_id' => $payDet["data.customer.id"]],
					[
						'customer_code' => $payDet["data.customer.customer_code"],
						'first_name' => $payDet["data.customer.first_name"],
						'last_name' => $payDet["data.customer.last_name"],
						'email' => $payDet["data.customer.email"],
						'phone' => $payDet["data.customer.phone"],
						'metadata' => json_encode(array_get($paymentDetails, 'data.customer.metadata')),
						'risk_action' => $payDet["data.customer.risk_action"],
						'password' => Hash::make(str_random(10))
					]
				);
				$donator->user()->associate($user);
				$donator->save();

				$payment = Payment::firstOrNew(
					['tranx_id' => $payDet["data.id"]],
					[
						'status' => $payDet["data.status"],
						'reference' => $payDet["data.reference"],
						'currency' => $payDet["data.currency"],
						'amount' => $payDet["data.amount"],
						'fees' => $payDet["data.fees"],
						'channel' => $payDet["data.channel"],
						'gateway_response' => $payDet["data.gateway_response"],
						'referrer' =>  $payDet["data.metadata.referrer"],
						'domain' => $payDet["data.domain"],
						'metadata' => json_encode(array_get($paymentDetails, 'data.metadata.custom_fields')),
						'ip_address' => $payDet["data.ip_address"],
						'transaction_date' => Carbon::now()
					]
				);

				$payment->donator()->associate($donator);
				$payment->project()->associate($project);

				$payment->save();
				//dd($payment);
				Mail::to($project->user)->send(new DonationReceived($payment, $project));

				return redirect()->route('paymentsuccess');

			}
			//~ dd($payDet);
		} else {


		}


    }

    /**
     * Success payment page
     * @param  \Illuminate\Http\Request  $request
     * @return Illuminate\Http\Response
     */
    public function paymentsuccess(Request  $request)
    {
		return view('paymentsuccess');
	}
}
