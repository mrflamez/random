<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use App\Project;
use App\Category;
use App\Comment;
use DB;
use Validator;
use Mail;
use App\Mail\CreateProject;
// use Carbon\Carbon;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$categories = Category::take(5)->get();
		$projects = Project::has('images')->with(['category', 'images' =>  function($q) {
						return $q->where('featured', true);
					}])->paginate(6);
        //dd($projects);
        //return $projects;
        return view('projects', ['projects' => $projects, 'categories' => $categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = [
		  'title'      => $request->title,
		  'description'      => $request->description,
		  'amount'      => $request->amount,
		  'start'      => $request->start,
		  'end'      => $request->end,
		];

        $rules = [
			'title'      =>  'required|string',
			'description'      =>  'required',
			'amount'      =>  'required',
			'start'      =>  'required',
			'end'      =>  'required',

		];

		$validator = Validator::make($data,$rules);
		if($validator->fails()) {
			return response()->json([
				'success' => false,
				'errors' => $validator->getMessageBag()->toArray()
			]);
		} else {

			$category = Category::find($request->category);
			$user = Auth::user();

			$project = new Project;
			$project->title = $request->title;
			$project->description = $request->description;
			$project->amount = $request->amount;
			$project->start_date = $request->start;
			$project->end_date = $request->end;
			$project->category()->associate($category);
			$project->user()->associate($user);
			$project->status = true;
			$project->save();

			Mail::to($user)->send(new CreateProject($project));

			return response()->json([
				'success' => true,
				'id' => $project-> id,
			]);
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		$project = Project::where('id', $id) ->first();
        //return $project;
       return view('project', [ 'project' => $project ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $return = ['success' => false];

        if ($request->has('type') && $request->has('status') && $request->type === 'publish') {
			$project = Project::find($id);
			$project->status = 1 ;
			$project->save();
			$return = [ 'id' => $project-> id, 'status' => 'complete', 'path' => route('projects.show', ['id' => $id]) ];
		}

		if ($request->has('action') && $request->action === 'deactivate') {
			$project = Project::find($id);
			$project->status = false ;
			$project->save();
			$return = [ 'id' => $project-> id, 'success' => true ];
		}

		if ($request->has('action') && $request->action === 'activate') {
			$project = Project::find($id);
			$project->status = true ;
			$project->save();
			$return = [ 'id' => $project-> id, 'success' => true ];
		}

		return response()->json($return);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $res = Project::destroy($id);
        return response()->json([
			'success' => filter_var( $res, FILTER_VALIDATE_BOOLEAN )
		]);
    }

     /**
     * Display the Single Project messages.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function projectmessages($id)
    {
		$project = Project::where('id', (int)$id) ->with(['comments'  =>  function($q) {
						  $q->where('is_parent', true)->with(['replies'])->take(5)->latest();
					}])->first();

		// $comment = Comment::selectRaw('count(*) AS cnt, user_id, ANY_VALUE(content)')->groupBy('user_id')->orderBy('cnt', 'DESC')->limit(5)->get();
		// $comment = Comment::selectRaw('')->get();
		//~ $comment = DB::table('comments')
                     //~ ->select(DB::raw('count(*) as cnt, content'))
                     //~ ->groupBy('cnt')
                     //~ ->get();
		 //~ $comment = DB::table('comments')->distinct('user_id')->get();


        //~ dd($project);
       // return $project;
       return view('projectmessages', [ 'project' => $project ]);
    }

    /**
     * Display the Single Project donations.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function projectdonations($id)
    {
		$project = Project::where('id', (int)$id) ->first();
        //~ return $project->payments;
       return view('projectdonations', [ 'project' => $project ]);
    }
}
