<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\RuleRegulation;

class RulesRegulationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$rules = RuleRegulation::all();
		 return view('rules', ['ruleregulations' => $rules ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$data = [
		  'title'      => $request->title,
		  'description'      => $request->description
		];

        $rules = [
			'title'      =>  'required|string',
			'description'     =>  'required|string'
		];

		$validator = Validator::make($data,$rules);
		if($validator->fails()) {
			return response()->json([
				'success' => false,
				'errors' => $validator->getMessageBag()->toArray()
			]);
		} else {
			$rule = RuleRegulation::firstOrNew([
				'title' => $request->title,
				'content' => $request->description
			]);

			$rule->save();

			return response()->json([
				'success' => true,
				'id' => $rule->id
			]);
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$data = [
		  'title'      => $request->title,
		  'description'      => $request->description
		];

        $rules = [
			'title'      =>  'required|string',
			'description'     =>  'required|string'
		];

		$validator = Validator::make($data,$rules);
		if($validator->fails()) {
			return response()->json([
				'success' => false,
				'errors' => $validator->getMessageBag()->toArray()
			]);
		} else {
			RuleRegulation::where('id', $id)
			->update([
				'title' => $request->title,
				'content' => $request->description
			]);

			return response()->json([
				'success' => true,
				'id' => $id
			]);
		}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		$res = RuleRegulation::destroy($id);
        return response()->json([
			'success' => filter_var( $res, FILTER_VALIDATE_BOOLEAN )
		]);

    }
}

