<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Setting;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $return = ['success' => false];

        if ($request->has('action') && $request->action === 'social') {

				Setting::where('id', $id)
				->update([
					'facebook' => $request->facebook,
					'twitter' => $request->twitter,
					'instagram' => $request->instagram,
				]);

				$return = [
					'success' => true,
					'id' => $id
				];
		}

		if ($request->has('action') && $request->action === 'story') {

				Setting::where('id', $id)
				->update([
					'story' => $request->story_content
				]);

				$return = [
					'success' => true,
					'id' => $id
				];
		}

		if ($request->has('action') && $request->action === 'fee') {

				Setting::where('id', $id)
				->update([
					'fee' => $request->fee
				]);

				$return = [
					'success' => true,
					'id' => $id
				];
		}

		if ($request->has('action') && $request->action === 'stats') {

				Setting::where('id', $id)
				->update([
					'investors' => $request->investors,
					'partners' => $request->partners,
					'satifaction' => $request->satifaction,
					'bonus' => $request->bonus,
				]);

				$return = [
					'success' => true,
					'id' => $id
				];
		}

		return response()->json($return);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
