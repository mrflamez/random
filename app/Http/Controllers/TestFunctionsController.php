<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\RegistrationVerification;
use Mail;
use App\User;

class TestFunctionsController extends Controller
{
    /**
     * Show the create Project view.
     *
     * @return Response
     */
    public function testsendmail()
    {
		$order = [
			'url' => 'https://laravel.com/docs/5.4/mail'
		];
		Mail::to('cliffjimmy27@gmail.com')->send(new RegistrationVerification($order));
    }

     public function updatemeberid()
    {
		$users = User::whereNull('member_id')->get();
		foreach ($users as $user) {
			$user-> member_id = $user->id.'-'.str_random(10) ;
			$user->save() ;

		}
    }

    public function grouping()
    {

		//~ $visitorTraffic = PageView::where('created_at', '>=', \Carbon\Carbon::now->subMonth())
                        //~ ->groupBy(DB::raw('Date(created_at)'))
                        //~ ->orderBy('created_at', 'DESC')->get();
         //~ $data = User::where('created_at', '>=', \Carbon\Carbon::now->subMonth())
                        //~ ->groupBy(DB::raw('Date(created_at)'))
                        //~ ->orderBy('created_at', 'DESC')
                        //~ ->get();
		//~ $data = User::get()
					//~ ->groupBy(function($item) {
					//~ return $item->created_at->month;
				//~ });
			//~ $data =	User::select(DB::raw('count(id) as `data`'),DB::raw("CONCAT_WS('-',MONTH(created_at),YEAR(created_at)) as monthyear"))
               //~ ->groupby('monthyear')
               //~ ->get();
    }

    //~ public function getRowNumData(Request $request)
    //~ {
        //~ DB::statement(DB::raw('set @rownum=0'));
        //~ $users = User::select([
            //~ DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            //~ 'id',
            //~ 'name',
            //~ 'email',
            //~ 'created_at',
            //~ 'updated_at']);
        //~ $datatables = Datatables::of($users);

        //~ if ($keyword = $request->get('search')['value']) {
            //~ $datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        //~ }

        //~ return $datatables->make(true);
    //~ }

    //~ public function getCarbonData()
    //~ {
        //~ $users = User::select(['id', 'name', 'email', 'created_at', 'updated_at']);

        //~ return Datatables::of($users)
            //~ ->editColumn('created_at', '{!! $created_at !!}')
            //~ ->editColumn('updated_at', function ($user) {
                //~ return $user->updated_at->format('Y/m/d');
            //~ })
            //~ ->filterColumn('updated_at', function ($query, $keyword) {
                //~ $query->whereRaw("DATE_FORMAT(updated_at,'%Y/%m/%d') like ?", ["%$keyword%"]);
            //~ })
            //~ ->make(true);
    //~ }

    //~ public function getBlacklist(Request $request)
    //~ {
        //if ($request->ajax()) {
            //~ return Datatables::of(User::query())
                //~ ->blacklist(['password', 'name'])
                //~ ->make(true);
        //}
    //~ }
}
