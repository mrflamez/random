<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Testimony;
use App\Image;

class TestimonyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [
		  'name'      => $request->name,
		  'alias'      => $request->alias,
		  'content'      => $request->content,
		  'image_id'      => $request->image_id,
		];

        $rules = [
			'name'      =>  'required|string',
			'alias'     =>  'required|string',
			'content'     =>  'required|string',
			'image_id'     =>  'required',
		];

		$validator = Validator::make($data,$rules);
		if($validator->fails()) {
			return response()->json([
				'success' => false,
				'errors' => $validator->getMessageBag()->toArray()
			]);
		} else {
			$image = Image::find((int)$request->image_id);

			$testimony = Testimony::firstOrNew([
				'name' => $request->name,
				'alias' => $request->alias,
				'content' => $request->content
			]);
			$testimony->user()->associate($request->user());
			$testimony->image()->associate($image);
			$testimony->save();

			return response()->json([
				'success' => true,
				'id' => $testimony->id
			]);
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
