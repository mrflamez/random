<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Validator;
use Response;
use Hash;
use App\Mail\RegistrationVerification;
use App\Mail\VerificationSucess;
use App\Setting;
use App\Commision;
use Mail;
use Auth;
use App\Banking;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         $userData = [
		  'f_name'      => $request->fname,
		  'l_name'      => $request->lname,
		  'email'      => $request->email,
		  'username'      => $request->username,
		  'address'      => $request->address,
		  'phone'      => $request->phone,
		 // 'referal'      => $request->referal,
		  'password'      => $request->password,
		  'password_confirmation'  => $request->password_confirmation
		];

		if (! is_null($request->referal)) {
			$userData['referal'] = $request->referal;
		 }

        $rules = [
			'f_name'      =>  'required|string',
			'l_name'     =>  'required|string',
			'email'     =>  'required|email|unique:users',
			'username'     =>  'required|string|unique:users,username',
			'address'     =>  'required',
			'phone'     =>  'required',
			//~ 'referal' =>  'exists:users,member_id',
			'password'  =>  'required|string|min:6|confirmed'
		];

		if (! is_null($request->referal)) {
			$rules['referal'] = 'exists:users,member_id';
		 }

		$validator = Validator::make($userData,$rules);
		if ($validator->fails()) {
			return response()->json([
				'success' => false,
				'errors' => $validator->getMessageBag()->toArray()
			]);
		} else {


			$token =  str_random(10) ;
			$user = User::create([
				'f_name' => $request->fname,
				'l_name' => $request->lname,
				'email' => $request->email,
				'password' => Hash::make($request->password),
				'username'      => $request->username,
				'address'      => $request->address,
				'phone'      => $request->phone,
				'email_token' =>  Hash::make($token),
				'status' => 0,
				'is_pending' => 1
			]);

			$user->member_id =  $user->id.'-'.str_random(10) ;
			$user-> save();

			$countryid =$request -> country;

			$user->countries()->attach((int)$countryid);

			$bank = new Banking();
			$bank->user_id = $user->id ;
			$bank-> save();

			if (! is_null($request->referal)) {
                $referer = User::where('member_id', $request->referal)->first();
                $setting = Setting::find(1);

				$commision = new Commision;
				$commision->status = false;
				$commision->refer_id = $user -> id;
				$commision->user_id = $referer -> id;
				$commision->amount = ($setting->fee/100)*10;
				$commision->save();
			}

			$emailData = [
			'url' => route('emailverify.index', ['token' => $token ])
			];

			Mail::to($user)->send(new RegistrationVerification($emailData));

			return response()->json([
				'success' => true,
				'id' => $user->id
			]);
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$return = ['success' => false];

		if ($request->has('action') && $request->action === 'admin_edit') {
			$userData = [
			  'f_name'      => $request->fname,
			  'l_name'      => $request->lname,
			  'email'      => $request->email,
			  'username'      => $request->username,
			  'address'      => $request->address,
			  'phone'      => $request->phone
			];

			$rules = [
				'f_name'      =>  'required|string',
				'l_name'     =>  'required|string',
				'email'     =>  'required|email',
				'username'     =>  'required|string',
				'address'     =>  'required',
				'phone'     =>  'required',
			];

			$validator = Validator::make($userData,$rules);
			if ($validator->fails()) {
				$return = [
					'success' => false,
					'errors' => $validator->getMessageBag()->toArray()
				];
			} else {

				User::where('id', $id)
				->update([
					'f_name' => $request->fname,
					'l_name' => $request->lname,
					'email' => $request->email,
					'username'      => $request->username,
					'address'      => $request->address,
					'phone'      => $request->phone,
				]);

				$user = User::find($id);
                $countryid =$request -> country;
				$user->countries()->sync([ $countryid ]);

				$return = [
					'success' => true,
					'message' => 'edit',
					'id' => $id
				];
			}
		}

        if ($request->has('action') && $request->action === 'admin_suspend') {

				User::where('id', $id)
				->update([
					'status' => false,
				]);

				$return = [
					'success' => true,
					'message' => 'suspend',
					'id' => $id
				];
		}

		if ($request->has('action') && $request->action === 'bank_edit') {

				$bank = User::find($id)->banking;
				$bank->bank_name = $request->bank_name ;
				$bank->account_name = $request->account_name ;
				$bank->account_num = $request->account_num ;
				$bank-> save();

				$return = [
					'success' => true,
					'id' => $id
				];
		}

		if ($request->has('action') && $request->action === 'admin_perms') {

			if ($request->has('type') && $request->type === 'makeadmin') {
				User::where('id', $id)
				->update([
					'is_admin' => true
				]);

				$return = [
					'success' => true,
					'message' => 'success'
				];
			}

			if ($request->has('type') && $request->type === 'removeadmin') {
				User::where('id', $id)
				->update([
					'is_admin' => false
				]);

				$return = [
					'success' => true,
					'message' => 'success'
				];
			}
		}


		return response()->json($return);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $res = User::destroy($id);
        return response()->json([
			'success' => filter_var( $res, FILTER_VALIDATE_BOOLEAN )
		]);
    }

    /**
     * Users emailverification view
     *
     * @param  string  $token
     * @return \Illuminate\Http\Response
     */
    public function emailverify(Request $request, $token)
    {
		//$user = User::where('', $token)->first();
		return response() -> view('emailverify', compact('token'));
	}

	/**
     * Users emailverification logic
     *
     * @param  Request  $request
     * @param  string  $token
     * @return Response
     */
    public function emailverifyupdate(Request $request, $token)
    {
        //
       $userData = [
		  'email'      => $request->email,
		  'password'      => $request->password
		];

        $rules = [
			'email'     =>  'required|email',
			'password'  =>  'required|string'
		];

		$validator = Validator::make($userData,$rules);
		if($validator->fails())
			return response()->json([
				'success' => false,
				'errors' => $validator->getMessageBag()->toArray()
			]);
		else {
			if (Auth::attempt(['email' => $request -> email, 'password' => $request -> password, 'status' => false, 'is_pending' => true ])) {
				// Authentication passed...
				$user = Auth::user();

				if (Hash::check($token, $user->email_token)) {
					// The token is a match...
					$user->is_pending = 0 ;
					// $user->member_id =  $user->id.'-'.str_random(10) ;
					$user->save();

                    Mail::to($user)->send(new VerificationSucess($user));
                    $path = route('membershippayment', ['memberid' => $user -> member_id]);

					//~ Auth::login($user);
					Auth::logout();

					return response()->json([
						'success' => true,
						'path' => $path
					]);

					// return redirect()->intended('dashboard');
				} else {
					// The token is  not a match...
					Auth::logout();

					return response()->json([
						'success' => false,
						'errors' => ['auth' => ['Token Not A Match']]
					]);
				}

			} else {
				// Authentication failed...
				return response()->json([
					'success' => false,
					'errors' => ['auth' => ['Authentication Failed']]
				]);
			}

		}
    }

   /**
     * Users emailverification logic
     *
     * @param  Request  $request
     * @param  string  $token
     * @return Response
     */
    public function membershipPayment(Request $request, $memberid)
    {

		$user =  User::where('member_id',  $memberid)->firstOrFail();
        $setting = Setting::find(1);
        $metadata =  json_encode ( [ 'custom_fields' => [ ['display_name' => "Member Id", 'variable_name' => "member_id", 'value' => $user -> member_id ] ] ]);
        $payment = collect([
          'f_name' => $user -> f_name,
          'l_name' => $user -> l_name,
          'metadata' => $metadata ,
          'email' => $user -> email,
        ]);

        //~ dd($payment);

       return view('membershippayment',[
					'setting' => $setting,
					'payment' => $payment
					] );
    }

    public function suspended(Request $request)
    {


       return view('suspended');
    }

}
