<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'featured'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'featured' => 'boolean',
    ];

    /**
     * The  projects that have this images.
     */
    public function projects()
    {
        return $this->belongsToMany('App\Project')->withTimestamps();
    }

    /**
     * The  Users that have this  image.
     */
    public function users()
    {
        return $this->belongsToMany('App\User')->withTimestamps();
    }

    /**
     * The  Event that have this  image.
     */
    public function events()
    {
        return $this->belongsToMany('App\Event')->withTimestamps();
    }

    /**
     * The  Posts that have this  image.
     */
    public function posts()
    {
        return $this->belongsToMany('App\Post')->withTimestamps();
    }

    /**
     * Get the testimonial of this image.
     */
    public function testimony()
    {
        return $this->hasOne('App\Testimony');
    }

    /**
     * The  Users that have this  image.
     */
    public function settings()
    {
        return $this->belongsToMany('App\Setting')->withTimestamps();
    }

    /**
     * Get the Founder of this image.
     */
    public function founder()
    {
        return $this->hasOne('App\Founder');
    }
}
