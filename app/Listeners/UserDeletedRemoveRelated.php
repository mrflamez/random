<?php

namespace App\Listeners;

use App\Events\UserDeleted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserDeletedRemoveRelated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserDeleted  $event
     * @return void
     */
    public function handle(UserDeleted $event)
    {
		$event-> user->images()->detach();

		$event-> user->roles()->detach();

        foreach ($event-> user -> projects as $project)
		{
			$project->delete();
		}

		foreach ($event-> user -> events as $uevent)
		{
			$uevent->delete();
		}

		foreach ($event-> user -> posts as $post)
		{
			$post->delete();
		}

		foreach ($event-> user -> faqs as $faq)
		{
			$faq->delete();
		}

		foreach ($event-> user -> comments as $comment)
		{
			$comment->delete();
		}

		foreach ($event-> user -> replies as $reply)
		{
			$reply->delete();
		}
    }
}
