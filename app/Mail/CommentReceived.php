<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Project;
use App\Comment;
use App\User;

class CommentReceived extends Mailable
{
    use Queueable, SerializesModels;

    public $project;
    public $user;
    public $comment;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, Comment $comment , Project $project)
    {
        $this->project = $project;
        $this->user = $user;
        $this->comment = $comment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.account.commentreceived');
    }
}
