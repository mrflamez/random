<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mailbox extends Model
{
    //
        /**
     * The Mass assigned attributes.
     *
     * @var array
     */
    protected $fillable = [
        'content', 'subject'
    ];

    protected $casts = [
        'is_parent' => 'boolean',
        'is_read' => 'boolean',
    ];

    /**
     * The User of the Mailbox Message receiving end. From address
     */
    public function inbox()
    {
        return $this->belongsTo('App\User', 'to', 'id');
    }

    /**
     * The User of the Mailbox Message sent end. To address
     */
    public function outbox()
    {
        return $this->belongsTo('App\User', 'from', 'id');
    }
}
