<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model
{
	use SoftDeletes;

	/**
     * The Mass assigned attributes.
     *
     * @var array
     */
    protected $fillable = [
        'status', 'reference', 'tranx_id', 'donator_id', 'currency', 'amount', 'fees', 'channel', 'gateway_response', 'referrer', 'domain', 'metadata', 'ip_address', 'transaction_date', 'project_id', 'is_membership', 'is_donations'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'transaction_date',
        'deleted_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'metadata' => 'array',
        'status' => 'boolean',
        'is_membership' => 'boolean',
        'is_donations' => 'boolean',
    ];

    /**
     * Set the metadata to json string on insert .
     *
     * @param  array  $value
     * @return void
     */
    public function setMetadataAttribute($value)
    {
        $this->attributes['metadata'] = json_encode($value);
    }

    /**
     * Set the status to boolean on insert .
     *
     * @param  string  $value
     * @return void
     */
    public function setStatusAttribute($value)
    {
		$status_boolen = 0 ;
		if ($value == 'success') {
			$status_boolen = 1 ;
		}

        $this->attributes['status'] = $status_boolen;
    }


     /**
     * The Donator of the payment transaction.
     */
    public function donator()
    {
        return $this->belongsTo('App\Donator');
    }

    /**
     * The Project of the payment transaction.
     */
    public function project()
    {
        return $this->belongsTo('App\Project');
    }

    /**
     * Get the Activation of this Payment.
     */
    public function activation()
    {
        return $this->hasOne('App\Activation', 'payment_id', 'id');
    }

    /**
     * Get the Activation of this Payment.
     */
    public function commision()
    {
        return $this->hasOne('App\Commision', 'payment_id', 'id');
    }
}
