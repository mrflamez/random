<?php

namespace App;

use App\Events\ProjectDeleted;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Project extends Model
{
	use SoftDeletes;

     /**
     * The Mass assigned attributes.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'description', 'amount', 'start_date', 'end_date'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'start_date',
        'end_date',
        'deleted_at'
    ];


	/**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'amount' => 'float',
        'status' => 'boolean',
    ];

     /**
     * The event map for the model.
     *
     * @var array
     */
    protected $events = [
        'deleted' => ProjectDeleted::class,
    ];


    /**
     * Set the amount to float on insert .
     *
     * @param  string  $value
     * @return void
     */
    public function setAmountAttribute($value)
    {
        $this->attributes['amount'] = round(floatval($value), 2 );
    }

    /**
     * The images that belong to the project.
     */
    public function images()
    {
        return $this->belongsToMany('App\Image')->withTimestamps();
    }


	/**
     * The Category of the project.
     */
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    /**
     * The User of the project.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get the  payment transaction of this Project.
     */
    public function payments()
    {
        return $this->hasMany('App\Payment');
    }

    /**
     * Get the  Comments of this Project.
     */
    public function comments()
    {
        return $this->hasMany('App\Comment');
    }
}
