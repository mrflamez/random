<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use App\Post;
use App\Setting;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
		$footer_blog_posts = Post::latest() ->take(3)->get();
		$settings = Setting::find(1);

        Schema::defaultStringLength(191);
        View::share('footer_blog_posts', $footer_blog_posts);
       View::share('social_links_setting', $settings);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
