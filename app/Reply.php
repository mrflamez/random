<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reply extends Model
{
	 use SoftDeletes;

	/**
     * The Mass assigned attributes.
     *
     * @var array
     */
    protected $fillable = [
        'content', 'comment_id', 'user_id'
    ];

     /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

	/**
     * Get the parent Comment of Reply .
     */
    public function comment()
    {
        return $this->belongsTo('App\Comment');
    }

    /**
     * The User of the Reply.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
