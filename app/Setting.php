<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{

	 /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];


    /**
     * The images that belong to the User.
     */
    public function sliderimages()
    {
        return $this->belongsToMany('App\Image', 'image_setting', 'setting_id', 'image_id')->withPivot('title', 'subtitle', 'content')->wherePivot('is_slider', 1 );
    }

    /**
     * The images that belong to the User.
     */
    public function sponsorsimages()
    {
        return $this->belongsToMany('App\Image', 'image_setting', 'setting_id', 'image_id')->wherePivot('is_sponsor' , 1);
    }
}
