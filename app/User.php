<?php

namespace App;

use App\Events\UserDeleted;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'f_name', 'l_name', 'email', 'password', 'phone', 'address', 'username', 'status', 'email_token', 'is_pending', 'is_paid'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $events = [
        'deleted' => UserDeleted::class,
    ];

	/**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_admin' => 'boolean',
        'status' => 'boolean',
        'is_pending' => 'boolean',
        'is_paid' => 'boolean',
    ];

     /**
     * Scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', true);
    }

	/**
     * Scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAdmin($query)
    {
        return $query->where('is_admin', true);
    }
    /**
     * Get the Projects for the User.
     */
    public function projects()
    {
        return $this->hasMany('App\Project');
    }

    /**
     * The images that belong to the User.
     */
    public function images()
    {
        return $this->belongsToMany('App\Image');
    }

    /**
     * The comments that belong to the User.
     */
    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    /**
     * The Replies of the Comment  that belong to the User.
     */
    public function replies()
    {
        return $this->hasMany('App\Reply');
    }

    /**
     * The Donators aacounts that belong to the User.
     */
    public function donators()
    {
        return $this->hasMany('App\Donator', 'user_id');
    }

    /**
     * The Mailbox Messages belong to the User. From address.Inbox messages
     */
    public function Inboxes()
    {
        return $this->hasMany('App\Mailbox', 'to', 'id');
    }

    /**
     * The Mailbox Messages belong to the User. To address, Sent messages
     */
    public function Outboxes()
    {
        return $this->hasMany('App\Mailbox', 'from', 'id');
    }

    /**
     * The  roles that belong to the user.
     */
    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }

     /**
     * Get the events of the user.
     */
    public function events()
    {
        return $this->hasMany('App\Event');
    }

     /**
     * Get the posts of the user.
     */
    public function posts()
    {
        return $this->hasMany('App\Post');
    }

    /**
     * Get the faqs of the user.
     */
    public function faqs()
    {
        return $this->hasMany('App\Faq');
    }

    /**
     * Get the testimonies for the User.
     */
    public function testimony()
    {
        return $this->hasMany('App\Testimony');
    }

    /**
     * Get the Founders for the User.
     */
    public function founder()
    {
        return $this->hasMany('App\Founder');
    }

    /**
     * Get the Activations for the User.
     */
    public function activations()
    {
        return $this->hasMany('App\Activation');
    }

    /**
     * Get the Activations for the User.
     */
    public function activators()
    {
        return $this->hasMany('App\Activation', 'activator_id', 'id');
    }

    /**
     * Get the commision for the User.
     */
    public function commisions()
    {
        return $this->hasMany('App\Commision');
    }

    /**
     * Get the refers for the User.
     */
    public function refers()
    {
        return $this->hasMany('App\Commision', 'refer_id', 'id');
    }

    /**
     * Get the Activation of this Payment.
     */
    public function banking()
    {
        return $this->hasOne('App\Banking');
    }

    /**
     * The countries that belong to the User.
     */
    public function countries()
    {
        return $this->belongsToMany('App\Country');
    }
}
