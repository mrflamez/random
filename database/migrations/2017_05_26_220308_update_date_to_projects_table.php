<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDateToProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projects', function (Blueprint $table) {
            //
            $table->dropColumn(['start-date', 'end-date']);
            $table->timestamp('start_date')->nullable()->after('amount');
            $table->timestamp('end_date')->nullable()->after('amount');
            //$table->renameColumn('start-date', 'start_date');
            //$table->renameColumn('end-date', 'end_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('projects', function (Blueprint $table) {
            //
        });
    }
}
