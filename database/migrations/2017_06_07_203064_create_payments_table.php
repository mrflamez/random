<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('status')->default(false);
            $table->string('reference')->unique();
            $table->integer('tranx_id')->unique();
            $table->integer('donator_id')->unsigned();
            $table->string('currency');
            $table->integer('amount');
            $table->integer('fees');
            $table->string('channel');
            $table->string('gateway_response');
            $table->string('referrer');
            $table->string('domain');
            $table->longText('metadata');
            $table->string('ip_address');
           // $table->dateTimeTz('transaction_date');
            $table->timestamps();

            $table->foreign('donator_id')->references('id')->on('donators');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
