<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateNullablePhoneToDonatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('donators', function (Blueprint $table) {
            //
           //  $table->dropColumn(['phone', 'metadata']);
            $table->string('phone')->nullable();
            $table->longText('metadata')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('donators', function (Blueprint $table) {
            //
        });
    }
}
