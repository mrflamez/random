<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImageSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('image_setting', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('image_id');
            $table->integer('setting_id');
            $table->boolean('is_slider')->default(false);
            $table->boolean('is_sponsor')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('image_setting');
    }
}
