<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImageContentToImageSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('image_setting', function (Blueprint $table) {
            //
             $table->string('title')->nullable();
             $table->string('subtitle')->nullable();
             $table->string('content')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('image_setting', function (Blueprint $table) {
           $table->dropColumn(['title', 'subtitle', 'content']);
        });
    }
}
