<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Banking;
class BankingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();

        foreach ($users as $user) {
			$bank = new Banking();
			$bank->user_id = $user->id ;
			$bank-> save();
		}
    }
}
