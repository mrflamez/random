<?php

use Illuminate\Database\Seeder;
use App\Category;
class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // factory(App\Category::class, 10)->create()->each(function ($u) {
		//	$u->posts()->save(factory(App\Post::class)->make());
		// });
		for ($i=1; $i<=10; $i++) {
			$n = 'Category-'.$i ;
			Category::create([
				'name' => $n,
				'slug' => $n
			]);
		}

    }
}
