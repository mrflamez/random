<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder);
        // $this->call(CategoryTableSeeder);
        $this->call('BankingSeeder');
		$this->call('CountriesSeeder');
		$this->call('SettingsSeeder');
		$this->command->info('Seeded the countries!');
    }
}
