$(document).ready(function(){
	$(window).scroll(function() {
        var headerHeight = $('header.main-header').height();
        if($(window).scrollTop() > headerHeight) {
            $('header.main-header').addClass('navbar-fixed');
        } else if($(window).scrollTop() < headerHeight) {
            $('header.main-header').removeClass('navbar-fixed');
        }
    });
 });
