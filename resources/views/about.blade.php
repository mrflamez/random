@extends('layouts.app')

@section('title', 'About')

@section('content')
<section class="hero is-small header-overlay-bg1 is-bold">
  <div class="hero-body bg-gray">
    <div class="container">
		<div class="heading  m-b-3 has-text-centered">
			<h3 class="title txt-green-yellow ">About Us</h3>
			<h5 class="subtitle txt-green-yellow"> </h5>
		</div>
    </div>
  </div>
</section>

<section class="section">
	<div class="container">
      <div class="heading m-b-3 has-text-centered">
        <h3 class="title">Our Story</h3>
        <h5 class="subtitle"> </h5>
      </div>
    </div>
    <div class=" columns">
		<div class="column">
			<div class="content ">
				@isset( $settings->story)
				{!! $settings->story  !!}
				 <h6 class="title">Club Admin</h6>
                @endisset
			 </div>
		</div>
	    <div class="column is-half">
	        <div class="block full-width">
			   <iframe class="video" src="https://www.youtube.com/embed/ob5HMRWcrB0" frameborder="0" allowfullscreen></iframe>
			</div>
	    </div>
	</div>
</section>

<section class="section">
	<div class="container">
      <div class="heading m-b-3 has-text-centered">
        <h3 class="title">How It Works</h3>
      </div>
    </div>
    <div class=" columns ">
		<div class="column is-4">

			<div class="field  has-addons has-addons-centered">
				  <p class="control">
					  <button class="is-outlined is-primary is-large button">
						<span class="icon  ">
						  <i class="fa fa-lightbulb-o"></i>
						</span>
					  </button>
				 </p>
			</div>
			<p class="title is-5 is-bold has-text-centered"> Register to Join</p>
			<div class="content ">
				 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean efficitur.
				  consectetur adipiscing elit. Aenean efficitur. consectetur adipiscing elit.
			 </div>
	    </div>
		<div class="column is-4">

			<div class="field  has-addons has-addons-centered">
				  <p class="control">
					<button class="is-info is-outlined is-large button">
						<span class="icon  ">
						  <i class="fa fa-money"></i>
						</span>
					</button>
				 </p>
			</div>
			<p class="title is-5 is-bold has-text-centered"> Attend Club Events</p>
			<div class="content ">
				  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean efficitur.
				  consectetur adipiscing elit. Aenean efficitur. consectetur adipiscing elit.
			 </div>
	    </div>
		<div class="column is-4">

			<div class="field  has-addons has-addons-centered">
				  <p class="control">
					  <button class="is-warning is-outlined is-large button">
						<span class="icon ">
						  <i class="fa fa-line-chart"></i>
						</span>
					</button>
				 </p>
			</div>
			<p class="title is-5 is-bold has-text-centered">Grow with the Club</p>
			<div class="content ">
				  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean efficitur.
				  consectetur adipiscing elit. Aenean efficitur. consectetur adipiscing elit.
			 </div>
	    </div>

    </div>
</section>

<section class="section">
	<div class="container">
      <div class="heading m-b-3 has-text-centered">
        <h3 class="title">WHO WE ARE</h3>
        <h5 class="subtitle">We assist entrepreneurs. We connect investees with Investors.</h5>
      </div>
    </div>
    <div class=" columns">
		<div class="column">
			<article class="content">
			  <p>
				  It is a club that is created to assists entrepreneurs to reach their maximum height leveraging on the knowledge and connection of already established industry experts.
					We connects investees(people with idea) with investors(people that have the money). We are the fastest growing investment club in Nigeria and Africa at large.
			  </p>
			  <p>
				  Our goal is to grow our membership to 1million in 5years globally and at the moment IIclub has been able to establish herself firmly in US and Dubai.
			  </p>
			  <p>
				  Our Vision is to raise billionaire investors with global impact.
			  </p>
			  <p>
				  Our mission is to create a community that fosters investment culture for financial freedom.
			  </p>

		  </article>
			<div class=" columns is-multiline">
			   <div class="column is-4">
					<div class="columns">
						<div class="column is-narrow">
							<div class="v-centered">
							  <div class="">
								  <a class=" is-info is-outlined button is-large">
										<span class="icon">
										  <i class="fa fa-line-chart"></i>
										</span>
									</a>
							  </div>
							</div>
						 </div>
						<div class="column">

						  <h3 class=""><strong>Investment</strong></h3>

						</div>
					</div>

			   </div>
			   <div class="column is-4">
					<div class="columns">
						<div class="column is-narrow">
							<div class="v-centered">
							  <div class="">
								  <a class=" is-info is-outlined button is-large">
										<span class="icon">
										  <i class="fa fa-money"></i>
										</span>
								   </a>
							  </div>
							</div>
						 </div>
						<div class="column">

						  <h3 class=""><strong>Grant Seeking</strong></h3>

						</div>
					</div>

			   </div>
			   <div class="column is-4">
					<div class="columns">
						<div class="column is-narrow">
							<div class="v-centered">
							  <div class="">
								<a class=" is-info is-outlined button is-large">
									<span class="icon">
									  <i class="fa fa-handshake-o"></i>
									</span>
								 </a>
							  </div>
							</div>
						 </div>
						<div class="column">

						  <h3 class=""><strong>Networking</strong></h3>

						</div>
					</div>

			   </div>
			   <div class="column is-4">
					<div class="columns">
						<div class="column is-narrow">
							<div class="v-centered">
							  <div class="">
								<a class=" is-info is-outlined button is-large">
									<span class="icon">
									  <i class="fa fa-sitemap"></i>
									</span>
								 </a>
							  </div>
							</div>
						 </div>
						<div class="column">

						  <h3 class=""><strong>Mass Empowerment</strong></h3>

						</div>
					</div>

			   </div>
			   <div class="column is-4">
					<div class="columns">
						<div class="column is-narrow">
							<div class="v-centered">
							  <div class="">
								<a class=" is-info is-outlined button is-large">
									<span class="icon">
									  <i class="fa fa-graduation-cap"></i>
									</span>
								 </a>
							  </div>
							</div>
						 </div>
						<div class="column">

						  <h3 class=""><strong>Training</strong></h3>

						</div>
					</div>

			   </div>
			   <div class="column is-4">
					<div class="columns">
						<div class="column is-narrow">
							<div class="v-centered">
							  <div class="">
								<a class=" is-info is-outlined button is-large">
									<span class="icon">
									  <i class="fa fa-cogs"></i>
									</span>
								 </a>
							  </div>
							</div>
						 </div>
						<div class="column">

						  <h3 class=""><strong>Thrift</strong></h3>

						</div>
					</div>

			   </div>

		  </div>
	    </div>
	    <div class="column is-4 ">
			<article class="v-centered   message bg-green-yellow ">
				<div class="has-text-centered">
					<p class="title is-2 m-b-5">Get Started</p>
					<p class="title is-5"></p>
					<div class="field  has-addons has-addons-centered">
						  <p class="control">
							  <a href="{{ route('login') }}" class=" button is-info is-medium">
								Login
							  </a>
						 </p>
						 <p class="control">
							  <a href="{{ route('join') }}" class=" button is-warning is-medium">
								Register
							  </a>
						 </p>
					</div>

				</div>
			</article>
	    </div>
	</div>
</section>

<section class="section">
	<div class="container u-m-10">
      <div class="heading m-b-2 has-text-centered">
        <h3 class="title">The Founders</h3>
        <h5 class="subtitle"> Meet the awesome club founders.</h5>
      </div>
    </div>
    <div class=" columns">
		@forelse ($founders as $founder)
		   @include('partials.aboutfounders')
		@empty

		@endforelse

	</div>
</section>
@endsection

