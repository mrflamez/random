

@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<div class="columns is-paddingless is-marginless">
	<div class="column is-narrow">
	    @include('partials.adminmenu')
	</div>
	<div class="column">
		<section class="section p-t-0-5">
             <div class="columns">
				 <div class="column">
					   <p class="title">Story Section</p>
                       <div class="field" id="tab-story-details">
						  <label class="label">Content</label>
						  <p class="control has-icons-right">
							<textarea  name="story_content"  class="textarea is-hovered" placeholder="Description">{!! $setting->story !!}</textarea>
						  </p>
						</div>
						<div class="field ">
						  <p class="control">
							<button class="button is-primary" id="update_story-btn-control">Update</button>
						  </p>
						</div>
				 </div>
             </div>
		</section>
		<section class="section p-t-0-5">
			<p class="title">Founder Section</p>
			<div class="columns">
				 <div class="column">
                       <div class="columns">
						   <div class="column">
                               <table class="table " id="all_founders_dt-table">
								<thead>
									<tr>
										<th></th>
										<th>Name</th>
										<th>Created On</th>
										<th></th>
									</tr>
								</thead>
							</table>
				           </div>
				           <div class="column" id="tab-founder-details">
							   <p class="subtitle">New Founder</p>
							   <div class="columns">
								   <div class="column">
									   <div class="field">
										  <label class="label">Name</label>
										  <p class="control has-icons-left has-icons-right">
											<input class="input is-hovered" name="founder_name" type="text" placeholder="Name">
											<span class="icon is-small is-left">
											  <i class="fa fa-envelope"></i>
											</span>
										  </p>
										</div>
										<div class="field">
										  <label class="label">Facebook</label>
										  <p class="control has-icons-left has-icons-right">
											<input class="input is-hovered" name="founder_facebook" type="text" placeholder="Name">
											<span class="icon is-small is-left">
											  <i class="fa fa-envelope"></i>
											</span>
										  </p>
										</div>
										<div class="field">
										  <label class="label">Twitter</label>
										  <p class="control has-icons-left has-icons-right">
											<input class="input is-hovered" name="founder_twitter" type="text" placeholder="Name">
											<span class="icon is-small is-left">
											  <i class="fa fa-envelope"></i>
											</span>
										  </p>
										</div>
										<div class="field ">
										  <p class="control">
											<button class="button is-primary" id="add_founder-btn-control">Add Founder</button>
										  </p>
										</div>

								   </div>
								   <div class="column">
									   <label class="label m-b-1-5 is-marginless">Avatar</label>
										<p class="help is-danger founder-avartar-label is-hidden">This field is required</p>
										<div  class="columns full-90-h is-marginless">
											<div  class="column">
												<div id="new-founder-avatar"  class=" dropzone">

												</div>
											</div>
										</div>
								   </div>
							   </div>

				           </div>
				       </div>
				 </div>
		    </div>
		</section>
	</div>
</div>
@endsection

@push('header-css')
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css"/>
<link rel="stylesheet"  href="//cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.5/sweetalert2.min.css" >
@endpush

@push('scripts')
<script src="{{ asset('js/dropzone.js') }}"></script>
<script src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.5/sweetalert2.min.js"></script>
<script type="text/javascript">
  Dropzone.autoDiscover = false;
  var regdata = { };

  var myDropzoneFounders = new Dropzone("#new-founder-avatar" , {
    paramName: "file",
    maxFilesize: 1,
    headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	},
	url: "{{ route('photos.store')}}",
	addRemoveLinks : true,
	autoProcessQueue: false,
	uploadMultiple: false,
    parallelUploads: 1,
    success : function(file, response){
        if (response.success) {
			regdata.image_id = response.id;
		}
    }
  });

  $(document).ready(function() {
       $( "input.input, textarea.textarea " ).focus(function() {
			if ($(this).hasClass("is-danger")) {
				$(this).removeClass("is-danger").next().remove();
				$(this).parent("p.control").nextAll("p").remove();
			}
		});

		var dtFoundersTable = $('#all_founders_dt-table').DataTable({
			processing: true,
			serverSide: true,
			lengthChange: false,
			pageLength: 5,
			ajax: "{{ route('datatables.founders') }}",
			columns: [
				{ data: 'founderimage', name: 'founderimage', orderable: false, searchable: false},
				{ data: 'name'},
				{ data: 'created_at'},
				{data: 'action', name: 'action', orderable: false, searchable: false}
			]
		});

        myDropzoneFounders.on("sending", function(file, xhr, formData) {
		  formData.append("action", "founders");
	    });

	    myDropzoneFounders.on("queuecomplete", function() {
		   console.log(regdata);
		   sendFormFounder();
		});

		function sendFormFounder() {

			 $.ajax({
			  headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			  },
			  method: 'POST',
			  url: "{{ route('founders.store')}}",
			  dataType: 'JSON',
			  data: regdata
			}).done(function(d) {
				console.log(d);
				if (d.success){
                     //~ $("#tab-founders-details" ).addClass( "is-hidden" );
                     //~ $("#tab-founders-sucess-notification" ).removeClass( "is-hidden" );
                     swal(
						'Success!',
						'New founder Added  !',
						'success'
					  );
					  $("#tab-founder-details input[name='founder_name']").val('');
					  $("#tab-founder-details input[name='founder_twitter']").val('');
					  $("#tab-founder-details input[name='founder_facebook']").val('');
					  myDropzoneFounders.removeAllFiles();
					  dtFoundersTable.draw();
				} else {
					$.each(d.errors, function( key, value ) {
						 if (key === 'name') {
							$("#tab-founder-details input[name='founder_name']").toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>" + value[0] + "</p>" );
						 }
						 if (key === 'twitter') {
							$("#tab-founder-details input[name='founder_twitter']").toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>" + value[0] + "</p>" );
						 }
						 if (key === 'facebook') {
							$("#tab-founder-details input[name='founder_facebook']").toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>" + value[0] + "</p>" );
						}
                  });
				}

			  });
		 }

		function UpdateSettings(formdata) {

			 $.ajax({
			  headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			  },
			  method: 'PUT',
			 url: "{{ route('settings.update', ['id' => 1])}}",
			  dataType: 'JSON',
			  data: formdata
			}).done(function(d) {
				console.log(d);
				if (d.success){
					 swal(
						'Success!',
						'Story Updated  !',
						'success'
					  );
				} else {

				}

			  });
		 }




		dtFoundersTable.on('click', 'button.founder-del', function (e) {
			e.preventDefault();
			var rwId = $(this).val();
			console.log(rwId);
			$.ajax({
			  headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			  },
			  method: 'POST',
			  url: "{{ route('datatables.deletefounder')}}",
			  dataType: 'JSON',
			  data: {'id':rwId},
			}).done(function(d) {
				console.log(d);
				if (d.success){
					dtFoundersTable.row( $(this).parents('tr') ).remove().draw();
					console.log('deleted');
				}
			});

		});

        function validateDetails() {

			var name = $("#tab-founder-details input[name='founder_name']");
			var twitter = $("#tab-founder-details input[name='founder_twitter']");
			var facebook = $("#tab-founder-details input[name='founder_facebook']");

			if ((! name.val()) && (! name.hasClass("is-danger"))) {
				name.toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>This field is required</p>" );
			}
			if ((! facebook.val()) && (! facebook.hasClass("is-danger"))) {
				facebook.toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>This field is required</p>" );
			}
			if ((! twitter.val()) && (! twitter.hasClass("is-danger"))) {
				twitter.toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>This field is required</p>" );
			}

			var avatar = myDropzoneFounders.getQueuedFiles();

			if (!(avatar.length > 0)) {
				$( ".founder-avartar-label" ).toggleClass( "is-hidden" );
			}

			if ((name.val()) && (facebook.val())  && (twitter.val()) && (avatar.length > 0)) {
				regdata.name = name.val();
				regdata.twitter = twitter.val();
				regdata.facebook = facebook.val();

				return true ;
			} else {
				return false ;
			}

		 }

		$('#add_founder-btn-control').click(function(e){
            e.preventDefault();
			$(this).prop("disabled", true);
			var r = validateDetails();

			if (r) {
				myDropzoneFounders.processQueue();
			}

			$(this).prop("disabled", false);
			return;
		});

		$('#update_story-btn-control').click(function(e){
            e.preventDefault();
			$(this).prop("disabled", true);
			var d = {action : "story" , story_content : $("#tab-story-details textarea[name='story_content']").val() } ;
			UpdateSettings(d);

			$(this).prop("disabled", false);
			return;
		});

  });

</script>
@endpush





