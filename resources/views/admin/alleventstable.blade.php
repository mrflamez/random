@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<div class="columns is-paddingless is-marginless">
	<div class="column is-narrow">
	    @include('partials.adminmenu')
	</div>
	<div class="column">
			@include('partials.table-chart-header',
					[

					't_active' => 'is-active' ,
					'c_active' => '' ,
					't_route' => 'administrator.events.all',
					'c_route' => 'administrator.events.chart'])

			<section class="section p-t-0-5">

					<nav class="level is-mobile">
					  <div class="level-item has-text-centered">
						<div>
						  <p class="heading">All Events</p>
						  <p class="title">{{ $all_events }}</p>
						</div>
					  </div>
					  <div class="level-item has-text-centered">
						<div>
						  <p class="heading">Active Events</p>
						  <p class="title">{{ $active_events }}</p>
						</div>
					  </div>
					  <div class="level-item has-text-centered">
						<div>
						  <p class="heading">Pending Events</p>
						  <p class="title">{{ $inactive_events }}</p>
						</div>
					  </div>
					  <div class="level-item has-text-centered">
						<div>
						  <p class="heading">Deleted Events</p>
						  <p class="title">{{ $deleted_events }}</p>
						</div>
					  </div>
					</nav>

			</section>

			<section class="section p-t-0-5">
				  <div class="columns">
					  <div class="column">
							<table class="table " id="all_events_dt-table">
								<thead>
									<tr>
										<th></th>
										<th>User</th>
										<th>Title</th>
										<th>Fee</th>
										<th>Venue</th>
										<th>Start date</th>
										<th>End date</th>
										<th></th>
									</tr>
								</thead>
							</table>
					  </div>
				  </div>
			</section>

	</div>
</div>
@endsection

@push('header-css')
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css"/>
@endpush

@push('scripts')
<script src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
	$(function() {
		$('#all_events_dt-table').DataTable({
			processing: true,
			serverSide: true,
			lengthChange: false,
			pageLength: 5,
			ajax: "{{ route('datatables.allevents') }}",
			columns: [
				{ data: 'rownum', name: 'rownum', orderable: false, searchable: false},
				{ data: 'user.f_name', name: 'user.f_name'},
				{ data: 'title'},
				{ data: 'fee'},
				{ data: 'venue'},
				{ data: 'start_date'},
				{ data: 'end_date'},
				{data: 'action', name: 'action', orderable: false, searchable: false}
			]
		});
	});
</script>
@endpush







