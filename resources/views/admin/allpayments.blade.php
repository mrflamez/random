@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<div class="columns is-paddingless is-marginless">
	<div class="column is-narrow">
	    @include('partials.adminmenu')
	</div>
	<div class="column">

			<section class="section p-t-0-5">

					<nav class="level is-mobile">
					  <div class="level-item has-text-centered">
						<div>
						  <p class="heading">All Transaction</p>
						  <p class="title">{{ $all_payments }}</p>
						</div>
					  </div>
					  <div class="level-item has-text-centered">
						<div>
						  <p class="heading">Month Transactions</p>
						  <p class="title">{{ $month_transactions }}</p>
						</div>
					  </div>
					  <div class="level-item has-text-centered">
						<div>
						  <p class="heading">Month Total </p>
						  <p class="title">{{ $month_payments/100 }}</p>
						</div>
					  </div>
					  <div class="level-item has-text-centered">
						<div>
						  <p class="heading">Pending Transaction</p>
						  <p class="title">{{ $inactive_payments }}</p>
						</div>
					  </div>
					</nav>

			</section>

			<section class="section p-t-0-5">
				  <div class="columns">
					  <div class="column">
							<table class="table " id="all_payments_dt-table">
								<thead>
									<tr>
										<th></th>
										<th>Payee</th>
										<th>Currency</th>
										<th>Tranx Id</th>
										<th>Amount</th>
										<th>Fee</th>
										<th>Status</th>
										<th>Tranx Date</th>
										<th>Tranx Type</th>
										<th></th>
									</tr>
								</thead>
							</table>
					  </div>
				  </div>
			</section>

	</div>
</div>
@endsection

@push('header-css')
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css"/>
@endpush

@push('scripts')
<script src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
	$(function() {
		$('#all_payments_dt-table').DataTable({
			processing: true,
			serverSide: true,
			lengthChange: false,
			pageLength: 5,
			ajax: "{{ route('datatables.allpaymentsreceived') }}",
			columns: [
				{ data: 'rownum', name: 'rownum', orderable: false, searchable: false},
				{ data: 'donator.first_name', name: 'donator.first_name'},
				{ data: 'currency'},
				{ data: 'tranx_id'},
				{ data: 'amount'},
				{ data: 'fees'},
				{ data: 'status_btn', name: 'status_btn', orderable: false, searchable: false},
				{ data: 'transaction_date'},
				{ data: 'transaction_type',  orderable: false, searchable: false},
				{ data: 'action', name: 'action', orderable: false, searchable: false}
			]
		});
	});
</script>
@endpush









