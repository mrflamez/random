@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<div class="columns is-paddingless is-marginless">
	<div class="column is-narrow">
	    @include('partials.adminmenu')
	</div>
	<div class="column">

			<section class="section p-t-0-5">
				  <div class="columns">
					  <div class="column">
							<table class="table " id="all_roles_dt-table">
								<thead>
									<tr>
										<th></th>
										<th>Name</th>
										<th>User Count</th>
										<th></th>
									</tr>
								</thead>
							</table>
					  </div>
				  </div>
			</section>

	</div>
</div>
@endsection

@push('header-css')
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css"/>
@endpush

@push('scripts')
<script src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
	$(function() {
		$('#all_roles_dt-table').DataTable({
			processing: true,
			serverSide: true,
			lengthChange: false,
			pageLength: 5,
			ajax: "{{ route('datatables.allpermisions') }}",
			columns: [
				{ data: 'id', name: 'id', orderable: false, searchable: false},
				{ data: 'slug'},
				{ data: 'counts'},
				{data: 'action', name: 'action', orderable: false, searchable: false}
			]
		});
	});
</script>
@endpush





