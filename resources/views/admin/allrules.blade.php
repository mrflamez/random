@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<div class="columns is-paddingless is-marginless">
	<div class="column is-narrow">
	    @include('partials.adminmenu')
	</div>
	<div class="column">

			<section class="section p-t-0-5">
				  <div class="columns">
					  <div class="column">
							<table class="table " id="all_rules_dt-table">
								<thead>
									<tr>
										<th></th>
										<th>Title</th>
										<th>Created On</th>
										<th></th>
									</tr>
								</thead>
							</table>
					  </div>
				  </div>
			</section>

	</div>
</div>
@endsection

@push('header-css')
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css"/>
@endpush

@push('scripts')
<script src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
	$(function() {
		$('#all_rules_dt-table').DataTable({
			processing: true,
			serverSide: true,
			lengthChange: false,
			pageLength: 5,
			ajax: "{{ route('datatables.allrules') }}",
			columns: [
				{ data: 'rownum', name: 'rownum', orderable: false, searchable: false},
				{ data: 'title'},
				{ data: 'created_at'},
				{ data: 'action', name: 'action', orderable: false, searchable: false}
			]
		});
	});
</script>
@endpush











