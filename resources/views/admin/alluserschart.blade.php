


@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<div class="columns is-paddingless is-marginless">
	<div class="column is-narrow">
	    @include('partials.adminmenu')
	</div>
	<div class="column">
			@include('partials.table-chart-header',
					[

					't_active' => '' ,
					'c_active' => 'is-active' ,
					't_route' => 'administrator.users.table',
					'c_route' => 'administrator.users.chart'])

			<section class="section p-t-0-5">

					<nav class="level is-mobile">
					  <div class="level-item has-text-centered">
						<div>
						  <p class="heading">Admins</p>
						  <p class="title">{{ $admin_users }}</p>
						</div>
					  </div>
					  <div class="level-item has-text-centered">
						<div>
						  <p class="heading">Active Users</p>
						  <p class="title">{{ $active_users }}</p>
						</div>
					  </div>
					  <div class="level-item has-text-centered">
						<div>
						  <p class="heading">Pending Users</p>
						  <p class="title">{{ $inactive_users }}</p>
						</div>
					  </div>
					  <div class="level-item has-text-centered">
						<div>
						  <p class="heading">Deleted Users</p>
						  <p class="title">{{ $deleted_users }}</p>
						</div>
					  </div>
					</nav>

			</section>

			<section class="section p-t-0-5">
				  <div class="columns">
					  <div class="column">
							<p class="title is-5">
							   Registrations Vs Months
						   </p>
							<div class="block m-b-0 ">
								<canvas id="registrations-chart"></canvas>
							</div>
					  </div>
				  </div>
			</section>

	</div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('js/Chart.js') }}"></script>
<script type="text/javascript">
	var ctxReg = document.getElementById("registrations-chart").getContext("2d");

	var chartColors = {
		red: 'rgb(255, 99, 132)',
		orange: 'rgb(255, 159, 64)',
		yellow: 'rgb(255, 205, 86)',
		green: 'rgb(75, 192, 192)',
		blue: 'rgb(54, 162, 235)',
		purple: 'rgb(153, 102, 255)',
		grey: 'rgb(201, 203, 207)'
	};
	var color = Chart.helpers.color;
	var configLine = {
            type: 'line',
            data: {
                labels: [],
                datasets: []
            },
            options: {
                responsive: true,
                title:{
                    display:false,
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Month'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Views'
                        }
                    }]
                }
            }
    };

    var configBar = {
		type: 'bar',
		data: {
			labels: [],
			datasets: []
		},
		options: {
				responsive: true,
				legend: {
					position: 'top',
				},
				title: {
					display: false,
				}
		 }
    };

	$(function() {
		$.ajax({
		  headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		  },
		  method: 'POST',
		  url: "{{ route('chartdata.usersvsmonth')}}",
		  dataType: 'JSON',
		}).done(function(d) {
			console.log(d);
			if (d.success){
				var chLbls = [];
				var chDt = [];
				$.each(d.data, function( key, value ) {
					chLbls.push(key)
					chDt.push(value)
				});
				configBar.data.labels = chLbls ;
				configBar.data.datasets = [{
                    label: "Users Registered",
                    backgroundColor: color(chartColors.green).alpha(0.5).rgbString(),
                    borderColor: chartColors.green,
                    data: chDt,
                    //~ fill: false,
                    borderWidth: 1,
                }] ;
                window.usersVsmonth = new Chart(ctxReg, configBar);
			}

		});

	});
</script>
@endpush





