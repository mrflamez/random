


@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<div class="columns is-paddingless is-marginless">
	<div class="column is-narrow">
	    @include('partials.adminmenu')
	</div>
	<div class="column">
			@include('partials.table-chart-header',
					[

					't_active' => 'is-active' ,
					'c_active' => '' ,
					't_route' => 'administrator.users.table',
					'c_route' => 'administrator.users.chart'])

			<section class="section p-t-0-5">

					<nav class="level is-mobile">
					  <div class="level-item has-text-centered">
						<div>
						  <p class="heading">Admins</p>
						  <p class="title">{{ $admin_users }}</p>
						</div>
					  </div>
					  <div class="level-item has-text-centered">
						<div>
						  <p class="heading">Active Users</p>
						  <p class="title">{{ $active_users }}</p>
						</div>
					  </div>
					  <div class="level-item has-text-centered">
						<div>
						  <p class="heading">Pending Users</p>
						  <p class="title">{{ $inactive_users }}</p>
						</div>
					  </div>
					  <div class="level-item has-text-centered">
						<div>
						  <p class="heading">Deleted Users</p>
						  <p class="title">{{ $deleted_users }}</p>
						</div>
					  </div>
					</nav>

			</section>

			<section class="section p-t-0-5">
				  <div class="columns">
					  <div class="column">
							<table class="table " id="all_users_dt-table">
								<thead>
									<tr>
										<th></th>
										<th>Member Id</th>
										<th>First Name</th>
										<th>Last Name</th>
										<th>Email</th>
										<th>Registered On</th>
										<th>Status</th>
										<th></th>
									</tr>
								</thead>
							</table>
					  </div>
				  </div>
			</section>

	</div>
</div>
@endsection

@push('header-css')
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css"/>
@endpush

@push('scripts')
<script src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
	$(function() {
		$('#all_users_dt-table').DataTable({
			processing: true,
			serverSide: true,
			lengthChange: false,
			pageLength: 5,
			ajax: "{{ route('datatables.allusers') }}",
			columns: [
				{ data: 'rownum', name: 'rownum', orderable: false, searchable: false},
				{ data: 'member_id'},
				{ data: 'f_name'},
				{ data: 'l_name'},
				{ data: 'email'},
				{ data: 'created_at'},
				{ data: 'status_btn'},
				{data: 'action', name: 'action', orderable: false, searchable: false}
			]
		});
	});
</script>
@endpush




