


@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<div class="columns is-paddingless is-marginless">
	<div class="column is-narrow">
	    @include('partials.adminmenu')
	</div>
	<div class="column">
		<section class="section p-t-0-5">
             <div class="columns">
				 <div class="column">
					   <p class="title">General Section</p>
					   <div class="columns">
				          <div class="column is-4">
							  <div class="field" id="tab-fee-details">
								  <label class="label">Membership Fee</label>
								  <p class="control has-icons-right">
									<input  value="{{ $setting->fee }}" name="fee" type="number"  class="input is-hovered" placeholder="Fee">
								  </p>
								</div>
								<div class="field ">
								  <p class="control">
									<button class="button is-primary" id="update_fee-btn-control">Update</button>
								  </p>
								</div>
						  </div>
					   </div>

				 </div>
             </div>
		</section>
	</div>
</div>
@endsection

@push('header-css')
<link rel="stylesheet"  href="//cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.5/sweetalert2.min.css" >
@endpush

@push('scripts')
<script src="//cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.5/sweetalert2.min.js"></script>
<script type="text/javascript">
  var regdata = { };

  $(document).ready(function() {
	  $( "input.input, textarea.textarea " ).focus(function() {
			if ($(this).hasClass("is-danger")) {
				$(this).removeClass("is-danger").next().remove();
				$(this).parent("p.control").nextAll("p").remove();
			}
		});

		function UpdateSettings() {

			 $.ajax({
			  headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			  },
			  method: 'PUT',
			 url: "{{ route('settings.update', ['id' => 1])}}",
			  dataType: 'JSON',
			  data: regdata
			}).done(function(d) {
				console.log(d);
				if (d.success){
					 swal(
						'Success!',
						'Story Updated  !',
						'success'
					  );
				} else {

				}

			  });
		 }


        function validateDetails() {

			var fee = $("#tab-fee-details input[name='fee']");
            if ((! fee.val()) && (! fee.hasClass("is-danger"))) {
				fee.toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>This field is required</p>" );
			}
			if (fee.val()) {
				regdata.fee = fee.val();
				regdata.action = "fee";

				return true ;
			} else {
				return false ;
			}

		 }

		$('#update_fee-btn-control').click(function(e){
            e.preventDefault();
			$(this).prop("disabled", true);
			var r = validateDetails();

			if (r) {
				UpdateSettings();
			}

			$(this).prop("disabled", false);
			return;
		});


  });

</script>
@endpush





