

@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<div class="columns is-paddingless is-marginless">
	<div class="column is-narrow">
	    @include('partials.adminmenu')
	</div>
	<div class="column">
		<section class="section p-t-0-5">

             <div class="columns">
				 <div class="column">
					   <p class="title">Slider Section</p>
                       <div class="columns">
						   <div class="column">
                              <table class="table " id="all_slidders_dt-table">
								<thead>
									<tr>
										<th>Image</th>
										<th>Active</th>
										<th></th>
									</tr>
									</thead>
								</table>
				           </div>
				           <div class="column">
							   <p class="subtitle">Add New Slider Image</p>
							   <div class="columns">
									<div class="column">
										<div class="field">
										  <label class="label">Title</label>
										  <p class="control  has-icons-right">
											<input class="input is-hovered" name="slider_title" type="text" placeholder="Name">

										  </p>
										</div>
										<div class="field">
										  <label class="label">Subtitle</label>
										  <p class="control  has-icons-right">
											<input class="input is-hovered" name="slider_subtitle" type="text" placeholder="Name">

										  </p>
										</div>
										<div class="field">
										  <label class="label">Content</label>
										  <p class="control has-icons-right">
											<textarea  name="slider_content"  class="textarea is-hovered" placeholder="Description"></textarea>
										  </p>
										</div>
										<div class="field ">
										  <p class="control">
											<button class="button is-primary" id="add_new_slider-btn-control">Add Slider Image</button>
										  </p>
										</div>
									</div>
									<div class="column">
										<label class="label m-b-1-5 is-marginless">Slider Image</label>
										<p class="help is-danger user-avartar-label is-hidden">This field is required</p>
										<div  class="columns h-250 is-marginless">
											<div  class="column">
												<div id="new-slider"  class=" dropzone">

												</div>
											</div>
										</div>
									</div>
								</div>
				           </div>
				       </div>
				 </div>
             </div>
                 <hr>
				 <div class="columns">
				   <div class="column">
					   <p class="title">Sponser Section</p>
					   <table class="table " id="all_sponsors_dt-table">
						<thead>
							<tr>
								<th>Image</th>
								<th>Active</th>
								<th></th>
							</tr>
							</thead>
						</table>

		           </div>
		           <div class="column">
						<label class="label m-b-1-5 is-marginless">Add New Sponser Image</label>
						<p class="help is-danger user-avartar-label is-hidden">This field is required</p>
						<div  class="columns h-250 is-marginless">
							<div  class="column">
								<div id="new-sponser"  class=" dropzone">

								</div>
							</div>
						</div>
						<div class="field ">
						  <p class="control">
							<button class="button is-primary" id="add_new_sponser-btn-control">Add Sponser Image</button>
						  </p>
						</div>
		           </div>
		       </div>

		</section>
	    <section class="section p-t-0-5">
			<div class="columns">
				 <div class="column">
					   <p class="title">Testimonials Section</p>
                       <div class="columns">
						   <div class="column">
                               <table class="table " id="all_testimony_dt-table">
								<thead>
									<tr>
										<th></th>
										<th>Name</th>
										<th>Alias</th>
										<th>Content</th>
										<th>Created On</th>
										<th></th>
									</tr>
								</thead>
							</table>
				           </div>
				           <div class="column" id="tab-testimony-details">
							   <p class="subtitle">New Testimonial</p>
							   <div class="columns">
								   <div class="column">
									   <div class="field">
										  <label class="label">Name</label>
										  <p class="control has-icons-left has-icons-right">
											<input class="input is-hovered" name="testimony_name" type="text" placeholder="Name">
											<span class="icon is-small is-left">
											  <i class="fa fa-envelope"></i>
											</span>
										  </p>
										</div>
										<div class="field">
										  <label class="label">Alias</label>
										  <p class="control has-icons-left has-icons-right">
											<input class="input is-hovered" name="testimony_alias" type="text" placeholder="@alias">
											<span class="icon is-small is-left">
											  <i class="fa fa-envelope"></i>
											</span>
										  </p>
										</div>
										<div class="field">
										  <label class="label">Content</label>
										  <p class="control has-icons-right">
											<textarea  name="testimony_content"  class="textarea is-hovered" placeholder="Description"></textarea>
										  </p>
										</div>
										<div class="field ">
										  <p class="control">
											<button class="button is-primary" id="add_testimony-btn-control">Add Testimony</button>
										  </p>
										</div>

								   </div>
								   <div class="column">
									   <label class="label m-b-1-5 is-marginless">Avatar</label>
										<p class="help is-danger testimonial-avartar-label is-hidden">This field is required</p>
										<div  class="columns full-90-h is-marginless">
											<div  class="column">
												<div id="new-testimonial-avatar"  class=" dropzone">

												</div>
											</div>
										</div>
								   </div>
							   </div>

				           </div>
				          <div class=" column is-hidden" id="tab-testimony-sucess-notification">
							<article class="v-centered">
								<div class="has-text-centered">
									<div class="field  has-addons has-addons-centered">
										  <p class="control">
											  <span class="icon is-large">
												  <i class="fa fa-check-circle-o"></i>
												</span>
										 </p>
									</div>
									<p class="title is-4">Testimony Added</p>
									<p class="title is-6">
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean efficitur.
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean efficitur.

									</p>
								</div>
							</article>
						 </div>
				       </div>
				 </div>
		    </div>
		</section>
		<section class="section p-t-0-5">
			<div class="columns">
				 <div class="column">
					 <p class="title">Stats Section</p>
					     <div class="field">
						  <label class="label">investors</label>
						  <p class="control has-icons-right">
							<input value="{{ $setting->investors }}" class="input is-hovered" name="investors_name" type="text" >

						  </p>
						</div>
						<div class="field">
						  <label class="label">partners</label>
						  <p class="control  has-icons-right">
							<input value="{{ $setting->partners }}" class="input is-hovered" name="partners_name" type="text" >

						  </p>
						</div>
						<div class="field">
						  <label class="label">satifaction</label>
						  <p class="control  has-icons-right">
							<input value="{{ $setting->satifaction }}" class="input is-hovered" name="satifaction_name" type="text" >

						  </p>
						</div>
						<div class="field">
						  <label class="label">bonus</label>
						  <p class="control  has-icons-right">
							<input value="{{ $setting->bonus }}" class="input is-hovered" name="bonus_name" type="text" >

						  </p>
						</div>
						<div class="field ">
						  <p class="control">
							<button class="button is-primary" id="add_stats_settings-btn-control">Update Links</button>
						  </p>
						</div>

				 </div>
				 <div class="column">
					 <p class="title">Social Links</p>
						 <div class="field">
						  <label class="label">Facebook</label>
						  <p class="control has-icons-left has-icons-right">
							<input value="{{ $setting->facebook }}" class="input is-hovered" name="facebook_name" type="text" >
							<span class="icon is-small is-left">
							  <i class="fa fa-facebook"></i>
							</span>
						  </p>
						</div>
						<div class="field">
						  <label class="label">Twitter</label>
						  <p class="control has-icons-left has-icons-right">
							<input value="{{ $setting->twitter }}" class="input is-hovered" name="twitter_name" type="text" >
							<span class="icon is-small is-left">
							  <i class="fa fa-twitter"></i>
							</span>
						  </p>
						</div>
						<div class="field">
						  <label class="label">Instagram</label>
						  <p class="control has-icons-left has-icons-right">
							<input value="{{ $setting->instagram }}" class="input is-hovered" name="instagram_name" type="text" >
							<span class="icon is-small is-left">
							  <i class="fa fa-instagram"></i>
							</span>
						  </p>
						</div>
						<div class="field ">
						  <p class="control">
							<button class="button is-primary" id="add_social-btn-control">Update Links</button>
						  </p>
						</div>
				 </div>
		    </div>
		</section>
	</div>
</div>
@endsection

@push('header-css')
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css"/>
<link rel="stylesheet"  href="//cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.5/sweetalert2.min.css" >
@endpush

@push('scripts')
<script src="{{ asset('js/dropzone.js') }}"></script>
<script src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.5/sweetalert2.min.js"></script>
<script type="text/javascript">
  Dropzone.autoDiscover = false;
  var regdata = { };

  var myDropzoneSlider = new Dropzone("#new-slider" , {
    paramName: "file",
    maxFilesize: 1,
    headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	},
	url: "{{ route('photos.store')}}",
	addRemoveLinks : true,
	autoProcessQueue: false,
	uploadMultiple: false,
    parallelUploads: 1,
    success : function(file, response){
        if (response.success) {
	     swal( 'Success!',
				'Slider Added  !',
				'success'
			  );
		}
    }
  });
  var myDropzoneSponser = new Dropzone("#new-sponser" , {
    paramName: "file",
    maxFilesize: 1,
    headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	},
	url: "{{ route('photos.store')}}",
	addRemoveLinks : true,
	autoProcessQueue: false,
	uploadMultiple: false,
    parallelUploads: 1
  });
  var myDropzonetestimonial = new Dropzone("#new-testimonial-avatar" , {
    paramName: "file",
    maxFilesize: 1,
    headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	},
	url: "{{ route('photos.store')}}",
	addRemoveLinks : true,
	autoProcessQueue: false,
	uploadMultiple: false,
    parallelUploads: 1 ,
    success : function(file, response){
        console.log(response);
        regdata.image_id = response.id;
    }
  });



  $(document).ready(function() {

	  myDropzonetestimonial.on("sending", function(file, xhr, formData) {
		  formData.append("action", "testimonial");
	  });

	  myDropzoneSlider.on("sending", function(file, xhr, formData) {
		  formData.append("title", $("input[name='slider_title']").val() );
		  formData.append("subtitle", $("input[name='slider_subtitle']").val() );
		  formData.append("content", $("textarea[name='slider_content']").val() );
		  formData.append("action", "slider");
	  });

	  myDropzoneSponser.on("sending", function(file, xhr, formData) {
		  formData.append("action", "sponser");
	  });

	  myDropzonetestimonial.on("queuecomplete", function() {
		   console.log(regdata);
		   sendFormTestimony();
		});

		$( "input.input, textarea.textarea " ).focus(function() {
			if ($(this).hasClass("is-danger")) {
				$(this).removeClass("is-danger").next().remove();
				$(this).parent("p.control").nextAll("p").remove();
			}
		});

		function sendFormTestimony() {
			 // console.log(regdata);
			 $.ajax({
			  headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			  },
			  method: 'POST',
			  url: "{{ route('testimony.store')}}",
			  dataType: 'JSON',
			  data: regdata
			}).done(function(d) {
				console.log(d);
				if (d.success){
                     $("#tab-testimony-details" ).addClass( "is-hidden" );
                     $("#tab-testimony-sucess-notification" ).removeClass( "is-hidden" );
				} else {
					$.each(d.errors, function( key, value ) {
						 if (key === 'name') {
							$("#tab-testimony-details input[name='testimony_name']").toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>" + value[0] + "</p>" );
						 }
						 if (key === 'alias') {
							$("#tab-testimony-details input[name='testimony_alias']").toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>" + value[0] + "</p>" );
						 }
						 if (key === 'content') {
							$("#tab-testimony-details textarea[name='testimony_content']").toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>" + value[0] + "</p>" );
						}
                  });
				}

			  });

		}

		function validateDetails() {

			var name = $("#tab-testimony-details input[name='testimony_name']");
			var alias = $("#tab-testimony-details input[name='testimony_alias']");
			var content = $("#tab-testimony-details textarea[name='testimony_content']");

			if ((! name.val()) && (! name.hasClass("is-danger"))) {
				name.toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>This field is required</p>" );
			}
			if ((! alias.val()) && (! alias.hasClass("is-danger"))) {
				alias.toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>This field is required</p>" );
			}
			if ((! content.val()) && (! content.hasClass("is-danger"))) {
				content.toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>This field is required</p>" );
			}

			var avatar = myDropzonetestimonial.getQueuedFiles();

			if (!(avatar.length > 0)) {
				$( ".testimonial-avartar-label" ).toggleClass( "is-hidden" );
			}

			if ((name.val()) && (alias.val())  && (content.val()) && (avatar.length > 0)) {
				regdata.name = name.val();
				regdata.alias = alias.val();
				regdata.content = content.val();

				return true ;
			} else {
				return false ;
			}

		 }

        function UpdateSocialLinks() {
			 $.ajax({
			  headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			  },
			  method: 'PUT',
			  url: "{{ route('settings.update', ['id' => 1])}}",
			  dataType: 'JSON',
			  data: regdata
			}).done(function(d) {
				console.log(d);
				if (d.success){
                   swal(
						'Success!',
						'Settings Updated  !',
						'success'
					  );
				} else {

				}

			  });
		 }



		$('#add_testimony-btn-control').click(function(e){
			e.preventDefault();
			$(this).prop("disabled", true);
			var r = validateDetails();

			if (r) {
				myDropzonetestimonial.processQueue();
			}

			$(this).prop("disabled", false);
			return;

		});



		var dtTestmonialTable = $('#all_testimony_dt-table').DataTable({
			processing: true,
			serverSide: true,
			lengthChange: false,
			pageLength: 5,
			ajax: "{{ route('datatables.testmonials') }}",
			columns: [
				{ data: 'rownum', name: 'rownum', orderable: false, searchable: false},
				{ data: 'name'},
				{ data: 'alias'},
				{ data: 'content'},
				{ data: 'created_at'},
				{data: 'action', name: 'action', orderable: false, searchable: false}
			]
		});

		dtTestmonialTable.on('click', 'button.testmonial-del', function (e) {
			e.preventDefault();
			var rwId = $(this).val();
			console.log(rwId);
			$.ajax({
			  headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			  },
			  method: 'POST',
			  url: "{{ route('datatables.deletetestmonial')}}",
			  dataType: 'JSON',
			  data: {'id':rwId},
			}).done(function(d) {
				console.log(d);
				if (d.success){
					dtTestmonialTable.row( $(this).parents('tr') ).remove().draw();
					console.log('deleted');
				}
			});

		});

		var dtSliderTable = $('#all_slidders_dt-table').DataTable({
			processing: true,
			serverSide: true,
			lengthChange: false,
			pageLength: 5,
			ajax: "{{ route('datatables.settingslider') }}",
			columns: [
				{ data: 'image'},
				{ data: 'featured'},
				{data: 'action', name: 'action', orderable: false, searchable: false}
			]
		});

		dtSliderTable.on('click', 'button.slider-del', function (e) {
			e.preventDefault();
			var sliderRwId = $(this).val();
			console.log(sliderRwId);
			$.ajax({
			  headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			  },
			  method: 'POST',
			  url: "{{ route('datatables.deleteslider')}}",
			  dataType: 'JSON',
			  data: {'id':sliderRwId},
			}).done(function(d) {
				console.log(d);
				if (d.success){
					dtSliderTable.row( $(this).parents('tr') ).remove().draw();
					console.log('deleted');
				}
			});

		});

		var dtSponsorsTable = $('#all_sponsors_dt-table').DataTable({
			processing: true,
			serverSide: true,
			lengthChange: false,
			pageLength: 5,
			ajax: "{{ route('datatables.sponsors') }}",
			columns: [
				{ data: 'image'},
				{ data: 'featured'},
				{data: 'action', name: 'action', orderable: false, searchable: false}
			]
		});

		dtSponsorsTable.on('click', 'button.sponsors-del', function (e) {
			e.preventDefault();
			var rwId = $(this).val();
			console.log(rwId);
			$.ajax({
			  headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			  },
			  method: 'POST',
			  url: "{{ route('datatables.deletesponsor')}}",
			  dataType: 'JSON',
			  data: {'id':rwId},
			}).done(function(d) {
				console.log(d);
				if (d.success){
					console.log('deleted');
					dtSponsorsTable.row( $(this).parents('tr') ).remove().draw();
				}
			});

		});

		$('#add_new_slider-btn-control').click(function(e){
			e.preventDefault();
			$(this).prop("disabled", true);
			var slideravatar = myDropzoneSlider.getQueuedFiles();

			if (!(slideravatar.length > 0)) {
				$( ".testimonial-avartar-label" ).toggleClass( "is-hidden" );
			} else {
				myDropzoneSlider.processQueue();
			}
			$(this).prop("disabled", false);
			return;

		});

		$('#add_new_sponser-btn-control').click(function(e){
			e.preventDefault();
			$(this).prop("disabled", true);
            myDropzoneSponser.processQueue();
			$(this).prop("disabled", false);
			return;

		});

		$('#add_social-btn-control').click(function(e){
			e.preventDefault();
			$(this).prop("disabled", true);
			regdata.facebook = $("input[name='facebook_name']").val();
			regdata.twitter = $("input[name='twitter_name']").val();
			regdata.instagram = $("input[name='instagram_name']").val();
			regdata.action = "social";
			UpdateSocialLinks();
			$(this).prop("disabled", false);
			return;

		});
		$('#add_stats_settings-btn-control').click(function(e){
			e.preventDefault();
			$(this).prop("disabled", true);
			regdata.investors = $("input[name='investors_name']").val();
			regdata.partners = $("input[name='partners_name']").val();
			regdata.satifaction = $("input[name='satifaction_name']").val();
			regdata.bonus = $("input[name='bonus_name']").val();
			regdata.action = "stats";
			UpdateSocialLinks();
			$(this).prop("disabled", false);
			return;

		});
  });

</script>
@endpush




