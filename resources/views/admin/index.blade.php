

@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<div class="columns is-paddingless is-marginless">
	<div class="column is-narrow">
	    @include('partials.adminmenu')
	</div>
	<div class="column">
			<section class="section p-t-0-5">

					<nav class="level is-mobile">
					  <div class="level-item has-text-centered">
						<div>
						  <p class="heading">Users</p>
						  <p class="title">{{ $users }}</p>
						</div>
					  </div>
					  <div class="level-item has-text-centered">
						<div>
						  <p class="heading">Projects</p>
						  <p class="title">{{ $projects }}</p>
						</div>
					  </div>
					  <div class="level-item has-text-centered">
						<div>
						  <p class="heading">Events</p>
						  <p class="title">{{ $events }}</p>
						</div>
					  </div>
					  <div class="level-item has-text-centered">
						<div>
						  <p class="heading">Posts</p>
						  <p class="title">{{ $posts }}</p>
						</div>
					  </div>
					</nav>

			</section>

<!--
			<section class="section p-t-0-5">
					<div class="columns">
						<div class="column">
						  <div class="box">
							 <p class="title is-6 m-b-0-5 has-text-centered">
							   New Users
							 </p>
							<article class="v-centered-block">
								<div class="progressbars_cont progressbars_1"></div>
							</article>
							<div class="columns is-mobile">
								<div class="column  is-8 is-offset-2">
									<nav class="level is-mobile">
									  <div class="level-left">
										  <div class="level-item has-text-centered">
											<div>
											  <p class="heading">Tweets</p>
											  <p class="heading">3,456</p>
											</div>
										  </div>
									  </div>
									  <div class="level-right">
										  <div class="level-item has-text-centered">
											<div>
											  <p class="heading">Tweets</p>
											  <p class="heading">3,456</p>
											</div>
										  </div>
									  </div>
									</nav>
								</div>
							</div>
						  </div>
						</div>
						<div class="column">
						   <div class="box">
							  <p class="title is-6 m-b-0-5 has-text-centered">
							   New Users
							 </p>
							<article class="v-centered-block">
								<div class="progressbars_cont progressbars_2"></div>
							</article>
							<div class="columns is-mobile">
								<div class="column  is-8 is-offset-2">
									<nav class="level is-mobile">
									  <div class="level-left">
										  <div class="level-item has-text-centered">
											<div>
											  <p class="heading">Tweets</p>
											  <p class="heading">3,456</p>
											</div>
										  </div>
									  </div>
									  <div class="level-right">
										  <div class="level-item has-text-centered">
											<div>
											  <p class="heading">Tweets</p>
											  <p class="heading">3,456</p>
											</div>
										  </div>
									  </div>
									</nav>
								</div>
							</div>
						   </div>
						</div>
						<div class="column">
						  <div class="box">
							<p class="title is-6 m-b-0-5 has-text-centered">
							   New Users
							</p>
							<article class="v-centered-block">
								<div class="progressbars_cont progressbars_3"></div>
							</article>
							<div class="columns is-mobile">
								<div class="column  is-8 is-offset-2">
									<nav class="level is-mobile">
									  <div class="level-left">
										  <div class="level-item has-text-centered">
											<div>
											  <p class="heading">Tweets</p>
											  <p class="heading">3,456</p>
											</div>
										  </div>
									  </div>
									  <div class="level-right">
										  <div class="level-item has-text-centered">
											<div>
											  <p class="heading">Tweets</p>
											  <p class="heading">3,456</p>
											</div>
										  </div>
									  </div>
									</nav>
								</div>
							</div>
						  </div>
						</div>
						<div class="column">
						   <div class="box">
							<p class="title is-6 m-b-0-5 has-text-centered">
							   New Users
							 </p>
							<article class="v-centered-block">
								<div class="progressbars_cont progressbars_4"></div>
							</article>
							<div class="columns is-mobile">
								<div class="column  is-8 is-offset-2">
									<nav class="level is-mobile">
									  <div class="level-left">
										  <div class="level-item has-text-centered">
											<div>
											  <p class="heading">Tweets</p>
											  <p class="heading">3,456</p>
											</div>
										  </div>
									  </div>
									  <div class="level-right">
										  <div class="level-item has-text-centered">
											<div>
											  <p class="heading">Tweets</p>
											  <p class="heading">3,456</p>
											</div>
										  </div>
									  </div>
									</nav>
								</div>
							</div>
						   </div>
						</div>

					</div>

			</section>
-->

			<section class="section p-t-0-5">
				  <div class="columns">
					  <div class="column is-6">
							<p class="title is-5">
							   Projects Vs Months
						   </p>
							  <div class="block chart-container">
								<canvas id="projects-chart"></canvas>
							  </div>
					  </div>
					  <div class="column">
						  <p class="title is-5">
							   Registrations Vs Months
						   </p>
							  <div class="block chart-container">
								<canvas id="registrations-chart"></canvas>
							  </div>
					  </div>
				  </div>
			</section>

<!--
			<section class="section p-t-0-5">

				  <div class="columns">
					  <div class="column ">
						  <p class="title is-5">
							   New Registrations
						   </p>
							<table class="table " id="new_users_dt-table">
								<thead>
									<tr>
										<th></th>
										<th>Name</th>
										<th>Email</th>
										<th></th>
									</tr>
								</thead>
							</table>
					  </div>
					  <div class="column is-6">
							<p class="title is-5">
							  New Projects
						   </p>
							<table class="table " id="projects_dt-table">
								<thead>
									<tr>
										<th>Id</th>
										<th>Name</th>
										<th>Email</th>
										<th>Registerd</th>
									</tr>
								</thead>
							</table>
					  </div>
				  </div>

			</section>
-->

	</div>
</div>
@endsection

@push('header-css')
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css"/>
@endpush

@push('scripts')
<script src="{{ asset('js/Chart.js') }}"></script>
<script src="{{ asset('js/progressbar.js') }}"></script>
<script src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
	//~ $(function() {
		//~ $('#new_users_dt-table').DataTable({
			//~ processing: true,
			//~ serverSide: true,
			//~ lengthChange: false,
			//~ pageLength: 5,
			//~ ajax: "{{ route('datatables.newusers') }}",
			//~ columns: [
				//~ { data: 'rownum', name: 'rownum', orderable: false, searchable: false},
				//~ { data: 'f_name'},
				//~ { data: 'email'},
				//~ {data: 'action', name: 'action', orderable: false, searchable: false}
			//~ ]
		//~ });
		//~ $('#projects_dt-table').DataTable({
			//~ processing: true,
			//~ serverSide: true,
			//~ ajax: "{{ route('datatables.newusers') }}",
			//~ columns: [
				//~ { data: 'rownum', name: 'rownum', orderable: false, searchable: false},
				//~ { data: 'f_name', name: 'f_name' },
				//~ { data: 'email', name: 'email' },
				//~ {data: 'action', name: 'action', orderable: false, searchable: false}
			//~ ]
		//~ });
	//~ });
</script>
<script type="text/javascript">

		var pBarsOptions = {
		  color: '#aaa',
		  strokeWidth: 4,
		  trailWidth: 1,
		  easing: 'easeInOut',
		  duration: 1400,
		  text: {
			autoStyleContainer: true,
		  },
		  from: { color: '#aaa', width: 1 },
		  to: { color: '#FFEA82', width: 4 },
		  // Set default step function for all animate calls
		  step: function(state, circle) {
			circle.path.setAttribute('stroke', state.color);
			circle.path.setAttribute('stroke-width', state.width);

			var value = Math.round(circle.value() * 100);
			if (value === 0) {
			  circle.setText('');
			} else {
			  circle.setText(value);
			}

		  }
		};
		var cProgressBars1 = new ProgressBar.Circle('.progressbars_1', pBarsOptions );
		var cProgressBars2 = new ProgressBar.Circle('.progressbars_2', pBarsOptions );
		var cProgressBars3 = new ProgressBar.Circle('.progressbars_3', pBarsOptions );
		var cProgressBars4 = new ProgressBar.Circle('.progressbars_4', pBarsOptions );
		// bar.text.style.fontFamily = '"Raleway", Helvetica, sans-serif';
		// cProgressBars.text.style.fontSize = '2rem';

		cProgressBars1.animate(0.7);  // Number from 0.0 to 1.0
		cProgressBars2.animate(0.4);
		cProgressBars3.animate(0.8);
		cProgressBars4.animate(0.6);
</script>
<script type="text/javascript">
	var ctxReg = document.getElementById("registrations-chart").getContext("2d");
	var ctxProjects = document.getElementById("projects-chart").getContext("2d");
	var projectsVsmonth ;
	var usersVsmonth ;

	var chartColors = {
		red: 'rgb(255, 99, 132)',
		orange: 'rgb(255, 159, 64)',
		yellow: 'rgb(255, 205, 86)',
		green: 'rgb(75, 192, 192)',
		blue: 'rgb(54, 162, 235)',
		purple: 'rgb(153, 102, 255)',
		grey: 'rgb(201, 203, 207)'
	};
	var color = Chart.helpers.color;
	var configLine = {
            type: 'line',
            data: {
                labels: [],
                datasets: []
            },
            options: {
                responsive: true,
                maintainAspectRatio: true,
                title:{
                    display:false,
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Month'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Views'
                        }
                    }]
                }
            }
    };

    var configBar = {
		type: 'bar',
		data: {
			labels: [],
			datasets: []
		},
		options: {
				responsive: true,
				legend: {
					position: 'top',
				},
				title: {
					display: false,
				}
		 }
    };

	$(function() {
		$.ajax({
		  headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		  },
		  method: 'POST',
		  url: "{{ route('chartdata.usersvsmonth')}}",
		  dataType: 'JSON',
		}).done(function(d) {
			console.log(d);
			if (d.success){
				var chLinebls = [];
				var chLineDt = [];
				$.each(d.data, function( key, value ) {
					chLinebls.push(key)
					chLineDt.push(value)
				});
				configLine.data.labels = chLinebls ;
				configLine.data.datasets = [{
                    label: "Users",
                    backgroundColor: color(chartColors.green).alpha(0.5).rgbString(),
                    borderColor: chartColors.green,
                    data: chLineDt,
                }] ;
                usersVsmonth = new Chart(ctxReg, configLine);
			}

		});

		$.ajax({
		  headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		  },
		  method: 'POST',
		  url: "{{ route('chartdata.usersvsmonth')}}",
		  dataType: 'JSON',
		}).done(function(d) {
			console.log(d);
			if (d.success){
				var chLbls = [];
				var chDt = [];
				$.each(d.data, function( key, value ) {
					chLbls.push(key)
					chDt.push(value)
				});
				configBar.data.labels = chLbls ;
				configBar.data.datasets = [{
                    label: "projects",
                    backgroundColor: color(chartColors.green).alpha(0.5).rgbString(),
                    borderColor: chartColors.green,
                    data: chDt,
                    borderWidth: 1,
                }] ;
                projectsVsmonth = new Chart(ctxProjects, configBar);
			}

		});

	});
</script>
@endpush




