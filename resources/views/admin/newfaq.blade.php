@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<div class="columns is-paddingless is-marginless">
	<div class="column is-narrow">
	    @include('partials.adminmenu')
	</div>
	<div class="column">

			<section class="section p-t-0-5">
				  <div class="columns">
					  <div class=" column is-10 is-offset-1">
							<div class="heading  m-b-3 has-text-centered">
								<h6 class="title txt-green-yellow ">New Faq</h5>
								<h6 class="subtitle txt-green-yellow">Lorem ipsum dolor sit amet consectetur.</h6>
							</div>

							<div class=" columns " id="tab-faq-details">
								<div class=" column ">

									<div class="field">
									  <label class="label">Assigned To </label>
									  <p class="control">
										<span class="select">
										  <select id="faq_type">
											<option value="0" selected>Members</option>
											<option value="1">Donators</option>
										  </select>
										</span>
									  </p>
									</div>

									<div class="field">
									  <label class="label">Faq Title </label>
									  <p class="control has-icons-left has-icons-right">
										<input name="faq_name" required class="input is-hovered" type="text" placeholder="Faq Title">
										<span class="icon is-small is-left">
										  <i class="fa fa-envelope"></i>
										</span>
									  </p>
									</div>

									<div class="field">
									  <label class="label">Description</label>
									  <p class="control has-icons-right">
										<textarea  name="faq_description"  class="textarea is-hovered" placeholder="Description"></textarea>
									  </p>
									</div>
								</div>
						   </div>

							<div class=" columns is-hidden" id="tab-faq-finish">
									<div class=" column">
										<article class="v-centered">
											<div class="has-text-centered">
												<div class="field  has-addons has-addons-centered">
													  <p class="control">
														  <span class="icon is-large">
															  <i class="fa fa-home"></i>
															</span>
													 </p>
												</div>
												<p class="title is-4">Congrtulations</p>
												<p class="title is-6">
													Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean efficitur.
													Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean efficitur.

												</p>

												<div class="field  has-addons has-addons-centered">
													  <p class="control">
														  <a class=" button">
															View Faq
														  </a>
													 </p>
												</div>
											</div>
										</article>
									</div>
							</div>

							<nav class="level is-mobile  m-t-1-5">
							  <div class="level-left">
								<div class="level-item">
									<div class="field m-t-1-5">
									  <p class="control">
										<button class="button is-medium is-primary btn-at-general" id="faq-create-btn-control">Create Faq</button>
									  </p>
									</div>
								</div>
							  </div>
							</nav>
					  </div>

				  </div>
			</section>

	</div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">

  var regdata = { };


  $(document).ready(function() {
		$( "input.input, textarea.textarea " ).focus(function() {
			if ($(this).hasClass("is-danger")) {
				$(this).removeClass("is-danger").next().remove();
				$(this).parent("p.control").next("p").remove();
			}
		});

		function createFaqForm(btn) {
			 console.log(regdata);
			 $.ajax({
			  headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			  },
			  method: 'POST',
			  url: "{{ route('faqs.store')}}",
			  dataType: 'JSON',
			  data: regdata
			}).done(function(d) {
				console.log(d);
				if (d.success){
					    $("#tab-faq-details" ).addClass( "is-hidden" );
						$("#tab-faq-finish" ).removeClass( "is-hidden" );
						btn.removeClass("btn-at-general").addClass("btn-at-finish is-hidden");
				}

			  });
		 }


		function validateDetails() {
			var faqName = $("#tab-faq-details input[name='faq_name']");
			var faqType = $("#faq_type");
			var faqDescription = $("#tab-faq-details textarea[name='faq_description']");

			if ((! faqName.val()) && (! faqName.hasClass("is-danger"))) {
				faqName.toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>This field is required</p>" );
			}

			if ((! faqDescription.val()) && (! faqDescription.hasClass("is-danger"))) {
				faqDescription.toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>This field is required</p>" );
			}

			if ((faqName.val()) && (faqDescription.val())) {
				regdata.title = faqName.val();
				regdata.description = faqDescription.val();
				regdata.type = faqType.val();
				return true ;
			} else {
				return false ;
			}
		}

		$('#faq-create-btn-control').click(function(e){
			e.preventDefault();
			if ($(this).hasClass("btn-at-general")) {
				$(this).prop("disabled", true);
				var r = validateDetails();
				if (r) {
					createFaqForm($(this));
				}
				$(this).prop("disabled", false);
				return;
			}
		});
 });
</script>
@endpush

