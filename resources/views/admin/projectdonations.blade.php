@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<div class="columns is-paddingless is-marginless">
	<div class="column is-narrow">
	    @include('partials.adminmenu')
	</div>
	<div class="column">
		<section class="section p-t-0-5">
			@include('partials.admin.projectheading')
		</section>

		<section class="section p-t-0-5">
			@include('partials.projectheader')
		</section>

		<section class="section p-t-0-5">
			@include('partials.admin.projectsubmenu',
					[
					'partials_general_active' => '' ,
					'partials_donations_active' => 'is-active' ,
					'partials_messages_active' => '' ,
					'partials_stats_active' => '' ,
					])
		</section>

		<section class="section p-t-0-5">
			<div class="columns">
				<div class="column">
					@if($project->payments->isEmpty())
						 <div class="heading  m-b-3 has-text-centered">
							<h4 class="subtitle"><strong>No Donations Made</strong></h4>
						</div>
					@else
						 <div class="heading  m-b-3 has-text-centered">
							<h4 class="subtitle">Donators</h4>
						</div>
						<table class="table">
						  <thead>
							<tr>
							  <th><abbr title="Position"></abbr></th>
							  <th>Names</th>
							  <th><abbr title="Played">Date</abbr></th>
							  <th><abbr title="Won">Amount</abbr></th>
							  <th><abbr title="Drawn">Status</abbr></th>
							  <th><abbr title="Lost"></abbr></th>
							</tr>
						  </thead>

						  <tbody>
							@foreach ($project->payments as $payment)
								<tr>
								  <th> {{ $loop->iteration }}</th>
								  <td>{{ $payment->donator->first_name }}</td>
								  <td>{{ $payment->created_at->toFormattedDateString() }}</td>
								  <td>{{ $payment->amount }}</td>
								  <td>PAID</td>
								  <td><a class="button is-primary">View</a></td>
								</tr>
							@endforeach
						  </tbody>
						</table>

					@endif

				</div>
			</div>
		</section>

	</div>
</div>
@endsection

@include('partials.admin.projectfooter', ['partials_project_id' => $project->id ])






