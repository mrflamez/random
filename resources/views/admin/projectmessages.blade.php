@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<div class="columns is-paddingless is-marginless">
	<div class="column is-narrow">
	    @include('partials.adminmenu')
	</div>
	<div class="column">
		<section class="section p-t-0-5">
			@include('partials.admin.projectheading')
		</section>

		<section class="section p-t-0-5">
			@include('partials.projectheader')
		</section>

		<section class="section p-t-0-5">
			@include('partials.admin.projectsubmenu',
					[
					'partials_general_active' => '' ,
					'partials_donations_active' => '' ,
					'partials_messages_active' => 'is-active' ,
					'partials_stats_active' => '' ,
					])
		</section>



		<section class="section p-t-0-5">
		   <div class="columns project_messages_container">
			  <div class="column">
					@forelse ($project->comments as $comment)
					<article class="media">
					  <figure class="media-left">
						<p class="image is-128x128">
						  <img src="http://bulma.io/images/placeholders/256x256.png">
						</p>
					  </figure>
					  <div class="media-content">
						<div class="content">
						  <p class="m-b-0-5">
							<strong>{{ $comment->subject }}</strong> <small>@ {{ $comment->user->f_name }}</small>
						  </p>
						  <p> {{ $comment->content }} </p>
						</div>
						<nav class="level is-mobile">
						  <div class="level-left">
							  <a class="level-item button" >
								  <span class="icon is-small"><i class="fa fa-reply"></i></span>
								  <span> {{ $comment->replies->count() }} Replys</span>
								</a>
								<a class="level-item button view_single_project_message">
								  <span class="icon is-small"><i class="fa fa-retweet"></i></span>
								  <span>View</span>
								</a>
						  </div>
						  <div class="level-right">
							<p><strong>{{ $comment-> created_at ->diffForHumans() }}</strong></p>
						  </div>
						</nav>
					  </div>

					</article>

				@empty
					<div class="heading  m-b-3 has-text-centered">
							<h4 class="subtitle"><strong>No Comments</strong></h4>
						</div>
				@endforelse

			  </div>
		   </div>

		</section>
	</div>
</div>
@endsection

@include('partials.admin.projectfooter', ['partials_project_id' => $project->id ])

@push('scripts')
<script type="text/javascript">
	$(document).ready(function() {
		$('.view_single_project_message').click(function(e){
			e.preventDefault();
			$('.project_messages_container').toggleClass( "is-hidden" );
			$('.project_conversation_container').toggleClass( "is-hidden" );
		});
	});
</script>
@endpush






