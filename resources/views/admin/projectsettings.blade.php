@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<div class="columns is-paddingless is-marginless">
	<div class="column is-narrow">
	    @include('partials.adminmenu')
	</div>
	<div class="column">
		<section class="section p-t-0-5">
			<div class="heading has-text-centered">
				<h6 class="title">{{ $project-> title }}</h5>
			</div>
		</section>

		<section class="section p-t-0-5">
			@include('partials.projectheader')
		</section>

		<section class="section p-t-0-5">
			@include('partials.admin.projectsubmenu',
					[
					'partials_general_active' => '' ,
					'partials_donations_active' => '' ,
					'partials_messages_active' => '' ,
					'partials_stats_active' => 'is-active' ,
					])
		</section>



	</div>
</div>
@endsection

@push('scripts')

@endpush






