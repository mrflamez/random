@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<div class="columns is-paddingless is-marginless">
	<div class="column is-narrow">
	    @include('partials.adminmenu')
	</div>
	<div class="column">
		<section class="section p-t-0-5">
			@include('partials.admin.projectheading')
		</section>

		<section class="section p-t-0-5">
			@include('partials.projectheader')
		</section>

		<section class="section p-t-0-5">
			@include('partials.admin.projectsubmenu',
					[
					'partials_general_active' => '' ,
					'partials_donations_active' => '' ,
					'partials_messages_active' => '' ,
					'partials_stats_active' => 'is-active' ,
					])
		</section>

		<section class="section p-t-0-5">
			<div class="columns">
				  <div class="column">
					  <p class="title is-5">
						   Project-views VS Months
					   </p>
						<div class="block chart_container">
							<canvas id="viewsChart"></canvas>
						</div>
				  </div>
				  <div class="column is-6">
						<p class="title is-5">
						   Donations VS Months
					   </p>
						<div class="block chart_container">
							<canvas id="donationsChart"></canvas>
						</div>
				  </div>
			  </div>

		</section>
	</div>
</div>
@endsection

@include('partials.admin.projectfooter', ['partials_project_id' => $project->id ])

@push('scripts')
<script src="{{ asset('js/Chart.js') }}"></script>
<script type="text/javascript">
	window.chartColors = {
		red: 'rgb(255, 99, 132)',
		orange: 'rgb(255, 159, 64)',
		yellow: 'rgb(255, 205, 86)',
		green: 'rgb(75, 192, 192)',
		blue: 'rgb(54, 162, 235)',
		purple: 'rgb(153, 102, 255)',
		grey: 'rgb(201, 203, 207)'
	};

	window.randomScalingFactor = function() {
		return (Math.random() > 0.5 ? 1.0 : -1.0) * Math.round(Math.random() * 100);
	};
	var MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        var config = {
            type: 'line',
            data: {
                labels: ["January", "February", "March", "April", "May", "June", "July"],
                datasets: [{
                    label: "My First dataset",
                    backgroundColor: window.chartColors.red,
                    borderColor: window.chartColors.red,
                    data: [
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor()
                    ],
                    fill: false,
                }
              ]
            },
            options: {
                responsive: true,
                title:{
                    display:false,
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Month'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Views'
                        }
                    }]
                }
            }
        };

		var color = Chart.helpers.color;

        var barChartData = {
            labels: ["January", "February", "March", "April", "May", "June", "July"],
            datasets: [{
                label: 'Dataset 1',
                backgroundColor: color(window.chartColors.green).alpha(0.5).rgbString(),
                borderColor: window.chartColors.green,
                borderWidth: 1,
                data: [
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor()
                ]
            }
           ]

        };

        window.onload = function() {
            var ctx = document.getElementById("viewsChart").getContext("2d");
            window.myLine = new Chart(ctx, config);

            var ctxBar = document.getElementById("donationsChart").getContext("2d");
            window.myBar = new Chart(ctxBar, {
                type: 'bar',
                data: barChartData,
                options: {
                    responsive: true,
                    legend: {
                        position: 'top',
                    },
                    title: {
                        display: true,
                        text: 'Chart.js Bar Chart'
                    }
                }
            });
        };



</script>
@endpush






