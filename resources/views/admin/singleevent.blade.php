@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<div class="columns is-paddingless is-marginless">
	<div class="column is-narrow">
	    @include('partials.adminmenu')
	</div>
	<div class="column">


		<section class="section p-t-0-5">
			@include('partials.admin.eventheader')
		</section>


		<section class="section p-t-0-5">
			<div class="columns">
				<div class="column">
					@include('partials.admin.eventheading')

					<div class="content">
						{!!  $event-> description  !!}
				   </div>
				</div>
				<div class="column is-6">
					<div class="columns">
						<div class="column">
							<figure class="image  featured">
								@foreach ($event -> images as $image)
								   @if ($loop->first)
									<img src="{{ asset('storage/'.$image->name) }}" alt="">
								   @endif
								@endforeach
							</figure>
					    </div>
					</div>
<!--
					<div class="columns is-multiline">
						<div class="column is-6">
							<figure class="image is-4by3">
								<img src="https://placehold.it/200x200">
							</figure>
						</div>
						<div class="column is-6 ">
							<figure class="image is-4by3">
								<img src="https://placehold.it/200x200">
							</figure>
						</div>
						<div class="column is-6">
							<figure class="image is-4by3">
								<img src="https://placehold.it/200x200">
							</figure>
						</div>
						<div class="column is-6">
							<figure class="image is-4by3">
								<img src="https://placehold.it/200x200">
							</figure>
						</div>
					</div>
-->
				</div>
			</div>
		</section>

	</div>
</div>
@endsection
@push('header-css')
<link rel="stylesheet"  href="//cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.5/sweetalert2.min.css" >
@endpush
@push('scripts')
<script src="//cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.5/sweetalert2.min.js"></script>
<script type="text/javascript">
 $(document).ready(function() {
	  $('#event_delete_button-btn-control').click(function(e){
			e.preventDefault();
			var dBtn = $(this);
			swal({
				  title: 'Are you sure?',
				  text: "You won't be able to revert this!",
				  type: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Yes, delete it!'
				}).then(function () {
					$.ajax({
					  headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					  },
					  method: 'DELETE',
					  url: "{{ route('events.destroy', ['id' => $event->id])}}",
					  dataType: 'JSON',
					}).done(function(d) {
						console.log(d);
						if (d.success){
							dBtn.parent('div.block').remove();
							swal(
							'Deleted!',
							'Event deleted.',
							'success'
						  )
						}

					  });

				})
	  });

  });
</script>
@endpush






