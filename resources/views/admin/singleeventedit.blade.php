@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<div class="columns is-paddingless is-marginless">
	<div class="column is-narrow">
	    @include('partials.adminmenu')
	</div>
	<div class="column">

			<section class="section p-t-0-5">
				  <div class="columns">
					  <div class=" column is-10 is-offset-1">
							<div class="heading  m-b-3 has-text-centered">
								<h6 class="title txt-green-yellow ">New Event</h5>
								<h6 class="subtitle txt-green-yellow">Lorem ipsum dolor sit amet consectetur.</h6>
							</div>

							<div class=" columns " id="tab-event-details">
								<div class=" column ">
									<div class="field">
									  <label class="label">Event Title </label>
									  <p class="control has-icons-left has-icons-right">
										<input value="{{ $event->title }}" name="event_name" required class="input is-hovered" type="text" placeholder="Event Title">
										<span class="icon is-small is-left">
										  <i class="fa fa-envelope"></i>
										</span>
									  </p>
									</div>
									<div class="field ">
										<label class="label">Tag Line</label>
										<p class="control  has-icons-left has-icons-right">
											<input value="{{ $event->tag_line }}"   id="event_tag_line"  class="input is-hovered" type="text" >
											<span class="icon is-small is-left">
											  <i class="fa fa-calendar"></i>
											</span>
										</p>
									</div>
									<div class="field-body m-b-0-7-5">
										<div class="field ">
											<label class="label">Start Date</label>
											<p class="control is-expanded has-icons-left">
												<input value="@isset( $event->start_date) {{ $event->start_date->toFormattedDateString() }} @endisset"   id="event_duration_start" class="input is-hovered" type="text" >
												<span class="icon is-small is-left">
												  <i class="fa fa-calendar"></i>
												</span>
											</p>
										</div>
										<div class="field">
											<label class="label">End Date</label>
											<p class="control is-expanded has-icons-left">
												<input value="@isset( $event->end_date) {{ $event->end_date->toFormattedDateString() }} @endisset"  id="event_duration_end" class="input is-hovered" type="text" >
												<span class="icon is-small is-left">
												  <i class="fa fa-calendar"></i>
												</span>
											</p>
										</div>
									</div>

									<div class="field-body m-b-0-7-5">
										<div class="field ">
											<label class="label">Fee</label>
											<p class="control is-expanded has-icons-left">
												<input  value="@isset( $event->fee) {{ $event->fee }} @endisset"  id="event_fee" class="input is-hovered" type="text" >
												<span class="icon is-small is-left">
												  <i class="fa fa-calendar"></i>
												</span>
											</p>
										</div>
										<div class="field">
											<label class="label">Venue</label>
											<p class="control is-expanded has-icons-left">
												<input value="{{ $event->venue }}"  id="event_venue" class="input is-hovered" type="text" >
												<span class="icon is-small is-left">
												  <i class="fa fa-calendar"></i>
												</span>
											</p>
										</div>
									</div>
									<div class="field">
									  <label class="label">Description</label>
									  <p class="control has-icons-right">
										<textarea  name="event_description"  class="textarea is-hovered" placeholder="Description">{!! $event->description !!}</textarea>
									  </p>
									</div>
								</div>
								<div class=" column is-5">
										<label class="label m-b-1-5 is-marginless">Featured Image </label>
										<p class="help is-danger featured-image-label is-hidden">This field is required</p>
										<div  class="columns">
								           <div  class="column">
												<figure class="image is-2by1">
													@foreach ($event -> images as $image)
													   @if ($image->featured)
														<img src="{{ asset('storage/'.$image->name) }}" alt="">
													   @endif
													@endforeach
												</figure>
												<p class="has-text-centered"><a href="">Remove</a></p>
										    </div>
										</div>

										<div  class="columns full-40-h is-hidden is-marginless">
											<div  class="column">
												<div id="featured-image"  class=" dropzone ">

												</div>
											</div>
										</div>

										<label class="label m-b-1-5 is-marginless">Gallery Images </label>
										<p class="help is-danger gallery-images-label is-hidden">This field is required</p>
										<div  class="columns is-hidden full-40-h is-marginless">
											<div  class="column">
												<div id="gallery-images"  class=" dropzone">

												</div>
											</div>
										</div>
								        <div  class="columns is-multiline">
											@foreach ($event -> images as $image)
											   @if (! $image->featured)
											   <div  class="column is-narrow">
												   <figure class="image is-128x128">
														 <img src="{{ asset('storage/'.$image->name) }}" alt="">
													</figure>
													<p class="has-text-centered"><a href="">Remove</a></p>
											   </div>
											   @endif
											@endforeach
										</div>

								</div>
							</div>

							<div class=" columns is-hidden" id="tab-event-finish">
									<div class=" column">
										<article class="v-centered">
											<div class="has-text-centered">
												<div class="field  has-addons has-addons-centered">
													  <p class="control">
														  <span class="icon is-large">
															  <i class="fa fa-home"></i>
															</span>
													 </p>
												</div>
												<p class="title is-4">Edit Success</p>
												<p class="title is-6">
													Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean efficitur.
													Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean efficitur.

												</p>

												<div class="field  has-addons has-addons-centered">
													  <p class="control">
														  <a class=" button">
															View Event
														  </a>
													 </p>
												</div>
											</div>
										</article>
									</div>
							</div>

							<nav class="level is-mobile  m-t-1-5">
							  <div class="level-left">
								<div class="level-item">
									<div class="field m-t-1-5">
									  <p class="control">
										<button class="button is-medium is-primary btn-at-general" id="event-edit-btn-control">Edit Event</button>
									  </p>
									</div>
								</div>
							  </div>
							</nav>
					  </div>

				  </div>
			</section>

	</div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('js/dropzone.js') }}"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script src="{{ asset('js/pikaday.js') }}"></script>
<script type="text/javascript">
  Dropzone.autoDiscover = false;
  var startdateIn ;
  var enddateIn ;
  var regdata = { };

  var featuredImage = new Dropzone("#featured-image" , {
    paramName: "file",
    maxFilesize: 1,
    headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	},
	url: "{{ route('photos.store')}}",
	addRemoveLinks : true,
	autoProcessQueue: false,
	uploadMultiple: false,
    parallelUploads: 1
  });
  var galleryImages = new Dropzone("#gallery-images" , {
    paramName: "file",
    maxFilesize: 1,
    headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	},
	url: "{{ route('photos.store')}}",
	addRemoveLinks : true,
	autoProcessQueue: false,
	uploadMultiple: false,
    parallelUploads: 1
  });

  var startPicker = new Pikaday({
	  field: $('#event_duration_start')[0],
	  format: 'D MMM YYYY',
	  onSelect: function() {
		console.log(this.getMoment().format('YYYY-MM-DD'));
		regdata.start = this.getMoment().format('YYYY-MM-DD');
	  }
	 });
  var endPicker = new Pikaday({
	  field: $('#event_duration_end')[0],
	  format: 'D MMM YYYY',
	  onSelect: function() {
		console.log(this.getMoment().format('YYYY-MM-DD'));
		regdata.end = this.getMoment().format('YYYY-MM-DD');
	  }
	 });

  $(document).ready(function() {
		regdata.start = startPicker.toString('YYYY-MM-DD');
		regdata.end = endPicker.toString('YYYY-MM-DD');
		console.log(regdata);

		$( "input.input, textarea.textarea " ).focus(function() {
			if ($(this).hasClass("is-danger")) {
				$(this).removeClass("is-danger").next().remove();
				$(this).parent("p.control").next("p").remove();
			}
		});

		function createEventForm(btn) {
			 console.log(regdata);
			 $.ajax({
			  headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			  },
			  method: 'PUT',
			  url: "{{ route('events.update', ['id' => $event->id])}}",
			  dataType: 'JSON',
			  data: regdata
			}).done(function(d) {
				console.log(d);
				if (d.success){
					//~ featuredImage.on("sending", function(file, xhr, formData) {
					  //~ formData.append("eventid", d.id);
					  //~ formData.append("isfeatured", 'true');
					//~ });

					//~ galleryImages.on("sending", function(file, xhr, formData) {
					  //~ formData.append("eventid", d.id);
					//~ });

					//~ featuredImage.processQueue();
					//~ galleryImages.processQueue();

					//~ featuredImage.on("queuecomplete", function() {
						//~ $("#tab-event-details" ).addClass( "is-hidden" );
						//~ $("#tab-event-finish" ).removeClass( "is-hidden" );
						//~ btn.removeClass("btn-at-general").addClass("btn-at-finish is-hidden");
					//~ });
					$("#tab-event-details" ).addClass( "is-hidden" );
					$("#tab-event-finish" ).removeClass( "is-hidden" );
					btn.removeClass("btn-at-general").addClass("btn-at-finish is-hidden");
				}

			  });
		 }


		function validateDetails() {
			//~ $( "#tab-event-details p.control" ).toggleClass( "is-loading" );

			var eventName = $("#tab-event-details input[name='event_name']");
			var eventTagLine = $("#event_tag_line");
			var eventDescription = $("#tab-event-details textarea[name='event_description']");
			var eventFee = $("#event_fee");
			var eventVenue = $("#event_venue");

			if ((! eventName.val()) && (! eventName.hasClass("is-danger"))) {
				eventName.toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>This field is required</p>" );
			}

			if ((! eventDescription.val()) && (! eventDescription.hasClass("is-danger"))) {
				eventDescription.toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>This field is required</p>" );
			}



			//~ $( "#tab-event-details p.control" ).toggleClass( "is-loading" );

			//~ var fImage = featuredImage.getQueuedFiles();

			//~ if (!(fImage.length > 0)) {
				//~ $( ".featured-image-label" ).toggleClass( "is-hidden" );
			//~ }

			//~ if ((eventName.val()) && (eventDescription.val()) && (fImage.length > 0)) {
			if ((eventName.val()) && (eventDescription.val())) {
				regdata.title = eventName.val();
				regdata.tag_line = eventTagLine.val();
				regdata.fee = eventFee.val();
				regdata.description = eventDescription.val();
				regdata.venue = eventVenue.val();
				return true ;
			} else {
				return false ;
			}
		}

		$('#event-edit-btn-control').click(function(e){
			e.preventDefault();
			if ($(this).hasClass("btn-at-general")) {
				$(this).prop("disabled", true);
				var r = validateDetails();
				if (r) {
					createEventForm($(this));
				}
				$(this).prop("disabled", false);
				return;
			}
		});
 });
</script>
@endpush

@push('header-css')
<link href="{{ asset('css/pikaday.css') }}" rel="stylesheet" type="text/css">
@endpush

