@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<div class="columns is-paddingless is-marginless">
	<div class="column is-narrow">
	    @include('partials.adminmenu')
	</div>
	<div class="column">


		<section class="section p-t-0-5">
			@include('partials.admin.faqssheader')
		</section>


		<section class="section p-t-0-5">
			<div class="columns">
				<div class="column">
					<p class="title is-3"><strong>{{ $faq-> title }}</strong></p>

					<div class="content">
						{!!  $faq-> description  !!}
				   </div>
				</div>
			</div>
		</section>

	</div>
</div>
@endsection

@push('header-css')
<link rel="stylesheet"  href="//cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.5/sweetalert2.min.css" >
@endpush

@push('scripts')
<script src="//cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.5/sweetalert2.min.js"></script>
<script type="text/javascript">
 $(document).ready(function() {
	  $('#faqs_delete_button-btn-control').click(function(e){
			e.preventDefault();
			var dBtn = $(this);
			swal({
				  title: 'Are you sure?',
				  text: "You won't be able to revert this!",
				  type: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Yes, delete it!'
				}).then(function () {
					$.ajax({
					  headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					  },
					  method: 'DELETE',
					  url: "{{ route('faqs.destroy', ['id' => $faq->id])}}",
					  dataType: 'JSON',
					}).done(function(d) {
						console.log(d);
						if (d.success){
							dBtn.remove();
							swal(
							'Deleted!',
							'Faq deleted.',
							'success'
						  )
						}

					  });

				})
	  });
  });
</script>
@endpush








