@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<div class="columns is-paddingless is-marginless">
	<div class="column is-narrow">
	    @include('partials.adminmenu')
	</div>
	<div class="column">

		<section class="section ">
			<div class="columns">
				<div class="column b-br">
					<h3 class="title">Payee</h3>
					<div class="content">
						<p class="subtitle is-5">Names : <strong>{{ $payment -> donator -> first_name }} {{ $payment -> donator -> last_name }}</strong></p>
						<p class="subtitle is-5">Phone : <strong>{{ $payment -> donator ->phone }}</strong></p>
						<p class="subtitle is-5">Email : <strong>{{ $payment -> donator ->email }}</strong></p>
				   </div>
				   <h3 class="title">Reason</h3>
					<div class="content">
						@if ($payment -> is_membership)
						 <p class="subtitle is-5"><span class="tag is-warning">Membership Fee</span></p>
						 @elseif ($payment ->is_donations)
							<p class="subtitle is-5"> <span class="tag is-primary">Donations</span></p>
						@else
						@endif
				   </div>
				</div>
				<div class="column is-narrow">
					<h3 class="title">Transaction Details</h3>
					<div class="content">
						<p class="subtitle is-5">Status : @if ($payment -> status) <span class="tag is-success is-medium">PAID</span> @else <span class="tag is-medium is-warning">PENDING</span> @endif</p>
						<p class="subtitle is-5">Amount : <strong>{{ $payment -> amount }}</strong></p>
						<p class="subtitle is-5">Reference : <strong>{{ $payment -> reference }} </strong></p>
						<p class="subtitle is-5">Tranx Id : <strong>{{ $payment -> tranx_id }}</strong></p>
						<p class="subtitle is-5">Currency : <strong>{{ $payment -> currency  }}</strong></p>
						<p class="subtitle is-5">Fees : <strong>{{ $payment -> fees }}</strong></p>
						<p class="subtitle is-5">Ip Address : <strong>{{ $payment -> ip_address }}</strong></p>
						<p class="subtitle is-5">Date : <strong>{{ $payment -> transaction_date  }}</strong></p>
				   </div>
				</div>

			</div>
		</section>
	</div>
</div>
@endsection

@push('scripts')

@endpush






