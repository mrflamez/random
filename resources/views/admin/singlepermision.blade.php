@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<div class="columns is-paddingless is-marginless">
	<div class="column is-narrow">
	    @include('partials.adminmenu')
	</div>
	<div class="column">

			<section class="section p-t-0-5">
				<div class="heading  m-b-3 has-text-centered">
					<h3 class="subtitle">Role : {{ $role->slug }}</h3>
				</div>

				  <div class="columns">
					  <div class="column is-7 b-br">
							<table class="table " id="single_permision_dt-table">
								<thead>
									<tr>
										<th>First Name</th>
										<th>Last Name</th>
										<th>Super Admin</th>
										<th></th>
									</tr>
								</thead>
							</table>
					  </div>
					  <div class="column">
						  <p class="subtitle">Give User Role</p>
						 <div class="field">
							  <label class="label">Role Name </label>
							  <p class="control ">
								<input disabled class="input " type="text" placeholder="{{ $role->slug }}">

							  </p>
						 </div>
						 <div class="field">
							  <label class="label">User </label>
							  <p class="control has-icons-left has-icons-right">
								<span class="select">
								  <select id="form_role-user_id">
									  @foreach ($users as $user)
											<option value="{{ $user -> id }}" >{{ $user -> f_name }} {{ $user -> l_name }}</option>
									  @endforeach
								  </select>
								</span>
								<span class="icon is-small is-left">
								  <i class="fa fa-envelope"></i>
								</span>
							  </p>
						 </div>
						 <div class="field ">
						  <p class="control">
							<button id="add-role_user-btn-control" class="button is-primary">Give Role</button>
						  </p>
						</div>

					  </div>
				  </div>
			</section>

	</div>
</div>
@endsection

@push('header-css')
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css"/>
@endpush

@push('scripts')
<script src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
	$(function() {
		var regdata = {};
		 $.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});

		$('#single_permision_dt-table').DataTable({
			processing: true,
			serverSide: true,
			lengthChange: false,
			pageLength: 5,
			ajax: "{{ route('datatables.singlepermision', ['id' => $role->id ]) }}",
			columns: [
				{ data: 'f_name'},
				{ data: 'l_name'},
				{ data: 'is_admin'},
				{data: 'action', name: 'action', orderable: false, searchable: false}
			]
		});

		$('#add-role_user-btn-control').click(function(e){
			e.preventDefault();
			$(this).prop("disabled", true);
			regdata.role_id = {{ $role->id }};
			regdata.user_id = $("#form_role-user_id").val();
			console.log(regdata);
			addUserRoleForm($(this));
			;
		});
		$('.remove-role_user-btn-control').click(function(e){
			e.preventDefault();
			var lnk = $(this).val();
			console.log(lnk);
			//removeUserRoleForm(lnk);

		});

		function addUserRoleForm(btn) {
			 $.ajax({
			  method: 'POST',
			  url: "{{ route('administrator.permisions.adduserrole')}}",
			  dataType: 'JSON',
			  data: regdata
			}).done(function(d) {
				console.log(d);
				btn.prop("disabled", false)
				if (d.success){

				}

			  });
		 }

		 function removeUserRoleForm(lk) {
			 $.ajax({
			  method: 'POST',
			  url: lk,
			  dataType: 'JSON',
			}).done(function(d) {
				console.log(d);
				if (d.success){

				}

			  });
		 }

	});
</script>
@endpush




