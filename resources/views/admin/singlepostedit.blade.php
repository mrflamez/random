@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<div class="columns is-paddingless is-marginless">
	<div class="column is-narrow">
	    @include('partials.adminmenu')
	</div>
	<div class="column">

			<section class="section p-t-0-5">
				  <div class="columns">
					  <div class=" column is-10 is-offset-1">
							<div class="heading  m-b-3 has-text-centered">
								<h6 class="title ">Edit : {{ $post-> title }}</h5>
							</div>

							<div class=" columns " id="tab-blog-details">
								<div class=" column ">
									<div class="field">
									  <label class="label">Blog Title </label>
									  <p class="control has-icons-left has-icons-right">
										<input value="{{ $post->title }}" name="blog_name" required class="input is-hovered" type="text" placeholder="Blog Title">
										<span class="icon is-small is-left">
										  <i class="fa fa-envelope"></i>
										</span>
									  </p>
									</div>
									<div class="field ">
										<label class="label">Tag Line</label>
										<p class="control  has-icons-left has-icons-right">
											<input value="{{ $post->tag_line }}"  id="blog_tag_line"  class="input is-hovered" type="text" >
											<span class="icon is-small is-left">
											  <i class="fa fa-calendar"></i>
											</span>
										</p>
									</div>

									<div class="field">
									  <label class="label">Description</label>
									  <p class="control has-icons-right">
										<textarea  name="blog_description"  class="textarea is-hovered" placeholder="Description">{{ $post->description }}</textarea>
									  </p>
									</div>
								</div>
								<div class=" column is-5">
										<label class="label m-b-1-5 is-marginless">Featured Image </label>
										<p class="help is-danger featured-image-label is-hidden">This field is required</p>
										<div  class="columns">
								           <div  class="column">
												<figure class="image is-2by1">
													@foreach ($post -> images as $image)
													   @if ($image->featured)
														<img src="{{ asset('storage/'.$image->name) }}" alt="">
													   @endif
													@endforeach
												</figure>
												<p class="has-text-centered"><a href="">Remove</a></p>
										    </div>
										</div>
										<div  class="columns full-40-h is-hidden is-marginless">
											<div  class="column">
												<div id="featured-image"  class=" dropzone">

												</div>
											</div>
										</div>
										<label class="label m-b-1-5 is-marginless">Gallery Images </label>
										<p class="help is-danger gallery-images-label is-hidden">This field is required</p>
										<div  class="columns is-multiline">
											@foreach ($post -> images as $image)
											   @if (! $image->featured)
											   <div  class="column is-narrow">
												   <figure class="image is-128x128">
														 <img src="{{ asset('storage/'.$image->name) }}" alt="">
													</figure>
													<p class="has-text-centered"><a href="">Remove</a></p>
											   </div>
											   @endif
											@endforeach
										</div>
										<div  class="columns full-40-h is-hidden is-marginless">
											<div  class="column">
												<div id="gallery-images"  class=" dropzone">

												</div>
											</div>
										</div>
								</div>
							</div>

							<div class=" columns is-hidden" id="tab-blog-finish">
									<div class=" column">
										<article class="v-centered">
											<div class="has-text-centered">
												<div class="field  has-addons has-addons-centered">
													  <p class="control">
														  <span class="icon is-large">
															  <i class="fa fa-home"></i>
															</span>
													 </p>
												</div>
												<p class="title is-4">Edit Success</p>
												<p class="title is-6">
													Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean efficitur.
													Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean efficitur.

												</p>

												<div class="field  has-addons has-addons-centered">
													  <p class="control">
														  <a class=" button">
															View Blog Post
														  </a>
													 </p>
												</div>
											</div>
										</article>
									</div>
							</div>

							<nav class="level is-mobile  m-t-1-5">
							  <div class="level-left">
								<div class="level-item">
									<div class="field m-t-1-5">
									  <p class="control">
										<button class="button is-medium is-primary btn-at-general" id="blog-edit-btn-control">Edit Blog Post</button>
									  </p>
									</div>
								</div>
							  </div>
							</nav>
					  </div>

				  </div>
			</section>

	</div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('js/dropzone.js') }}"></script>
<script type="text/javascript">
  Dropzone.autoDiscover = false;
  var regdata = { };

  var featuredImage = new Dropzone("#featured-image" , {
    paramName: "file",
    maxFilesize: 1,
    headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	},
	url: "{{ route('photos.store')}}",
	addRemoveLinks : true,
	autoProcessQueue: false,
	uploadMultiple: false,
    parallelUploads: 1
  });
  var galleryImages = new Dropzone("#gallery-images" , {
    paramName: "file",
    maxFilesize: 1,
    headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	},
	url: "{{ route('photos.store')}}",
	addRemoveLinks : true,
	autoProcessQueue: false,
	uploadMultiple: false,
    parallelUploads: 1
  });


  $(document).ready(function() {

		$( "input.input, textarea.textarea " ).focus(function() {
			if ($(this).hasClass("is-danger")) {
				$(this).removeClass("is-danger").next().remove();
				$(this).parent("p.control").next("p").remove();
			}
		});

		function createBlogForm(btn) {
			 console.log(regdata);
			 $.ajax({
			  headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			  },
			  method: 'PUT',
			  url: "{{ route('blog.update', ['id' => $post->id])}}",
			  dataType: 'JSON',
			  data: regdata
			}).done(function(d) {
				console.log(d);
				if (d.success){
					//~ featuredImage.on("sending", function(file, xhr, formData) {
					  //~ formData.append("blogid", d.id);
					  //~ formData.append("isfeatured", 'true');
					//~ });

					//~ galleryImages.on("sending", function(file, xhr, formData) {
					  //~ formData.append("blogid", d.id);
					//~ });

					//~ featuredImage.processQueue();
					//~ galleryImages.processQueue();

					//~ featuredImage.on("queuecomplete", function() {
						//~ $("#tab-blog-details" ).addClass( "is-hidden" );
						//~ $("#tab-blog-finish" ).removeClass( "is-hidden" );
						//~ btn.removeClass("btn-at-general").addClass("btn-at-finish is-hidden");
					//~ });
					$("#tab-blog-details" ).addClass( "is-hidden" );
					$("#tab-blog-finish" ).removeClass( "is-hidden" );
					btn.removeClass("btn-at-general").addClass("btn-at-finish is-hidden");
				}

			  });
		 }


		function validateDetails() {
			//~ $( "#tab-blog-details p.control" ).toggleClass( "is-loading" );

			var blogName = $("#tab-blog-details input[name='blog_name']");
			var blogTagLine = $("#blog_tag_line");
			var blogDescription = $("#tab-blog-details textarea[name='blog_description']");

			if ((! blogName.val()) && (! blogName.hasClass("is-danger"))) {
				blogName.toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>This field is required</p>" );
			}

			if ((! blogDescription.val()) && (! blogDescription.hasClass("is-danger"))) {
				blogDescription.toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>This field is required</p>" );
			}



			//~ $( "#tab-blog-details p.control" ).toggleClass( "is-loading" );

			//~ var fImage = featuredImage.getQueuedFiles();

			//~ if (!(fImage.length > 0)) {
				//~ $( ".featured-image-label" ).toggleClass( "is-hidden" );
			//~ }

			if ((blogName.val()) && (blogDescription.val())) {
				regdata.title = blogName.val();
				regdata.tag_line = blogTagLine.val();
				regdata.description = blogDescription.val();
				return true ;
			} else {
				return false ;
			}
		}

		$('#blog-edit-btn-control').click(function(e){
			e.preventDefault();
			if ($(this).hasClass("btn-at-general")) {
				$(this).prop("disabled", true);
				var r = validateDetails();
				if (r) {
					createBlogForm($(this));
				}
				$(this).prop("disabled", false);
				return;
			}
		});
 });
</script>
@endpush


