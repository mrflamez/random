@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<div class="columns is-paddingless is-marginless">
	<div class="column is-narrow">
	    @include('partials.adminmenu')
	</div>
	<div class="column">
		<section class="section p-t-0-5">
			 @include('partials.admin.projectheading')

		</section>

		<section class="section p-t-0-5">
			@include('partials.projectheader')
		</section>

		<section class="section p-t-0-5">
			@include('partials.admin.projectsubmenu',
					[
					'partials_general_active' => 'is-active' ,
					'partials_donations_active' => '' ,
					'partials_messages_active' => '' ,
					'partials_stats_active' => '' ,
					])
		</section>

		<section class="section p-t-0-5">
			<div class="columns">
				<div class="column">
					<figure class="image  featured">
						@foreach ($project -> images as $image)
						   @if ($image->featured)
							<img src="{{ asset('storage/'.$image->name) }}" alt="">
						   @endif
						@endforeach
					</figure>
				</div>
				<div class="column is-4">
					<div class="columns is-multiline">
						@foreach ($project -> images as $image)
						   @if (! $image->featured)
							<div class="column is-narrow">
								<div class="image is-128x128">
									<img src="{{ asset('storage/'.$image->name) }}" alt="">
								</div>
							</div>

						   @endif
						@endforeach
					</div>
				</div>
			</div>
		</section>

		<section class="section p-t-0-5">
		   <div class="content">
				{{  $project-> description  }}
		   </div>
		</section>
	</div>
</div>
@endsection

@include('partials.admin.projectfooter', ['partials_project_id' => $project->id ])





