@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<div class="columns is-paddingless is-marginless">
	<div class="column is-narrow">
	    @include('partials.adminmenu')
	</div>
	<div class="column">

			<section class="section p-t-0-5">
				  <div class="columns">
					  <div class=" column is-10 is-offset-1">
							<div class="heading  m-b-3 has-text-centered">
								<p class="title txt-green-yellow ">Edit Rule Regulation</p>
							</div>

							<div class=" columns " id="tab-faq-details">
								<div class=" column ">

									<div class="field">
									  <label class="label"> Title </label>
									  <p class="control has-icons-left has-icons-right">
										<input value="{{ $rule->title }}" name="faq_name" required class="input is-hovered" type="text" placeholder="Faq Title">
										<span class="icon is-small is-left">
										  <i class="fa fa-envelope"></i>
										</span>
									  </p>
									</div>

									<div class="field">
									  <label class="label">Description</label>
									  <p class="control has-icons-right">
										<textarea  name="faq_description"  class="textarea is-hovered" placeholder="Description">{!! $rule->content !!}</textarea>
									  </p>
									</div>
								</div>
						   </div>

							<div class=" columns is-hidden" id="tab-faq-finish">
									<div class=" column">
										<article class="v-centered">
											<div class="has-text-centered">
												<div class="field  has-addons has-addons-centered">
													  <p class="control">
														  <span class="icon is-large">
															  <i class="fa fa-home"></i>
															</span>
													 </p>
												</div>
												<p class="title is-4">Edit Success</p>
												<p class="title is-6">

												</p>


											</div>
										</article>
									</div>
							</div>

							<nav class="level is-mobile  m-t-1-5">
							  <div class="level-left">
								<div class="level-item">
									<div class="field m-t-1-5">
									  <p class="control">
										<button class="button is-medium is-primary btn-at-general" id="rule-edit-btn-control">Edit Faq</button>
									  </p>
									</div>
								</div>
							  </div>
							</nav>
					  </div>

				  </div>
			</section>

	</div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">

  var regdata = { };


  $(document).ready(function() {
		$( "input.input, textarea.textarea " ).focus(function() {
			if ($(this).hasClass("is-danger")) {
				$(this).removeClass("is-danger").next().remove();
				$(this).parent("p.control").next("p").remove();
			}
		});

		function createRuleForm(btn) {
			 console.log(regdata);
			 $.ajax({
			  headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			  },
			  method: 'PUT',
			  url: "{{ route('rules.update', ['id' => $rule->id])}}",
			  dataType: 'JSON',
			  data: regdata
			}).done(function(d) {
				console.log(d);
				if (d.success){
					    $("#tab-faq-details" ).addClass( "is-hidden" );
						$("#tab-faq-finish" ).removeClass( "is-hidden" );
						btn.removeClass("btn-at-general").addClass("btn-at-finish is-hidden");
				}

			  });
		 }


		function validateDetails() {
			var ruleName = $("#tab-faq-details input[name='faq_name']");
			var ruleDescription = $("#tab-faq-details textarea[name='faq_description']");

			if ((! ruleName.val()) && (! ruleName.hasClass("is-danger"))) {
				ruleName.toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>This field is required</p>" );
			}

			if ((! ruleDescription.val()) && (! ruleDescription.hasClass("is-danger"))) {
				ruleDescription.toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>This field is required</p>" );
			}

			if ((ruleName.val()) && (ruleDescription.val())) {
				regdata.title = ruleName.val();
				regdata.description = ruleDescription.val();
				return true ;
			} else {
				return false ;
			}
		}

		$('#rule-edit-btn-control').click(function(e){
			e.preventDefault();
			if ($(this).hasClass("btn-at-general")) {
				$(this).prop("disabled", true);
				var r = validateDetails();
				if (r) {
					createRuleForm($(this));
				}
				$(this).prop("disabled", false);
				return;
			}
		});
 });
</script>
@endpush



