@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<div class="columns is-paddingless is-marginless">
	<div class="column is-narrow">
	    @include('partials.adminmenu')
	</div>
	<div class="column">
         @include('partials.admin.userlevels')


		  <div class="tabs is-boxed is-fullwidth">
			@include('partials.singleuserheader', [
				'partials_project_active' => 'is-active' ,
				'partials_trans_active' => '' ,
				'partials_membership_active' => '' ,
				'partials_profile_active' => '' ,
				'partials_commisions_active' => '' ,
				'partials_banking_active' => '' ,
			])
		  </div>

		<section class="section p-t-0-5">
			@if (!$projects->isEmpty())
			<nav class="nav m-b-0-5">
				  <div class="nav-left">

				  </div>

				  <div class="nav-right">
					  <a class="nav-item"><strong>All</strong></a>
					  <a class="nav-item">Active</a>
					  <a class="nav-item">Pending</a>
					  <a class="nav-item">Deleted</a>
					  <a class="nav-item">Ended</a>
				  </div>
				</nav>
            @endif

				<div class="columns is-multiline">
				  @forelse ($projects as $project)
				      <div class="column is-4">
						@include('partials.admin.projectcard')
					  </div>
				  @empty
                      <div class="column">
                         	  <p class="title has-text-centered">No Projects</p>
					  </div>
				  @endforelse
				</div>
		</section>
	</div>
</div>
@endsection






