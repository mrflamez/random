@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<div class="columns is-paddingless is-marginless">
	<div class="column is-narrow">
	    @include('partials.adminmenu')
	</div>
	<div class="column">
         @include('partials.admin.userlevels')


		  <div class="tabs is-boxed is-fullwidth">
			@include('partials.singleuserheader', [
				'partials_project_active' => '' ,
				'partials_trans_active' => '' ,
				'partials_membership_active' => '' ,
				'partials_profile_active' => '' ,
				'partials_commisions_active' => 'is-active' ,
				'partials_banking_active' => '' ,
			])
		  </div>
        <section class="section p-t-0-5">
			<div class="columns">
			  <div class="column">
				@if( $user -> commisions -> isEmpty() )
					<div class="heading  m-b-3 has-text-centered">

						<p class="subtitle">Ref Code : <strong>{{ $user ->member_id }}</strong></p>
						<p class="title">No Commisions</p>
					</div>
				@else
					<div class="heading  m-b-3 has-text-centered">
						<p class="subtitle">Ref Code : <strong>{{ $user ->member_id }}</strong></p>
						<p class="title">Your Commisions</p>
					</div>
					<table class="table">
					  <thead>
						<tr>
						  <th><abbr title="Position"></abbr></th>
						  <th>Refer</th>
						  <th>Commission</th>
						  <th><abbr title="Date">Payment Ref</abbr></th>
						  <th><abbr title="Date">Status</abbr></th>
						  <th><abbr title="Date">Date</abbr></th>
						</tr>
					  </thead>

					  <tbody>
						@foreach ($user -> commisions as $commision)
							<tr>
							  <th> {{ $loop->iteration }}</th>
							  <td>{{ $commision->refer->email }} </td>
							  <td>{{ $commision->amount }}</td>
							  <td>{{ $commision->payment->reference }}</td>
							   <td>
								  @if ($commision->status)
									 <span class="tag is-primary">Paid</span>
								  @else
									<span class="tag is-warning">Pending</span>
								  @endif
							  </td>
							  <td>{{ $commision->created_at->toFormattedDateString() }}</td>
							</tr>
						@endforeach
					  </tbody>
					</table>

				@endif
			  </div>

			</div>
		</section>
	</div>
</div>
@endsection














