@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<div class="columns is-paddingless is-marginless">
	<div class="column is-narrow">
	    @include('partials.adminmenu')
	</div>
	<div class="column">
         @include('partials.admin.userlevels')


		  <div class="tabs is-boxed is-fullwidth">
			@include('partials.singleuserheader', [
				'partials_project_active' => '' ,
				'partials_trans_active' => '' ,
				'partials_membership_active' => 'is-active' ,
				'partials_profile_active' => '' ,
				'partials_commisions_active' => '' ,
				'partials_banking_active' => '' ,
			])
		  </div>
        <section class="section p-t-0-5">
			<div class="columns">
			  <div class="column">
				@if( $user -> activations -> isEmpty() )
					<div class="heading  m-b-3 has-text-centered">
						<h4 class="subtitle"><strong>No activations</strong></h4>
					</div>
				@else
					<div class="heading  m-b-3 has-text-centered">
						<h4 class="subtitle">Your activations</h4>
					</div>
					<table class="table">
					  <thead>
						<tr>
						  <th><abbr title="Position"></abbr></th>
						  <th>Activator</th>
						  <th>From Date</th>
						  <th>To Date</th>
						  <th><abbr title="Date">Payment Ref</abbr></th>
						  <th><abbr title="Date">Status</abbr></th>
						</tr>
					  </thead>

					  <tbody>
						@foreach ($user -> activations as $activation)
							<tr>
							  <th> {{ $loop->iteration }}</th>
							  <td>
								  @if($activation->payment)
								    <p class="subtitle is-bold">{{ $activation->activator->f_name }} {{  $activation->activator->l_name }}</p>
								  @else
									 <span class="tag is-warning">System Admin</span>
								  @endif
							  </td>
							  <td>{{ $activation->start_date->toFormattedDateString() }}</td>
							  <td>{{ $activation->end_date->toFormattedDateString() }}</td>
							  <td>
								  @if($activation->payment)
								   {{ $activation->payment->reference }}
								  @else
									 <span class="tag is-warning">{{ $activation->activator->email }}</span>
								  @endif
							   </td>
							   <td>
								  @if ($activation->status)
									 <span class="tag is-primary">Active</span>
								  @endif
							  </td>
							</tr>
						@endforeach
					  </tbody>
					</table>

				@endif
			  </div>
			  <div class="column is-3">
				  <h6 class="subtitle ">New Membership</h6>
				  <div class="field ">
						<label class="label">Start Date</label>
						<p class="control is-expanded has-icons-left">
							<input value="{{ $now ->copy() ->addDay()->toFormattedDateString() }}" id="membership_duration_start" class="input is-hovered" type="text" >
							<span class="icon is-small is-left">
							  <i class="fa fa-calendar"></i>
							</span>
					    </p>
					</div>
					<div class="field">
						<label class="label">End Date</label>
						<p class="control is-expanded has-icons-left">
							<input value="{{ $now ->copy() ->addYear()->toFormattedDateString() }}"  id="membership_duration_end" class="input is-hovered" type="text" >
							<span class="icon is-small is-left">
							  <i class="fa fa-calendar"></i>
							</span>
					    </p>
					</div>
					<div class="field">
					  <p class="control">
						<button class="button is-medium is-primary " id="create-membership-btn-control">Create</button>
					  </p>
					</div>
				</div>
			  </div>
			</div>
		</section>
	</div>
</div>
@endsection

@push('scripts')
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script src="{{ asset('js/pikaday.js') }}"></script>
<script type="text/javascript">
  var regdata = { };

  var startPicker = new Pikaday({
	  field: $('#membership_duration_start')[0],
	  format: 'D MMM YYYY',
	  minDate: moment("{{ $now ->copy() ->addDay()->toIso8601String() }}").toDate(),
	  onSelect: function() {
		console.log(this.getMoment().format('YYYY-MM-DD'));
		regdata.start = this.getMoment().format('YYYY-MM-DD');
	  }
	 });
  var endPicker = new Pikaday({
	  field: $('#membership_duration_end')[0],
	  format: 'D MMM YYYY',
	  minDate: moment("{{ $now ->copy() ->addYear()->toIso8601String() }}").toDate(),
	  onSelect: function() {
		console.log(this.getMoment().format('YYYY-MM-DD'));
		regdata.end = this.getMoment().format('YYYY-MM-DD');
	  }
	 });

  $(document).ready(function() {
		regdata.start = startPicker.toString('YYYY-MM-DD');
		regdata.end = endPicker.toString('YYYY-MM-DD');
		regdata.user_id = {{ $user->id }};
		console.log(regdata);

		$( "input.input, textarea.textarea " ).focus(function() {
			if ($(this).hasClass("is-danger")) {
				$(this).removeClass("is-danger").next().remove();
				$(this).parent("p.control").next("p").remove();
			}
		});

		function createMembershipForm(btn) {
			 console.log(regdata);
			 $.ajax({
			  headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			  },
			  method: 'POST',
			  url: "{{ route('activations.store')}}",
			  dataType: 'JSON',
			  data: regdata
			}).done(function(d) {
				console.log(d);
				if (d.success){
					btn.prop("disabled", false);
				}

			  });
		 }

		$('#create-membership-btn-control').click(function(e){
			e.preventDefault();
			$(this).prop("disabled", true);
			createMembershipForm($(this));
			return;
		});
 });
</script>
@endpush

@push('header-css')
<link href="{{ asset('css/pikaday.css') }}" rel="stylesheet" type="text/css">
@endpush












