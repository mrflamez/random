@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<div class="columns is-paddingless is-marginless">
	<div class="column is-narrow">
	    @include('partials.adminmenu')
	</div>
	<div class="column">
         @include('partials.admin.userlevels')


		  <div class="tabs is-boxed is-fullwidth">
			@include('partials.singleuserheader', [
				'partials_project_active' => '' ,
				'partials_trans_active' => '' ,
				'partials_membership_active' => '' ,
				'partials_profile_active' => 'is-active' ,
				'partials_commisions_active' => '' ,
				'partials_banking_active' => '' ,
			])
		  </div>

		<section class="section p-t-0-5">
			<div class="columns">
			  <div class="column is-5">
				  @forelse ($user -> images as $image)
					   @if ($loop->first)
						  <figure class="image is-2by1">
							  <img src="{{ asset('storage/'.$image->name) }}" alt="">
						  </figure>
						  <hr>
					   @endif
					@empty

					@endforelse

					<div class="field is-grouped" id="profile-btns-cont">
					  <p class="control">
						<button id="profile-edit-btn-control" class="button is-primary">
						  Edit
						</button>
					  </p>
					  @if ($user->is_admin)
                      <p class="control">
						<button id="profile-remove-admin-btn-control" class="button is-warning">
						  Remove Admin
						</button>
					  </p>
					 @else
						<p class="control">
						<button id="profile-make-admin-btn-control" class="button is-warning">
						  Make Admin
						</button>
					  </p>
					 @endif
					  <p class="control is-hidden">
						<button id="profile-save-btn-control" class="button  is-info">
						  Save Edits
						</button>
					  </p>
					  <p class="control">
						<button id="profile-suspend-btn-control" class="button  is-warning">
                           Suspend
						</button>
					  </p>
					  <p class="control">
						<button id="profile-delete-btn-control"  class="button  is-danger">
						  Delete
						</button>
					  </p>

					</div>

			  </div>
			  <div class="column" id="tab-profile-details">
				    <div class=" columns ">
						<div class=" column">
							<div class="field">
							  <label class="label">First Name </label>
							  <p class="control has-icons-left has-icons-right">
								<input value="{{ $user->f_name }}" disabled class="input is-hovered" name="f-name" required type="text" placeholder="First Name">
								<span class="icon is-small is-left">
								  <i class="fa fa-user-circle-o"></i>
								</span>
							  </p>
							</div>
						</div>
						<div class=" column">
							<div class="field">
							  <label class="label">Last Name </label>
							  <p class="control has-icons-left has-icons-right">
								<input value="{{ $user->l_name }}" disabled name="l-name" required  class="input is-hovered" type="text" placeholder="Last Name">
								<span class="icon is-small is-left">
								  <i class="fa fa-user-circle-o"></i>
								</span>
							  </p>
							</div>

						</div>
					</div>
					 <div class=" columns ">
						<div class=" column">
							<div class="field">
							  <label class="label">Email Address </label>
							  <p class="control has-icons-left has-icons-right">
								<input value="{{ $user->email }}" disabled class="input is-hovered" name="email" type="email" placeholder="Email">
								<span class="icon is-small is-left">
								  <i class="fa fa-envelope"></i>
								</span>
							  </p>
							</div>
						</div>
						<div class=" column">
							<div class="field">
							  <label class="label">UserName </label>
							  <p class="control has-icons-left has-icons-right">
								<input value="{{ $user->username }}" disabled class="input is-hovered" name="username" type="text" placeholder="UserName">
								<span class="icon is-small is-left">
								  <i class="fa fa-user"></i>
								</span>
							  </p>
							</div>
						</div>
					</div>
					<div class=" columns">
						<div class=" column is-half">
							<div class="field">
							  <label class="label">Country </label>
							  <p class="control has-icons-left">
								<span class="select">
								 <select disabled id="user_country">
									  @foreach ($countries as $country)
										<option value="{{ $country->id }}"
										@if(!$user -> countries ->isEmpty())
											@if($country->id == $user -> countries ->first() ->id)
											 selected
											@endif
										@endif
										> {{ $country->name }}</option>
									  @endforeach
								  </select>
								</span>
								<span class="icon is-small is-left">
								  <i class="fa fa-globe"></i>
								</span>
							  </p>
							</div>
						</div>
						<div class=" column">
							<div class="field">
							  <label class="label">Address </label>
							  <p class="control has-icons-left has-icons-right">
								<input value="{{ $user->address }}" disabled name="address" required class="input is-hovered" type="text" placeholder="Address">
								<span class="icon is-small is-left">
								  <i class="fa fa-map-marker"></i>
								</span>
							  </p>
							</div>
						</div>
					</div>
					<div class=" columns">

						<div class=" column">
							<div class="field">
							  <label class="label">Telephone </label>
							  <p class="control has-icons-left has-icons-right">
								<input value="{{ $user->phone }}" disabled name="telephone" required class="input is-hovered" type="text" placeholder="Telephone">
								<span class="icon is-small is-left">
								  <i class="fa fa-mobile"></i>
								</span>
							  </p>
							</div>

						</div>
					</div>
			  </div>
			</div>

		</section>
	</div>
</div>
@endsection

@push('header-css')
<link rel="stylesheet"  href="//cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.5/sweetalert2.min.css" >
@endpush

@push('scripts')
<script src="//cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.5/sweetalert2.min.js"></script>
<script type="text/javascript">

  var regdata = { };


  $(document).ready(function() {
		$( "input.input, textarea.textarea " ).focus(function() {
			if ($(this).hasClass("is-danger")) {
				$(this).removeClass("is-danger").next().remove();
				$(this).parent("p.control").nextAll("p").remove();
			}
		});

		function btnControlFunc() {
            var btnsCont = $('#profile-btns-cont');
			var btnSave = $('#profile-save-btn-control');
            btnsCont.children('p').not('.is-hidden').addClass('is-hidden');
            btnSave.parent("p.control").removeClass('is-hidden');
            $('#tab-profile-details input.input , #tab-profile-details select').prop("disabled", false);
            $("#tab-profile-details input[name='f-name']").focus();
		}

		function btnControlFuncHide() {
			var btnsCont = $('#profile-btns-cont');
            btnsCont.children('p').toggleClass('is-hidden');
            $('#tab-profile-details input.input, #tab-profile-details select').prop("disabled", true);
		}

		function editUserForm() {
			 console.log(regdata);
			 $.ajax({
			  headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			  },
			  method: 'PUT',
			  url: "{{ route('users.update', ['id' => $user->id])}}",
			  dataType: 'JSON',
			  data: regdata
			}).done(function(d) {
				console.log(d);
				if (d.success){
					if (d.message === 'edit'){
						swal(
						'Success!',
						'User Edited !',
						'success'
					  );
					  btnControlFuncHide();
					}

					if (d.message === 'suspend') {
						swal(
						'Success!',
						'User Suspended !',
						'success'
					  );
					}

				}

			  });
		 }

		function makeAdminUserForm() {
			 $.ajax({
			  headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			  },
			  method: 'PUT',
			  url: "{{ route('users.update', ['id' => $user->id])}}",
			  dataType: 'JSON',
			  data: {action : "admin_perms" , type: "makeadmin"}
			}).done(function(d) {
				console.log(d);
				if (d.success){
					if (d.message === 'success'){
						swal(
						'Success!',
						'User Is Now Admin !',
						'success'
					  );
					}

				}

			  });
		 }

		function removeAdminUserForm() {
			 $.ajax({
			  headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			  },
			  method: 'PUT',
			  url: "{{ route('users.update', ['id' => $user->id])}}",
			  dataType: 'JSON',
			  data: {action : "admin_perms" , type: "removeadmin"}
			}).done(function(d) {
				console.log(d);
				if (d.success){
					if (d.message === 'success'){
						swal(
						'Success!',
						'User Is Now Not Admin !',
						'success'
					  );
					}
				}

			});
		 }

		function deleteUserForm() {
			 $.ajax({
			  headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			  },
			  method: 'DELETE',
			  url: "{{ route('users.destroy', ['id' => $user->id])}}",
			  dataType: 'JSON',
			}).done(function(d) {
				console.log(d);
				if (d.success){
						swal(
						'Success!',
						'User Deleted !',
						'success'
					  );
					  $('#profile-btns-cont').children('p').addClass('is-hidden');
				}

			  });
		 }

	    function validateDetails() {

            var fName = $("#tab-profile-details input[name='f-name']");
			var lName = $("#tab-profile-details input[name='l-name']");
			var address = $("#tab-profile-details input[name='address']");
			var telephone = $("#tab-profile-details input[name='telephone']");
			var email = $("#tab-profile-details input[name='email']");
			var username = $("#tab-profile-details input[name='username']");
			var userCountry = $("#user_country");

			if ((! email.val()) && (! email.hasClass("is-danger"))) {
				email.toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>This field is required</p>" );
			}
			if ((! username.val()) && (! username.hasClass("is-danger"))) {
				username.toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>This field is required</p>" );
			}
			if ((! fName.val()) && (! fName.hasClass("is-danger"))) {
				fName.toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>This field is required</p>" );
			}
			if ((! lName.val()) && (! lName.hasClass("is-danger"))) {
				lName.toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>This field is required</p>" );
			}
			if ((! address.val()) && (! address.hasClass("is-danger"))) {
				address.toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>This field is required</p>" );
			}
			if ((! telephone.val()) && (! telephone.hasClass("is-danger"))) {
				telephone.toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>This field is required</p>" );
			}

			if (fName.val() && lName.val() && telephone.val() && email.val() && username.val()) {
				regdata.fname = fName.val();
				regdata.lname = lName.val();
				regdata.address = address.val();
				regdata.phone = telephone.val();
				regdata.email = email.val();
				regdata.username = username.val();
				regdata.country = userCountry.val();
				regdata.action = "admin_edit";

				return true ;
			} else {
				return false ;
			}
		 }


		$('#profile-edit-btn-control').click(function(e){
			e.preventDefault();
			btnControlFunc();
		});
		$('#profile-suspend-btn-control').click(function(e){
			e.preventDefault();
			swal({
			  title: 'Are you sure?',
			  text: "",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Yes Suspend'
			}).then(function () {
				regdata.action = "admin_suspend";
				editUserForm();
			}, function () {});
		});

		$('#profile-delete-btn-control').click(function(e){
			e.preventDefault();
			swal({
			  title: 'Are you sure?',
			  text: "",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Yes Delete'
			}).then(function () {
				deleteUserForm();
			}, function () {});
		});

		$('#profile-make-admin-btn-control').click(function(e){
			e.preventDefault();
			swal({
			  title: 'Are you sure?',
			  text: "",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Yes Make Admin'
			}).then(function () {
				makeAdminUserForm();
			}, function () {});
		});

		$('#profile-remove-admin-btn-control').click(function(e){
			e.preventDefault();
			swal({
			  title: 'Are you sure?',
			  text: "",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Yes Remove Admin'
			}).then(function () {
				removeAdminUserForm();
			}, function () {});
		});

		$('#profile-save-btn-control').click(function(e){
			e.preventDefault();
			$(this).prop("disabled", true);
			var r = validateDetails();
			console.log(r);
			console.log(regdata);
			if (r) {

				swal({
				  title: 'Are you sure?',
				  text: "",
				  type: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Yes Continue'
				}).then(function () {
					editUserForm();
				}, function () {});

			}
			$(this).prop("disabled", false);
			return;

		});
 });
</script>
@endpush






