@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<div class="columns is-paddingless is-marginless">
	<div class="column is-narrow">
	    @include('partials.adminmenu')
	</div>
	<div class="column">
         @include('partials.admin.userlevels')


		  <div class="tabs is-boxed is-fullwidth">
			@include('partials.singleuserheader', [
				'partials_project_active' => '' ,
				'partials_trans_active' => 'is-active' ,
				'partials_membership_active' => '' ,
				'partials_profile_active' => '' ,
				'partials_commisions_active' => '' ,
				'partials_banking_active' => '' ,
			])
		  </div>
        <section class="section p-t-0-5">
			<div class="columns">
			  <div class="column">
				@if( $payments -> isEmpty() )

					<div class="heading  m-b-3 has-text-centered">
						<h4 class="subtitle"><strong>No Contributions Made</strong></h4>
					</div>
				@else
					<div class="heading  m-b-3 has-text-centered">
						<h4 class="subtitle">Your Transactions</h4>
					</div>
					<table class="table">
					  <thead>
						<tr>
						  <th><abbr title="Position"></abbr></th>
						  <th>Payee</th>
						  <th>Transaction Type</th>
						  <th>Transaction Id</th>
						  <th>Transaction Ref</th>
						  <th><abbr title="Currency">Currency</abbr></th>
						  <th><abbr title="Amount">Amount</abbr></th>
						  <th><abbr title="Date">Date</abbr></th>
						</tr>
					  </thead>

					  <tbody>
						@foreach ($payments as $payment)
						  @foreach ($payment -> payments as $trans)
							<tr>
							  <th> {{ $loop->iteration }}</th>
							  <td>{{ $payment->first_name }} {{ $payment->last_name }}</td>
							  <td>
								  @if ($trans->is_membership)
									 <span class="tag is-warning">Membership Fee</span>
								  @elseif ($trans ->is_donations)
									 <span class="tag is-primary">Donations</span>
								  @else
								  @endif
							  </td>
							  <td>{{ $trans->tranx_id }}</td>
							  <td>{{ $trans->reference }}</td>
							  <td>{{ $trans->currency }}</td>
							  <td>{{ $trans->amount/100 }}</td>
							  <td>{{ $trans->created_at->toFormattedDateString() }}</td>
							</tr>
						  @endforeach
						@endforeach
					  </tbody>
					</table>
				@endif
			  </div>

			</div>
		</section>
	</div>
</div>
@endsection







