
@extends('layouts.app')

@section('title', 'Login')

@section('content')
<section class="hero is-small header-overlay-bg1 is-bold">
  <div class="hero-body bg-gray">
    <div class="container">
		<div class="heading  m-b-3 has-text-centered">
			<h3 class="title txt-green-yellow ">Login</h3>
			<h5 class="subtitle txt-green-yellow"> </h5>
		</div>
    </div>
  </div>
</section>

<section class="section">
	<div class=" columns">

		<div class=" column is-6 is-offset-3">
			<form name="login-form" class="clearfix" role="form" method="POST" action="{{ route('login') }}" >
				{{ csrf_field() }}
			<h5 class="title has-text-centered txt-green-yellow ">Login </h5>
			<div class="field">
			  <label class="label">Email </label>
			  <p class="control has-icons-left has-icons-right">
				<input class="input is-hovered" id="email" name="email" type="email" placeholder="Email">
				<span class="icon is-small is-left">
				  <i class="fa fa-envelope"></i>
				</span>
			  </p>
			  @if ($errors->has('email'))
				<p class="help is-danger">{{ $errors->first('email') }}</p>
			 @endif
			</div>
			<div class="field">
			  <label class="label">Password </label>
			  <p class="control has-icons-left has-icons-right">
				<input class="input is-hovered" id="password" name="password" type="password" placeholder="****">
				<span class="icon is-small is-left">
				  <i class="fa fa-lock"></i>
				</span>
			  </p>
			  @if ($errors->has('password'))
				<p class="help is-danger">{{ $errors->first('password') }}</p>
			 @endif
			</div>
			<div class="field">
			  <p class="control">
				  <a href="{{ route('password.request') }}">Reset Password</a>

			  </p>
			</div>
			<div class="field">
			  <p class="control">
				<button type="submit" class="button is-success">
				 Login
				</button>
			  </p>
			</div>
			</form>
		</div>


</div>
</section>

@endsection

@push('scripts')

</script>
@endpush
