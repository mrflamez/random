@extends('layouts.app')

@section('title', 'Reset Password')

@section('content')
<section class="hero is-small header-overlay-bg1 is-bold">
  <div class="hero-body bg-gray">
    <div class="container">
		<div class="heading  m-b-3 has-text-centered">
			<h3 class="title txt-green-yellow ">Reset Password</h3>
			<h5 class="subtitle txt-green-yellow"> </h5>
		</div>
    </div>
  </div>
</section>

<section class="section">
	<div class=" columns">

		<div class=" column is-6 is-offset-3">
			@if (session('status'))
				<div class="notification is-success">
					{{ session('status') }}
				</div>
			@else

			<form name="login-form" class="clearfix" role="form" method="POST" action="{{ route('password.email') }}" >
				{{ csrf_field() }}
			<div class="field">
			  <label class="label">Email </label>
			  <p class="control has-icons-left has-icons-right">
				<input class="input is-hovered" id="email" name="email" type="email" value="{{ old('email') }}" required placeholder="Email">
				<span class="icon is-small is-left">
				  <i class="fa fa-envelope"></i>
				</span>
			  </p>
			  @if ($errors->has('email'))
				<p class="help is-danger">{{ $errors->first('email') }}</p>
			 @endif
			</div>
			<div class="field">
			  <p class="control">
				<button type="submit" class="button is-success">
				 Send Password Reset Link
				</button>
			  </p>
			</div>
			</form>
			@endif
		</div>


</div>
</section>
@endsection
