@extends('layouts.app')

@section('content')
<section class="hero is-small header-overlay-bg1 is-bold">
  <div class="hero-body bg-gray">
    <div class="container">
		<div class="heading  m-b-3 has-text-centered">
			<h3 class="title txt-green-yellow ">Reset Password</h3>
			<h5 class="subtitle txt-green-yellow"> </h5>
		</div>
    </div>
  </div>
</section>

<section class="section">
	<div class=" columns">

		<div class=" column is-6 is-offset-3">
			@if (session('status'))
				<div class="notification is-success">
					{{ session('status') }}
				</div>
			@else

			<form  class="clearfix" role="form" method="POST" action="{{ route('password.request') }}" >
				{{ csrf_field() }}

			<input type="hidden" name="token" value="{{ $token }}">

			<div class="field">
			  <label class="label">Email </label>
			  <p class="control has-icons-left has-icons-right">
				<input class="input is-hovered" id="email" name="email"  type="email" placeholder="Email" value="{{ $email or old('email') }}" required autofocus>
				<span class="icon is-small is-left">
				  <i class="fa fa-envelope"></i>
				</span>
			  </p>
			  @if ($errors->has('email'))
				<p class="help is-danger">{{ $errors->first('email') }}</p>
			 @endif
			</div>
			<div class="field">
			  <label class="label">Password </label>
			  <p class="control has-icons-left has-icons-right">
				<input class="input is-hovered" id="password" name="password" type="password" placeholder="****">
				<span class="icon is-small is-left">
				  <i class="fa fa-lock"></i>
				</span>
			  </p>
			  @if ($errors->has('password'))
				<p class="help is-danger">{{ $errors->first('password') }}</p>
			 @endif
			</div>

			<div class="field">
			  <label class="label">Confirm Password </label>
			  <p class="control has-icons-left has-icons-right">
				<input class="input is-hovered" id="password_confirmation" name="password_confirmation" type="password" placeholder="****">
				<span class="icon is-small is-left">
				  <i class="fa fa-lock"></i>
				</span>
			  </p>
			  @if ($errors->has('password_confirmation'))
				<p class="help is-danger">{{ $errors->first('password_confirmation') }}</p>
			 @endif
			</div>

			<div class="field">
			  <p class="control">
				<button type="submit" class="button is-success">
				 Reset Password
				</button>
			  </p>
			</div>
			</form>
			@endif
		</div>


</div>
</section>

@endsection
