@extends('layouts.app')

@section('title', 'Blog And News')

@section('content')
<section class="hero is-small header-overlay-bg1 is-bold">
  <div class="hero-body bg-gray">
    <div class="container">
		<div class="heading  m-b-3 has-text-centered">
			<h3 class="title txt-green-yellow ">Blog And News</h3>
			<h5 class="subtitle txt-green-yellow"> </h5>
		</div>
    </div>
  </div>
</section>

<section class="section">
	<div class=" columns">
		<div class=" column">
			@forelse ($posts as $post)
			   @include('partials.postsingledefault', ['post' => $post])
			@empty
                <div class="heading  m-b-3 has-text-centered">
					<h4 class="subtitle"><strong>No Posts</strong></h4>
				</div>
			@endforelse
			<div class=" columns">
				<div class=" column">
						{{ $posts->links('vendor.pagination.default') }}
				</div>
			</div>
		</div>
		<div class=" column is-4">
		</div>
    </div>
</section>

@endsection



