@extends('layouts.app')

@section('title', $post-> title)

@section('content')
<section class="hero is-small header-overlay-bg1 is-bold">
  <div class="hero-body bg-gray">
    <div class="container">
		<div class="heading  m-b-3 has-text-centered">
			<h3 class="title txt-green-yellow ">{{ $post-> title }}</h3>
			<h5 class="subtitle txt-green-yellow"> {{ $post-> tag_line }}.</h5>
		</div>
    </div>
  </div>
</section>

<section class="section">
	<div class=" columns">
		<div class=" column">
			@forelse ($post -> images as $image)
			   @if ($loop->first)
			   <figure class="image is-2by1">
				<img src="{{ asset('storage/'.$image->name) }}" alt="">
			   </figure>
			   @endif
			@empty

			@endforelse
			<div class="content m-t-1-5">
				{!!  $post-> description  !!}
		   </div>

		</div>
		<div class=" column is-4">
		</div>
    </div>
</section>

@endsection




