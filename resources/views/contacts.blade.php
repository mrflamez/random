
@extends('layouts.app')

@section('title', 'Contact')

@section('content')
<section class="hero is-small header-overlay-bg1 is-bold">
  <div class="hero-body bg-gray">
    <div class="container">
		<div class="heading  m-b-3 has-text-centered">
			<h3 class="title txt-green-yellow ">Contact Us</h3>
			<h5 class="subtitle txt-green-yellow"> </h5>
		</div>
    </div>
  </div>
</section>

<section class="section">

    <div class=" columns has-text-centered">
		<div class="column is-4">

			<div class="field  has-addons has-addons-centered">
				  <p class="control">
					<span class="icon is-medium">
					  <i class="fa fa-phone"></i>
					</span>
				 </p>
			</div>
			<h5 class="title is-bold">Call Us</h5>
			<h6 class="subtitle "> Phone: +254 695 2601</h6>
	    </div>
		<div class="column is-4">

			<div class="field  has-addons has-addons-centered">
				  <p class="control">
					<span class="icon is-medium">
					  <i class="fa fa-map-marker"></i>
					</span>
				 </p>
			</div>
			<h5 class="title is-bold"> Address</h5>
			<h6 class="subtitle "> 1314 Mombasa , Kenya</h6>
	    </div>
		<div class="column is-4">

			<div class="field  has-addons has-addons-centered">
				  <p class="control">
					<span class="icon is-medium">
					  <i class="fa fa-envelope"></i>
					</span>
				 </p>
			</div>
			<h5 class="title is-bold"> Email</h5>
			<h6 class="subtitle "> you@yourdomain.com</h6>

	    </div>

    </div>
</section>

<section class="section">
	<div class="container">
      <div class="heading m-b-3 has-text-centered">
        <h3 class="title">Send  Mail</h3>
        <h5 class="subtitle"> </h5>
      </div>
    </div>
    <div class=" columns">
		<div class=" column">
				<div class="field">
				  <label class="label">Name</label>
				  <p class="control has-icons-left">
					<input class="input" type="text" placeholder="Name ">
					<span class="icon is-small is-left">
					  <i class="fa fa-envelope"></i>
					</span>
				  </p>
				</div>
				<div class="field">
				  <label class="label">Email</label>
				  <p class="control has-icons-left">
					<input class="input " type="text" placeholder="Email " >
					<span class="icon is-small is-left">
					  <i class="fa fa-envelope"></i>
					</span>

				  </p>
				</div>

				<div class="field">
				  <label class="label">Subject</label>
				  <p class="control">
					<span class="select">
					  <select>
						<option>Select dropdown</option>
						<option>With options</option>
					  </select>
					</span>
				  </p>
				</div>
		</div>
		<div class=" column">
			<div class="field">
			  <label class="label">Message</label>
			  <p class="control">
				<textarea class="textarea" placeholder="Textarea"></textarea>
			  </p>
			</div>

			<div class="field">
			  <p class="control">
				<label class="checkbox">
				  <input type="checkbox">
				  I agree to the <a href="#">terms and conditions</a>
				</label>
			  </p>
			</div>



			<div class="field is-grouped">
			  <p class="control">
				<button class="button is-primary">Submit</button>
			  </p>
			  <p class="control">
				<button class="button is-link">Cancel</button>
			  </p>
			</div>
		</div>
	</div>
</section>
<section class="columns is-gapless is-paddingless is-marginless">
    <div class=" column contact-map">

	</div>
</section>
@endsection

