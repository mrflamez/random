@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<div class="columns is-gapless is-paddingless is-marginless">
	<div class="column">
		<div class="profile-options p-t-1">
		  <div class="tabs is-fullwidth">
			 @include('layouts.dashheader')
		  </div>
		</div>
	</div>
</div>

<section class="section">
	<div class="columns">
	  <div class="column is-6 is-offset-3" id="tab-bank-details">

				<div class="heading  m-b-3 has-text-centered">
					<p class="title">Banking Info</p>
				</div>
				<div class="field">
				  <label class="label">Bank Name </label>
				  <p class="control has-icons-left has-icons-right">
					<input value="{{ $user->banking->bank_name }}"  class="input is-hovered" name="bank-name" required type="text" placeholder="bank Name">
					<span class="icon is-small is-left">
					  <i class="fa fa-bank"></i>
					</span>
				  </p>
				</div>
				<div class="field">
				  <label class="label">Account Name </label>
				  <p class="control has-icons-left has-icons-right">
					<input value="{{ $user->banking->account_name }}"   class="input is-hovered" name="account-name" required type="text" placeholder="Account Name">
					<span class="icon is-small is-left">
					  <i class="fa fa-user"></i>
					</span>
				  </p>
				</div>
				<div class="field">
				  <label class="label">Account Number </label>
				  <p class="control has-icons-left has-icons-right">
					<input value="{{ $user->banking->account_num }}"  class="input is-hovered" name="account-num" required type="text" placeholder="Account Number">
					<span class="icon is-small is-left">
					  <i class="fa fa-user"></i>
					</span>
				  </p>
				</div>
				<p class="control ">
				<button id="banking-save-btn-control" class="button  is-info">
				  Update
				</button>
			  </p>
		  </div>

	</div>

</section>
@endsection

@push('header-css')
<link rel="stylesheet"  href="//cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.5/sweetalert2.min.css" >
@endpush

@push('scripts')
<script src="//cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.5/sweetalert2.min.js"></script>
<script type="text/javascript">

  var regdata = { };


  $(document).ready(function() {
		$( "input.input, textarea.textarea " ).focus(function() {
			if ($(this).hasClass("is-danger")) {
				$(this).removeClass("is-danger").next().remove();
				$(this).parent("p.control").nextAll("p").remove();
				$('#banking-save-btn-control').prop("disabled", false);
			}
		});




		function editUserBank() {
			 console.log(regdata);
			 $.ajax({
			  headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			  },
			  method: 'PUT',
			  url: "{{ route('users.update', ['id' => $user->id])}}",
			  dataType: 'JSON',
			  data: regdata
			}).done(function(d) {
				console.log(d);
				if (d.success){
					swal(
					'Success!',
					'Banking Info Updated !',
					'success'
				  );
				}

			  });
		 }


	    function validateDetails() {

            var bName = $("#tab-bank-details input[name='bank-name']");
			var accName = $("#tab-bank-details input[name='account-name']");
			var accNum = $("#tab-bank-details input[name='account-num']");


			if ((! bName.val()) && (! bName.hasClass("is-danger"))) {
				bName.toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>This field is required</p>" );
			}
			if ((! accName.val()) && (! accName.hasClass("is-danger"))) {
				accName.toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>This field is required</p>" );
			}
			if ((! accNum.val()) && (! accNum.hasClass("is-danger"))) {
				accNum.toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>This field is required</p>" );
			}


			if (bName.val() && accName.val() && accNum.val()) {
				regdata.bank_name = bName.val();
				regdata.account_name = accName.val();
				regdata.account_num = accNum.val();
				regdata.action = "bank_edit";

				return true ;
			} else {
				return false ;
			}
		 }

		$('#banking-save-btn-control').click(function(e){
			e.preventDefault();
			$(this).prop("disabled", true);
			var r = validateDetails();
			if (r) {

				swal({
				  title: 'Are you sure?',
				  text: "",
				  type: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Yes Continue'
				}).then(function () {
					editUserBank();
				}, function () {});

			}
			$(this).prop("disabled", false);
			return;

		});
 });
</script>
@endpush





















