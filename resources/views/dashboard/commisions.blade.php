@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<div class="columns is-gapless is-paddingless is-marginless">
	<div class="column">
		<div class="profile-options p-t-1">
		  <div class="tabs is-fullwidth">
			 @include('layouts.dashheader')
		  </div>
		</div>
	</div>
</div>

<section class="section">
	<div class="columns">
	  <div class="column">
			@if( $user -> commisions -> isEmpty() )
				<div class="heading  m-b-3 has-text-centered">

					<p class="subtitle">Ref Code : <strong>{{ $user ->member_id }}</strong></p>
					<p class="title">No Commisions</p>
				</div>
			@else
				<div class="heading  m-b-3 has-text-centered">
					<p class="subtitle">Ref Code : <strong>{{ $user ->member_id }}</strong></p>
					<p class="title">Your Commisions</p>
				</div>
				<table class="table">
				  <thead>
					<tr>
					  <th><abbr title="Position"></abbr></th>
					  <th>Refer</th>
					  <th>Commission</th>
					  <th><abbr title="Date">Payment Ref</abbr></th>
					  <th><abbr title="Date">Status</abbr></th>
					  <th><abbr title="Date">Date</abbr></th>
					</tr>
				  </thead>

				  <tbody>
					@foreach ($user -> commisions as $commision)
						<tr>
						  <th> {{ $loop->iteration }}</th>
						  <td>{{ $commision->refer->email }} </td>
						  <td>{{ $commision->amount }}</td>
						  <td>{{ $commision->payment->reference }}</td>
						   <td>
							  @if ($commision->status)
								 <span class="tag is-primary">Paid</span>
							  @else
								<span class="tag is-warning">Pending</span>
							  @endif
						  </td>
						  <td>{{ $commision->created_at->toFormattedDateString() }}</td>
						</tr>
					@endforeach
				  </tbody>
				</table>
			@endif
		  </div>
	</div>

</section>
@endsection










