

@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<div class="columns is-gapless is-paddingless is-marginless">
	<div class="column">
		<div class="profile-options p-t-1">
		  <div class="tabs is-fullwidth">
			@include('layouts.dashheader')
		  </div>
		</div>
	</div>
</div>

<section class="columns">
	<div class=" column is-8 is-offset-2">
		<div class="heading  m-b-3 has-text-centered">
			<h6 class="title txt-green-yellow ">New Cause</h5>

		</div>

		<div class=" columns " id="tab-cause-details">
			<div class=" column ">
				<div class="field">
				  <label class="label">Cause Title </label>
				  <p class="control has-icons-left has-icons-right">
					<input name="cause_name" required class="input is-hovered" type="text" placeholder="Cause Title">
					<span class="icon is-small is-left">
					  <i class="fa fa-folder"></i>
					</span>
				  </p>
				</div>
				<div class="field-body m-b-0-7-5">
					<div class="field ">
						<label class="label">Start Date</label>
						<p class="control is-expanded has-icons-left">
							<input value="{{ $now ->copy() ->addDay()->toFormattedDateString() }}" id="cause_duration_start" class="input is-hovered" type="text" >
							<span class="icon is-small is-left">
							  <i class="fa fa-calendar"></i>
							</span>
					    </p>
					</div>
					<div class="field">
						<label class="label">End Date</label>
						<p class="control is-expanded has-icons-left">
							<input value="{{ $now ->copy() ->addWeek()->toFormattedDateString() }}"  id="cause_duration_end" class="input is-hovered" type="text" >
							<span class="icon is-small is-left">
							  <i class="fa fa-calendar"></i>
							</span>
					    </p>
					</div>
				</div>
				<div class="field ">
					<label class="label">Target Amount</label>
					<p class="control  has-icons-left has-icons-right">
						<input  id="cause_goal" name="cause_goal" class="input is-hovered" placeholder="1000" type="number" >
						<span class="icon is-small is-left">
						  <i class="fa fa-money"></i>
						</span>
				    </p>
				</div>
				<div class="field ">
					<label class="label">Category</label>
					<p class="control has-icons-left has-icons-right">
						<span class="select">
						  <select id="cause_category" name="cause_category">
							  @foreach ($categories as $category)
									<option value="{{ $category -> id }}" >{{ $category -> name }}</option>
							  @endforeach
						  </select>
						</span>
						<span class="icon is-small is-left">
						  <i class="fa fa-cogs"></i>
						</span>
				   </p>
				</div>
				<div class="field">
				  <label class="label">Description</label>
				  <p class="control has-icons-right">
					<textarea  name="cause_description"  class="textarea is-hovered" placeholder="Description"></textarea>
				  </p>
				</div>
			</div>
			<div class=" column is-5">
					<label class="label m-b-1-5 is-marginless">Featured Image </label>
					<p class="help is-danger featured-image-label is-hidden">This field is required</p>
					<div  class="columns full-40-h is-marginless">
						<div  class="column">
							<div id="featured-image"  class=" dropzone">

						    </div>
						</div>
					</div>
					<label class="label m-b-1-5 is-marginless">Gallery Images </label>
					<p class="help is-danger gallery-images-label is-hidden">This field is required</p>
					<div  class="columns full-40-h is-marginless">
						<div  class="column">
							<div id="gallery-images"  class=" dropzone">

						    </div>
						</div>
					</div>
			</div>
		</div>

		<div class=" columns is-hidden" id="tab-cause-finish">
				<div class=" column">
					<article class="v-centered">
						<div class="has-text-centered">
							<div class="field  has-addons has-addons-centered">
								  <p class="control">
									  <span class="icon is-large">
										  <i class="fa fa-check-circle-o"></i>
										</span>
								 </p>
							</div>
							<p class="title is-4">Congrtulations</p>
							<p class="title is-6">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean efficitur.
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean efficitur.

							</p>

							<div class="field  has-addons has-addons-centered">
								  <p class="control">
									  <a class=" button">
										View Cause
									  </a>
								 </p>
							</div>
						</div>
					</article>
				</div>
		</div>

		<nav class="level is-mobile b-tp m-t-1-5">
		  <div class="level-left"></div>
		  <div class="level-right">
			<div class="level-item">
				<div class="field m-t-1-5">
				  <p class="control">
					<button class="button is-medium is-primary btn-at-general" id="cause-create-btn-control">Next</button>
				  </p>
				</div>
		    </div>
		  </div>
		</nav>
	</div>
</section>

@endsection


@push('scripts')
<script src="{{ asset('js/dropzone.js') }}"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script src="{{ asset('js/pikaday.js') }}"></script>
<script type="text/javascript">
  Dropzone.autoDiscover = false;
  var startdateIn ;
  var enddateIn ;
  var regdata = { };

  var featuredImage = new Dropzone("#featured-image" , {
    paramName: "file",
    maxFilesize: 1,
    headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	},
	url: "{{ route('photos.store')}}",
	addRemoveLinks : true,
	autoProcessQueue: false,
	uploadMultiple: false,
    parallelUploads: 1
  });
  var galleryImages = new Dropzone("#gallery-images" , {
    paramName: "file",
    maxFilesize: 1,
    headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	},
	url: "{{ route('photos.store')}}",
	addRemoveLinks : true,
	autoProcessQueue: false,
	uploadMultiple: false,
    parallelUploads: 1
  });

  var startPicker = new Pikaday({
	  field: $('#cause_duration_start')[0],
	  format: 'D MMM YYYY',
	  minDate: moment("{{ $now ->copy() ->addDay()->toIso8601String() }}").toDate(),
	  maxDate: moment("{{ $now ->copy() ->addMonth()->toIso8601String() }}").toDate(),
	  onSelect: function() {
		console.log(this.getMoment().format('YYYY-MM-DD'));
		regdata.start = this.getMoment().format('YYYY-MM-DD');
	  }
	 });
  var endPicker = new Pikaday({
	  field: $('#cause_duration_end')[0],
	  format: 'D MMM YYYY',
	  minDate: moment("{{ $now ->copy() ->addWeek()->toIso8601String() }}").toDate(),
	  maxDate: moment("{{ $now ->copy() ->addMonths(3)->toIso8601String() }}").toDate(),
	  onSelect: function() {
		console.log(this.getMoment().format('YYYY-MM-DD'));
		regdata.end = this.getMoment().format('YYYY-MM-DD');
	  }
	 });

  $(document).ready(function() {
		regdata.start = startPicker.toString('YYYY-MM-DD');
		regdata.end = endPicker.toString('YYYY-MM-DD');
		console.log(regdata);

		$( "input.input, textarea.textarea " ).focus(function() {
			if ($(this).hasClass("is-danger")) {
				$(this).removeClass("is-danger").next().remove();
				$(this).parent("p.control").next("p").remove();
			}
		});

		function createCauseForm(btn) {
			 console.log(regdata);
			 $.ajax({
			  headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			  },
			  method: 'POST',
			  url: "{{ route('projects.store')}}",
			  dataType: 'JSON',
			  data: regdata
			}).done(function(d) {
				console.log(d);
				if (d.success){
					featuredImage.on("sending", function(file, xhr, formData) {
					  formData.append("projectid", d.id);
					  formData.append("isfeatured", 'true');
					});

					galleryImages.on("sending", function(file, xhr, formData) {
					  formData.append("projectid", d.id);
					});

					featuredImage.processQueue();
					galleryImages.processQueue();

					galleryImages.on("queuecomplete", function() {
						$("#tab-cause-details" ).addClass( "is-hidden" );
						$("#tab-cause-finish" ).removeClass( "is-hidden" );
						btn.removeClass("btn-at-general").addClass("btn-at-finish is-hidden");
					});
				}

			  });
		 }


		function validateDetails() {
			$( "#tab-cause-details p.control" ).toggleClass( "is-loading" );

			var causeName = $("#tab-cause-details input[name='cause_name']");
			var causeDescription = $("#tab-cause-details textarea[name='cause_description']");
			var causeGoal = $("#tab-cause-details input[name='cause_goal']");
			var causeCategory = $("#cause_category");

			if ((! causeName.val()) && (! causeName.hasClass("is-danger"))) {
				causeName.toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>This field is required</p>" );
			}

			if ((! causeDescription.val()) && (! causeDescription.hasClass("is-danger"))) {
				causeDescription.toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>This field is required</p>" );
			}

			if ((! causeGoal.val()) && (! causeGoal.hasClass("is-danger"))) {
				causeGoal.toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>This field is required</p>" );
			}

			$( "#tab-cause-details p.control" ).toggleClass( "is-loading" );

			var fImage = featuredImage.getQueuedFiles();
			var gImages = galleryImages.getQueuedFiles();

			if (!(fImage.length > 0)) {
				$( ".featured-image-label" ).toggleClass( "is-hidden" );
			}

			if (!(gImages.length > 0)) {
				$( ".gallery-images-label" ).toggleClass( "is-hidden" );
			}

			if ((causeName.val()) && (causeDescription.val()) && (causeGoal.val()) && (fImage.length > 0) && (gImages.length > 0)) {
				regdata.title = causeName.val();
				regdata.description = causeDescription.val();
				regdata.amount = causeGoal.val();
				regdata.category = causeCategory.val();
				return true ;
			} else {
				return false ;
			}
		}

		$('#cause-create-btn-control').click(function(e){
			e.preventDefault();
			if ($(this).hasClass("btn-at-general")) {
				$(this).prop("disabled", true);
				var r = validateDetails();
				if (r) {
					createCauseForm($(this));
				}
				$(this).prop("disabled", false);
				return;
			}
		});
 });
</script>
@endpush

@push('header-css')
<link href="{{ asset('css/pikaday.css') }}" rel="stylesheet" type="text/css">
@endpush



