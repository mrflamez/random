

@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<div class="columns is-gapless is-paddingless is-marginless">
	<div class="column">
		<div class="profile-options p-t-1">
		  <div class="tabs is-fullwidth">
			 @include('layouts.dashheader')
		  </div>
		</div>
	</div>
</div>

<section class="hero is-small  is-bold">
  <div class="hero-body ">
    <div class="container">
		@include('partials.donationsheader')
	</div>
  </div>
  <div class="hero-foot">
	  <div class="container">
		<nav class="tabs is-boxed is-fullwidth">
			<ul class="b-bt">
			  <li ><a href="{{ route('mycontributions')}}">Contributions</a></li>
			  <li class="is-active"><a href="{{ route('mydonations')}}" >Donations Received</a></li>
			</ul>
		</nav>
    </div>
  </div>
</section>
<section class="section ">
	<div class="container">
		@if( $projects -> isEmpty() )

			<div class="heading  m-b-3 has-text-centered">
				<h4 class="subtitle"><strong>No Donations Received</strong></h4>
			</div>
		@else
			<div class="heading  m-b-3 has-text-centered">
				<h4 class="subtitle">Your Donations Received</h4>
			</div>
			<table class="table">
			  <thead>
				<tr>
				  <th><abbr title="Position"></abbr></th>
				  <th>Names</th>
				  <th><abbr title="Played">Date</abbr></th>
				  <th><abbr title="Won">Amount</abbr></th>
				</tr>
			  </thead>

			  <tbody>
				@foreach ($projects as $project)
				  @foreach ($project -> payments as $payment)
					<tr>
					  <th> {{ $loop->iteration }}</th>
					  <td>{{ $payment->donator->first_name }}</td>
					  <td>{{ $payment->created_at->toFormattedDateString() }}</td>
					  <td>{{ $payment->amount }}</td>
					</tr>
				  @endforeach
				@endforeach
			  </tbody>
			</table>
	    @endif
	</div>
</section>
@endsection


@push('scripts')

@endpush

@push('header-css')
@endpush





