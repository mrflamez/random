@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<section class="section p-t-0-5">
<div class="container profile">

    <div class="section profile-heading">
      <div class="columns">
        <div class="column is-2">
          <div class="image is-128x128 avatar">
			  @foreach ($user -> images as $image)
				   @if ($loop->first)
					<img src="{{ asset('storage/'.$image->name) }}" alt="">
			       @endif
			   @endforeach
          </div>
        </div>
        <div class="column is-4 name">
          <p>
            <span class="title is-bold">{{ $user->f_name }} {{ $user->l_name }}</span>
          </p>
          <div class="tagline">
             <p>MemberID : <span class=" is-bold">{{ $user->member_id }}</span></p>
             <p>Email : <span class=" is-bold">{{ $user->email }}</span></p>
             <p>phone : <span class=" is-bold">{{ $user->phone }}</span></p>
             <p>Status : <span class="tag is-primary is-small">Active</span></p>
          </div>
        </div>
        {{--
        <div class="column is-2 followers has-text-centered">
          <p class="stat-val"> {{ $funds }}</p>
          <p class="stat-key">Funds Raised</p>
        </div>
        <div class="column is-2 following has-text-centered">
          <p class="stat-val">0</p>
          <p class="stat-key">Contacts</p>
        </div>
        --}}
        <div class="column is-2 likes has-text-centered">
          <p class="stat-val">{{ $projects->count() }}</p>
          <p class="stat-key">Projects</p>
        </div>
      </div>
    </div>
    <div class="profile-options">
      <div class="tabs is-fullwidth">
		@include('layouts.dashheader')
      </div>
    </div>

	  @if(!$projects -> isEmpty())
	    <nav class="nav">
		  <div class="nav-left">

		  </div>

		  <div class="nav-right">
			  <a class="nav-item"><strong>All</strong></a>
			  <a class="nav-item">Active</a>
			  <a class="nav-item">Pending</a>
			  <a class="nav-item">Deleted</a>
			  <a class="nav-item">Ended</a>
			  <div class="nav-item">
				  <div class="field is-grouped">
					<p class="control">
					  <a href="{{ route('createproject')}}" class="button is-success" >
						New
					  </a>
					</p>
				</div>
			</div>
		  </div>
		</nav>
		<hr class="m-t-0">
	     <div class="columns is-multiline">
			 @foreach ($projects as $project)
			     @include('partials.projectdashcard')
			  @endforeach
		 </div>
		 <div class=" columns">
			<div class=" column">

					{{ $projects->links('vendor.pagination.default') }}
			</div>
		</div>
	  @else
		<div class="heading  m-b-3 has-text-centered">
			<h2 class="title"><strong>No Projects</strong></h2>
		</div>
		<div class="field  has-addons has-addons-centered">
			  <p class="control">
				  <a href="{{ route('createproject') }}" class=" button is-success is-medium">
					Create Project
				  </a>
			 </p>
		</div>
	  @endif


</div>
</section>

@endsection

@push('scripts')

@endpush




