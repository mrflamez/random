
@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<div class="columns is-gapless is-paddingless is-marginless">
	<div class="column">
		<div class="profile-options p-t-1">
		  <div class="tabs is-fullwidth">
			 @include('layouts.dashheader')
		  </div>
		</div>
	</div>
</div>

<section class="section">
	<div class="columns">
		<div class="column">
				@if( $user -> activations -> isEmpty() )
					<div class="heading  m-b-3 has-text-centered">
						<h4 class="subtitle"><strong>No activations</strong></h4>
					</div>
				@else
					<div class="heading  m-b-3 has-text-centered">
						<h4 class="subtitle">Your activations</h4>
					</div>
					<table class="table">
					  <thead>
						<tr>
						  <th><abbr title="Position"></abbr></th>
						  <th>Activator</th>
						  <th>From Date</th>
						  <th>To Date</th>
						  <th><abbr title="Date">Payment Ref</abbr></th>
						  <th><abbr title="Date">Status</abbr></th>
						</tr>
					  </thead>

					  <tbody>
						@foreach ($user -> activations as $activation)
							<tr>
							  <th> {{ $loop->iteration }}</th>
							  <td>
								  @if($activation->payment)
								    <p class="subtitle is-bold">{{ $activation->activator->f_name }} {{  $activation->activator->l_name }}</p>
								  @else
									 <span class="tag is-warning">System Admin</span>
								  @endif

							   </td>
							  <td>{{ $activation->start_date->toFormattedDateString() }}</td>
							  <td>{{ $activation->end_date->toFormattedDateString() }}</td>
							  <td>
								  @if($activation->payment)
								   {{ $activation->payment->reference }}
								  @else
									 <span class="tag is-warning">{{ $activation->activator->email }}</span>
								  @endif
							   </td>
							   <td>
								  @if ($activation->status)
									 <span class="tag is-primary">Active</span>
								  @endif
							  </td>
							</tr>
						@endforeach
					  </tbody>
					</table>

				@endif
			  </div>

	</div>

</section>
@endsection










