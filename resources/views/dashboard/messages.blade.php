

@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<div class="columns is-gapless is-paddingless is-marginless">
    <div class="column">
        <div class="profile-options p-t-1">
          <div class="tabs is-fullwidth">
             @include('layouts.dashheader')
          </div>
        </div>
    </div>
</div>

<section class="section p-t-0">
      <div class="columns ">
          <div class="column is-5 b-br">
                    <div class="tabs is-fullwidth">
                      <ul>
                        <li class="mailbox-tabs">
                          <a id="mailbox-inbox-btn">
                            <span class="icon"><i class="fa fa-image"></i></span>
                            <span>Inbox</span>
                          </a>
                        </li>
                        <li class="mailbox-tabs">
                          <a id="mailbox-sent-btn">
                            <span class="icon"><i class="fa fa-music"></i></span>
                            <span>Sent</span>
                          </a>
                        </li>
                        <li class="is-active mailbox-tabs">
                          <a id="mailbox-compose-btn">
                            <span class="icon "><i class="fa fa-film"></i></span>
                            <span>Compose</span>
                          </a>
                        </li>

                      </ul>
                    </div>
                    <div class="content" id="mail-outbox-container">

                    </div>

          </div>
          <div class="column">
             <div class="is-hidden" id="mailbox-read">
               <div  id="mailbox-read-cont"></div>
             </div>
             <div class="" id="mailbox-compose">
                    <nav class="nav is-primary b-bt m-b-0-5">
                      <div class="nav-left">

                      </div>

                      <div class="nav-right">
                            <div class="nav-item">
                               <div class="field is-grouped">
                                <p class="control">
                                  <button class="button" >
                                    <span>Cancel</span>
                                  </button>
                                </p>
                                <p class="control">
                                  <button id="mailbox-compose-send-btn" class="button is-primary">
                                    <span>Send</span>
                                  </button>
                                </p>
                              </div>
                            </div>
                      </div>
                    </nav>
                    <div class="field ">
                         <div class="field-body ">
                            <div class="field">
                                <label class="label">From</label>
                              <p class="control  has-icons-left ">
                                <input class="input" type="text" value="{{ Auth::user()->email }}"  disabled>
                                <span class="icon is-small is-left">
                                  <i class="fa fa-envelope"></i>
                                </span>
                              </p>
                            </div>
                            <div class="field">
							  <label class="label">To </label>
							  <p class="control  has-icons-left has-icons-right">
								<span class="select">
								  <select name="to_email">
									  @forelse ($emails as $email)
										<option value="{{ $email }}">{{ $email }}</option>
									  @empty
									  @endforelse

								  </select>
								</span>
								<span class="icon is-small is-left">
								   <i class="fa fa-envelope"></i>
								</span>
							  </p>
							</div>

                         </div>
                  </div>
                  <div class="field ">
                      <label class="label">Subject</label>
                   <p class="control has-icons-right has-icons-left ">
                    <input name="subject" class="input" type="text" placeholder="Subject">
                    <span class="icon is-small is-left">
                      <i class="fa fa-user"></i>
                    </span>
                  </p>
                 </div>
                 <div class="field">
                  <label class="label">Message</label>
                  <p class="control has-icons-right">
                    <textarea name="message" class="textarea" placeholder="textarea"></textarea>

                  </p>
                </div>
             </div>
             <article class="v-centered is-hidden " id="mailbox-compose-notification">
                    <div class="has-text-centered">
                        <div class="field  has-addons has-addons-centered">
                              <p class="control">
                                  <span class="icon is-large">
                                      <i class="fa fa-home"></i>
                                    </span>
                             </p>
                        </div>
                        <p class="title is-5">Message Sent</p>
                        <p class="title is-6">
                            Wait for reply ... <br>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean efficitur.
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean efficitur.

                        </p>

                    </div>
            </article>
          </div>
      </div>
</section>
@endsection


@push('scripts')
<script type="text/javascript">
    var regdata = { };

    $(document).ready(function() {
        $('#mailbox-compose-btn').click(function(e){
            e.preventDefault();
            $('.mailbox-tabs').removeClass( "is-active" );
            $(this).parent('li.mailbox-tabs').addClass( "is-active" );

            //~ $('#mailbox-read').addClass( "is-hidden" );
            if (! ($('#mailbox-read').hasClass( "is-hidden" ))) {
                $('#mailbox-read').addClass( "is-hidden" );
            }
			$('#mail-outbox-container').html('');


            if (! ($('#mailbox-compose-notification').hasClass( "is-hidden" ))) {
                $('#mailbox-compose-notification').addClass( "is-hidden" );
            }
            if ($('#mailbox-compose').hasClass( "is-hidden" )) {
                $('#mailbox-compose').removeClass( "is-hidden" );
            }
        });
        $('#mailbox-inbox-btn').click(function(e){
            e.preventDefault();
            $('.mailbox-tabs').removeClass( "is-active" );
            $(this).parent('li.mailbox-tabs').addClass( "is-active" );

            //~ $('#mailbox-compose').addClass("is-hidden");
            if (! ($('#mailbox-compose').hasClass( "is-hidden" ))) {
                $('#mailbox-compose').addClass( "is-hidden" );
            }

            $('#mail-outbox-container').html('');

            if (! ($('#mailbox-compose-notification').hasClass( "is-hidden" ))) {
                $('#mailbox-compose-notification').addClass( "is-hidden" );
            }

            if ($('#mailbox-read').hasClass( "is-hidden" )) {
                $('#mailbox-read').removeClass( "is-hidden" );
            }

            getInboxMessages().done(function(d) {
                console.log(d);
				//return d ;
				if (d.success){
                    var tpl = '' ;
                    var tlpmail = '' ;
                    var outboxcont = $("#mail-outbox-container");
                    var readcont = $("#mailbox-read-cont");
                    outboxcont.html('');
                    readcont.html('');
                    if (d.mail.inboxes.length == 0) {
						console.log('empty');
						var tmlNotice = '<article class="v-centered">\
								<div class="has-text-centered">\
									<div class="field  has-addons has-addons-centered">\
										  <p class="control">\
											  <span class="icon is-large">\
												  <i class="fa fa-home"></i>\
												</span>\
										 </p>\
									</div>\
									<p class="title is-5">No  Mail</p>\
									<p class="title is-6">\
										Wait for reply ... <br>\
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean efficitur.\
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean efficitur.\
									</p>\
								</div>\
						</article>';
						outboxcont.html(tmlNotice);
						readcont.html(tmlNotice);
					} else {
					  $.each(d.mail.inboxes, function( key, value ) {
                        console.log(key);
                        if (key == 0){
                            tlpmail = '<h5 class="title is-bold b-bt  m-b-0-5 has-text-centered">'+ value.subject +' </h5>\
                             <nav class="nav is-primary b-bt m-b-0-5">\
                              <div class="nav-left">\
                                    <a class="nav-item is-tab">\
                                      <span class="icon-btn">\
                                        <i class="fa fa-print"></i>\
                                      </span>\
                                    </a>\
                                    <a class="nav-item is-tab">\
                                      <span class="icon-btn thin">\
                                        <i class="fa fa-lock"></i>\
                                      </span>\
                                    </a>\
                                    <a class="nav-item is-tab">\
                                      <span class="icon-btn">\
                                        <i class="fa fa-trash"></i>\
                                      </span>\
                                    </a>\
                              </div>\
                              <div class="nav-right">\
                                <a class="nav-item is-tab">\
                                  <span class="icon-btn thin">\
                                    <i class="fa fa-angle-left"></i>\
                                  </span>\
                                </a>\
                                <a class="nav-item is-tab">\
                                  <span class="icon-btn">\
                                    <i class="fa fa-angle-right"></i>\
                                  </span>\
                                </a>\
                              </div>\
                            </nav>\
                            <article class="media b-bt m-b-0-5">\
                                  <figure class="media-left">\
                                    <p class="image is-48x48">\
                                      <img src="http://bulma.io/images/placeholders/96x96.png">\
                                    </p>\
                                  </figure>\
                                  <div class="media-content">\
                                    <div class="content">\
                                      <p class="m-b-0-5">\
                                        <strong>'+ value.outbox.f_name +' ' + value.outbox.l_name +'</strong> <br> <small>@'+ value.outbox.email +'</small>\
                                      </p>\
                                    </div>\
                                  </div>\
                                  <div class="media-right">\
                                    <p><strong>31m</strong></p>\
                                  </div>\
                                </article>\
                            <article class="content">'+ value.content +'</article>';
                            readcont.html(tlpmail);
                        }

                        tpl =  '<a class="media m-t-0-2 p-t-0-2">\
                              <figure class="media-left">\
                                <p class="image is-96x96">\
                                  <img src="http://bulma.io/images/placeholders/128x128.png">\
                                </p>\
                              </figure>\
                              <div class="media-content">\
                                <div class="content">\
                                  <p class="m-b-0-5">\
                                    <strong>'+ value.outbox.f_name +'</strong> <small>@'+ value.outbox.email +'</small><br>\
                                    <strong>'+ value.subject +'</strong>\
                                  </p>\
                                  <p>'+ value.content +'</p>\
                                </div>\
                              </div>\
                              <div class="media-right">\
                                <p><strong>31m</strong></p>\
                              </div>\
                          </a>' ;
                        outboxcont.append(tpl);
                    });

					}

                } else {
                  // {{-- server validation failed --}}

                }

              });


        });
        $('#mailbox-sent-btn').click(function(e){
            e.preventDefault();
            $('.mailbox-tabs').removeClass( "is-active" );
            $(this).parent('li.mailbox-tabs').addClass( "is-active" );

            // $('#mailbox-compose').addClass("is-hidden");
            if (! ($('#mailbox-compose').hasClass( "is-hidden" ))) {
                $('#mailbox-compose').addClass( "is-hidden" );
            }

            if (! ($('#mailbox-compose-notification').hasClass( "is-hidden" ))) {
                $('#mailbox-compose-notification').addClass( "is-hidden" );
            }

            if ($('#mailbox-read').hasClass( "is-hidden" )) {
                $('#mailbox-read').removeClass( "is-hidden" );

            }
			getSentMessages().done(function(d) {
                console.log(d);
				//return d ;
				if (d.success){
                    var tpl = '' ;
                    var tlpmail = '' ;
                    var outboxcont = $("#mail-outbox-container");
                    var readcont = $("#mailbox-read-cont");
                    outboxcont.html('');
                    readcont.html('');
                    if (d.mail.outboxes.length == 0) {
						var tmlNotice = '<article class="v-centered">\
								<div class="has-text-centered">\
									<div class="field  has-addons has-addons-centered">\
										  <p class="control">\
											  <span class="icon is-large">\
												  <i class="fa fa-home"></i>\
												</span>\
										 </p>\
									</div>\
									<p class="title is-5">No  Mail</p>\
									<p class="title is-6">\
										Wait for reply ... <br>\
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean efficitur.\
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean efficitur.\
									</p>\
								</div>\
						</article>';
						outboxcont.html(tmlNotice);
						readcont.html(tmlNotice);
					} else {
                    $.each(d.mail.outboxes, function( key, value ) {
                        console.log(key);
                        if (key == 0){
                            tlpmail = '<h5 class="title is-bold b-bt  m-b-0-5 has-text-centered">'+ value.subject +' </h5>\
                             <nav class="nav is-primary b-bt m-b-0-5">\
                              <div class="nav-left">\
                                    <a class="nav-item is-tab">\
                                      <span class="icon-btn">\
                                        <i class="fa fa-print"></i>\
                                      </span>\
                                    </a>\
                                    <a class="nav-item is-tab">\
                                      <span class="icon-btn thin">\
                                        <i class="fa fa-lock"></i>\
                                      </span>\
                                    </a>\
                                    <a class="nav-item is-tab">\
                                      <span class="icon-btn">\
                                        <i class="fa fa-trash"></i>\
                                      </span>\
                                    </a>\
                              </div>\
                              <div class="nav-right">\
                                <a class="nav-item is-tab">\
                                  <span class="icon-btn thin">\
                                    <i class="fa fa-angle-left"></i>\
                                  </span>\
                                </a>\
                                <a class="nav-item is-tab">\
                                  <span class="icon-btn">\
                                    <i class="fa fa-angle-right"></i>\
                                  </span>\
                                </a>\
                              </div>\
                            </nav>\
                            <article class="media b-bt m-b-0-5">\
                                  <figure class="media-left">\
                                    <p class="image is-48x48">\
                                      <img src="http://bulma.io/images/placeholders/96x96.png">\
                                    </p>\
                                  </figure>\
                                  <div class="media-content">\
                                    <div class="content">\
                                      <p class="m-b-0-5">\
                                        <strong>'+ value.inbox.f_name +' ' + value.inbox.l_name +'</strong> <br> <small>@'+ value.inbox.email +'</small>\
                                      </p>\
                                    </div>\
                                  </div>\
                                  <div class="media-right">\
                                    <p><strong>31m</strong></p>\
                                  </div>\
                                </article>\
                            <article class="content">'+ value.content +'</article>';
                            readcont.html(tlpmail);
                        }
                        //~ var inboxeduser = {} ;
                        //~ $.each(value.inbox, function( ky, vl ) {
                            //~ inboxeduser.email = vl.email ;
                            //~ inboxeduser.name = vl.f_name + ' ' + vl.l_name;
                        //~ });
                        tpl =  '<a class="media m-t-0-2 p-t-0-2">\
                              <figure class="media-left">\
                                <p class="image is-96x96">\
                                  <img src="http://bulma.io/images/placeholders/128x128.png">\
                                </p>\
                              </figure>\
                              <div class="media-content">\
                                <div class="content">\
                                  <p class="m-b-0-5">\
                                    <strong>'+ value.inbox.f_name +'</strong> <small>@'+ value.inbox.email +'</small><br>\
                                    <strong>'+ value.subject +'</strong>\
                                  </p>\
                                  <p>'+ value.content +'</p>\
                                </div>\
                              </div>\
                              <div class="media-right">\
                                <p><strong>31m</strong></p>\
                              </div>\
                          </a>' ;
                        outboxcont.append(tpl);
                    });

				    }
                    //$("#mailbox-compose").addClass("is-hidden");
                    //$("#mailbox-compose-notification").removeClass("is-hidden");
                } else {
                  // {{-- server validation failed --}}

                }

              });
        });

        $( "input.input, textarea.textarea " ).focus(function() {
            if ($(this).hasClass("is-danger")) {
                $(this).removeClass("is-danger").next().remove();
                $(this).parent("p.control").next("p").remove();
            }
        });

        $('#mailbox-compose-send-btn').click(function(e){
            e.preventDefault();
            var r = validateDetails();
            if (r) {
                sendMessage();

            }
        });

        function getSentMessages() {
             return $.ajax({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              method: 'POST',
              url: "{{ route('mailboxes.sentmail', ['id' => Auth::user()->id]) }}",
              dataType: 'JSON'
            })

         }

         function getInboxMessages() {
             return $.ajax({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              method: 'POST',
              url: "{{ route('mailboxes.inboxmail', ['id' => Auth::user()->id]) }}",
              dataType: 'JSON'
            })

         }


        function sendMessage() {
             console.log(regdata);
             $.ajax({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              method: 'POST',
              url: "{{ route('mailboxes.store')}}",
              dataType: 'JSON',
              data: regdata
            }).done(function(d) {
                console.log(d);
                if (d.success){
                    $("#mailbox-compose").addClass("is-hidden");
                    $("#mailbox-compose-notification").removeClass("is-hidden");
                } else {
                  // {{-- server validation failed --}}
                  $.each(d.errors, function( key, value ) {
                     if (key === 'message') {
                        $("#mailbox-compose input[name='message']").toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>" + value[0] + "</p>" );
                     }

                     if (key === 'subject') {
                        $("#mailbox-compose input[name='subject']").toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>" + value[0] + "</p>" );
                     }

                     if (key === 'email') {
                        $("#mailbox-compose input[name='to_email']").toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>" + value[0] + "</p>" );
                     }
                  });
                }

              });

         }

        function validateDetails() {

            var toEmail = $("#mailbox-compose input[name='to_email']");
            var subject = $("#mailbox-compose input[name='subject']");
            var message = $("#mailbox-compose textarea[name='message']");

            if ((! toEmail.val()) && (! toEmail.hasClass("is-danger"))) {
                toEmail.toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>This field is required</p>" );
            }

            if ((! subject.val()) && (! subject.hasClass("is-danger"))) {
                subject.toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>This field is required</p>" );
            }

            if ((! message.val()) && (! message.hasClass("is-danger"))) {
                message.toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>This field is required</p>" );
            }


            if ((toEmail.val()) && (subject.val()) && (message.val())) {
                regdata.email = toEmail.val();
                regdata.subject = subject.val();
                regdata.message = message.val();
                return true ;
            } else {
                return false ;
            }
        }

    });
</script>
@endpush






