

@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<div class="columns is-gapless is-paddingless is-marginless">
	<div class="column">
		<div class="profile-options p-t-1">
		  <div class="tabs is-fullwidth">
			 @include('layouts.dashheader')
		  </div>
		</div>
	</div>
</div>

<section class="section">
	<div class="container m-b-1-5">
		<div class="heading has-text-centered">
			<h6 class="title">{{ $project-> title }}</h5>
		</div>
	</div>
	<div class="container m-b-1-5">
		@include('partials.projectheader')
	</div>
	<div class="container m-b-1-5">
		<div class="tabs  is-fullwidth is-boxed">
				<ul>
					<li class="is-active">
					  <a href="{{ route('myproject', ['id' => $project-> id]) }}">
						<span class="icon is-small"><i class="fa fa-image"></i></span>
						<span>General Details</span>
					  </a>
					</li>
					{{--
					<li>
					  <a href="{{ route('myprojectdonations', ['id' => $project-> id]) }}">
						<span class="icon is-small"><i class="fa fa-music"></i></span>
						<span>Donations</span>
					  </a>
					</li>
					 --}}
					<li>
					  <a href="{{ route('myprojectmessages', ['id' => $project-> id]) }}" >
						<span class="icon is-small"><i class="fa fa-film"></i></span>
						<span>Messages</span>
					  </a>
					</li>
					<li>
					  <a href="{{ route('myprojectsettings', ['id' => $project-> id]) }}" >
						<span class="icon is-small"><i class="fa fa-file-text-o"></i></span>
						<span>Settings</span>
					  </a>
					</li>
					{{--
					<li>
					  <a href="{{ route('myprojectstats', ['id' => $project-> id]) }}" >
						<span class="icon is-small"><i class="fa fa-file-text-o"></i></span>
						<span>Stats</span>
					  </a>
					</li>
					 --}}
				</ul>

		</div>
	</div>
	<div class="container m-b-1-5">
		<div class="columns">
			<div class="column">
				<figure class="image  featured">
					@foreach ($project -> images as $image)
					   @if ($image->featured)
						<img src="{{ asset('storage/'.$image->name) }}" alt="">
					   @endif
					@endforeach
				</figure>
			</div>
			<div class="column is-4">
				<div class="columns is-multiline">
					<div class="columns is-multiline">
					@foreach ($project -> images as $image)
					   @if (! $image->featured)
						<div class="column is-narrow">
							<div class="image is-128x128">
								<img src="{{ asset('storage/'.$image->name) }}" alt="">
							</div>
						</div>

					   @endif
					@endforeach
				</div>
				</div>
			</div>
		</div>

	</div>
	<div class="container m-b-1-5">
	   <div class="content">
			{{  $project-> description  }}
	   </div>
	</div>
</section>
@endsection


@push('scripts')

@endpush

@push('header-css')
@endpush





