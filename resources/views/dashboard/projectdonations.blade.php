@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<div class="columns is-gapless is-paddingless is-marginless">
	<div class="column">
		<div class="profile-options p-t-1">
		  <div class="tabs is-fullwidth">
			 @include('layouts.dashheader')
		  </div>
		</div>
	</div>
</div>

<section class="section">
	<div class="container m-b-1-5">
		<div class="heading has-text-centered">
			<h6 class="title">{{ $project-> title }}</h5>
		</div>
	</div>
	<div class="container m-b-1-5">
		@include('partials.projectheader')
	</div>
	<div class="container m-b-1-5">
		 <div class="tabs  is-fullwidth is-boxed">
			   <ul>
					<li >
					  <a href="{{ route('myproject', ['id' => $project-> id]) }}">
						<span class="icon is-small"><i class="fa fa-image"></i></span>
						<span>General Details</span>
					  </a>
					</li>
					<li class="is-active">
					  <a href="{{ route('myprojectdonations', ['id' => $project-> id]) }}">
						<span class="icon is-small"><i class="fa fa-music"></i></span>
						<span>Donations</span>
					  </a>
					</li>
					<li>
					  <a href="{{ route('myprojectmessages', ['id' => $project-> id]) }}" >
						<span class="icon is-small"><i class="fa fa-film"></i></span>
						<span>Messages</span>
					  </a>
					</li>
					<li>
					  <a href="{{ route('myprojectsettings', ['id' => $project-> id]) }}" >
						<span class="icon is-small"><i class="fa fa-file-text-o"></i></span>
						<span>Settings</span>
					  </a>
					</li>
					<li>
					  <a href="{{ route('myprojectstats', ['id' => $project-> id]) }}" >
						<span class="icon is-small"><i class="fa fa-file-text-o"></i></span>
						<span>Stats</span>
					  </a>
					</li>

				</ul>
		 </div>
	</div>
	<div class="container m-b-1-5">
		@if($project->payments->isEmpty())
			 <div class="heading  m-b-3 has-text-centered">
				<h4 class="subtitle"><strong>No Donations Made</strong></h4>
			</div>
		@else
			 <div class="heading  m-b-3 has-text-centered">
				<h4 class="subtitle">Donators</h4>
			</div>
			<table class="table">
			  <thead>
				<tr>
				  <th><abbr title="Position"></abbr></th>
				  <th>Names</th>
				  <th><abbr title="Played">Date</abbr></th>
				  <th><abbr title="Won">Amount</abbr></th>
				  <th><abbr title="Drawn">Status</abbr></th>
				  <th><abbr title="Lost"></abbr></th>
				</tr>
			  </thead>

			  <tbody>
				@foreach ($project->payments as $payment)
					<tr>
					  <th> {{ $loop->iteration }}</th>
					  <td>{{ $payment->donator->first_name }}</td>
					  <td>{{ $payment->created_at->toFormattedDateString() }}</td>
					  <td>{{ $payment->amount }}</td>
					  <td>PAID</td>
					  <td><a class="button is-primary">View</a></td>
					</tr>
				@endforeach
			  </tbody>
			</table>

	    @endif

	</div>

</section>
@endsection


@push('scripts')

@endpush

@push('header-css')
@endpush





