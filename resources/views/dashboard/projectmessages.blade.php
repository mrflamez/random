@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<div class="columns is-gapless is-paddingless is-marginless">
	<div class="column">
		<div class="profile-options p-t-1">
		  <div class="tabs is-fullwidth">
			 @include('layouts.dashheader')
		  </div>
		</div>
	</div>
</div>

<section class="section">
	<div class="container m-b-1-5">
		<div class="heading has-text-centered">
			<h6 class="title">{{ $project-> title }}</h5>
		</div>
	</div>
	<div class="container m-b-1-5">
		@include('partials.projectheader')
	</div>
	<div class="container m-b-1-5">
		 <div class="tabs  is-fullwidth is-boxed">
			   <ul>
					<li >
					  <a href="{{ route('myproject', ['id' => $project-> id]) }}">
						<span class="icon is-small"><i class="fa fa-image"></i></span>
						<span>General Details</span>
					  </a>
					</li>
					{{--
					<li>
					  <a href="{{ route('myprojectdonations', ['id' => $project-> id]) }}">
						<span class="icon is-small"><i class="fa fa-music"></i></span>
						<span>Donations</span>
					  </a>
					</li>
					 --}}
					<li class="is-active">
					  <a href="{{ route('myprojectmessages', ['id' => $project-> id]) }}" >
						<span class="icon is-small"><i class="fa fa-film"></i></span>
						<span>Messages</span>
					  </a>
					</li>
					<li>
					  <a href="{{ route('myprojectsettings', ['id' => $project-> id]) }}" >
						<span class="icon is-small"><i class="fa fa-file-text-o"></i></span>
						<span>Settings</span>
					  </a>
					</li>
					{{--
					<li>
					  <a href="{{ route('myprojectstats', ['id' => $project-> id]) }}" >
						<span class="icon is-small"><i class="fa fa-file-text-o"></i></span>
						<span>Stats</span>
					  </a>
					</li>
					 --}}

				</ul>
		 </div>
	</div>
	<div class="container project_messages_container m-b-1-5">
       <div class="columns">
          <div class="column">
				@forelse ($project->comments as $comment)
				<article class="media">
				  <figure class="media-left">
					@forelse ($comment -> user -> images as $image)
						   @if ($loop->first)
						   <p class="image is-96x96">
							 <img src="{{ asset('storage/'.$image->name) }}" alt="">
						   </p>
						   @endif
						@empty
							<p class="image is-96x96">

							</p>
						@endforelse
				  </figure>
				  <div class="media-content">
					<div class="content">
					  <p class="m-b-0-5">
						<strong>{{ $comment->subject }}</strong> <small>@ {{ $comment->user->f_name }}</small>
					  </p>
					  <p> {!! Helper::words( strip_tags($comment->content), $limit = 15, $end = '...') !!} </p>
					</div>
					<nav class="level is-mobile">
					  <div class="level-left">
						  <div class="level-item" >
							  <a class="">
								  <span> {{ $comment->replies->count() }} Replys</span>
								</a>
							</div>
							<div class="level-item" >
							  <small>{{ $comment-> created_at ->diffForHumans() }}</small>
							</div>

					  </div>
					  <div class="level-right">
						<a href="{{ route('viewprojectmessagescomments', ['id' => $project-> id, 'commentid' => $comment-> id]) }}" class="level-item button view_single_project_message">
							  <span class="icon is-small"><i class="fa fa-reply"></i></span>
							  <span>View</span>
							</a>
					  </div>
					</nav>
				  </div>

				</article>

			@empty
				<div class="heading  m-b-3 has-text-centered">
						<h4 class="subtitle"><strong>No Comments</strong></h4>
					</div>
			@endforelse

		  </div>
	   </div>

	</div>


</section>
@endsection


@push('scripts')
@endpush







