@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<div class="columns is-gapless is-paddingless is-marginless">
	<div class="column">
		<div class="profile-options p-t-1">
		  <div class="tabs is-fullwidth">
			 @include('layouts.dashheader')
		  </div>
		</div>
	</div>
</div>

<section class="section">
	<div class="container m-b-1-5">
		<div class="heading has-text-centered">
			<h6 class="title">{{ $project-> title }}</h5>
		</div>
	</div>
	<div class="container m-b-1-5">
		@include('partials.projectheader')
	</div>
	<div class="container m-b-1-5">
		 <div class="tabs  is-fullwidth is-boxed">
			   <ul>
					<li >
					  <a href="{{ route('myproject', ['id' => $project-> id]) }}">
						<span class="icon is-small"><i class="fa fa-image"></i></span>
						<span>General Details</span>
					  </a>
					</li>
					{{--
					<li>
					  <a href="{{ route('myprojectdonations', ['id' => $project-> id]) }}">
						<span class="icon is-small"><i class="fa fa-music"></i></span>
						<span>Donations</span>
					  </a>
					</li>
					 --}}
					<li class="is-active">
					  <a href="{{ route('myprojectmessages', ['id' => $project-> id]) }}" >
						<span class="icon is-small"><i class="fa fa-film"></i></span>
						<span>Comments</span>
					  </a>
					</li>
					<li>
					  <a href="{{ route('myprojectsettings', ['id' => $project-> id]) }}" >
						<span class="icon is-small"><i class="fa fa-file-text-o"></i></span>
						<span>Settings</span>
					  </a>
					</li>
					{{--
					<li>
					  <a href="{{ route('myprojectstats', ['id' => $project-> id]) }}" >
						<span class="icon is-small"><i class="fa fa-file-text-o"></i></span>
						<span>Stats</span>
					  </a>
					</li>
					 --}}

				</ul>
		 </div>
	</div>
	<div class="container project_messages_container m-b-1-5">
       <div class="columns">
          <div class="column">
				<article class="media">
				  <figure class="media-left">
					@forelse ($comment -> user -> images as $image)
					   @if ($loop->first)
					   <p class="image is-96x96">
						 <img src="{{ asset('storage/'.$image->name) }}" alt="">
					   </p>
					   @endif
					@empty
						<p class="image is-96x96">

						</p>
					@endforelse
				  </figure>
				  <div class="media-content">
					<div class="content">
					  <p class="m-b-0-5">
						<strong>{{ $comment->subject }}</strong> <small>@ {{ $comment->user->f_name }}</small>
					  </p>
					  {!! $comment->content !!}
					</div>
				  @forelse ($comment -> replies  as $reply)
                         <article class="media">
							  <figure class="media-left">
								@forelse ($reply -> user -> images as $replyuserimage)
								   @if ($loop->first)
								   <p class="image is-64x64">
									 <img src="{{ asset('storage/'.$replyuserimage->name) }}" alt="">
								   </p>
								   @endif
								@empty
									<p class="image is-64x64">

									</p>
								@endforelse
							  </figure>
							  <div class="media-content">
								<div class="content">
								  <p class="m-b-0-5">
									<strong>@ {{ $reply->user->f_name }}</strong>
								  </p>
								   {!! $reply->content !!}
								</div>
							  </div>
							  <div class="media-right">
								<small>{{ $reply-> created_at ->diffForHumans() }}</small>
							  </div>
							</article>
				  @empty

				  @endforelse
				  </div>
                  <div class="media-right">
					<small>{{ $comment-> created_at ->diffForHumans() }}</small>
				  </div>
				</article>
				<article class="media" id="comment-reply-form">
				  <figure class="media-left">
					@forelse (Auth::user() -> images as $userimage)
					   @if ($loop->first)
					   <p class="image is-64x64">
						 <img src="{{ asset('storage/'.$userimage->name) }}" alt="">
					   </p>
					   @endif
					@empty
						<p class="image is-64x64">

						</p>
					@endforelse
				  </figure>
				  <div class="media-content">
					<div class="field">
					  <p class="control">
						<textarea id="comment_text" class="textarea" placeholder="Add a comment..."></textarea>
					  </p>
					</div>
					<div class="field">
					  <p class="control">
						<button id="comment-reply-btn-control"class="button">Reply comment</button>
					  </p>
					</div>
				  </div>
				</article>
				<article class="v-centered is-hidden message-notification">
						<div class="has-text-centered">
							<div class="field  has-addons has-addons-centered">
								  <p class="control">
									  <span class="icon is-large">
										  <i class="fa fa-check-circle-o"></i>
										</span>
								 </p>
							</div>
							<p class="title is-5">reply Sent</p>
						</div>
					</article>
		  </div>
	   </div>

	</div>

</section>
@endsection

@push('scripts')
<script type="text/javascript">

  $(document).ready(function() {
		var regdata = {} ;

		$( "input.input, textarea.textarea " ).focus(function() {
			if ($(this).hasClass("is-danger")) {
				$(this).removeClass("is-danger");
				$(this).parent("p.control").next("p").remove();
			}
		});

		function sendComment() {
			 console.log(regdata);
			 $.ajax({
			  headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			  },
			  method: 'PUT',
			  url: "{{ route('comments.update', ['id' => $comment->id])}}",
			  dataType: 'JSON',
			  data: regdata
			}).done(function(d) {
				console.log(d);
				if (d.success){
					$("#comment-reply-form").addClass("is-hidden");
				    $(".message-notification").removeClass("is-hidden");
				} else {
                  // {{-- server validation failed --}}
				  $.each(d.errors, function( key, value ) {
					 if (key === 'comment_text') {
						$("#comment_text").toggleClass( "is-danger" ).parent("p.control").after( " <p class='help is-danger'>" + value[0] + "</p>" );
					 }
				  });
				}

			  });

		 }


		function validateDetails() {

			var commentText = $("#comment_text");

			if ((! commentText.val()) && (! commentText.hasClass("is-danger"))) {
				commentText.toggleClass( "is-danger" ).parent("p.control").after( " <p class='help is-danger'>This field is required</p>" );
			}


			if (commentText.val()) {
				regdata.comment_text = commentText.val();
				regdata.action = "new_reply";
				return true ;
			} else {
				return false ;
			}
		}

		$('#comment-reply-btn-control').click(function(e){
			e.preventDefault();
			$(this).prop("disabled", true);
			var r = validateDetails();

			if (r) {
				sendComment();
			}

			$(this).prop("disabled", false);
		});
 });
</script>
@endpush







