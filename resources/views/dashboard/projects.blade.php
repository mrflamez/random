

@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<div class="columns is-gapless is-paddingless is-marginless">
	<div class="column">
		<div class="profile-options p-t-1">
		  <div class="tabs is-fullwidth">
			 @include('layouts.dashheader')
		  </div>
		</div>
	</div>
</div>

<section class="section p-t-0-5">
	<div class="container profile">
		@if(!$projects -> isEmpty())
	    <nav class="nav">
		  <div class="nav-left">

		  </div>

		  <div class="nav-right">
			  <a class="nav-item"><strong>All</strong></a>
			  <a class="nav-item">Active</a>
			  <a class="nav-item">Pending</a>
			  <a class="nav-item">Deleted</a>
			  <a class="nav-item">Ended</a>
			  <div class="nav-item">
				  <div class="field is-grouped">
					<p class="control">
					  <a href="{{ route('createproject')}}" class="button is-success" >
						New
					  </a>
					</p>
				</div>
			</div>
		  </div>
		</nav>
		<hr class="m-t-0">
	     <div class="columns is-multiline">
			 @foreach ($projects as $project)
			     @include('partials.projectdashcard')
			  @endforeach
		 </div>
		 <div class=" columns">
			<div class=" column">

					{{ $projects->links('vendor.pagination.default') }}
			</div>
		</div>
	  @else
		<div class="heading  m-b-3 has-text-centered">
			<h2 class="title"><strong>No Projects</strong></h2>
		</div>
		<div class="field  has-addons has-addons-centered">
			  <p class="control">
				  <a href="{{ route('createproject') }}" class=" button is-success is-medium">
					Create Project
				  </a>
			 </p>
		</div>
	  @endif

	</div>
</section>
@endsection


@push('scripts')

@endpush

@push('header-css')
@endpush




