@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<div class="columns is-gapless is-paddingless is-marginless">
	<div class="column">
		<div class="profile-options p-t-1">
		  <div class="tabs is-fullwidth">
			 @include('layouts.dashheader')
		  </div>
		</div>
	</div>
</div>

<section class="section">
	<div class="container m-b-1-5">
		<div class="heading has-text-centered">
			<h6 class="title">{{ $project-> title }}</h5>
		</div>
	</div>
	<div class="container m-b-1-5">
		@include('partials.projectheader')
	</div>
	<div class="container m-b-1-5">
		 <div class="tabs  is-fullwidth is-boxed">
			   <ul>
					<li >
					  <a href="{{ route('myproject', ['id' => $project-> id]) }}">
						<span class="icon is-small"><i class="fa fa-image"></i></span>
						<span>General Details</span>
					  </a>
					</li>
					{{--
					<li>
					  <a href="{{ route('myprojectdonations', ['id' => $project-> id]) }}">
						<span class="icon is-small"><i class="fa fa-music"></i></span>
						<span>Donations</span>
					  </a>
					</li>
					 --}}
					<li >
					  <a href="{{ route('myprojectmessages', ['id' => $project-> id]) }}" >
						<span class="icon is-small"><i class="fa fa-film"></i></span>
						<span>Messages</span>
					  </a>
					</li>
					<li class="is-active">
					  <a href="{{ route('myprojectsettings', ['id' => $project-> id]) }}" >
						<span class="icon is-small"><i class="fa fa-file-text-o"></i></span>
						<span>Settings</span>
					  </a>
					</li>
					{{--
					<li>
					  <a href="{{ route('myprojectstats', ['id' => $project-> id]) }}" >
						<span class="icon is-small"><i class="fa fa-file-text-o"></i></span>
						<span>Stats</span>
					  </a>
					</li>
					 --}}

				</ul>
		 </div>
	</div>
	<div class="container m-b-1-5">
	  <div class="columns">
		  <div class="column is-6">
				<article class="message is-warning">
					<div class="message-header">
						<p>Deactivate Project</p>
					 </div>
					 <div class="message-body">
						 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
						 <strong>Pellentesque risus mi</strong>, tempus quis placerat
						  ut, porta nec nulla. Vestibulum rhoncus ac ex sit amet.
						 </p>
						 <div class="block p-t-0-5">
							  <button id="project-suspend-btn-control" class="button is-danger">Deactivate Project</button>
						 </div>
					 </div>
				</article>
		  </div>

		  <div class="column is-6">
				<article class="message is-info">
					<div class="message-header">
						<p>Request Funds</p>
					 </div>
					 <div class="message-body">
						 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
						 <strong>Pellentesque risus mi</strong>, tempus quis placerat
						  ut, porta nec nulla. Vestibulum rhoncus ac ex sit amet.
						 </p>
						 <div class="block p-t-0-5">
							  <button class="button is-success">Request Funds</button>
						 </div>
					 </div>
				</article>
		  </div>

	  </div>
	</div>
</section>
@endsection

@push('header-css')
<link rel="stylesheet"  href="//cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.5/sweetalert2.min.css" >
@endpush

@push('scripts')
<script src="//cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.5/sweetalert2.min.js"></script>
<script type="text/javascript">

  var regdata = { };


  $(document).ready(function() {
		//~ $( "input.input, textarea.textarea " ).focus(function() {
			//~ if ($(this).hasClass("is-danger")) {
				//~ $(this).removeClass("is-danger").next().remove();
				//~ $(this).parent("p.control").nextAll("p").remove();
			//~ }
		//~ });

		function suspendProject() {
			 console.log(regdata);
			 $.ajax({
			  headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			  },
			  method: 'PUT',
			  url: "{{ route('projects.update', ['id' => $project->id])}}",
			  dataType: 'JSON',
			  data: regdata
			}).done(function(d) {
				console.log(d);
				if (d.success){

					if (d.message === 'suspend') {
						swal(
						'Success!',
						'Project Deactivateed !',
						'success'
					  );
					}

				}

			  });
		 }

		$('#project-suspend-btn-control').click(function(e){
			e.preventDefault();
			$(this).prop("disabled", true);
			swal({
				  title: 'Are you sure?',
				  text: "",
				  type: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Yes Continue'
				}).then(function () {
					regdata.action = "deactivate";
					suspendProject();
				}, function () {});
			$(this).prop("disabled", false);
		});
 });
</script>
@endpush


















