
@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<div class="columns is-gapless is-paddingless is-marginless">
	<div class="column">
		<div class="profile-options p-t-1">
		  <div class="tabs is-fullwidth">
			 @include('layouts.dashheader')
		  </div>
		</div>
	</div>
</div>

<section class="section">
	<div class="container m-b-1-5">
		<div class="heading has-text-centered">
			<h6 class="title">{{ $project-> title }}</h5>
		</div>
	</div>
	<div class="container m-b-1-5">
	     @include('partials.projectheader')
	</div>
	<div class="container m-b-1-5">
		 <div class="tabs  is-fullwidth is-boxed">
			   <ul>
					<li >
					  <a href="{{ route('myproject', ['id' => $project-> id]) }}">
						<span class="icon is-small"><i class="fa fa-image"></i></span>
						<span>General Details</span>
					  </a>
					</li>
					{{--
					<li>
					  <a href="{{ route('myprojectdonations', ['id' => $project-> id]) }}">
						<span class="icon is-small"><i class="fa fa-music"></i></span>
						<span>Donations</span>
					  </a>
					</li>
					--}}
					<li >
					  <a href="{{ route('myprojectmessages', ['id' => $project-> id]) }}" >
						<span class="icon is-small"><i class="fa fa-film"></i></span>
						<span>Messages</span>
					  </a>
					</li>
					<li>
					  <a href="{{ route('myprojectsettings', ['id' => $project-> id]) }}" >
						<span class="icon is-small"><i class="fa fa-file-text-o"></i></span>
						<span>Settings</span>
					  </a>
					</li>
					{{--
					<li class="is-active">
					  <a href="{{ route('myprojectstats', ['id' => $project-> id]) }}" >
						<span class="icon is-small"><i class="fa fa-file-text-o"></i></span>
						<span>Stats</span>
					  </a>
					</li>
                    --}}
				</ul>
		 </div>
	</div>
	<div class="container m-b-1-5">
	  <div class="columns">

		  <div class="column">
			  <p class="title is-5">
				   Notifications
			   </p>

<!--
			  <div class="notification is-warning">
				<button class="delete"></button>
				<strong>Server 101a is at 90% disk capacity.</strong>
				<br>
				<small>1h ago | via: system</small>
			  </div>
			  <div class="notification ">
				<button class="delete"></button>
				<strong>Server 101a is at 90% disk capacity.</strong>
				<br>
				<small>1h ago | via: system</small>
			  </div>
			  <div class="notification is-primary">
				<button class="delete"></button>
				<strong>Server 101a is at 90% disk capacity.</strong>
				<br>
				<small>1h ago | via: system</small>
			  </div>
			  <div class="notification is-info">
				<button class="delete"></button>
				<strong>Server 101a is at 90% disk capacity.</strong>
				<br>
				<small>1h ago | via: system</small>
			  </div>
-->
		  </div>
		  <div class="column is-6">
				<p class="title is-5">
				   Donations VS Month
			   </p>
				<div class="block chart_container">
					<canvas id="donationsChart"></canvas>
				</div>
		  </div>
	  </div>

	</div>
</section>
@endsection


@push('scripts')
<script src="{{ asset('js/Chart.js') }}"></script>
<script type="text/javascript">
	var ctxReg = document.getElementById("donationsChart").getContext("2d");

	var chartColors = {
		red: 'rgb(255, 99, 132)',
		orange: 'rgb(255, 159, 64)',
		yellow: 'rgb(255, 205, 86)',
		green: 'rgb(75, 192, 192)',
		blue: 'rgb(54, 162, 235)',
		purple: 'rgb(153, 102, 255)',
		grey: 'rgb(201, 203, 207)'
	};
	var color = Chart.helpers.color;
	var configLine = {
            type: 'line',
            data: {
                labels: [],
                datasets: []
            },
            options: {
                responsive: true,
                title:{
                    display:false,
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Month'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Views'
                        }
                    }]
                }
            }
    };

    var configBar = {
		type: 'bar',
		data: {
			labels: [],
			datasets: []
		},
		options: {
				responsive: true,
				legend: {
					position: 'top',
				},
				title: {
					display: false,
				}
		 }
    };

	$(function() {
		$.ajax({
		  headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		  },
		  method: 'POST',
		  url: "{{ route('analytics.donations', ['id' => $project->id]) }}",
		  dataType: 'JSON',
		}).done(function(d) {
			console.log(d);
			if (d.success){
				var chLbls = [];
				var chDt = [];
				$.each(d.data, function( key, value ) {
					chLbls.push(key)
					chDt.push(value)
				});
				configBar.data.labels = chLbls ;
				configBar.data.datasets = [{
                    label: " donations",
                    backgroundColor: color(chartColors.green).alpha(0.5).rgbString(),
                    borderColor: chartColors.green,
                    data: chDt,
                    //~ fill: false,
                    borderWidth: 1,
                }] ;
                window.usersVsmonth = new Chart(ctxReg, configBar);
			}

		});

	});

</script>
@endpush
