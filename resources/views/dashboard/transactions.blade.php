
@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<div class="columns is-gapless is-paddingless is-marginless">
	<div class="column">
		<div class="profile-options p-t-1">
		  <div class="tabs is-fullwidth">
			 @include('layouts.dashheader')
		  </div>
		</div>
	</div>
</div>

<section class="section">
	<div class="columns">
 <div class="column">
				@if( $payments -> isEmpty() )

					<div class="heading  m-b-3 has-text-centered">
						<h4 class="subtitle"><strong>No Contributions Made</strong></h4>
					</div>
				@else
					<div class="heading  m-b-3 has-text-centered">
						<h4 class="subtitle">Your Transactions</h4>
					</div>
					<table class="table">
					  <thead>
						<tr>
						  <th><abbr title="Position"></abbr></th>
						  <th>Payee</th>
						  <th>Transaction Type</th>
						  <th>Transaction Id</th>
						  <th>Transaction Ref</th>
						  <th><abbr title="Currency">Currency</abbr></th>
						  <th><abbr title="Amount">Amount</abbr></th>
						  <th><abbr title="Date">Date</abbr></th>
						</tr>
					  </thead>

					  <tbody>
						@foreach ($payments as $payment)
						  @foreach ($payment -> payments as $trans)
							<tr>
							  <th> {{ $loop->iteration }}</th>
							  <td>{{ $payment->first_name }} {{ $payment->last_name }}</td>
							  <td>
								  @if ($trans->is_membership)
									 <span class="tag is-warning">Membership Fee</span>
								  @elseif ($trans ->is_donations)
									 <span class="tag is-primary">Donations</span>
								  @else
								  @endif
							  </td>
							  <td>{{ $trans->tranx_id }}</td>
							  <td>{{ $trans->reference }}</td>
							  <td>{{ $trans->currency }}</td>
							  <td>{{ $trans->amount/100 }}</td>
							  <td>{{ $trans->created_at->toFormattedDateString() }}</td>
							</tr>
						  @endforeach
						@endforeach
					  </tbody>
					</table>
				@endif
			  </div>


	</div>

</section>
@endsection











