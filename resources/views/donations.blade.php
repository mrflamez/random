
@extends('layouts.app')

@section('title', 'Support Us')

@section('content')
<section class="hero is-small ">

  <div class="hero-body ">
    <div class="container">
		<div class="heading  m-b-3 has-text-centered">
			<h3 class="title is-bold">Support Us</h3>
		</div>
		<div class=" columns">
	       <div class=" column is-6 is-offset-3">
				<article class="box   has-text-centered ">
					<form id="form-donation-details" method="POST" action="{{ route('pay') }}" accept-charset="UTF-8"  role="form">
						{{-- <input type="hidden" name="amount" value="50">  required in kobo --}}
						<input type="hidden" name="quantity" value="1">
						<input type="hidden" name="reference" value="{{ Paystack::genTranxRef() }}"> {{-- required --}}
						<input type="hidden" name="key" value="{{ config('paystack.secretKey') }}"> {{-- required --}}
						{{ csrf_field() }}
					   <div class="heading  m-b-3 has-text-centered">
							<h4 class="subtitle">Make A Donation</h4>
						</div>
						<h6 class="subtitle"><strong>How much would you like to donate?</strong></h6>
						<div class="field" >
						{{--
						<div class="field  " id="grouped_placeholder_amounts">
						   <p class="control">
							  <button value="10" class="button placeholder_amount b-r-2 is-large is-primary">10</button>
						   </p>
						   <p class="control">
							  <button value="20"  class="button placeholder_amount b-r-2 is-large is-light">20</button>
						   </p>
						   <p class="control">
							  <button value="50"  class="button placeholder_amount b-r-2 is-large is-light">50</button>
						   </p>
						   <p class="control">
							  <button value="100"  class="button placeholder_amount b-r-2 is-large is-light">100</button>
						   </p>
						    <p class="control is-expanded">
							 <input id="donation_f_amount" class="input is-large" type="text" >
						   </p>
						--}}
						   <p class="control is-expanded">
							 <input id="amount" name="amount" class="input" type="number" placeholder="Amount" >
						   </p>
						</div>
						<h6 class="subtitle"><strong>Please tells about yourself ?</strong></h6>
						<div class=" columns">
							<div class=" column is-6">
								<div class="field">
									<p class="control ">
									 <input name="first_name" class="input " type="text" placeholder="First Name">
								   </p>
								</div>
							</div>
							<div class=" column is-6">
								<div class="field">
									<p class="control ">
									 <input name="last_name" class="input " type="text" placeholder="Last Name">
								   </p>
								</div>
							</div>
						</div>
						

						<div class="field">
						  <p class="control">
							<input class="input " name="email" type="text" placeholder="Email">
						  </p>
						</div>
						<h6 class="subtitle"><strong>Any Comments ?</strong></h6>
						<div class="field">
						  <p class="control">
							<textarea class="textarea" placeholder="Message"></textarea>
						  </p>
						</div>
						<div class="field has-addons has-addons-centered">
						  <p class="control">
							<button id="donation-create-btn-control" class="button  is-info">Secure Payment</button>
						  </p>
						</div>
					</form>
					</article>
		   </div>
	    </div>
    </div>
  </div>
</section>


@endsection

@push('scripts')
<script type="text/javascript">
  $(document).ready(function() {
		var regdata = { };
		var metadata = {
			 custom_fields: [
				{
					display_name: "Action",
					variable_name: "action",
					value: "Donation"
				}
			 ]
		};
		$( "input.input" ).focus(function() {
			if ($(this).hasClass("is-danger")) {
				$(this).removeClass("is-danger").next().remove();
				$(this).parent("p.control").nextAll("p").remove();
			}
		});

		function sendValidationForm() {
			 console.log(regdata);
			 $.ajax({
			  headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			  },
			  method: 'POST',
			  url: "{{ route('pay')}}",
			  dataType: 'JSON',
			  data: regdata
			}).done(function(d) {
				console.log(d);
				if (d.success){
					// {{-- server validation ok --}}
					//~ $('#form-donation-details').submit(function(eventObj) {
						//~ $(this).append('<input type="hidden" name="metadata" value="'+ JSON.stringify(metadata) +'">');
						//~ return true;
					//~ });
					$("#form-donation-details").append("<input type='hidden' name='metadata' value='"+ regdata.metadata +"'>");
					$("#form-donation-details").submit();
				} else {
                  // {{-- server validation failed --}}
				  $.each(d.errors, function( key, value ) {
					 if (key === 'email') {
						console.log(value[0]);
						$("#form-donation-details input[name='email']").toggleClass( "is-danger" ).parent("p.control").after( " <p class='help is-danger'>" + value[0] + "</p>" );
					 }
					 if (key === 'first_name') {
						console.log(value[0]);
						$("#form-donation-details input[name='first_name']").toggleClass( "is-danger" ).parent("p.control").after( " <p class='help is-danger'>" + value[0] + "</p>" );
					 }
					 if (key === 'last_name') {
						console.log(value[0]);
						 $("#form-donation-details input[name='last_name']").toggleClass( "is-danger" ).parent("p.control").after( " <p class='help is-danger'>" + value[0] + "</p>" );
					 }
				  });
				}

			  });

		 }

		function validateDetails() {

			var fName = $("#form-donation-details input[name='first_name']");
			var lName = $("#form-donation-details input[name='last_name']");
			var email = $("#form-donation-details input[name='email']");
			var amount = $("#form-donation-details input[name='amount']");

			if ((! fName.val()) && (! fName.hasClass("is-danger"))) {
				fName.toggleClass( "is-danger" ).parent("p.control").after( " <p class='help is-danger'>This field is required</p>" );
			}

			if ((! lName.val()) && (! lName.hasClass("is-danger"))) {
				lName.toggleClass( "is-danger" ).parent("p.control").after( " <p class='help is-danger'>This field is required</p>" );
			}

			if ((! email.val()) && (! email.hasClass("is-danger"))) {
				email.toggleClass( "is-danger" ).parent("p.control").after( " <p class='help is-danger'>This field is required</p>" );
			}
			
			if ((! amount.val()) && (! amount.hasClass("is-danger"))) {
				amount.toggleClass( "is-danger" ).parent("p.control").after( " <p class='help is-danger'>This field is required</p>" );
			}


			if ( fName.val() && lName.val() && email.val() && amount.val() ) {
				regdata.email = email.val();
				regdata.first_name = fName.val();
				regdata.last_name = lName.val();
				regdata.amount = amount.val();
				regdata.metadata = JSON.stringify(metadata);
				return true ;
			} else {
				return false ;
			}
		}



		$('#donation-create-btn-control').click(function(e){
			e.preventDefault();
			$(this).prop("disabled", true);
			var r = validateDetails();
			{{-- console.log($("#form-donation-details input[name='amount']").val()); --}}
			if (r) {
			   sendValidationForm();
			}

			$(this).prop("disabled", false);
		});
		
       {{--
		//~ $('.placeholder_amount').click(function(e){
			//~ e.preventDefault();
			//~ $(".placeholder_amount").removeClass("is-primary");
			//~ $(this).addClass("is-primary");
			//~ var v = $(this).val();
			//~ $("#donation_f_amount").val(v);
		//~ });
	  --}}
  });
</script>
@endpush





