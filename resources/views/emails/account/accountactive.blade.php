@component('mail::message')
# Introduction

Hello {{ $user->f_name }}  {{ $user->l_name }}

@component('mail::panel')
 YOUR ACCOUNT IS NOW ACTIVE
@endcomponent


Thanks,<br>
{{ config('app.name') }}
@endcomponent
