@component('mail::message')
# Introduction

Hello {{ $project -> user->f_name }}  {{ $project -> user->l_name }}

New Comment on your project , {{ $project->title }}

@component('mail::panel')
 " {!! Helper::words( strip_tags($comment->content), $limit = 20, $end = '...') !!} " @ {{ $user->f_name }}
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent


