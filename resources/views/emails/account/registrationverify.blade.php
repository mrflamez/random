@component('mail::message')
# Verification Email

Thanks for joining the fastest growing investment club in Nigeria and Africa at large.
Click the button below to get verified..

@component('mail::button', ['url' => $url])
Click To Verify
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
