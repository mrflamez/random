@component('mail::message')

Hello {{ $project -> user->f_name }}  {{ $project -> user->l_name }} ,

New Payment on your project , {{ $project->title }}


@component('mail::table')
|               |                                                        |
| ------------- |:------------------------------------------------------:|
| Amount        | {{ $payment -> amount }}                               |
@endcomponent


Thanks,<br>
{{ config('app.name') }}
@endcomponent


