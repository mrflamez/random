@component('mail::message')

Your Project has being created

#  {{ $project -> title }}

@component('mail::panel')
{!! Helper::words( strip_tags($project->description), $limit = 10, $end = '...') !!}
@endcomponent

@component('mail::table')
|               |                                                        |
| ------------- |:------------------------------------------------------:|
| Amount        | {{ $project -> amount }}                               |
| Start         | {{ $project -> start_date -> toFormattedDateString() }}|
| End           | {{ $project -> end_date -> toFormattedDateString() }}  |
@endcomponent

@component('mail::button', ['url' => route('projects.show', ['id' => $project-> id]) ])
View Project
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent

