
@extends('layouts.app')

@section('title', 'Verify Email')

@section('content')
<section class="hero is-small header-overlay-bg1 is-bold">
  <div class="hero-body bg-gray">
    <div class="container">
		<div class="heading  m-b-3 has-text-centered">
			<h3 class="title txt-green-yellow ">Verify Email to Continue</h3>

		</div>
    </div>
  </div>
</section>

<section class="section">
	<div class=" columns">

		<div class=" column is-6 is-offset-3">
			<h5 class="title has-text-centered txt-green-yellow ">Login To Verify Account</h5>
			<div class="field">
			  <label class="label">Email </label>
			  <p class="control has-icons-left has-icons-right">
				<input class="input is-hovered" name="email" type="email" placeholder="Email">
				<span class="icon is-small is-left">
				  <i class="fa fa-envelope"></i>
				</span>
			  </p>
			</div>
			<div class="field">
			  <label class="label">Password </label>
			  <p class="control has-icons-left has-icons-right">
				<input class="input is-hovered" name="password" type="password" placeholder="****">
				<span class="icon is-small is-left">
				  <i class="fa fa-lock"></i>
				</span>
			  </p>
			</div>
			<div class="field">
			  <p class="control">
				<button id="verify-btn-control" class="button is-success">
				  Verify Email
				</button>
			  </p>
			</div>
		</div>


</div>
</section>

@endsection

@push('scripts')
<script type="text/javascript">


  $(document).ready(function() {
		var regdata = { };
		$( "input.input" ).focus(function() {
			if ($(this).hasClass("is-danger")) {
				$(this).removeClass("is-danger").next().remove();
				$(this).parent("p.control").nextAll("p").remove();
			}
		});

		 function sendFormReg() {
			 console.log(regdata);
			 $.ajax({
			  headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			  },
			  method: 'POST',
			  url: "{{ route('emailverify.store', ['token' => $token]) }}",
			  dataType: 'JSON',
			  data: regdata
			}).done(function(d) {
				console.log(d);
				if (d.success) {
					window.location.replace(d.path)
				} else {
					$.each(d.errors, function( key, value ) {
						 if (key === 'email') {
							$("input[name='email']").toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>" + value[0] + "</p>" );
						 }

						 if (key === 'password') {
							$("input[name='password']").toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>" + value[0] + "</p>" );
						}
						if (key === 'auth') {
							$("input[name='password']").toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>" + value[0] + "</p>" );
							$("input[name='email']").toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>" + value[0] + "</p>" );
						}
                  });
				}

			  });
		 }

		 function validateDetails() {
			//$( "p.control" ).addClass( "is-loading" );

			var email = $("input[name='email']");
			var password = $("input[name='password']");

			if ((! email.val()) && (! email.hasClass("is-danger"))) {
				email.toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>This field is required</p>" );
			}

			if ((! password.val()) && (! password.hasClass("is-danger"))) {
				password.toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>This field is required</p>" );
			}

			console.log(password.val());

			if ((email.val()) && (password.val())) {
				regdata.email = email.val();
				regdata.password = password.val();
				$( "input.input" ).each(function() {
					if ($(this).hasClass("is-danger")) {
						$(this).removeClass("is-danger").next().remove();
						$(this).parent("p.control").nextAll("p").remove();
					}
				});
				return true ;
			} else {
				return false ;
			}
		 }


		$('#verify-btn-control').click(function(e){
			e.preventDefault();
			$(this).prop("disabled", true);
			var r = validateDetails();
			if (r) {
				sendFormReg();
			}

			$(this).prop("disabled", false);
		});
  });
</script>
@endpush



