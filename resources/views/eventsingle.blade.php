@extends('layouts.app')

@section('title', $event-> title)

@section('content')
<section class="hero is-small header-overlay-bg1 is-bold">
  <div class="hero-body bg-gray">
    <div class="container">
		<div class="heading  m-b-3 has-text-centered">
			<h3 class="title txt-green-yellow ">{{ $event-> title }}</h3>
			<h5 class="subtitle txt-green-yellow"> {{ $event-> tag_line }}.</h5>
		</div>
    </div>
  </div>
</section>

<section class="section">
	<div class="columns">
	<div class="column  is-half">
	  <section class="content has-text-centered is-primary">
			@include('partials.admin.eventheading')
			<div class="content">
				{!!  $event-> description  !!}
		   </div>
	  </section>
	</div>
	<div class="column ">
		@forelse ($event -> images as $image)
		   @if ($loop->first)
		   <figure class="image">
			<img src="{{ asset('storage/'.$image->name) }}" alt="">
		   </figure>
		   @endif
		@empty
			<p>No Image</p>
		@endforelse
  </div>
</div>
</section>

@endsection





