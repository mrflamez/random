@extends('layouts.app')

@section('title', 'Events')

@section('content')
<section class="hero is-small  is-primary is-bold">
  <div class="hero-head ">
    <div class="container">
		<div class="heading  has-text-centered">
			<h3 class="title">Get Help And Support</h3>
		</div>
    </div>
  </div>
  <div class="hero-body ">
    <div class="container">
		<div class="field">
			  <p class="control has-icons-left ">
				<input class="input is-large is-hovered"  type="text" placeholder="Search Help">
				<span class="icon is-small is-left">
				  <i class="fa fa-search"></i>
				</span>
			  </p>
			</div>
    </div>
  </div>
</section>

<section class="section">
	<div class=" columns">

		<div class=" column is-6">
			<p class="title">Members</p>
			<div class="panel">
				@forelse ($faq_members as $faq)
					<a href="{{ route('faqs.show', ['id' => $faq -> id ]) }}" class="panel-block"> {{ $faq->title }}</a>
				@empty
					<p>No Faqs available</p>
				@endforelse
			</div>
		</div>
		<div class=" column ">
			<p class="title">Donators</p>
			<div class="panel">
			   @forelse ($faq_donators as $faq)
					<a href="{{ route('faqs.show', ['id' => $faq -> id ]) }}" class="panel-block"> {{ $faq->title }}</a>
				@empty
					<p>No Faqs available</p>
				@endforelse
			</div>
		</div>
    </div>
</section>

@endsection





