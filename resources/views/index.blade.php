@extends('layouts.app')

@section('title', 'Home')

@section('content')

@if (! $settings->sliderimages->isEmpty())
<section class="columns is-gapless is-paddingless is-marginless">
    <div class="column">
	   <!-- Slider Revolution Start -->

	   <div class="rev_slider_wrapper txt-green-yellow">
		 <div class="rev_slider" data-version="5.0">
			<ul>
				@include('partials.sliderimage')


			</ul>
		  </div>
	  </div>
	  <!-- end .rev_slider -->
	</div>
</section>
@endif

<section class="columns b-bt is-primary is-gapless is-paddingless is-marginless">
   <div class="column is-4 b-br">
			  <h3 class="m-t-1-5 has-text-centered"><strong>Business Benefits</strong></h3>
			  <div class="content m-b-1-5">
				<ul>
					<li>Access to potential investors</li>
					<li>Access to investment opportunities</li>
					<li>Access to world class business men and entrepreneurs</li>
				  </ul>
			  </div>
   </div>
  <div class="column is-4 b-br">

			  <h3 class="m-t-1-5 has-text-centered"><strong>Social Benefits</strong></h3>
			  <div class="content m-b-1-5">
				  <ul>
					<li>Access to two annual international trip</li>
					<li>Access to world class resort centers in Nigeria</li>
					<li>Discount on services at choice hotels and reservation centers in Nigeria</li>

				  </ul>

			  </div>
   </div>
   <div class="column is-4 b-br">

			  <h3 class="m-t-1-5 has-text-centered"><strong>Health Benefits</strong></h3>
			  <div class="content m-b-1-5">
				  <ul>
					<li>Cover for health care plans</li>
					<li>Access to high brow sporting and fitness centers in Nigeria</li>
					<li>Access to high brow sporting and fitness centers</li>
				  </ul>

			  </div>

   </div>
</section>


<section class="hero is-default is-bold header-overlay-bg2">

  <div class="hero-body">
    <div class="container">
		<nav class="level is-mobile">
		  <div class="level-item has-text-centered ">
			<div>
			  <p class="heading is-1 txt-green-yellow"><strong>Investors</strong></p>
			  <p class="title is-1 txt-green-yellow "><strong>{{ $settings-> investors }}</strong></p>
			</div>
		  </div>
		  <div class="level-item has-text-centered">
			<div>
			  <p class="heading is-1 txt-green-yellow"><strong>Partners</strong></p>
			  <p class="title is-1 txt-green-yellow "><strong>{{ $settings-> partners }}</strong></p>
			</div>
		  </div>
		  <div class="level-item has-text-centered">
			<div>
			  <p class="heading is-1 txt-green-yellow"><strong>Bonus Packages</strong></p>
			  <p class="title is-1 txt-green-yellow "><strong>{{ $settings-> bonus }}</strong></p>
			</div>
		  </div>
		  <div class="level-item has-text-centered">
			<div>
			  <p class="heading is-1 txt-green-yellow"><strong>Satifaction</strong></p>
			  <p class="title is-1 txt-green-yellow "><strong>{{ $settings-> satifaction }}%</strong></p>
			</div>
		  </div>
		</nav>
	</div>
  </div>
</section>

<section class="section">
	<div class="container">
      <div class="heading m-b-3 has-text-centered">
        <h3 class="title">How It Works</h3>
      </div>
    </div>
    <div class=" columns ">
		<div class="column is-4">

			<div class="field  has-addons has-addons-centered">
				  <p class="control">
					  <button class="is-outlined is-primary is-large button">
						<span class="icon  ">
						  <i class="fa fa-lightbulb-o"></i>
						</span>
					  </button>
				 </p>
			</div>
			<p class="title is-5 is-bold has-text-centered"> Register to Join</p>
			<div class="content ">
				 Register and get your club membership identification number.
			 </div>
	    </div>
		<div class="column is-4">

			<div class="field  has-addons has-addons-centered">
				  <p class="control">
					<button class="is-info is-outlined is-large button">
						<span class="icon  ">
						  <i class="fa fa-money"></i>
						</span>
					</button>
				 </p>
			</div>
			<p class="title is-5 is-bold has-text-centered"> Attend Club Events</p>
			<div class="content ">
				 Ensure to attend at least one event per month.
			 </div>
	    </div>
		<div class="column is-4">

			<div class="field  has-addons has-addons-centered">
				  <p class="control">
					  <button class="is-warning is-outlined is-large button">
						<span class="icon ">
						  <i class="fa fa-line-chart"></i>
						</span>
					</button>
				 </p>
			</div>
			<p class="title is-5 is-bold has-text-centered">Grow with the Club</p>
			<div class="content ">
				  Stay focsed on our objective and grow with us.
			 </div>
	    </div>

    </div>
</section>

<section class="section">
	<div class="container">
      <div class="heading m-b-3 has-text-centered">
        <h3 class="title">WHO WE ARE</h3>
        <h5 class="subtitle">We assist entrepreneurs. We connect investees with Investors.</h5>
      </div>
    </div>
    <div class=" columns">
		<div class="column">
			<article class="content">
			  <p>
				  It is a club that is created to assists entrepreneurs to reach their maximum height leveraging on the knowledge and connection of already established industry experts.
					We connects investees(people with idea) with investors(people that have the money). We are the fastest growing investment club in Nigeria and Africa at large.
			  </p>
			  <p>
				  Our goal is to grow our membership to 1million in 5years globally and at the moment IIclub has been able to establish herself firmly in US and Dubai.
			  </p>
			  <p>
				  Our Vision is to raise billionaire investors with global impact.
			  </p>
			  <p>
				  Our mission is to create a community that fosters investment culture for financial freedom.
			  </p>

		  </article>
			<div class=" columns is-multiline">
			   <div class="column is-4">
					<div class="columns">
						<div class="column is-narrow">
							<div class="v-centered">
							  <div class="">
								  <a class=" is-info is-outlined button is-large">
										<span class="icon">
										  <i class="fa fa-line-chart"></i>
										</span>
									</a>
							  </div>
							</div>
						 </div>
						<div class="column">

						  <h3 class=""><strong>Investment</strong></h3>

						</div>
					</div>

			   </div>
			   <div class="column is-4">
					<div class="columns">
						<div class="column is-narrow">
							<div class="v-centered">
							  <div class="">
								  <a class=" is-info is-outlined button is-large">
										<span class="icon">
										  <i class="fa fa-money"></i>
										</span>
								   </a>
							  </div>
							</div>
						 </div>
						<div class="column">

						  <h3 class=""><strong>Grant Seeking</strong></h3>

						</div>
					</div>

			   </div>
			   <div class="column is-4">
					<div class="columns">
						<div class="column is-narrow">
							<div class="v-centered">
							  <div class="">
								<a class=" is-info is-outlined button is-large">
									<span class="icon">
									  <i class="fa fa-handshake-o"></i>
									</span>
								 </a>
							  </div>
							</div>
						 </div>
						<div class="column">

						  <h3 class=""><strong>Networking</strong></h3>

						</div>
					</div>

			   </div>
			   <div class="column is-4">
					<div class="columns">
						<div class="column is-narrow">
							<div class="v-centered">
							  <div class="">
								<a class=" is-info is-outlined button is-large">
									<span class="icon">
									  <i class="fa fa-sitemap"></i>
									</span>
								 </a>
							  </div>
							</div>
						 </div>
						<div class="column">

						  <h3 class=""><strong>Mass Empowerment</strong></h3>

						</div>
					</div>

			   </div>
			   <div class="column is-4">
					<div class="columns">
						<div class="column is-narrow">
							<div class="v-centered">
							  <div class="">
								<a class=" is-info is-outlined button is-large">
									<span class="icon">
									  <i class="fa fa-graduation-cap"></i>
									</span>
								 </a>
							  </div>
							</div>
						 </div>
						<div class="column">

						  <h3 class=""><strong>Training</strong></h3>

						</div>
					</div>

			   </div>
			   <div class="column is-4">
					<div class="columns">
						<div class="column is-narrow">
							<div class="v-centered">
							  <div class="">
								<a class=" is-info is-outlined button is-large">
									<span class="icon">
									  <i class="fa fa-cogs"></i>
									</span>
								 </a>
							  </div>
							</div>
						 </div>
						<div class="column">

						  <h3 class=""><strong>Thrift</strong></h3>

						</div>
					</div>

			   </div>

		  </div>
	    </div>
	    <div class="column is-5 ">
            <div class="block full-width">
			   <iframe class="video" src="https://www.youtube.com/embed/ob5HMRWcrB0" frameborder="0" allowfullscreen></iframe>
			</div>
	    </div>
	</div>
</section>


<section class="section">
 <div class="columns">
	<div class="column  is-7">
		@isset( $event)
		<h3 class="title has-text-centered">Upcoming Event</h3>
		<div class="card">
		  @forelse ($event -> images as $image)
			   @if ($loop->first)
			   <div class="card-image">
				  <figure class="image is-2by1">
					  <img src="{{ asset('storage/'.$image->name) }}" alt="">
				  </figure>
			   </div>
			   @endif
			@empty

			@endforelse

		  <div class="card-content">
		    <p class="title is-4">{{ $event-> title }}</p>
			<p class="">
				<span class="">
					<span class="icon is-small"><i class="fa fa-calendar"></i></span>  Venue : {{ $event-> venue }}
				</span>
				@isset( $event->fee)
				<span class="is-pulled-right">
					<span class="icon is-small"><i class="fa fa-calendar"></i></span> Fee:    {{ $event->fee }}
				</span>
				@endisset
			</p>
			<p class="m-b-0-7-5 m-t-0-2">
			@isset( $event->start_date)

				  <span class="icon is-small">
					  <i class="fa fa-calendar"></i></span> From : {{ $event->start_date ->toFormattedDateString() }}
			  @endisset
			</p>
			<div class="content">
			  {!! Helper::words( strip_tags($event->description), $limit = 25, $end = '...') !!}
			</div>
		  </div>
		  <footer class="card-footer">
			<a class="card-footer-item">View More</a>
		  </footer>
		</div>
		@endisset
	</div>
	<div class="column ">
		<h3 class="title has-text-centered">Testimonials</h3>
		@forelse ($testimony as $post)
		   @include('partials.singletestimony', ['post' => $post])
		@empty

		@endforelse
    </div>
 </div>
</section>


@isset( $posts)
<section class="section">
	<div class="heading m-b-3 has-text-centered">
		<h3 class="title">Blog Posts</h3>
	 </div>
	<div class=" columns">
		@forelse ($posts as $post)
		   @include('partials.postsinglegrid', ['post' => $post])
		@empty

		@endforelse
    </div>
</section>
@endisset

@if (! $settings->sponsorsimages->isEmpty())
<section class="section">
	<div class="heading m-b-3 has-text-centered">
		<h3 class="title">Our Sponsors</h3>
	 </div>
	 <div class=" columns is-multiline">
        @forelse ($settings->sponsorsimages as $sponsorsimage)
		 <div class=" column is-narrow">
			 <figure class="image is-128x128">
				 <img src="{{ asset('storage/'.$sponsorsimage->name) }}" alt="">
			 </figure>
		 </div>
		 @empty

		@endforelse
	 </div>
</section>
@endif

@endsection

@push('header-css')
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick-theme.css"/>
<!-- Revolution Slider 5.x CSS settings -->
<link  href="{{ asset('js/revolution-slider/css/settings.css') }}" rel="stylesheet" type="text/css"/>
<link  href="{{ asset('js/revolution-slider/css/layers.css') }}" rel="stylesheet" type="text/css"/>
<link  href="{{ asset('js/revolution-slider/css/navigation.css') }}" rel="stylesheet" type="text/css"/>
@endpush

@push('header-scripts')
<script src="{{ asset('js/revolution-slider/js/jquery.themepunch.tools.min.js') }}"></script>
<script src="{{ asset('js/revolution-slider/js/jquery.themepunch.revolution.min.js') }}"></script>
@endpush

@push('scripts')
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
<script type="text/javascript">

 $(document).ready(function(){
   $('.sponsors').slick({
	  dots: false,
	  infinite: true,
	  speed: 300,
	  slidesToShow: 4,
	  slidesToScroll: 4,
	  responsive: [
		{
		  breakpoint: 1024,
		  settings: {
			slidesToShow: 3,
			slidesToScroll: 3,
			infinite: true,
			dots: true
		  }
		},
		{
		  breakpoint: 600,
		  settings: {
			slidesToShow: 2,
			slidesToScroll: 2
		  }
		},
		{
		  breakpoint: 480,
		  settings: {
			slidesToShow: 1,
			slidesToScroll: 1
		  }
		}
	  ]
	});
 });

</script>
<script type="text/javascript">
  $(document).ready(function(e) {
	$(".rev_slider").revolution({
	  sliderType:"standard",
	  sliderLayout: "auto",
	  dottedOverlay: "none",
	  delay: 5000,
	  navigation: {
		  keyboardNavigation: "off",
		  keyboard_direction: "horizontal",
		  mouseScrollNavigation: "off",
		  onHoverStop: "off",
		  touch: {
			  touchenabled: "on",
			  swipe_threshold: 75,
			  swipe_min_touches: 1,
			  swipe_direction: "horizontal",
			  drag_block_vertical: false
		  },
		  arrows: {
			  style:"zeus",
			  enable:true,
			  hide_onmobile:true,
			  hide_under:600,
			  hide_onleave:true,
			  hide_delay:200,
			  hide_delay_mobile:1200,
			  tmp:'<div class="tp-title-wrap"><div class="tp-arr-imgholder"></div></div>',
			  left: {
				h_align:"left",
				v_align:"center",
				h_offset:30,
				v_offset:0
			  },
			  right: {
				h_align:"right",
				v_align:"center",
				h_offset:30,
				v_offset:0
			  }
			},
		  bullets: {
			  enable:true,
			  hide_onmobile:true,
			  hide_under:600,
			  style:"metis",
			  hide_onleave:true,
			  hide_delay:200,
			  hide_delay_mobile:1200,
			  direction:"horizontal",
			  h_align:"center",
			  v_align:"bottom",
			  h_offset:0,
			  v_offset:30,
			  space:5
		  }
	  },
	  responsiveLevels: [1240, 1024, 778],
	  visibilityLevels: [1240, 1024, 778],
	  gridwidth: [1170, 1024, 778, 480],
	  gridheight: [400, 768, 960, 720],
	  lazyType: "none",
	  parallax: {
		  origo: "slidercenter",
		  speed: 1000,
		  levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 46, 47, 48, 49, 50, 100, 55],
		  type: "scroll"
	  },
	  shadow: 0,
	  spinner: "off",
	  stopLoop: "on",
	  stopAfterLoops: 0,
	  stopAtSlide: -1,
	  shuffle: "off",
	  autoHeight: "off",
	  fullScreenAutoWidth: "off",
	  fullScreenAlignForce: "off",
	  fullScreenOffsetContainer: "",
	  fullScreenOffset: "0",
	  hideThumbsOnMobile: "off",
	  hideSliderAtLimit: 0,
	  hideCaptionAtLimit: 0,
	  hideAllCaptionAtLilmit: 0,
	  debugMode: false,
	  fallbacks: {
		  simplifyAll: "off",
		  nextSlideOnWindowFocus: "off",
		  disableFocusListener: false,
	  }
	});
  });
</script>
@endpush
