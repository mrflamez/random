@extends('layouts.app')

@section('title', 'Join')

@section('content')
<section class="hero is-small header-overlay-bg1 is-bold">
  <div class="hero-body bg-gray">
    <div class="container">
		<div class="heading  m-b-3 has-text-centered">
			<h3 class="title txt-green-yellow ">Join</h3>
			<h5 class="subtitle txt-green-yellow"> </h5>
		</div>
    </div>
  </div>
</section>

<section class="section">
	<div class=" columns">
		<div class=" column is-8 is-offset-2">
			<div class="heading  m-b-3 has-text-centered">
				<h5 class="title txt-green-yellow ">Register</h5>
				<h6 class="subtitle txt-green-yellow"> Gettting Started in 3 Steps</h6>
			</div>
		   <div class="tabs is-fullwidth">
			  <ul>
				<li class="is-active tab-item-general-details">
				  <a>
					<span>1. General Details</span>
					<span class="icon"><i class="fa fa-angle-right"></i></span>
				  </a>
				</li>
				<li class=" tab-item-profile-details">
				  <a>
					<span>2. Account Info</span>
					<span class="icon"><i class="fa fa-angle-right"></i></span>
				  </a>
				</li>
				<li class="tab-item-finish">
				 <a>
					<span>3. Verify Email</span>
					<span class="icon"><i class="fa fa-angle-right"></i></span>
				  </a>

				</li>
			  </ul>
			</div>

			<div class=" columns " id="tab-general-details">
				<div class=" column ">
					<div class=" columns ">
						<div class=" column">
							<div class="field">
							  <label class="label">First Name </label>
							  <p class="control has-icons-left has-icons-right">
								<input class="input is-hovered" name="f-name" required type="text" placeholder="First Name">
								<span class="icon is-small is-left">
								  <i class="fa fa-user-circle-o"></i>
								</span>
							  </p>
							</div>
						</div>
						<div class=" column">
							<div class="field">
							  <label class="label">Last Name </label>
							  <p class="control has-icons-left has-icons-right">
								<input name="l-name" required  class="input is-hovered" type="text" placeholder="Last Name">
								<span class="icon is-small is-left">
								  <i class="fa fa-user-circle-o"></i>
								</span>
							  </p>
							</div>

						</div>
					</div>
					<div class=" columns">
						<div class=" column is-half">
							<div class="field">
							  <label class="label">Country </label>
							  <p class="control has-icons-left">
								<span class="select">
								  <select id="user_country">
									  @foreach ($countries as $country)
										<option value="{{ $country->id }}"
										@if($country->name === 'Nigeria')
										 selected
										@endif
										> {{ $country->name }}</option>
									  @endforeach
								  </select>
								</span>
								<span class="icon is-small is-left">
								  <i class="fa fa-globe"></i>
								</span>
							  </p>
							</div>
						</div>
						<div class=" column">
							<div class="field">
							  <label class="label">Address </label>
							  <p class="control has-icons-left has-icons-right">
								<input name="address" required class="input is-hovered" type="text" placeholder="Address">
								<span class="icon is-small is-left">
								  <i class="fa fa-map-marker"></i>
								</span>
							  </p>
							</div>
						</div>
					</div>
					<div class=" columns">

						<div class=" column">
							<div class="field">
							  <label class="label">Telephone </label>
							  <p class="control has-icons-left has-icons-right">
								<input name="telephone" required class="input is-hovered" type="text" placeholder="Telephone">
								<span class="icon is-small is-left">
								  <i class="fa fa-mobile"></i>
								</span>
							  </p>
							</div>

						</div>
					</div>

				</div>
				<div class=" column is-4 ">
					<label class="label m-b-1-5 is-marginless">Avatar </label>
					<p class="help is-danger user-avartar-label is-hidden">This field is required</p>
					<div  class="columns full-90-h is-marginless">
						<div  class="column">
							<div id="user-avartar"  class=" dropzone">

						    </div>
						</div>
					</div>

				</div>

			</div>

			<div class=" columns is-hidden" id="tab-profile-details">
				<div class=" column is-6 is-offset-3">
					<div class="field">
					  <label class="label">Email Address </label>
					  <p class="control has-icons-left has-icons-right">
						<input class="input is-hovered" name="email" type="email" placeholder="Email">
						<span class="icon is-small is-left">
						  <i class="fa fa-envelope"></i>
						</span>
					  </p>
					</div>
					<div class="field">
					  <label class="label">UserName </label>
					  <p class="control has-icons-left has-icons-right">
						<input class="input is-hovered" name="username" type="text" placeholder="UserName">
						<span class="icon is-small is-left">
						  <i class="fa fa-user"></i>
						</span>
					  </p>
					</div>
					<div class="field">
					  <label class="label">Password</label>
					  <p class="control has-icons-left has-icons-right">
						<input class="input is-hovered" name="password" type="password" placeholder="*****">
						<span class="icon is-small is-left">
						  <i class="fa fa-unlock-alt"></i>
						</span>
					  </p>
					</div>
					<div class="field">
					  <label class="label">Repeat Password</label>
					  <p class="control has-icons-left has-icons-right">
						<input class="input is-hovered " name="repeat-password" type="password" placeholder="*****">
						<span class="icon is-small is-left">
						  <i class="fa fa-unlock-alt"></i>
						</span>
					  </p>
					</div>
					<div class="field">
					  <label class="label">Referal Code (optional)</label>
					  <p class="control has-icons-left has-icons-right">
						<input class="input is-hovered " name="refer_code" type="text" placeholder="Referal Code">
						<span class="icon is-small is-left">
						  <i class="fa fa-bookmark"></i>
						</span>
					  </p>
					</div>
					<div class="field">
					  <p class="control">
						<label class="checkbox">
						  <input type="checkbox">
						  I agree to the <a href="{{ route('rules.index') }}">terms and conditions</a>
						</label>
					  </p>
					</div>

				</div>
			</div>

			<div class=" columns is-hidden" id="tab-finish">
				<div class=" column">
					<article class="v-centered">
						<div class="has-text-centered">
							<div class="field  has-addons has-addons-centered">
								  <p class="control">
									  <span class="icon is-large">
										  <i class="fa fa-check-circle-o"></i>
										</span>
								 </p>
							</div>
							<p class="title is-4">Congrtulations</p>
							<p class="title is-6">
								Thanks for joining the fastest growing investment club in Nigeria and Africa at large.
								Please Check you email on instructions to get verified.

							</p>

							<div class="field  has-addons has-addons-centered">
								  <p class="control">
									  <button id="resend-verify-account-email" class="button ">
										Resend Verificatiion email
									  </button>
								 </p>
							</div>
						</div>
					</article>
				</div>
			</div>

			<nav class="level is-mobile b-tp m-t-1-5">
			  <div class="level-left"></div>
			  <div class="level-right">
				<div class="level-item">
					<div class="field m-t-1-5">
					  <p class="control">
						<button class="button is-medium is-primary btn-at-general" id="reg-btn-control">Next</button>
					  </p>
					</div>
			    </div>
			  </div>
			</nav>

		</div>
    </div>
</section>

@endsection

@push('scripts')
<script src="{{ asset('js/dropzone.js') }}"></script>
<script type="text/javascript">
  Dropzone.autoDiscover = false;
  var myDropzone = new Dropzone("#user-avartar" , {
    paramName: "file",
    maxFilesize: 1,
    headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	},
	url: "{{ route('photos.store')}}",
	addRemoveLinks : true,
	autoProcessQueue: false,
	uploadMultiple: false,
    parallelUploads: 1
  });

  $(document).ready(function() {
		var regdata = { };
		$( "input.input" ).focus(function() {
			if ($(this).hasClass("is-danger")) {
				$(this).removeClass("is-danger").next().remove();
				$(this).parent("p.control").nextAll("p").remove();
			}
		});

		 function sendFormReg(btn) {
			 // console.log(regdata);
			 $.ajax({
			  headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			  },
			  method: 'POST',
			  url: "{{ route('users.store')}}",
			  dataType: 'JSON',
			  data: regdata
			}).done(function(d) {
				console.log(d);
				if (d.success){
					var avatar = myDropzone.getQueuedFiles();
                    myDropzone.on("sending", function(file, xhr, formData) {
					  formData.append("userid", d.id);
					  console.log(formData);
					});

					if (avatar.length > 0) {

						myDropzone.processQueue();

					}

					$("#resend-verify-account-email").val(d.id);
					$("#tab-profile-details" ).addClass( "is-hidden" );
					$("#tab-finish" ).removeClass( "is-hidden" );
					$(".tab-item-finish" ).addClass( "is-active" );
					btn.removeClass("btn-at-profile").addClass("btn-at-finish is-hidden");


				} else {
					$.each(d.errors, function( key, value ) {
						 if (key === 'email') {
							$("#tab-profile-details input[name='email']").toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>" + value[0] + "</p>" );
						 }
						  if (key === 'referal') {
							$("#tab-profile-details input[name='refer_code']").toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>" + value[0] + "</p>" );
						 }
						 if (key === 'username') {
							$("#tab-profile-details input[name='username']").toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>" + value[0] + "</p>" );
						 }
						 if (key === 'password') {
							$("#tab-profile-details input[name='password']").toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>" + value[0] + "</p>" );
							$("#tab-profile-details input[name='repeat-password']").toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>" + value[0] + "</p>" );
						 }
                  });
				}

			  });
		 }

		function validateDetails() {
			//$( "#tab-profile-details p.control" ).addClass( "is-loading" );

			var email = $("#tab-profile-details input[name='email']");
			var username = $("#tab-profile-details input[name='username']");
			var password = $("#tab-profile-details input[name='password']");
			var passwordRepeat = $("#tab-profile-details input[name='repeat-password']");
			var referal = $("#tab-profile-details input[name='refer_code']");


			if ((! email.val()) && (! email.hasClass("is-danger"))) {
				email.toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>This field is required</p>" );
			}
			if ((! username.val()) && (! username.hasClass("is-danger"))) {
				username.toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>This field is required</p>" );
			}
			if ((! password.val()) && (! password.hasClass("is-danger"))) {
				password.toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>This field is required</p>" );
			}
			if ((! passwordRepeat.val()) && (! passwordRepeat.hasClass("is-danger"))) {
				passwordRepeat.toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>This field is required</p>" );
			}

			console.log(password.val());

			if ((password.val()) && (passwordRepeat.val()) && (password.val() != passwordRepeat.val())) {
				password.toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>Password Do Not Match</p>" );
				passwordRepeat.toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>Password Do Not Match</p>" );
			}

			//$( "#tab-general-details p.control" ).removeClass( "is-loading" );

			if ((email.val()) && (username.val()) && (password.val()) && (passwordRepeat.val()) && (password.val() === passwordRepeat.val())) {
				regdata.email = email.val();
				regdata.username = username.val();
				regdata.password = password.val();
				regdata.password = password.val();
				regdata.referal = referal.val();

				regdata.password_confirmation = passwordRepeat.val();

				$( "#tab-profile-details input.input" ).each(function() {
					if ($(this).hasClass("is-danger")) {
						$(this).removeClass("is-danger").next().remove();
						$(this).parent("p.control").nextAll("p").remove();
					}
				});

				return true ;
			} else {
				return false ;
			}
		 }

	    function validateGenP() {
			$( "#tab-general-details p.control" ).toggleClass( "is-loading" );

			var fName = $("#tab-general-details input[name='f-name']");
			var lName = $("#tab-general-details input[name='l-name']");
			var address = $("#tab-general-details input[name='address']");
			var telephone = $("#tab-general-details input[name='telephone']");
            var userCountry = $("#user_country");

			if ((! fName.val()) && (! fName.hasClass("is-danger"))) {
				fName.toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>This field is required</p>" );
			}
			if ((! lName.val()) && (! lName.hasClass("is-danger"))) {
				lName.toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>This field is required</p>" );
			}
			if ((! address.val()) && (! address.hasClass("is-danger"))) {
				address.toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>This field is required</p>" );
			}
			if ((! telephone.val()) && (! telephone.hasClass("is-danger"))) {
				telephone.toggleClass( "is-danger" ).after( "<span class='icon is-small is-right'> <i class='fa fa-warning'></i></span>").parent("p.control").after( " <p class='help is-danger'>This field is required</p>" );
			}

			$( "#tab-general-details p.control" ).toggleClass( "is-loading" );
			//~ var avatar = myDropzone.getQueuedFiles();

			//~ if (!(avatar.length > 0)) {
				//~ $( ".user-avartar-label" ).toggleClass( "is-hidden" );
			//~ }

			if ((fName.val()) && (lName.val()) && (address.val()) && (telephone.val())) {
				regdata.fname = fName.val();
				regdata.lname = lName.val();
				regdata.address = address.val();
				regdata.phone = telephone.val();
				regdata.country = userCountry.val();
				return true ;
			} else {
				return false ;
			}
		}

		$('#resend-verify-account-email').click(function(e){
			e.preventDefault();
			var verifyButon = $(this).val() ;
			$(this).remove();
		});

		$('#reg-btn-control').click(function(e){
			e.preventDefault();
			if ($(this).hasClass("btn-at-general")) {
				$(this).prop("disabled", true);
				var r = validateGenP();
				if (r) {
					$( "#tab-general-details" ).toggleClass( "is-hidden" );
					$( "#tab-profile-details" ).toggleClass( "is-hidden" );
					$( ".tab-item-profile-details" ).addClass( "is-active" );
					$(this).removeClass("btn-at-general").addClass("btn-at-profile");
				}
				$(this).prop("disabled", false);
				return;
			}

			if ($(this).hasClass("btn-at-profile")) {
				$(this).prop("disabled", true);
				var r = validateDetails();
				if (r) {
					sendFormReg($(this));
				}

				$(this).prop("disabled", false);
				return;
			}
		});
  });

</script>
@endpush



