<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Infinity Investment Club - @yield('title')</title>
  <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
  <link rel="stylesheet"  href="{{ mix('css/app.css') }}" >
  <link rel="stylesheet"  href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" >
  @stack('header-css')
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js" ></script>
  @stack('header-scripts')
</head>
<body>

@include('layouts.header')

@yield('content')

@include('layouts.footer')

<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script src="{{ mix('js/app.js') }}"></script>

@stack('scripts')



</body>
</html>
