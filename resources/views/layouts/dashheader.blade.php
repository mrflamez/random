<ul>
  <li class="link {{{ (Route::is('myprojects') ? 'is-active' : '') }}} " ><a href="{{ route('myprojects')}}" ><span class="icon"><i class="fa fa-list"></i></span> <span>Projects</span></a></li>
  {{--<li class="link {{{ (Route::is('mycontributions') ? 'is-active' : '') }}} "><a href="{{ route('mycontributions')}}" ><span class="icon"><i class="fa fa-heart"></i></span> <span>Transactions</span></a></li> --}}
  <li class="link {{{ (Route::is('mymessages') ? 'is-active' : '') }}} "><a href="{{ route('mymessages')}}" ><span class="icon"><i class="fa fa-th"></i></span> <span>Mailbox</span></a></li>
  <li class="link {{{ (Route::is('profile.index') ? 'is-active' : '') }}} "><a href="{{ route('profile.index')}}" ><span class="icon"><i class="fa fa-user-o"></i></span> <span> Profile</span></a></li>
  <li class="link {{{ (Route::is('dash.transactions') ? 'is-active' : '') }}} "><a href="{{ route('dash.transactions')}}" ><span class="icon"><i class="fa fa-heart"></i></span> <span> Transactions</span></a></li>
  <li class="link {{{ (Route::is('dash.membership') ? 'is-active' : '') }}} "><a href="{{ route('dash.membership')}}" ><span class="icon"><i class="fa fa-bookmark"></i></span> <span> Membership</span></a></li>
  <li class="link {{{ (Route::is('dash.commisions') ? 'is-active' : '') }}} "><a href="{{ route('dash.commisions')}}" ><span class="icon"><i class="fa fa-money"></i></span> <span> Commisions</span></a></li>
  <li class="link {{{ (Route::is('dash.banking') ? 'is-active' : '') }}} "><a href="{{ route('dash.banking')}}" ><span class="icon"><i class="fa fa-bank"></i></span> <span> Banking</span></a></li>
</ul>
