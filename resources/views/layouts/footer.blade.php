<footer class="footer p-b-3 is-dark">
 <div class=" columns">
	<div class="column">
		<figure class="image is-2by3">
		   <img src="{{ asset('images/logo.jpeg') }}" >
		</figure>

		<p class="is-bold"><strong>Mission</strong></p>
		<p>
			To create a community that fosters investment culture for financial freedom
		</p>
		<p class="is-bold"><strong>Vision</strong></p>
		<p>
			To raise billionaire investors with global impact
             Mission: To create a community that fosters investment culture for financial freedom
		</p>
		<p class="is-bold"><strong>Motto</strong></p>
		<p>
			IIClub, Impactful Billionaires
Core Values: Integrity, Savings, Networking, Discipline, Fun, Profitability, Motivation, Mutual respect
		</p>

		<ul class="soc">
			<li><a target="_blank"   class="soc-twitter" href="{{ $social_links_setting->twitter }}" ></a></li>
			<li><a target="_blank"  class="soc-facebook" href="{{ $social_links_setting->facebook }}" ></a></li>
			<li><a target="_blank" class="soc-instagram soc-icon-last" href="{{ $social_links_setting->instagram }}" ></a></li>
		</ul>
	</div>
	<div class="column">
	   <div class="content">
		<p class=""><strong>Resent Blog Posts</strong></p>
			@forelse ($footer_blog_posts as $post)
			   @include('partials.postsinglefooter', ['post' => $post])
			@empty

			@endforelse
		</div>
		<p class=""><strong>Useful Links</strong></h5>
		<p>
		  <a href="{{ route('home')}}">Home</a> |
		  <a href="{{ route('about')}}">About us</a> |
		  <a href="{{ route('rules.index')}}">Rules</a> |
		  <a href="{{ route('faqs.index')}}">Faq's</a> |
		  <a href="{{ route('contacts')}}">Contact</a>
		</p>
	</div>
	<div class="column">
	  <div class=" columns">
	    <div class=" column is-10 is-offset-1">
			<div class="field">
			  <label class="label">Email </label>
			  <p class="control has-icons-left has-icons-right">
				<input class="input  is-hovered" id="footer_email" name="footer_email" type="email" placeholder="Email">
				<span class="icon is-small is-left">
				  <i class="fa fa-envelope"></i>
				</span>
			  </p>
			</div>
			<div class="field">
			  <label class="label">Password </label>
			  <p class="control has-icons-left has-icons-right">
				<input class="input  is-hovered" id="footer_password" name="footer_password" type="password" placeholder="****">
				<span class="icon is-small is-left">
				  <i class="fa fa-lock"></i>
				</span>
			  </p>
			</div>
			<div class="field">
			  <p class="control">
				<label class="checkbox">
				  <input type="checkbox">
				   Remember Me
				</label>
			  </p>
			</div>
			<div class="field is-grouped">
			  <p class="control">
				<button class="button is-primary">Login</button>
			  </p>
			  <p class="control">
				<button class="button is-link">Register</button>
			  </p>
			</div>
		</div>
	  </div>
	</div>

  </div>
<div class="container">
<div class="content has-text-centered">
  <p>
	Copyright <strong> <a href="{{ route('home')}}">Infinity Investment Club</a></strong> 2017.
  </p>
</div>
</div>
</footer>
