<div class="columns is-gapless is-paddingless is-marginless">
	<div class="column">
		<nav class="nav is-primary b-bt">
		  <div class="nav-left">
		    <div class="nav-item">
				<ul class="soc">
					<li><a  target="_blank"  class="soc-twitter" href="{{ $social_links_setting->twitter }}" ></a></li>
					<li><a target="_blank"  class="soc-facebook" href="{{ $social_links_setting->facebook }}" ></a></li>
					<li><a target="_blank"  class="soc-instagram soc-icon-last" href="{{ $social_links_setting->instagram }}" ></a></li>
				</ul>
			</div>
		  </div>

		  <div class="nav-right">
			<a  href="{{ route('faqs.index') }}" class="nav-item">
			  Faq
			</a>
			<a  href="{{ route('donations') }}" class="nav-item">
			  Donate
			</a>
			<a href="{{ route('rules.index') }}" class="nav-item">
			   Rules & Regulations
			</a>
			<div class="nav-item">
			  <div class="field is-grouped">
				  @if (Auth::check())
				    <p class="control">
					  <a href="{{ route('createproject') }}" class="button is-primary">
						<span>New Project</span>
					  </a>
					</p>
					<p class="control">
					  <a href="{{ route('logout') }}" class="button is-primary">
						<span class="icon">
						  <i class="fa fa-sign-out"></i>
						</span>
						<span>Logout</span>
					  </a>
					</p>

				  @else
					<p class="control">
					  <a href="{{ route('login') }}" class="button" >
						<span class="icon">
						  <i class="fa fa-sign-in"></i>
						</span>
						<span>Login</span>
					  </a>
					</p>
					<p class="control">
					  <a href="{{ route('join') }}" class="button is-primary">
						<span class="icon">
						  <i class="fa fa-user-plus"></i>
						</span>
						<span>Join</span>
					  </a>
					</p>
				  @endif


			  </div>
			</div>
		  </div>
		</nav>

	</div>
</div>

<header class="nav main-header  bg-warning  has-shadow">
  <div class="container">
	<div class="nav-left">
	  <a href="{{ route('home')}}" class="nav-item is-brand">
		 <img src="{{ asset('images/logo.jpeg') }}"  alt="Logo">
	  </a>
	</div>
	<span class="nav-toggle">
	  <span></span>
	  <span></span>
	  <span></span>
	</span>
	<div class="nav-right nav-menu">
	  @if (Auth::check())
	    @if (Auth::user()->is_admin)
	      <a href="{{ route('administrator.dashboard')}}" class="nav-item  {{{ (Request::is('administrator.dashboard') ? 'is-active' : '') }}}">Dashboard</a>
		@else
		  <a href="{{ route('dashboard')}}" class="nav-item  {{{ (Request::is('dashboard') ? 'is-active' : '') }}}">Dashboard</a>
		@endif
	  @endif
	  <a href="{{ route('home')}}" class="nav-item  {{{ (Request::is('/') ? 'is-active' : '') }}}">Home</a>
	  <a href="{{ route('about')}}" class="nav-item {{{ (Request::is('about') ? 'is-active' : '') }}}">About Us</a>
	  <a href="{{ route('projects.index')}}" class="nav-item {{{ (Request::is('projects.index') ? 'is-active' : '') }}}">Explore</a>
	  <a href="{{ route('blog.index')}}" class="nav-item {{{ (Request::is('blog.index') ? 'is-active' : '') }}}">Blog</a>
	  <a href="{{ route('events.index')}}" class="nav-item {{{ (Request::is('events.index') ? 'is-active' : '') }}}">Events</a>
	  <a href="{{ route('contacts')}}" class="nav-item {{{ (Request::is('contacts') ? 'is-active' : '') }}}">Contact</a>
	</div>
  </div>
</header>
