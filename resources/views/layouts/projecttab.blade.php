
<ul>
	<li class=" {{{ (Route::is('myprojects') ? 'is-active' : '') }}} ">
	  <a href="{{ route('myproject', ['id' => $project-> id]) }}">
		<span class="icon is-small"><i class="fa fa-image"></i></span>
		<span>General Details</span>
	  </a>
	</li>
	<li class="  ">
	  <a href="">
		<span class="icon is-small"><i class="fa fa-music"></i></span>
		<span>Donations</span>
	  </a>
	</li>
	<li>
	  <a>
		<span class="icon is-small"><i class="fa fa-film"></i></span>
		<span>Messages</span>
	  </a>
	</li>
	<li>
	  <a>
		<span class="icon is-small"><i class="fa fa-file-text-o"></i></span>
		<span>Settings</span>
	  </a>
	</li>
	<li>
	  <a>
		<span class="icon is-small"><i class="fa fa-file-text-o"></i></span>
		<span>Stats</span>
	  </a>
	</li>

</ul>
