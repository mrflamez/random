
@extends('layouts.app')

@section('title', 'Activate Account')

@section('content')
<section class="hero is-small is-primary">
  <div class="hero-body ">
    <div class="container">
		<div class="heading  m-b-3 has-text-centered">
			<h3 class="title is-bold">Activate Your Account</h3>
		</div>

    </div>
  </div>
</section>

<section class="section">
	<div class=" columns">
	   <div class=" column is-6 is-offset-3">

          <article class="box   has-text-centered ">
					<p class="title is-4 m-b-5">Please Pay Membership Fee of <span  class="tag is-dark  is-large">{{ $setting->fee }}</span> to activate your Account </p>
					<p class="title is-5"></p>
					<form id="form-membership-activate" method="POST" action="{{ route('pay') }}" accept-charset="UTF-8"  role="form">
						{{ csrf_field() }}
						<input type="hidden" name="amount" value="{{ $setting->fee *100 }}"> {{-- required in kobo --}}
						<input type="hidden" name="quantity" value="1">
						<input type="hidden" name="first_name" value="{{ $payment['f_name'] }}">
						<input type="hidden" name="last_name" value="{{ $payment['l_name'] }}">
						<input type="hidden" name="metadata" value="{{ $payment['metadata'] }}">
						<input type="hidden" name="email" value="{{ $payment['email'] }}">
						<input type="hidden" name="reference" value="{{ Paystack::genTranxRef() }}"> {{-- required --}}
						<input type="hidden" name="key" value="{{ config('paystack.secretKey') }}"> {{-- required --}}
						<div class="field  has-addons has-addons-centered">
							  <p class="control">
								  <button type="submit" class=" button is-info is-medium">
									Activate
								  </button>
							 </p>
						</div>
					</form>
			</article>

      </div>
   </div>
</section>

@endsection

@push('scripts')
<script type="text/javascript">


  $(document).ready(function() {
		var regdata = { };
		$( "input.input" ).focus(function() {
			if ($(this).hasClass("is-danger")) {
				$(this).removeClass("is-danger").next().remove();
				$(this).parent("p.control").nextAll("p").remove();
			}
		});


		$('#verify-btn-control').click(function(e){
			e.preventDefault();
			$(this).prop("disabled", true);


			$(this).prop("disabled", false);
		});
  });
</script>
@endpush




