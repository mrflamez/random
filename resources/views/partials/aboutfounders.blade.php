<div class="column is-3">
	<div class="card">
	  <div class="card-image">
		<figure class="image is-4by3">
			<img src="{{ asset('storage/'.$founder->image->name) }}" alt="">
		</figure>
	  </div>
	  <div class="card-content">
		  <p class="title is-4 has-text-centered">{{ $founder->name }}</p>
		  <div class="field  has-addons is-grouped has-addons-centered">
			<p class="control">
			  <a href="{{ $founder->facebook }}"  class="button">
				<span class="icon is-medium">
				  <i class="fa fa-facebook"></i>
				</span>
			  </a>
			</p>
			<p class="control">
				<a href="{{ $founder->twitter }}"  class="button">
					<span class="icon is-medium">
					  <i class="fa fa-twitter"></i>
					</span>
				</a>
			</p>
		  </div>
	  </div>

	</div>

</div>
