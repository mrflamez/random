<nav class="level is-mobile">
  <div class="level-item has-text-centered">
	<div>
	  <p class="title">Posted On</p>
	  <p class="sub-title">{{ $event->created_at->format('Y/m/d') }}</p>
	</div>
  </div>
  <div class="level-item has-text-centered">
	<div>
	  <p class="title">Posted By</p>
	  <p class="sub-title">{{ $event-> user -> f_name }} {{ $event-> user -> l_name  }}</p>
	</div>
  </div>
  <div class="level-item has-text-centered">
	<div>
	  <p class="title">Views</p>
	  <p class="sub-title">0</p>
	</div>
  </div>
  <div class="level-item ">
		<div class="block has-text-right">
		  <a href="{{ route('administrator.events.edit', ['id' => $event-> id ]) }}" class="button is-warning is-medium">Edit</a>
		  <button id="event_delete_button-btn-control" class="button is-danger is-medium">Delete</button>
		</div>
  </div>
</nav>

