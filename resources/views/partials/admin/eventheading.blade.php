<p class="title is-3"><strong>{{ $event-> title }}</strong></p>
<p class="subtitle is-4 is-dark">{{ $event-> tag_line }}</p>
<p class="subtitle is-5"><strong>Venue : {{ $event-> venue }} </strong></p>
<p class="subtitle is-5">
	@isset( $event->start_date) <span class="icon is-small"><i class="fa fa-calendar"></i></span> From : {{ $event->start_date ->toFormattedDateString() }}  @endisset
	@isset( $event->end_date) |  <span class="icon is-small"><i class="fa fa-calendar"></i></span> To:    {{ $event->end_date ->toFormattedDateString() }} @endisset
</p>
@isset( $event->fee)
<p class="subtitle is-5">
	<span class="icon is-small"><i class="fa fa-calendar"></i></span> Fee:    {{ $event->fee }}
</p>
@endisset

