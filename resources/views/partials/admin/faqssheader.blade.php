<nav class="level is-mobile">
  <div class="level-item has-text-centered">
	<div>
	  <p class="title">Posted On</p>
	  <p class="sub-title">{{ $faq->created_at->format('Y/m/d') }}</p>
	</div>
  </div>
  <div class="level-item has-text-centered">
	<div>
	  <p class="title">Posted By</p>
	  <p class="sub-title">{{ $faq-> user -> f_name }} {{ $faq-> user -> l_name  }}</p>
	</div>
  </div>
  <div class="level-item has-text-centered">
	<div>
	  <p class="title">Views</p>
	  <p class="sub-title">0</p>
	</div>
  </div>
  <div class="level-item ">
		<div class="block has-text-right">
		  <a href="{{ route('administrator.faqs.edit', ['id' => $faq-> id ]) }}" class="button is-warning is-medium">Edit</a>
		  <button id="faqs_delete_button-btn-control" class="button is-danger is-medium">Delete</button>
		</div>
  </div>
</nav>



