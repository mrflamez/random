@push('header-css')
<link rel="stylesheet"  href="//cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.5/sweetalert2.min.css" >
@endpush
@push('scripts')
<script src="//cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.5/sweetalert2.min.js"></script>
<script type="text/javascript">
 $(document).ready(function() {
	  $('#project_deactivate_button-btn-control').click(function(e){
			e.preventDefault();
			var dBtn = $(this);
			swal({
				  title: 'Are you sure?',
				  text: "The User wont access this Project!",
				  type: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Yes, De-activate it!'
				}).then(function () {
					$.ajax({
					  headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					  },
					  method: 'PUT',
					  url: "{{ route('projects.update', ['id' => $partials_project_id])}}",
					  dataType: 'JSON',
					  data: {action: 'deactivate'},
					}).done(function(d) {
						console.log(d);
						if (d.success){
							//~ dBtn.parent('div.block').remove();
							dBtn.prop("disabled", true);
							swal(
							'Deactivated!!',
							'Project deactivated.',
							'success'
						  )
						}

					  });

				}, function (dismiss) {
				  // dismiss can be 'cancel', 'overlay',
				  // 'close', and 'timer'
				  if (dismiss === 'cancel') {
					swal(
					  'Cancelled',
					  'Operation Cancelled',
					  'error'
					)
				  }
				})
	  });
      $('#project_activate_button-btn-control').click(function(e){
			e.preventDefault();
			var dBtn = $(this);
			swal({
				  title: 'Are you sure?',
				  text: "The User will regain access this Project!",
				  type: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Yes, Activate it!'
				}).then(function () {
					$.ajax({
					  headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					  },
					  method: 'PUT',
					  url: "{{ route('projects.update', ['id' => $partials_project_id])}}",
					  dataType: 'JSON',
					  data: {action: 'activate'},
					}).done(function(d) {
						console.log(d);
						if (d.success){
							//~ dBtn.parent('div.block').remove();
							dBtn.prop("disabled", true);
							swal(
							'Activated!!',
							'Project has being Activated.',
							'success'
						  )
						}

					  });

				}, function (dismiss) {
				  // dismiss can be 'cancel', 'overlay',
				  // 'close', and 'timer'
				  if (dismiss === 'cancel') {
					swal(
					  'Cancelled',
					  'Operation Cancelled',
					  'error'
					)
				  }
				})
	  });
      $('#project_delete_button-btn-control').click(function(e){
			e.preventDefault();
			var dBtn = $(this);
			swal({
				  title: 'Are you sure?',
				  text: "You won't be able to revert this!",
				  type: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Yes, delete it!'
				}).then(function () {
					$.ajax({
					  headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					  },
					  method: 'DELETE',
					  url: "{{ route('projects.destroy', ['id' => $partials_project_id])}}",
					  dataType: 'JSON',
					}).done(function(d) {
						console.log(d);
						if (d.success){
							dBtn.parent('div.block').remove();
							swal(
							'Deleted!',
							'Projects deleted.',
							'success'
						  )
						}

					  });

				}, function (dismiss) {
				  // dismiss can be 'cancel', 'overlay',
				  // 'close', and 'timer'
				  if (dismiss === 'cancel') {
					swal(
					  'Cancelled',
					  'Operation Cancelled',
					  'error'
					)
				  }
				})
	  });

  });
</script>
@endpush






