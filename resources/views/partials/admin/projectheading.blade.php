<div class="columns">
	  <div class="column">
				<p class="title m-b-0-5">{{ $project-> title }}</p>
				<p class=""><a class="">{{ $project-> user -> f_name }} {{ $project-> user -> l_name  }}</a></p>
	  </div>
	  <div class="column">
		   <div class="block has-text-right">
			 @if($project->status)
			  <button id="project_deactivate_button-btn-control" class="button is-warning is-medium">Deactivate</button>
			 @else
			  <button id="project_activate_button-btn-control" class="button is-primary is-medium">Activate</button>
			 @endif
			  <button id="project_delete_button-btn-control" class="button is-danger is-medium">Delete</button>
			</div>
	  </div>
</div>

