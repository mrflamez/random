<div class="tabs  is-fullwidth is-boxed">
		<ul>
			<li class="{!! $partials_general_active !!} ">
			  <a href="{{ route('administrator.projects.single', ['id' => $project-> id]) }}">
				<span class="icon is-small"><i class="fa fa-image"></i></span>
				<span>General Details</span>
			  </a>
			</li>
			{{--
			<li class="{!! $partials_donations_active !!} ">
			  <a href="{{ route('administrator.projects.projectdonations', ['id' => $project-> id]) }}">
				<span class="icon is-small"><i class="fa fa-money"></i></span>
				<span>Donations</span>
			  </a>
			</li>
			--}}
			<li class="{!! $partials_messages_active !!} ">
			  <a href="{{ route('administrator.projects.projectmessages', ['id' => $project-> id]) }}" >
				<span class="icon is-small"><i class="fa fa-comments-o"></i></span>
				<span>Messages</span>
			  </a>
			</li>
			{{--
			<li class="{!! $partials_stats_active !!} ">
			  <a href="{{ route('administrator.projects.projectstats', ['id' => $project-> id]) }}" >
				<span class="icon is-small"><i class="fa fa-chart-line-o"></i></span>
				<span>Stats</span>
			  </a>
			</li>
			--}}
		</ul>

</div>

