<p class="title is-4 m-b-0-5 is-spaced"> {{ $user -> f_name }} {{ $user -> l_name }} </p>
<p class="subtitle is-6 m-b-0-5">@ {{ $user -> email }} </p>
<p class="subtitle is-bold is-6 m-b-0-5">{{ $user -> member_id }} </p>
@if ($user->is_pending)
	 <span class="tag is-warning is-small">PENDING VERIFICATION </span>
@else
   @if ($user->status && $user->is_paid)
	 <span class="tag is-primary is-small">ACTIVE</span>
   @elseif ($user->is_paid && !$user->status)
	 <span class="tag is-warning is-small">SUSPENDED</span>
   @elseif (!$user->is_paid)
	 <span class="tag is-warning is-small">NOT PAID</span>
   @else
	 <span class="tag is-danger is-small">NEEDS REVIEW</span>
   @endif
@endif
<nav class="level is-mobile">

  <div class="level-item has-text-centered">
	<div>
	  <p class="heading">Projects</p>
	  <p class="title">{{ $user -> projects -> count() }}</p>
	</div>
  </div>
  <div class="level-item has-text-centered">
	<div>
	  <p class="heading">Transactions</p>
	  <p class="title">{{ $payments_count}}</p>
	</div>
  </div>
  {{--
  <div class="level-item has-text-centered">
	<div>
	  <p class="heading">Withdrawals</p>
	  <p class="title">0</p>
	</div>
  </div>
  <div class="level-item has-text-centered">
	<div>
	  <p class="heading">Balance</p>
	  <p class="title">0</p>
	</div>
  </div>
  --}}
</nav>
