<aside class="menu">
	<p class="menu-label">General</p>
  <ul class="menu-list">
    <li><a class=" {{{ (Route::is('administrator.dashboard') ? 'is-active' : '') }}} " href="{{ route('administrator.dashboard')}}">Dashboard</a></li>
  </ul>
  <p class="menu-label">User Manager</p>
   <ul class="menu-list">
    <li><a class=" {{{ ( (Route::is('administrator.users.table') || Route::is('administrator.users.chart') ) ? 'is-active' : '') }}} " href="{{ route('administrator.users.table')}}">All Users</a></li>
    <li><a class=" {{{ (Route::is('administrator.permisions.all') ? 'is-active' : '') }}} " href="{{ route('administrator.permisions.all')}}">Permisions</a></li>
  </ul>
  <p class="menu-label">Projects Manager</p>
  <ul class="menu-list">
    <li><a class=" {{{ ( (Route::is('administrator.projects.all') || Route::is('administrator.projects.chart')  || Route::is('administrator.projects.single')) ? 'is-active' : '') }}} "  href="{{ route('administrator.projects.all')}}">All Projects</a></li>
  </ul>
  <p class="menu-label"> Transactions Manager</p>
  <ul class="menu-list">
	<li><a class=" {{{ ( Route::is('administrator.payments.allreceived') ? 'is-active' : '') }}} " href="{{ route('administrator.payments.allreceived')}}">Payments Received</a></li>

  </ul>
  <p class="menu-label"> Events Manager</p>
  <ul class="menu-list">
    <li><a class=" {{{ ( (Route::is('administrator.events.all') || Route::is('administrator.events.chart')) ? 'is-active' : '') }}} " href="{{ route('administrator.events.all')}}">All Events</a></li>
    <li><a class=" {{{ (Route::is('administrator.events.new') ? 'is-active' : '') }}} " href="{{ route('administrator.events.new')}}">New Event</a></li>
  </ul>
  <p class="menu-label"> Blog Manager</p>
  <ul class="menu-list">
     <li><a class=" {{{ ( (Route::is('administrator.blog.all') || Route::is('administrator.blog.chart')) ? 'is-active' : '') }}} " href="{{ route('administrator.blog.all')}}">All Posts</a></li>
    <li><a class=" {{{ (Route::is('administrator.blog.new') ? 'is-active' : '') }}} " href="{{ route('administrator.blog.new')}}">New Post</a></li>
  </ul>
  <p class="menu-label"> Faq Manager</p>
  <ul class="menu-list">
     <li><a class=" {!! Route::is('administrator.faqs.all')  ? 'is-active' : '' !!} " href="{{ route('administrator.faqs.all')}}">All Faqs</a></li>
    <li><a class=" {{{ (Route::is('administrator.faqs.new') ? 'is-active' : '') }}} " href="{{ route('administrator.faqs.new')}}">New Faq</a></li>
  </ul>
  <p class="menu-label"> Rule & Regulation</p>
  <ul class="menu-list">
     <li><a class=" {!! Route::is('administrator.rules.all')  ? 'is-active' : '' !!} " href="{{ route('administrator.rules.all')}}">All Rules</a></li>
    <li><a class=" {{{ (Route::is('administrator.rules.new') ? 'is-active' : '') }}} " href="{{ route('administrator.rules.new')}}">New Rule</a></li>
  </ul>
  <p class="menu-label"> Setting Manager</p>
  <ul class="menu-list">
     <li><a class=" {!! Route::is('administrator.pages.homepage')  ? 'is-active' : '' !!} " href="{{ route('administrator.pages.homepage')}}">Homepage</a></li>
     <li><a class=" {!! Route::is('administrator.pages.about')  ? 'is-active' : '' !!} " href="{{ route('administrator.pages.about')}}">About</a></li>
     <li><a class=" {!! Route::is('administrator.pages.generalsettings')  ? 'is-active' : '' !!} " href="{{ route('administrator.pages.generalsettings')}}">General Settings</a></li>
  </ul>
</aside>
