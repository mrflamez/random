@if ($is_pending)
	PENDING VERIFICATION
@else
	@if ($is_paid && $status)
		ACTIVE
	@elseif ($is_paid && !$status)
		SUSPENDED
	@elseif (!$is_paid)
		NOT PAID
	@else
		----
	@endif
@endif
