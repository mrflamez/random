<nav class="level is-mobile">
  <div class="level-item has-text-centered">
	<div>
	  <p class="heading">Contributions</p>
	  <p class="title">{{ $funds_made }}</p>
	</div>
  </div>
  <div class="level-item has-text-centered">
	<div>
	  <p class="heading">Donated</p>
	  <p class="title">{{ $total_received }}</p>
	</div>
  </div>
  <div class="level-item has-text-centered">
	<div>
	  <p class="heading">Withdrawals</p>
	  <p class="title">0</p>
	</div>
  </div>
  <div class="level-item has-text-centered">
	<div>
	  <p class="heading">Balance</p>
	  <p class="title">0</p>
	</div>
  </div>
</nav>


