
<article class="columns">
  <div class="column is-5">
	<figure class="image is-3by2">
		@forelse ($event -> images as $image)
		   @if ($loop->first)
			<img src="{{ asset('storage/'.$image->name) }}" alt="">
		   @endif
		@empty
		@endforelse
	</figure>
  </div>
  <div class="column">
	 <h5 class="title m-b-0-5">{{ $event-> title }}</h5>
     <p class=" m-b-0-2">
		<span class="icon is-small"><i class="fa fa-calendar"></i></span>  Venue : {{ $event-> venue }}

	</p>
	@isset( $event->start_date)
	<p class=" m-b-0-5">
		<span class="icon is-small"><i class="fa fa-calendar"></i></span> From : {{ $event->start_date ->toFormattedDateString() }}
	</p>
	 @endisset
	<div class="content">
		{!! Helper::words( strip_tags($event->description), $limit = 20, $end = '...') !!}
	</div>
	<nav class="level is-mobile">
	  <div class="level-left">
		<div class="level-item">
			<p class="field">
				@isset( $event->fee)
					<span class="icon is-small"><i class="fa fa-money"></i></span> Fee:    {{ $event->fee }}
				@endisset
			</p>
		</div>
	  </div>
	  <div class="level-right">
		<div class="level-item">
			<div class="field ">
			  <p class="control">
				<a href="{{ route('events.show', ['id' => $event -> id ]) }}" class="button is-primary">Read More</a>
			  </p>
			</div>
	    </div>
	  </div>
	</nav>
  </div>
</article>


