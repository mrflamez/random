
<article class="columns">
  <div class="column is-5">
	<figure class="image is-3by2">
		@foreach ($post -> images as $image)
		   @if ($loop->first)
			<img src="{{ asset('storage/'.$image->name) }}" alt="">
		   @endif
		@endforeach
	</figure>
  </div>
  <div class="column">
	 <h5 class="title">{{ $post-> title }}</h5>
     <h6 class="subtitle">By {{ $post->user->f_name }}  <small class="is-pulled-right"><span class="icon is-small"><i class="fa fa-calendar"></i></span> {{ $post->created_at -> diffForHumans() }} </small></h6>
	<div class="content">
		{!! Helper::words( strip_tags($post->description), $limit = 20, $end = '...') !!}
	</div>
	<nav class="level is-mobile">
	  <div class="level-left">
		<div class="level-item">
			<p class="field">
				<small class=" ">
				  <span class="icon "><i class="fa fa-reply"></i></span>
				  <span class="">23 Replys</span>
				</small>
			</p>
		</div>
	  </div>
	  <div class="level-right">
		<div class="level-item">
			<div class="field ">
			  <p class="control">
				<a href="{{ route('blog.show', ['id' => $post -> id ]) }}" class="button is-primary">Read More</a>
			  </p>
			</div>
	    </div>
	  </div>
	</nav>
  </div>
</article>

