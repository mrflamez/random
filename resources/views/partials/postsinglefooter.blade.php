<article class="media">
  <div class="media-left">
	 @foreach ($post -> images as $image)
	   @if ($loop->first)
	   	<figure class="image is-96x96">
		  <img src="{{ asset('storage/'.$image->name) }}" alt="">
		</figure>
	   @endif
	@endforeach
  </div>
  <div class="media-content">
	<div class="content">
	  <p>
		<strong>{{ $post-> title }}</strong> <small>@ {{ $post-> user -> f_name }}</small> <small style="float:right;">{{ $post-> created_at -> diffForHumans() }}</small>
		<br>
		{!! Helper::words( strip_tags($post->description), $limit = 8, $end = '...') !!}
		<a href="{{ route('blog.show', ['id' => $post -> id ]) }}" class="">Read More</a>
	  </p>
	</div>
  </div>
</article>
