<div class="column is-4">
		<div class="card">
		  <div class="card-image">
			<figure class="image is-4by3">
				@foreach ($post -> images as $image)
				   @if ($loop->first)
					<img src="{{ asset('storage/'.$image->name) }}" alt="">
				   @endif
				@endforeach
			</figure>
		  </div>
		  <div class="card-content">
			<div class="media">
			  <div class="media-left">
				@forelse ($post -> user-> images as $image)
					   @if ($loop->first)
						<figure class="image is-48x48">
						  <img src="{{ asset('storage/'.$image->name) }}" alt="">
						</figure>
					   @endif
				 @empty

				 @endforelse
			  </div>
			  <div class="media-content">
				<p class="title is-4">{{ $post-> title }} </p>
				<p class="subtitle is-6">@ {{ $post->user->f_name }} <small class="is-pulled-right">{{ $post->created_at  -> diffForHumans() }}</small></p>
			  </div>
			</div>

			<div class="content">

				<p> {!! Helper::words( strip_tags($post->description), $limit = 20, $end = '...') !!} </p>

			</div>

		 </div>
		  <footer class="card-footer">
			  <a href="{{ route('blog.show', ['id' => $post -> id ]) }}" class="card-footer-item">Read More</a>
		  </footer>
		</div>

</div>

