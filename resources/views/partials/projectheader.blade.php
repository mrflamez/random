<nav class="level is-mobile">
  <div class="level-item has-text-centered">
	<div>
	  <p class="heading">Target</p>
	  <p class="title">{{ $project-> amount }}</p>
	</div>
  </div>
   {{--
  <div class="level-item has-text-centered">
	<div>
	  <p class="heading">Raised</p>
	  <p class="title">{{ $project->payments->sum('amount') }}</p>
	</div>
  </div>
   --}}
  <div class="level-item has-text-centered">
	<div>
	  <p class="heading">Duration</p>
	  <p class="title">{{  $project-> end_date->diffInDays($project-> start_date, true) }}</p>
	</div>
  </div>
  <div class="level-item has-text-centered">
	<div>
	  <p class="heading">Days Remaining</p>
	  <p class="title"> @if($project->end_date->isPast()) ENDED  @else {{  $project-> end_date->diffInDays(null, true) }}  @endif</p>
	</div>
  </div>
</nav>

