<article class="media">
  <figure class="media-left">
	<p class="image is-96x96">
		<img src="{{ asset('storage/'.$post->image->name) }}" alt="">
	</p>
  </figure>
  <div class="media-content">
	<div class="content">
	  <p>
		<strong>{{ $post->name }}</strong> <small>@ {{ $post->alias }}</small>
	  </p>
	  {!! Helper::words( strip_tags($post->content), $limit = 8, $end = '...') !!}
	</div>
  </div>
</article>

