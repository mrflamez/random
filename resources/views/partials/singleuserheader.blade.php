<ul>
  <li class="{{ $partials_profile_active }} "><a href="{{ route('administrator.users.profile', ['id' => $user-> id]) }}" ><span class="icon"><i class="fa fa-th"></i></span> <span>Profile</span></a></li>
  <li class="{{ $partials_project_active }} " ><a href="{{ route('administrator.users.single', ['id' => $user-> id]) }}" ><span class="icon"><i class="fa fa-list"></i></span> <span>Projects</span></a></li>
  <li class="{{ $partials_trans_active }} "><a href="{{ route('administrator.users.transactions', ['id' => $user-> id]) }}" ><span class="icon"><i class="fa fa-heart"></i></span> <span>Transactions</span></a></li>
  <li class="{{ $partials_membership_active }} "><a href="{{ route('administrator.users.membership', ['id' => $user-> id]) }}" ><span class="icon"><i class="fa fa-user-o"></i></span> <span>Membership</span></a></li>
  <li class="{{ $partials_commisions_active }} "><a href="{{ route('administrator.users.commisions', ['id' => $user-> id]) }}" ><span class="icon"><i class="fa fa-money"></i></span> <span>Commisions</span></a></li>
  <li class="{{ $partials_banking_active }} "><a href="{{ route('administrator.users.banking', ['id' => $user-> id]) }}" ><span class="icon"><i class="fa fa-bank"></i></span> <span>Banking</span></a></li>
</ul>

