
@forelse ($settings->sliderimages as $sliderimage)
   <li data-index="rs-{{ $loop->iteration }}" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="{{ asset('storage/'.$sliderimage->name) }}" data-rotate="0" data-saveperformance="off" data-title="Slide {{ $loop->iteration }}" data-description="">
	<!-- MAIN IMAGE -->
	<img src="{{ asset('storage/'.$sliderimage->name) }}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="10" data-no-retina>

	@isset( $sliderimage->pivot->title)
	<div class="tp-caption tp-resizeme text-uppercase  bg-dark-transparent text-white font-raleway pl-30 pr-30"
	  id="rs-{{ $loop->iteration }}-layer-1"

	  data-x="['center']"
	  data-hoffset="['0']"
	  data-y="['middle']"
	  data-voffset="['-90']"
	  data-fontsize="['28']"
	  data-lineheight="['54']"
	  data-width="none"
	  data-height="none"
	  data-whitespace="nowrap"
	  data-transform_idle="o:1;s:500"
	  data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
	  data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
	  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
	  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
	  data-start="1000"
	  data-splitin="none"
	  data-splitout="none"
	  data-responsive_offset="on"
	  style="z-index: 7; white-space: nowrap; font-weight:400; border-radius: 30px;">{{ $sliderimage->pivot->title }}
	</div>
   @endisset

   @isset( $sliderimage->pivot->subtitle)
	<div class="tp-caption tp-resizeme text-uppercase bg-theme-colored-transparent text-white font-raleway pl-30 pr-30"
	  id="rs-{{ $loop->iteration }}-layer-2"

	  data-x="['center']"
	  data-hoffset="['0']"
	  data-y="['middle']"
	  data-voffset="['-20']"
	  data-fontsize="['48']"
	  data-lineheight="['70']"
	  data-width="none"
	  data-height="none"
	  data-whitespace="nowrap"
	  data-transform_idle="o:1;s:500"
	  data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
	  data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
	  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
	  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
	  data-start="1000"
	  data-splitin="none"
	  data-splitout="none"
	  data-responsive_offset="on"
	  style="z-index: 7; white-space: nowrap; font-weight:700; border-radius: 30px;">{{ $sliderimage->pivot->subtitle }}
	</div>
    @endisset

	@isset( $sliderimage->pivot->content)
	<div class="tp-caption tp-resizeme text-white text-center"
	  id="rs-{{ $loop->iteration }}-layer-3"

	  data-x="['center']"
	  data-hoffset="['0']"
	  data-y="['middle']"
	  data-voffset="['50']"
	  data-fontsize="['14','16',20']"
	  data-lineheight="['25']"
	  data-width="none"
	  data-height="none"
	  data-whitespace="nowrap"
	  data-transform_idle="o:1;s:500"
	  data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
	  data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
	  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
	  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
	  data-start="1400"
	  data-splitin="none"
	  data-splitout="none"
	  data-responsive_offset="on"
	  style="z-index: 5; white-space: nowrap; letter-spacing:0px; font-weight:400;">{{ $sliderimage->pivot->content }}
	</div>
	@endisset
</li>

@empty

@endforelse
