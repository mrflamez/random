<div class="columns is-gapless is-paddingless is-marginless">
	<div class="column">
		<div class="profile-options p-t-1">
		  <div class="tabs is-fullwidth">
			  <ul class="b-bt">
				  <li class="{!! $t_active !!} link"><a href="{{ route($t_route)}}"> <span class="icon"><i class="fa fa-list"></i></span> <span> Table </span></a></li>
				  <li class="{!! $c_active !!} link"  ><a href="{{ route($c_route) }}" ><span class="icon"><i class="fa fa-th"></i></span> <span>Chart</span></a></li>
			  </ul>
		  </div>
		</div>
	</div>
</div>
