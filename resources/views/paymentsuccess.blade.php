
@extends('layouts.app')

@section('title', 'Donating Success')

@section('content')
<section class="hero is-small header-overlay-bg1 is-bold">
  <div class="hero-body bg-gray">
    <div class="container">
		<div class="heading  m-b-3 has-text-centered">
			<h3 class="title txt-green-yellow ">Thank You For Donating</h3>
		</div>
    </div>
  </div>
</section>

<section class="section">
	<div class=" columns " >
				<div class=" column">
					<article class="v-centered">
						<div class="has-text-centered">
							<div class="field  has-addons has-addons-centered">
								  <p class="control">
									  <span class="icon is-large">
										  <i class="fa fa-home"></i>
										</span>
								 </p>
							</div>
							<p class="title is-4">Congrtulations</p>
							<p class="title is-6">
								Thank You For Donating and your support

							</p>


						</div>
					</article>
				</div>
		</div>
</section>

@endsection





