@extends('layouts.app')

@section('title', $project-> title)

@section('content')
<section class="hero is-small  is-bold">
  <div class="hero-body ">
    <div class="container">
		<div class="heading  m-b-3 has-text-centered">
			<h3 class="title">{{ $project-> title }}</h3>
			<h5 class="subtitle">@ {{ $project-> user -> f_name }}</h5>
		</div>
    </div>
    <div class="container">
		@include('partials.projectheader')
	</div>
  </div>
  <div class="hero-foot">
	  <div class="container">
		<nav class="tabs is-boxed is-fullwidth">
			<ul class="b-bt">
			  <li class="is-active"><a href="{{ route('projects.show', ['id' => $project-> id]) }}" >Overview</a></li>
			  <li><a href="{{ route('projectmessages', ['id' => $project-> id]) }}" >Comments</a></li>
			   {{-- <li><a href="{{ route('projectdonations', ['id' => $project-> id]) }}">Donations</a></li> --}}
			</ul>
		</nav>
    </div>
  </div>
</section>

<section class="section">
	<div class="container m-b-1-5">
		<div class="columns">
			<div class="column">
				<figure class="image  featured">
					@foreach ($project -> images as $image)
					   @if ($image->featured)
						<img src="{{ asset('storage/'.$image->name) }}" alt="">
					   @endif
					@endforeach
				</figure>
			</div>
			<div class="column is-4">
				<div class="columns is-multiline">
					@foreach ($project -> images as $image)
					   @if (! $image->featured)
						<div class="column is-narrow">
							<div class="image is-128x128">
								<img src="{{ asset('storage/'.$image->name) }}" alt="">
							</div>
						</div>

					   @endif
					@endforeach
				</div>
			</div>
		</div>
	</div>
	<div class="container">
	   <div class="content">
			{{  $project-> description  }}
	   </div>
	</div>

</section>

@endsection


