@extends('layouts.app')

@section('title', $project-> title)

@section('content')
<section class="hero is-small  is-bold">
  <div class="hero-body ">
    <div class="container">
		<div class="heading  m-b-3 has-text-centered">
			<h3 class="title">{{ $project-> title }}</h3>
			<h5 class="subtitle">@ {{ $project-> user -> f_name }}</h5>
		</div>
    </div>
    <div class="container">
		@include('partials.projectheader')
	</div>
  </div>
  <div class="hero-foot">
	  <div class="container">
		<nav class="tabs is-boxed is-fullwidth">
			<ul class="b-bt">
			  <li ><a href="{{ route('projects.show', ['id' => $project-> id]) }}" >Overview</a></li>
			  <li class="is-active"><a href="{{ route('projectmessages', ['id' => $project-> id]) }}" >Comments</a></li>
			  {{-- <li><a href="{{ route('projectdonations', ['id' => $project-> id]) }}">Donations</a></li> --}}
			</ul>
		</nav>
    </div>
  </div>
</section>

<section class="section">
	<div class="container project_messages_container m-b-1-5">
       <div class="columns">
          <div class="column b-br">
			@forelse ($project->comments->take(3) as $comment)
				<article class="media">
				  <figure class="media-left">
					  @forelse ($comment -> user -> images as $image)
						   @if ($loop->first)
						   <p class="image is-96x96">
							 <img src="{{ asset('storage/'.$image->name) }}" alt="">
						   </p>
						   @endif
						@empty
							<p class="image is-96x96">

							</p>
						@endforelse

				  </figure>
				  <div class="media-content">
					<div class="content">
					  <p class="m-b-0-5">
						<strong>{{ $comment->subject }}</strong> <small>@ {{ $comment->user->f_name }}</small>
					  </p>
					  <p> {!! Helper::words( strip_tags($comment->content), $limit = 15, $end = '...') !!} </p>
					</div>

				  </div>
				  <div class="media-right">
					<small>{{ $comment-> created_at ->diffForHumans() }}</small>
				  </div>
				</article>

			@empty
				<div class="heading  m-b-3 has-text-centered">
						<h4 class="subtitle"><strong>No Comments</strong></h4>
					</div>
			@endforelse

		  </div>
		  <div class="column is-5">
		    <div class="columns">
		      <div class="column is-1 ">
			  </div>
		      <div class="column">
                @if($project->end_date->isPast())

					<article class="v-centered  message-notification">
						<div class="has-text-centered">
							<div class="field  has-addons has-addons-centered">
								  <p class="control">
									  <span class="icon is-large">
										  <i class="fa fa-home"></i>
										</span>
								 </p>
							</div>
							<p class="title is-5">Ended Project</p>
							<p class="title is-6">
								You Cannot Comment on ended project.
							</p>

						</div>
					</article>
				@else
                    @if (Auth::check())
                      @if (Auth::user()-> email != $project->user->email)

					<article class="v-centered is-hidden message-notification">
						<div class="has-text-centered">
							<div class="field  has-addons has-addons-centered">
								  <p class="control">
									  <span class="icon is-large">
										  <i class="fa fa-home"></i>
										</span>
								 </p>
							</div>
							<p class="title is-5">Comment Sent</p>
							<p class="title is-6">
								Wait for reply ... <br>

							</p>

						</div>
					</article>
					<div class="content " id="message-form">
						<div class="heading  m-b-3 has-text-centered">
							<h4 class="subtitle">Make A Comment</h4>
						</div>
						<div class="field">
						  <p class="control">
							<input id="comment_subject" class="input" placeholder="Subject"></textarea>
						  </p>
						</div>
						<div class="field">
						  <p class="control">
							<textarea id="comment_text" class="textarea" placeholder="Message"></textarea>
						  </p>
						</div>
						<div class="field">
						  <p class="control">
							<button id="comment-create-btn-control" class="button">Add Comment</button>
						  </p>
						</div>
					</div>
					@endif

					@else
					<article class="v-centered">
							<div class="has-text-centered">
								<div class="field  has-addons has-addons-centered">
									  <p class="control">
										  <span class="icon is-large">
											  <i class="fa fa-home"></i>
											</span>
									 </p>
								</div>
								<p class="title is-5">Login/ Register Make a Comment</p>
								<p class="title is-6">

								</p>
								<div class="field  has-addons has-addons-centered">
									  <p class="control">
										  <a href="{{ route('login') }}" class=" button">
											Login
										  </a>
									 </p>
									 <p class="control">
										  <a href="{{ route('join') }}"  class=" button">
											Register
										  </a>
									 </p>
								</div>

							</div>
						</article>
					@endif
				@endif

		      </div>
		    </div>
		  </div>
	   </div>

	</div>
</section>

@endsection
@push('scripts')
<script type="text/javascript">

  $(document).ready(function() {
		var regdata = {} ;

		$( "input.input, textarea.textarea " ).focus(function() {
			if ($(this).hasClass("is-danger")) {
				$(this).removeClass("is-danger");
				$(this).parent("p.control").next("p").remove();
			}
		});

		function sendComment() {
			 console.log(regdata);
			 $.ajax({
			  headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			  },
			  method: 'POST',
			  url: "{{ route('comments.store')}}",
			  dataType: 'JSON',
			  data: regdata
			}).done(function(d) {
				console.log(d);
				if (d.success){
					$("#message-form").addClass("is-hidden");
				    $(".message-notification").removeClass("is-hidden");
				} else {
                  // {{-- server validation failed --}}
				  $.each(d.errors, function( key, value ) {
					 if (key === 'comment_text') {
						$("#comment_text").toggleClass( "is-danger" ).parent("p.control").after( " <p class='help is-danger'>" + value[0] + "</p>" );
					 }

					 if (key === 'comment_subject') {
						$("#comment_subject").toggleClass( "is-danger" ).parent("p.control").after( " <p class='help is-danger'>" + value[0] + "</p>" );
					 }
				  });
				}

			  });

		 }


		function validateDetails() {

			var commentText = $("#comment_text");
			var commentSubject = $("#comment_subject");

			if ((! commentText.val()) && (! commentText.hasClass("is-danger"))) {
				commentText.toggleClass( "is-danger" ).parent("p.control").after( " <p class='help is-danger'>This field is required</p>" );
			}
			if ((! commentSubject.val()) && (! commentSubject.hasClass("is-danger"))) {
				commentSubject.toggleClass( "is-danger" ).parent("p.control").after( " <p class='help is-danger'>This field is required</p>" );
			}

			if ((commentText.val()) && (commentSubject.val())) {
				regdata.comment_text = commentText.val();
				regdata.comment_subject = commentSubject.val();
				regdata.project_id = {{ $project-> id }};
				return true ;
			} else {
				return false ;
			}
		}

		$('#comment-create-btn-control').click(function(e){
			e.preventDefault();
			$(this).prop("disabled", true);
			var r = validateDetails();

			if (r) {
				sendComment();
			}

			$(this).prop("disabled", false);
		});
 });
</script>
@endpush


