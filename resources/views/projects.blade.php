@extends('layouts.app')

@section('title', 'Explore Causes')

@section('content')
<section class="hero is-small header-overlay-bg2 is-bold">
  <div class="hero-body bg-gray">
    <div class="container">
		<div class="heading  m-b-3 has-text-centered">
			<h3 class="title txt-green-yellow ">Explore Our Projects</h3>
			<h5 class="subtitle txt-green-yellow"> </h5>
		</div>
    </div>
  </div>
</section>

<section class="section">


    <div class=" columns is-multiline">
		@foreach ($projects as $project)
		  <div class="column is-4">
			<div class="card">
			  <div class="card-image">
				<figure class="image is-4by3">
					@foreach ($project -> images as $image)
					   @if ($image->featured)
						<img src="{{ asset('storage/'.$image->name) }}" alt="">
				       @endif
				    @endforeach

				</figure>
			  </div>
			  <div class="card-content">
				<p class="title is-4"> {{ $project-> title }}</p>
				<p class="subtitle is-6">@ {{ $project-> user -> f_name }}
				   <span class="timestamp">
					 <strong> @if($project->end_date->isPast()) Ended @else {{  $project-> end_date->diffInDays(null, true) }} days Remaining @endif</strong>
					</span>
				</p>

				<div class="">
				  <span class="tag is-dark">#{{ $project-> category -> name }}</span>
				</div>
				<div class="content">
					{!! Helper::words( strip_tags($project->description), $limit = 13, $end = '...') !!}
				</div>
				<progress class="progress is-success" value="{{ ($project->payments->sum('amount')/$project-> amount) * 100 }}" max="100">{{ ($project->payments->sum('amount')/$project-> amount) * 100 }}%</progress>
				<div class="level is-marginless is-mobile">
					<div class="level-left">
					 {{--
					  <div class="level-item">
						<div>
						  <p class="">Raised : <strong> {{ $project->payments->sum('amount') }} </strong></p>
						</div>
					  </div>
					  --}}
					 </div>
					 <div class="level-right">
					   <div class="level-item ">
						 <div>
							 <p class="">Goal : <strong>{{ $project-> amount }}</strong></p>
						</div>
						</div>
					 </div>
				</div>

			  </div>
			  <footer class="card-footer">
				<a href="{{ route('projects.show', ['id' => $project-> id])  }}" class="card-footer-item">View</a>
				 {{--<a href="{{ route('projectdonations', ['id' => $project-> id]) }}" class="card-footer-item">Donate</a>  --}}
			  </footer>
			</div>
		  </div>
	    @endforeach
    </div>

    <div class=" columns">
		<div class=" column">

				{{ $projects->links('vendor.pagination.default') }}
		</div>
	</div>

</section>

@endsection


