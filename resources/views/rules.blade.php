@extends('layouts.app')

@section('title', 'Events')

@section('content')
<section class="hero is-small  is-primary is-bold">
  <div class="hero-body ">
    <div class="container">
		<div class="heading  has-text-centered">
			<h3 class="title">Rules And Terms Of Use</h3>
		</div>
    </div>
  </div>
</section>

<section class="section">
	<div class=" columns">


		<div class=" column ">
			@forelse ($ruleregulations as $ruleregulation)
				<p class="title is-3"><strong>{{ $ruleregulation -> title }}</strong></p>

				<div class="content">{{ $ruleregulation -> content }} </div>
			@empty
				<p>No  Content </p>
			@endforelse


		</div>
    </div>
</section>

@endsection







