
@extends('layouts.app')

@section('title', 'Account Suspend')

@section('content')
<section class="hero is-medium is-danger">
  <div class="hero-body ">
    <div class="container">
		<div class="heading  m-b-3 has-text-centered">
			<h3 class="title is-bold"> Your Account Suspended</h3>
		</div>

    </div>
  </div>
</section>

@endsection







