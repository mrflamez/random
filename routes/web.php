<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'PagesController@index')->name('home');
Route::get('/about', 'PagesController@about')->name('about');
Route::get('/donations', 'PagesController@donations')->name('donations');
Route::get('/join', 'PagesController@join')->name('join')->middleware('guest');

Route::get('/contacts', function () {
    return view('contacts');
})->name('contacts');

Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/suspended', 'UserController@suspended');
Route::get('emailverification/{token}', 'UserController@emailverify')->name('emailverify.index')->middleware('guest');
Route::post('emailverification/{token}', 'UserController@emailverifyupdate')->name('emailverify.store')->middleware('guest');
Route::get('membershippayment/{memberid}', 'UserController@membershipPayment')->name('membershippayment');

Auth::routes();

Route::group(['prefix' => 'administrator', 'middleware' => ['auth', 'checkadmin' ], 'as'=>'administrator.'], function () {
	Route::get('/', 'AdminController@index')->name('dashboard');
	Route::group(['prefix' => 'users', 'as'=>'users.'], function () {
		Route::get('single/{id}/projects', 'AdminController@singleUser')->name('single');
		Route::get('single/{id}/trasactions', 'AdminController@singleUsertrasactions')->name('transactions');
		Route::get('single/{id}/profile', 'AdminController@singleUserprofile')->name('profile');
		Route::get('single/{id}/membership', 'AdminController@singleUsermembership')->name('membership');
		Route::get('single/{id}/commisions', 'AdminController@singleUsercommisions')->name('commisions');
		Route::get('single/{id}/banking', 'AdminController@singleUserbanking')->name('banking');
		Route::get('table', 'AdminController@alluserstable')->name('table');
		Route::get('chart', 'AdminController@alluserschart')->name('chart');
	});
	Route::group(['prefix' => 'permisions', 'as'=>'permisions.'], function () {
		Route::get('single/{id}', 'AdminController@singlePermision')->name('single');
		Route::get('table', 'AdminController@allpermisions')->name('all');
		Route::post('addroleuser', 'AdminController@addUserRole')->name('adduserrole');
		Route::post('removeroleuser/{userid}/{roleid}', 'AdminController@removeRoleUser')->name('removeroleuser');
	});

	Route::group(['prefix' => 'projects', 'as'=>'projects.'], function () {
		Route::get('single/{id}', 'AdminController@singleProject')->name('single');
		Route::get('table', 'AdminController@allprojects')->name('all');
		Route::get('chart', 'AdminController@allprojectschart')->name('chart');
		Route::get('single/{id}/donations', 'AdminController@viewprojectdonations')->name('projectdonations');
		Route::get('single/{id}/messages', 'AdminController@viewprojectmessages')->name('projectmessages');
		Route::get('single/{id}/settings', 'AdminController@viewprojectsettings')->name('projectsettings');
		Route::get('single/{id}/stats', 'AdminController@viewprojectstats')->name('projectstats');
	});

	Route::group(['prefix' => 'events', 'as'=>'events.'], function () {
		Route::get('single/{id}/view', 'AdminController@singleEvent')->name('single');
		Route::get('single/{id}/edit', 'AdminController@singleEventEdit')->name('edit');
		Route::get('table', 'AdminController@alleventstable')->name('all');
		Route::get('chart', 'AdminController@alleventschart')->name('chart');
		Route::get('new', 'AdminController@newevent')->name('new');
	});

	Route::group(['prefix' => 'blog', 'as'=>'blog.'], function () {
		Route::get('single/{id}/view', 'AdminController@singlePost')->name('single');
		Route::get('single/{id}/edit', 'AdminController@singlePostEdit')->name('edit');
		Route::get('table', 'AdminController@allblogposts')->name('all');
		Route::get('chart', 'AdminController@allblogpostschart')->name('chart');
		Route::get('new', 'AdminController@newblogpost')->name('new');
	});

	Route::group(['prefix' => 'payments', 'as'=>'payments.'], function () {
		Route::get('all', 'AdminController@allPaymentsreceived')->name('allreceived');
		Route::get('single/{id}', 'AdminController@singlePaymentreceivedTrans')->name('single');
	});

	Route::group(['prefix' => 'faqs', 'as'=>'faqs.'], function () {
		Route::get('all', 'AdminController@allFaqs')->name('all');
		Route::get('single/{id}/view', 'AdminController@singleFaq')->name('single');
		Route::get('single/{id}/edit', 'AdminController@singleFaqEdit')->name('edit');
		Route::get('new', 'AdminController@createFaq')->name('new');
	});

	Route::group(['prefix' => 'rules', 'as'=>'rules.'], function () {
		Route::get('all', 'AdminController@allRules')->name('all');
		Route::get('single/{id}/view', 'AdminController@singleRules')->name('single');
		Route::get('single/{id}/edit', 'AdminController@singleRulesEdit')->name('edit');
		Route::get('new', 'AdminController@createRules')->name('new');
	});

	Route::group(['prefix' => 'pages', 'as'=>'pages.'], function () {
		Route::get('homepage', 'AdminController@homepageSetings')->name('homepage');
		Route::get('about', 'AdminController@aboutSetings')->name('about');
		Route::get('generalsettings', 'AdminController@generalSettings')->name('generalsettings');
	});

});

Route::group(['prefix' => 'datatables', 'middleware' => ['auth', 'checkadmin' ], 'as'=>'datatables.'], function () {
	Route::any('newusers', 'DatatableController@newusers')->name('newusers');
	Route::any('allusers', 'DatatableController@allusers')->name('allusers');
	Route::any('allpermisions', 'DatatableController@allpermisions')->name('allpermisions');
	Route::any('singlepermision/{id}', 'DatatableController@singlePermision')->name('singlepermision');
	Route::any('allprojects', 'DatatableController@allprojects')->name('allprojects');
	Route::any('allevents', 'DatatableController@allevents')->name('allevents');
	Route::any('allposts', 'DatatableController@allposts')->name('allposts');
	Route::any('allpaymentsreceived', 'DatatableController@allpaymentsreceived')->name('allpaymentsreceived');
	Route::any('allfaqs', 'DatatableController@allfaqs')->name('allfaqs');
	Route::any('allrules', 'DatatableController@allrules')->name('allrules');
	Route::any('testmonials', 'DatatableController@testmonials')->name('testmonials');
	Route::any('slider', 'DatatableController@settingSlider')->name('settingslider');
	Route::any('sponsors', 'DatatableController@settingSponsors')->name('sponsors');
	Route::any('founders', 'DatatableController@founders')->name('founders');
	Route::post('deleteslider', 'DatatableController@deleteSlider')->name('deleteslider');
	Route::post('deletesponsor', 'DatatableController@deleteSponsor')->name('deletesponsor');
	Route::post('deletetestmonial', 'DatatableController@deleteTestmonial')->name('deletetestmonial');
	Route::post('deletefounder', 'DatatableController@deleteFounder')->name('deletefounder');
});

Route::group(['prefix' => 'chartdata', 'middleware' => ['auth', 'checkadmin' ], 'as'=>'chartdata.'], function () {
	Route::any('usersvsmonth', 'ChartController@usersVsMonth')->name('usersvsmonth');
	Route::any('projectsvsmonth', 'ChartController@projectsVsmonth')->name('projectsvsmonth');
	Route::any('eventsvsmonth', 'ChartController@eventsVsmonth')->name('eventsvsmonth');
	Route::any('postsvsmonth', 'ChartController@postsVsmonth')->name('postsvsmonth');
});

Route::group(['prefix' => 'dashboard', 'middleware' => ['auth']], function () {

    Route::get('/', 'HomeController@index')->name('dashboard');
    Route::get('createproject', 'HomeController@createproject')->name('createproject');
    Route::get('myproject/{id}/view', 'HomeController@viewproject')->name('myproject');
    Route::get('myproject/{id}/donations', 'HomeController@viewprojectdonations')->name('myprojectdonations');
    Route::get('myproject/{id}/messages', 'HomeController@viewprojectmessages')->name('myprojectmessages');
    Route::get('myproject/{id}/messages/{comment}', 'HomeController@viewprojectmessagescomments')->name('viewprojectmessagescomments');
    Route::get('myproject/{id}/settings', 'HomeController@viewprojectsettings')->name('myprojectsettings');
    Route::get('myproject/{id}/stats', 'HomeController@viewprojectstats')->name('myprojectstats');
    Route::get('myprojects', 'HomeController@viewprojects')->name('myprojects');
    Route::get('mycontributions', 'HomeController@mycontributions')->name('mycontributions');
    Route::get('mydonations', 'HomeController@mydonations')->name('mydonations');
    Route::get('mymessages', 'HomeController@mymessages')->name('mymessages');
    Route::group(['prefix' => 'profile', 'as'=>'profile.'], function () {
		Route::get('/', 'HomeController@myProfile')->name('index');
	});

	Route::group(['as'=>'dash.'], function () {
		Route::get('transactions', 'HomeController@myTransactions')->name('transactions');
		Route::get('membership', 'HomeController@myMembership')->name('membership');
		Route::get('commisions', 'HomeController@myCommisions')->name('commisions');
		Route::get('banking', 'HomeController@myBanking')->name('banking');
	});

	Route::group(['prefix' => 'chartdata', 'as'=>'analytics.'], function () {
		Route::any('donationsvsmonth/{id}', 'ChartController@donationsVsmonth')->name('donations');
	});

});

Route::get('projects/{id}/messages', 'ProjectController@projectmessages')->name('projectmessages');
// Route::get('projects/{id}/donations', 'ProjectController@projectdonations')->name('projectdonations');
Route::any('mailboxes/{id}/sent', 'MailboxController@sentmail')->name('mailboxes.sentmail');
Route::any('mailboxes/{id}/inbox', 'MailboxController@inboxmail')->name('mailboxes.inboxmail');

Route::resource('photos', 'ImageController');
Route::resource('projects', 'ProjectController');
Route::resource('users', 'UserController');
Route::resource('comments', 'CommentController');
Route::resource('mailboxes', 'MailboxController');
Route::resource('events', 'EventsController');
Route::resource('blog', 'PostsController');
Route::resource('faqs', 'FaqsController');
Route::resource('rules', 'RulesRegulationController');
Route::resource('testimony', 'TestimonyController');
Route::resource('settings', 'SettingController');
Route::resource('founders', 'FounderController');
Route::resource('activations', 'ActivationController');

Route::post('/pay', 'PaymentController@redirectToGateway')->name('pay');
Route::get('/payment/callback', 'PaymentController@handleGatewayCallback');
Route::get('/payment/success', 'PaymentController@paymentsuccess')->name('paymentsuccess');

Route::get('testsendmail', 'TestFunctionsController@testsendmail')->name('testsendmail');
Route::get('updatemeberid', 'TestFunctionsController@updatemeberid')->name('updatemeberid');



//Clear Cache facade value:
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    return '<h1>Cache facade value cleared</h1>';
});

//Reoptimized class loader:
Route::get('/optimize', function() {
    $exitCode = Artisan::call('optimize');
    return '<h1>Reoptimized class loader</h1>';
});

//Route cache:
Route::get('/route-cache', function() {
    $exitCode = Artisan::call('route:cache');
    return '<h1>Routes cached</h1>';
});

//Clear Route cache:
Route::get('/route-clear', function() {
    $exitCode = Artisan::call('route:clear');
    return '<h1>Route cache cleared</h1>';
});

//Clear View cache:
Route::get('/view-clear', function() {
    $exitCode = Artisan::call('view:clear');
    return '<h1>View cache cleared</h1>';
});

//Clear Config cache:
Route::get('/config-cache', function() {
    $exitCode = Artisan::call('config:cache');
    return '<h1>Clear Config cleared</h1>';
});

//Clear Config cache:
Route::get('/storage-link', function() {
    $exitCode = Artisan::call('storage:link');
    return '<h1>storage linked </h1>';
});

//Clear Config cache:
Route::get('/clear-compiled', function() {
    $exitCode = Artisan::call('clear-compiled');
    return '<h1>clear-compiled </h1>';
});
