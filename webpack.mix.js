const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

var resourcesDir = 'resources/assets';
var publicDir = 'public';
var nodeModulesPath = 'node_modules';

// mix.js( resourcesDir + '/js/app.js', publicDir + '/js')
//	.sass(resourcesDir + '/sass/app.scss', publicDir + '/css')
//		.copy(resourcesDir + '/images/favicons', publicDir + '/images');
// mix.copyDirectory(resourcesDir + '/raw', publicDir);

mix.sass(resourcesDir + '/sass/app.scss', publicDir + '/css').options({
      processCssUrls: false
   }).version();

mix.js('resources/assets/js/app.js', 'public/js').version();

mix.copyDirectory(resourcesDir + '/fonts', publicDir + '/fonts')
	.copy(resourcesDir + '/css/font-awesome.min.css', publicDir + '/css')
	.copy(resourcesDir + '/css/daterangepicker.css', publicDir + '/css')
	.copy(nodeModulesPath + '/pikaday/css/pikaday.css', publicDir + '/css')
	.copyDirectory(resourcesDir + '/js/vendor/revolution-slider', publicDir + '/js/revolution-slider')
	.copy(resourcesDir + '/js/vendor/dropzone.js', publicDir + '/js')
	.copy(resourcesDir + '/js/vendor/daterangepicker.js', publicDir + '/js')
	.copy(nodeModulesPath + '/pikaday/pikaday.js', publicDir + '/js')
	.copy(nodeModulesPath + '/chart.js/dist/Chart.js', publicDir + '/js')
	.copy(nodeModulesPath + '/progressbar.js/dist/progressbar.js', publicDir + '/js')
	.copyDirectory(resourcesDir + '/images', publicDir + '/images');
